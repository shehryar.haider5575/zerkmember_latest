// import 'package:fcm_config/fcm_config.dart';
// import 'package:flutter/material.dart';
// import 'package:overlay_support/overlay_support.dart';
//
// class FirebaseMassagingGlobal extends StatefulWidget {
//   @override
//   _FirebaseMassagingGlobalState createState() =>
//       _FirebaseMassagingGlobalState();
// }
//
// class _FirebaseMassagingGlobalState extends State<FirebaseMassagingGlobal> {
//   int _totalNotifications;
//   PushNotification _notificationInfo;
//
//   final FirebaseMessaging firebaseMessaging = FirebaseMessaging.instance;
//
//   Future<void> _firebaseMessagingBackgroundHandler(
//       RemoteMessage message) async {
//     print("Handling a background message: ${message.messageId}");
//   }
//
//   void registerNotification() async {
//     FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
//
//     NotificationSettings settings = await firebaseMessaging.requestPermission(
//       alert: true,
//       badge: true,
//       provisional: false,
//       sound: true,
//     );
//
//     if (settings.authorizationStatus == AuthorizationStatus.authorized) {
//       print('User granted permission');
//
//       FirebaseMessaging.onMessage.listen((RemoteMessage message) {
//         print(
//             'Message title: ${message.notification?.title}, body: ${message.notification?.body}, data: ${message.data}');
//
//         // Parse the message received
//         PushNotification notification = PushNotification(
//           title: message.notification?.title,
//           body: message.notification?.body,
//           dataTitle: message.data['title'],
//           dataBody: message.data['body'],
//         );
//
//         setState(() {
//           _notificationInfo = notification;
//           _totalNotifications++;
//         });
//
//         if (_notificationInfo != null) {
//           // For displaying the notification as an overlay
//           showSimpleNotification(
//             Text(_notificationInfo.title),
//             leading: NotificationBadge(totalNotifications: _totalNotifications),
//             subtitle: Text(_notificationInfo.body),
//             background: Colors.cyan.shade700,
//             duration: Duration(seconds: 2),
//           );
//         }
//       });
//     } else {
//       print('User declined or has not accepted permission');
//     }
//   }
//
//   checkForInitialMessage() async {
//     // await Firebase.initializeApp();
//     RemoteMessage initialMessage =
//         await FirebaseMessaging.instance.getInitialMessage();
//
//     if (initialMessage != null) {
//       PushNotification notification = PushNotification(
//         title: initialMessage.notification?.title,
//         body: initialMessage.notification?.body,
//         dataTitle: initialMessage.data['title'],
//         dataBody: initialMessage.data['body'],
//       );
//
//       setState(() {
//         _notificationInfo = notification;
//         _totalNotifications++;
//       });
//     }
//   }
//
//   @override
//   void initState() {
//     // TODO: implement initState
//     super.initState();
//     firebaseMessaging.getToken().then((value) {
//       print('this is token \n' + value);
//     });
//     _totalNotifications = 0;
//     registerNotification();
//     checkForInitialMessage();
//
//     // / For handling notification when the app is in background
//     // but not terminated
//
//     FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
//       PushNotification notification = PushNotification(
//         title: message.notification?.title,
//         body: message.notification?.body,
//         dataTitle: message.data['title'],
//         dataBody: message.data['body'],
//       );
//
//       setState(() {
//         _notificationInfo = notification;
//         _totalNotifications++;
//       });
//     });
//
//     super.initState();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text('Notify'),
//         brightness: Brightness.dark,
//       ),
//       body: Column(
//         mainAxisAlignment: MainAxisAlignment.center,
//         children: [
//           Text(
//             'App for capturing Firebase Push Notifications',
//             textAlign: TextAlign.center,
//             style: TextStyle(
//               color: Colors.black,
//               fontSize: 20,
//             ),
//           ),
//           SizedBox(height: 16.0),
//           NotificationBadge(totalNotifications: _totalNotifications),
//           SizedBox(height: 16.0),
//           _notificationInfo != null
//               ? Column(
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   children: [
//                     Text(
//                       'TITLE: ${_notificationInfo.dataTitle ?? _notificationInfo.title}',
//                       style: TextStyle(
//                         fontWeight: FontWeight.bold,
//                         fontSize: 16.0,
//                       ),
//                     ),
//                     SizedBox(height: 8.0),
//                     Text(
//                       'BODY: ${_notificationInfo.dataBody ?? _notificationInfo.body}',
//                       style: TextStyle(
//                         fontWeight: FontWeight.bold,
//                         fontSize: 16.0,
//                       ),
//                     ),
//                   ],
//                 )
//               : Container(),
//         ],
//       ),
//     );
//   }
// }
//
// class NotificationBadge extends StatelessWidget {
//   final int totalNotifications;
//
//   const NotificationBadge({@required this.totalNotifications});
//
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       width: 40.0,
//       height: 40.0,
//       decoration: new BoxDecoration(
//         color: Colors.red,
//         shape: BoxShape.circle,
//       ),
//       child: Center(
//         child: Padding(
//           padding: const EdgeInsets.all(8.0),
//           child: Text(
//             '$totalNotifications',
//             style: TextStyle(color: Colors.white, fontSize: 20),
//           ),
//         ),
//       ),
//     );
//   }
// }
//
// class PushNotification {
//   PushNotification({
//     this.title,
//     this.body,
//     this.dataTitle,
//     this.dataBody,
//   });
//
//   String title;
//   String body;
//   String dataTitle;
//   String dataBody;
// }
// // import 'package:firebase_messaging/firebase_messaging.dart';
// // import 'package:flutter/material.dart';
// //
// // class FirebaseNotificationGlobal extends StatefulWidget {
// //   @override
// //   _FirebaseNotificationGlobalState createState() =>
// //       _FirebaseNotificationGlobalState();
// // }
// //
// // class _FirebaseNotificationGlobalState
// //     extends State<FirebaseNotificationGlobal> {
// //   FirebaseMessaging messaging = FirebaseMessaging.instance;
// //
// //   firebaseMassaging(token) async {
// //     NotificationSettings settings = await messaging.requestPermission(
// //       alert: true,
// //       announcement: false,
// //       badge: true,
// //       carPlay: false,
// //       criticalAlert: false,
// //       provisional: false,
// //       sound: true,
// //     );
// //
// //     print('User granted permission: ${settings.authorizationStatus}');
// //   }
// //
// //   @override
// //   void initState() {
// //     // TODO: implement initState
// //     super.initState();
// //     // Firebase.initializeApp();
// //     messaging = FirebaseMessaging.instance;
// //     messaging.getToken().then((value) {
// //       print('this is token \n' + value);
// //     });
// //
// //     // handle notifications in foreground
// //
// //     FirebaseMessaging.onMessage.listen((RemoteMessage event) {
// //       print("message recieved");
// //       print(event.notification.body);
// //     });
// //     FirebaseMessaging.onMessageOpenedApp.listen((message) {
// //       print('Message clicked!');
// //     });
// //   }
// //
// //   @override
// //   Widget build(BuildContext context) {
// //     return Scaffold(
// //       appBar: AppBar(),
// //       body: Column(
// //         mainAxisAlignment: MainAxisAlignment.center,
// //         // crossAxisAlignment: CrossAxisAlignment.center,
// //         children: [
// //           Center(
// //             child: InkWell(
// //                 onTap: () {
// //                   print('this is it');
// //                   FirebaseMessaging.onMessage.listen((RemoteMessage event) {
// //                     print("message recieved");
// //                     print(event.notification.body);
// //                     showDialog(
// //                         context: context,
// //                         builder: (BuildContext context) {
// //                           return AlertDialog(
// //                             title: Text("Notification"),
// //                             content: Text(event.notification.body),
// //                             actions: [
// //                               TextButton(
// //                                 child: Text("Ok"),
// //                                 onPressed: () {
// //                                   Navigator.of(context).pop();
// //                                 },
// //                               )
// //                             ],
// //                           );
// //                         });
// //                   });
// //                 },
// //                 child: Text('Click here')),
// //           )
// //         ],
// //       ),
// //     );
// //   }
// // }
