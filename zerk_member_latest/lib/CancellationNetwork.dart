import 'dart:convert';

import 'package:http/http.dart' as http;

import 'model/CancellationHistoryModel.dart';

class Network {
  Future<List<Datum>> getCancellationData(String id) async {
    final response = await http.get(Uri.parse(
        'https://zerk.com.au/panel/public/api/transaction/cancel/$id'));
    if (response.statusCode == 200) {
      print(response.body);
      CancellationHistory cancellationHistory =
          CancellationHistory.fromJson(jsonDecode(response.body));
      List<Datum> list = cancellationHistory.data;
      return list;
      // return CancellationHistory.fromJson(jsonDecode(response.body));
      // CancellationHistory cancellationHistory = CancellationHistory.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Could not get cancellation history');
    }
  }

  Future<void> getRetrieveData(memberId) async {
    try {
      final response = await http.post(
        Uri.parse('https://zerk.com.au/panel/public/api/member/retrive'),
        headers: <String, String>{
          'Accept': 'application/json',
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{
          'memberId': memberId,
        }),
      );
      if (response.statusCode == 200) {
        print('************THIS IS RETRIEVE DATA *********');
        print(response.body);
        print('************THIS IS RETRIEVE DATA *********');
      } else {
        print(response.statusCode);
      }
    } catch (err) {
      print(err);
    }
  }
}
