import 'package:flutter/material.dart';

// if (username == null) {
//       Navigator.pushReplacementNamed(context, '/login');
//     } else {
//       Navigator.pushReplacementNamed(context, '/main_page');
//     }
class MyNavigator {
  static void goToLoginScreen(BuildContext context) {
    Navigator.pushReplacementNamed(context, '/LoginScreen');
    // Navigator.push(
    //   context,
    //   MaterialPageRoute(
    //       fullscreenDialog: true, builder: (context) => SignUpScreen()),
    // );
  }

  static void goToBottomNavBar(BuildContext context) {
    Navigator.pushReplacementNamed(context, '/BottomNavBar');
    // Navigator.push(
    //   context,
    //   MaterialPageRoute(
    //       fullscreenDialog: true, builder: (context) => BottomNavBar()),
    // );
  }
}
