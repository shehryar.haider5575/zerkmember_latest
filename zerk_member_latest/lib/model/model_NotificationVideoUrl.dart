class NotificationVideoUrl {
  String videoUrl;
  String message;
  String facility;

  NotificationVideoUrl({this.videoUrl, this.message, this.facility});

  NotificationVideoUrl.fromJson(Map<String, dynamic> json) {
    videoUrl = json['video_url'];
    message = json['message'];
    facility = json['facility'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['video_url'] = this.videoUrl;
    data['message'] = this.message;
    data['facility'] = this.facility;
    return data;
  }
}
