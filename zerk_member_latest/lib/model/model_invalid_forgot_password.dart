class InvalidForgotPassword {
  InvalidForgotPasswordData data;
  int code;
  String message;

  InvalidForgotPassword({this.data, this.code, this.message});

  InvalidForgotPassword.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null
        ? new InvalidForgotPasswordData.fromJson(json['data'])
        : null;
    code = json['code'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['code'] = this.code;
    data['message'] = this.message;
    return data;
  }
}

class InvalidForgotPasswordData {
  List<String> email;

  InvalidForgotPasswordData({this.email});

  InvalidForgotPasswordData.fromJson(Map<String, dynamic> json) {
    email = json['email'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this.email;
    return data;
  }
}
