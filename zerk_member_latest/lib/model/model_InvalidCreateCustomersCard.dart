class InvalidCreateCustomersCard {
  Error error;

  InvalidCreateCustomersCard({this.error});

  InvalidCreateCustomersCard.fromJson(Map<String, dynamic> json) {
    error = json['error'] != null ? new Error.fromJson(json['error']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.error != null) {
      data['error'] = this.error.toJson();
    }
    return data;
  }
}

class Error {
  String message;
  String param;
  String type;

  Error({this.message, this.param, this.type});

  Error.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    param = json['param'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['param'] = this.param;
    data['type'] = this.type;
    return data;
  }
}
