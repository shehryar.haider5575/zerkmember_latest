class InvaildUserProfileUpdated {
  Data data;
  int code;
  String message;
  String imageUrl;

  InvaildUserProfileUpdated(
      {this.data, this.code, this.message, this.imageUrl});

  InvaildUserProfileUpdated.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    code = json['code'];
    message = json['message'];
    imageUrl = json['image_url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['code'] = this.code;
    data['message'] = this.message;
    data['image_url'] = this.imageUrl;
    return data;
  }
}

class Data {
  List<String> email;
  List<String> dob;
  List<String> first_name;
  List<String> last_name;

  Data({this.email, this.dob, this.first_name, this.last_name});

  Data.fromJson(Map<String, dynamic> json) {
    email = json['email'].cast<String>();
    dob = json['dob'].cast<String>();
    first_name = json['first_name'].cast<String>();
    last_name = json['last_name'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this.email;
    data['dob'] = this.dob;
    data['first_name'] = this.first_name;
    data['last_name'] = this.last_name;

    return data;
  }
}
