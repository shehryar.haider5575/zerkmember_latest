// class UserDetails {
//   UserDetailsData data;
//   int code;
//   String message;
//   String imageUrl;
//
//   UserDetails({this.data, this.code, this.message, this.imageUrl});
//
//   UserDetails.fromJson(Map<String, dynamic> json) {
//     data = json['data'] != null
//         ? new UserDetailsData.fromJson(json['data'])
//         : null;
//     code = json['code'];
//     message = json['message'];
//     imageUrl = json['image_url'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     if (this.data != null) {
//       data['data'] = this.data.toJson();
//     }
//     data['code'] = this.code;
//     data['message'] = this.message;
//     data['image_url'] = this.imageUrl;
//     return data;
//   }
// }
//
// class UserDetailsData {
//   int id;
//   String stripeCustomerId;
//   String sourceCardId;
//   String firstName;
//   String lastName;
//   String dob;
//   String avatar;
//   String licence;
//   String address;
//   String deviceToken;
//   String email;
//   String phone;
//   String totalAvails;
//   int verified;
//   int verifiedAt;
//   int member;
//   String memberAt;
//   int status;
//   String deletedAt;
//   String createdAt;
//   String updatedAt;
//   String expiredAt;
//   int cancelSubscription;
//   int membershipId;
//
//   UserDetailsData(
//       {this.id,
//       this.stripeCustomerId,
//       this.sourceCardId,
//       this.firstName,
//       this.lastName,
//       this.dob,
//       this.avatar,
//       this.licence,
//       this.address,
//       this.deviceToken,
//       this.email,
//       this.phone,
//       this.totalAvails,
//       this.verified,
//       this.verifiedAt,
//       this.member,
//       this.memberAt,
//       this.status,
//       this.deletedAt,
//       this.createdAt,
//       this.updatedAt,
//       this.expiredAt,
//       this.cancelSubscription,
//       this.membershipId});
//
//   UserDetailsData.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     stripeCustomerId = json['stripe_customer_id'];
//     sourceCardId = json['source_card_id'];
//     firstName = json['first_name'];
//     lastName = json['last_name'];
//     dob = json['dob'];
//     avatar = json['avatar'];
//     licence = json['licence'];
//     address = json['address'];
//     deviceToken = json['device_token'];
//     email = json['email'];
//     phone = json['phone'];
//     totalAvails = json['total_avails'];
//     verified = json['verified'];
//     verifiedAt = json['verified_at'];
//     member = json['member'];
//     memberAt = json['member_at'];
//     status = json['status'];
//     deletedAt = json['deleted_at'];
//     createdAt = json['created_at'];
//     updatedAt = json['updated_at'];
//     expiredAt = json['expired_at'];
//     cancelSubscription = json['cancel_subscription'];
//     membershipId = json['membership_id'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['id'] = this.id;
//     data['stripe_customer_id'] = this.stripeCustomerId;
//     data['source_card_id'] = this.sourceCardId;
//     data['first_name'] = this.firstName;
//     data['last_name'] = this.lastName;
//     data['dob'] = this.dob;
//     data['avatar'] = this.avatar;
//     data['licence'] = this.licence;
//     data['address'] = this.address;
//     data['device_token'] = this.deviceToken;
//     data['email'] = this.email;
//     data['phone'] = this.phone;
//     data['total_avails'] = this.totalAvails;
//     data['verified'] = this.verified;
//     data['verified_at'] = this.verifiedAt;
//     data['member'] = this.member;
//     data['member_at'] = this.memberAt;
//     data['status'] = this.status;
//     data['deleted_at'] = this.deletedAt;
//     data['created_at'] = this.createdAt;
//     data['updated_at'] = this.updatedAt;
//     data['expired_at'] = this.expiredAt;
//     data['cancel_subscription'] = this.cancelSubscription;
//     data['membership_id'] = this.membershipId;
//     return data;
//   }
// }
class UserDetails {
  UserDetailsData data;
  int code;
  String message;
  String imageUrl;

  UserDetails({this.data, this.code, this.message, this.imageUrl});

  UserDetails.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null
        ? new UserDetailsData.fromJson(json['data'])
        : null;
    code = json['code'];
    message = json['message'];
    imageUrl = json['image_url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['code'] = this.code;
    data['message'] = this.message;
    data['image_url'] = this.imageUrl;
    return data;
  }
}

class UserDetailsData {
  int id;
  String stripeCustomerId;
  String sourceCardId;
  String firstName;
  String lastName;
  String dob;
  String avatar;
  String licence;
  String address;
  String deviceToken;
  String email;
  String phone;
  String totalAvails;
  dynamic addressLat;
  dynamic addressLong;
  dynamic verified;
  int verifiedAt;
  dynamic member;
  String memberAt;
  dynamic status;
  int deletedAt;
  String createdAt;
  String updatedAt;
  String expiredAt;
  int cancelSubscription;
  int membershipId;
  int yearlyCancel;
  UserDetailsData(
      {this.id,
      this.stripeCustomerId,
      this.sourceCardId,
      this.firstName,
      this.lastName,
      this.dob,
      this.avatar,
      this.licence,
      this.address,
      this.deviceToken,
      this.email,
      this.phone,
      this.totalAvails,
      this.addressLat,
      this.addressLong,
      this.verified,
      this.verifiedAt,
      this.member,
      this.memberAt,
      this.status,
      this.deletedAt,
      this.createdAt,
      this.updatedAt,
      this.expiredAt,
      this.cancelSubscription,
      this.membershipId,
      this.yearlyCancel});

  UserDetailsData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    stripeCustomerId = json['stripe_customer_id'];
    sourceCardId = json['source_card_id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    dob = json['dob'];
    avatar = json['avatar'];
    licence = json['licence'];
    address = json['address'];
    deviceToken = json['device_token'];
    email = json['email'];
    phone = json['phone'];
    totalAvails = json['total_avails'];
    addressLat = json['address_lat'];
    addressLong = json['address_long'];
    verified = json['verified'];
    verifiedAt = json['verified_at'];
    member = json['member'];
    memberAt = json['member_at'];
    status = json['status'];
    deletedAt = json['deleted_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    expiredAt = json['expired_at'];
    cancelSubscription = json['cancel_subscription'];
    membershipId = json['membership_id'];
    yearlyCancel = json['cancel_yearly'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['stripe_customer_id'] = this.stripeCustomerId;
    data['source_card_id'] = this.sourceCardId;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['dob'] = this.dob;
    data['avatar'] = this.avatar;
    data['licence'] = this.licence;
    data['address'] = this.address;
    data['device_token'] = this.deviceToken;
    data['email'] = this.email;
    data['phone'] = this.phone;
    data['total_avails'] = this.totalAvails;
    data['address_lat'] = this.addressLat;
    data['address_long'] = this.addressLong;
    data['verified'] = this.verified;
    data['verified_at'] = this.verifiedAt;
    data['member'] = this.member;
    data['member_at'] = this.memberAt;
    data['status'] = this.status;
    data['deleted_at'] = this.deletedAt;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['expired_at'] = this.expiredAt;
    data['cancel_subscription'] = this.cancelSubscription;
    data['membership_id'] = this.membershipId;
    data['cancel_yearly'] = this.yearlyCancel;
    return data;
  }
}
