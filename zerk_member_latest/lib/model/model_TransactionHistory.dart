class TransactionHistory {
  List<Data> data;
  int code;
  String message;
  String imageUrl;

  TransactionHistory({this.data, this.code, this.message, this.imageUrl});

  TransactionHistory.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
    code = json['code'];
    message = json['message'];
    imageUrl = json['image_url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    data['code'] = this.code;
    data['message'] = this.message;
    data['image_url'] = this.imageUrl;
    return data;
  }
}

class Data {
  int id;
  int membershipId;
  String membershipName;
  String avails;
  String remainingAvails;
  int amount;
  String date;
  String expiredAt;
  String status;

  Data(
      {this.id,
      this.membershipId,
      this.membershipName,
      this.avails,
      this.remainingAvails,
      this.amount,
      this.date,
      this.expiredAt,
      this.status});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    membershipId = json['membership_id'];
    membershipName = json['membership_name'];
    avails = json['avails'];
    remainingAvails = json['remaining_avails'];
    amount = json['amount'];
    date = json['date'];
    expiredAt = json['expired_at'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['membership_id'] = this.membershipId;
    data['membership_name'] = this.membershipName;
    data['avails'] = this.avails;
    data['remaining_avails'] = this.remainingAvails;
    data['amount'] = this.amount;
    data['date'] = this.date;
    data['expired_at'] = this.expiredAt;
    data['status'] = this.status;
    return data;
  }
}
