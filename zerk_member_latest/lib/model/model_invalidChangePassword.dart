class InvalidChangePassword {
  InvalidChangePasswordData data;
  int code;
  String message;

  InvalidChangePassword({this.data, this.code, this.message});

  InvalidChangePassword.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null
        ? new InvalidChangePasswordData.fromJson(json['data'])
        : null;
    code = json['code'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['code'] = this.code;
    data['message'] = this.message;
    return data;
  }
}

class InvalidChangePasswordData {
  List<String> password;

  InvalidChangePasswordData({this.password});

  InvalidChangePasswordData.fromJson(Map<String, dynamic> json) {
    password = json['password'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['password'] = this.password;
    return data;
  }
}
