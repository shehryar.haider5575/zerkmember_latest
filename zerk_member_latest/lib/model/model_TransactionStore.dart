class TransactionStore {
  String data;
  int code;
  String message;

  TransactionStore({this.data, this.code, this.message});

  TransactionStore.fromJson(Map<String, dynamic> json) {
    data = json['data'];
    code = json['code'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['data'] = this.data;
    data['code'] = this.code;
    data['message'] = this.message;
    return data;
  }
}
