/*class SpecifiedFacilityDetail {
  Data data;
  int code;
  String message;
  String imageUrl;

  SpecifiedFacilityDetail({this.data, this.code, this.message, this.imageUrl});

  SpecifiedFacilityDetail.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    code = json['code'];
    message = json['message'];
    imageUrl = json['image_url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['code'] = this.code;
    data['message'] = this.message;
    data['image_url'] = this.imageUrl;
    return data;
  }
}

class Data {
  int id;
  String bussinessName;
  String abn;
  String firstName;
  String lastName;
  String avatar;
  String email;
  String digitCode;
  String phone;
  String address;
  double addressLat;
  double addressLong;
  int userType;
  int verified;
  String verifiedAt;
  Null createdBy;
  Null updatedBy;
  int status;
  Null emailVerifiedAt;
  Null deletedAt;
  String createdAt;
  String updatedAt;

  Data(
      {this.id,
      this.bussinessName,
      this.abn,
      this.firstName,
      this.lastName,
      this.avatar,
      this.email,
      this.digitCode,
      this.phone,
      this.address,
      this.addressLat,
      this.addressLong,
      this.userType,
      this.verified,
      this.verifiedAt,
      this.createdBy,
      this.updatedBy,
      this.status,
      this.emailVerifiedAt,
      this.deletedAt,
      this.createdAt,
      this.updatedAt});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    bussinessName = json['bussiness_name'];
    abn = json['abn'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    avatar = json['avatar'];
    email = json['email'];
    digitCode = json['digit_code'];
    phone = json['phone'];
    address = json['address'];
    addressLat = json['address_lat'];
    addressLong = json['address_long'];
    userType = json['user_type'];
    verified = json['verified'];
    verifiedAt = json['verified_at'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    status = json['status'];
    emailVerifiedAt = json['email_verified_at'];
    deletedAt = json['deleted_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['bussiness_name'] = this.bussinessName;
    data['abn'] = this.abn;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['avatar'] = this.avatar;
    data['email'] = this.email;
    data['digit_code'] = this.digitCode;
    data['phone'] = this.phone;
    data['address'] = this.address;
    data['address_lat'] = this.addressLat;
    data['address_long'] = this.addressLong;
    data['user_type'] = this.userType;
    data['verified'] = this.verified;
    data['verified_at'] = this.verifiedAt;
    data['created_by'] = this.createdBy;
    data['updated_by'] = this.updatedBy;
    data['status'] = this.status;
    data['email_verified_at'] = this.emailVerifiedAt;
    data['deleted_at'] = this.deletedAt;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}*/
//-----------------------------------------------------------------------------
class SpecifiedFacilityDetail {
  Data data;
  int code;
  String message;
  String imageUrl;

  SpecifiedFacilityDetail({this.data, this.code, this.message, this.imageUrl});

  SpecifiedFacilityDetail.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    code = json['code'];
    message = json['message'];
    imageUrl = json['image_url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['code'] = this.code;
    data['message'] = this.message;
    data['image_url'] = this.imageUrl;
    return data;
  }
}

class Data {
  int id;
  String bussinessName;
  String abn;
  String phone;
  String firstName;
  String lastName;
  String avatar;
  String deviceToken;
  String email;
  String address;
  dynamic addressLat;
  dynamic addressLong;
  String website;
  String services;
  String openAt;
  String closeAt;
  String emailVerifiedAt;
  String digitCode;
  dynamic userType;
  dynamic verified;
  String verifiedAt;
  String createdBy;
  String updatedBy;
  dynamic status;
  String deletedAt;
  String createdAt;
  String updatedAt;

  Data(
      {this.id,
      this.bussinessName,
      this.abn,
      this.phone,
      this.firstName,
      this.lastName,
      this.avatar,
      this.deviceToken,
      this.email,
      this.address,
      this.addressLat,
      this.addressLong,
      this.website,
      this.services,
      this.openAt,
      this.closeAt,
      this.emailVerifiedAt,
      this.digitCode,
      this.userType,
      this.verified,
      this.verifiedAt,
      this.createdBy,
      this.updatedBy,
      this.status,
      this.deletedAt,
      this.createdAt,
      this.updatedAt});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    bussinessName = json['bussiness_name'];
    abn = json['abn'];
    phone = json['phone'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    avatar = json['avatar'];
    deviceToken = json['device_token'];
    email = json['email'];
    address = json['address'];
    addressLat = json['address_lat'];
    addressLong = json['address_long'];
    website = json['website'];
    services = json['services'];
    openAt = json['open_at'];
    closeAt = json['close_at'];
    emailVerifiedAt = json['email_verified_at'];
    digitCode = json['digit_code'];
    userType = json['user_type'];
    verified = json['verified'];
    verifiedAt = json['verified_at'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    status = json['status'];
    deletedAt = json['deleted_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['bussiness_name'] = this.bussinessName;
    data['abn'] = this.abn;
    data['phone'] = this.phone;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['avatar'] = this.avatar;
    data['device_token'] = this.deviceToken;
    data['email'] = this.email;
    data['address'] = this.address;
    data['address_lat'] = this.addressLat;
    data['address_long'] = this.addressLong;
    data['website'] = this.website;
    data['services'] = this.services;
    data['open_at'] = this.openAt;
    data['close_at'] = this.closeAt;
    data['email_verified_at'] = this.emailVerifiedAt;
    data['digit_code'] = this.digitCode;
    data['user_type'] = this.userType;
    data['verified'] = this.verified;
    data['verified_at'] = this.verifiedAt;
    data['created_by'] = this.createdBy;
    data['updated_by'] = this.updatedBy;
    data['status'] = this.status;
    data['deleted_at'] = this.deletedAt;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
