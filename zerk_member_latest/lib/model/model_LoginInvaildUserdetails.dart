class LoginInvaildUserdetails {
  String error;
  String message;
  String hint;

  LoginInvaildUserdetails({this.error, this.message, this.hint});

  LoginInvaildUserdetails.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    message = json['message'];
    hint = json['hint'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['message'] = this.message;
    data['hint'] = this.hint;
    return data;
  }
}
