class UserLogin {
  UserLoginData data;
  int code;
  String message;

  UserLogin({this.data, this.code, this.message});

  UserLogin.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new UserLoginData.fromJson(json['data']) : null;
    code = json['code'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['code'] = this.code;
    data['message'] = this.message;
    return data;
  }
}

class UserLoginData {
  String token;

  UserLoginData({this.token});

  UserLoginData.fromJson(Map<String, dynamic> json) {
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['token'] = this.token;
    return data;
  }
}
