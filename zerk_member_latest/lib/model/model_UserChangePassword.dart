class ChangePassword {
  ChangePasswordData data;
  int code;
  String message;

  ChangePassword({this.data, this.code, this.message});

  ChangePassword.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null
        ? new ChangePasswordData.fromJson(json['data'])
        : null;
    code = json['code'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['code'] = this.code;
    data['message'] = this.message;
    return data;
  }
}

class ChangePasswordData {
  int id;
  String firstName;
  String lastName;
  String dob;
  String email;
  String avatar;
  String licence;
  String address;
  int member;
  int verified;
  String verifiedAt;
  String deletedAt;
  String createdAt;
  String updatedAt;

  ChangePasswordData(
      {this.id,
      this.firstName,
      this.lastName,
      this.dob,
      this.email,
      this.avatar,
      this.licence,
      this.address,
      this.member,
      this.verified,
      this.verifiedAt,
      this.deletedAt,
      this.createdAt,
      this.updatedAt});

  ChangePasswordData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    dob = json['dob'];
    email = json['email'];
    avatar = json['avatar'];
    licence = json['licence'];
    address = json['address'];
    member = json['member'];
    verified = json['verified'];
    verifiedAt = json['verified_at'];
    deletedAt = json['deleted_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['dob'] = this.dob;
    data['email'] = this.email;
    data['avatar'] = this.avatar;
    data['licence'] = this.licence;
    data['address'] = this.address;
    data['member'] = this.member;
    data['verified'] = this.verified;
    data['verified_at'] = this.verifiedAt;
    data['deleted_at'] = this.deletedAt;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
