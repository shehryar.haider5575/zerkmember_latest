// To parse this JSON data, do
//
//     final cancellationHistory = cancellationHistoryFromJson(jsonString);

import 'dart:convert';

CancellationHistory cancellationHistoryFromJson(String str) =>
    CancellationHistory.fromJson(json.decode(str));

String cancellationHistoryToJson(CancellationHistory data) =>
    json.encode(data.toJson());

class CancellationHistory {
  CancellationHistory({
    this.data,
    this.code,
    this.message,
    this.imageUrl,
  });

  List<Datum> data;
  int code;
  String message;
  String imageUrl;

  factory CancellationHistory.fromJson(Map<String, dynamic> json) =>
      CancellationHistory(
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        code: json["code"],
        message: json["message"],
        imageUrl: json["image_url"],
      );

  Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "code": code,
        "message": message,
        "image_url": imageUrl,
      };
}

class Datum {
  Datum({
    this.id,
    this.customerId,
    this.membershipId,
    this.pmId,
    this.prId,
    this.card,
    this.cvv,
    this.cardExpiryDate,
    this.amount,
    this.avails,
    this.remainingAvails,
    this.date,
    this.cancelAt,
    this.expiredAt,
    this.membershipExpiredAt,
    this.expire,
    this.status,
    this.paymentStatus,
    this.createdAt,
    this.updatedAt,
    this.membership,
  });

  int id;
  int customerId;
  int membershipId;
  String pmId;
  String prId;
  String card;
  String cvv;
  String cardExpiryDate;
  int amount;
  String avails;
  String remainingAvails;
  DateTime date;
  DateTime cancelAt;
  DateTime expiredAt;
  DateTime membershipExpiredAt;
  int expire;
  String status;
  String paymentStatus;
  DateTime createdAt;
  DateTime updatedAt;
  Membership membership;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        customerId: json["customer_id"],
        membershipId: json["membership_id"],
        pmId: json["pm_id"],
        prId: json["pr_id"],
        card: json["card"],
        cvv: json["cvv"],
        cardExpiryDate: json["card_expiry_date"],
        amount: json["amount"],
        avails: json["avails"],
        remainingAvails: json["remaining_avails"],
        date: DateTime.parse(json["date"]),
        cancelAt: DateTime.parse(json["cancel_at"]),
        expiredAt: DateTime.parse(json["membership_expired_at"]),
        membershipExpiredAt: DateTime.parse(json["membership_expired_at"]),
        expire: json["expire"],
        status: json["status"],
        paymentStatus: json["payment_status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        membership: Membership.fromJson(json["membership"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "customer_id": customerId,
        "membership_id": membershipId,
        "pm_id": pmId,
        "pr_id": prId,
        "card": card,
        "cvv": cvv,
        "card_expiry_date": cardExpiryDate,
        "amount": amount,
        "avails": avails,
        "remaining_avails": remainingAvails,
        "date":
            "${date.year.toString().padLeft(4, '0')}-${date.month.toString().padLeft(2, '0')}-${date.day.toString().padLeft(2, '0')}",
        "cancel_at": cancelAt.toIso8601String(),
        "expired_at": expiredAt.toIso8601String(),
        "membership_expired_at": membershipExpiredAt.toIso8601String(),
        "expire": expire,
        "status": status,
        "payment_status": paymentStatus,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "membership": membership.toJson(),
      };
}

class Membership {
  Membership({
    this.id,
    this.stripPackageId,
    this.name,
    this.amount,
    this.avail,
    this.days,
    this.membershipDays,
    this.agreementLink,
    this.status,
    this.createdBy,
    this.updatedBy,
    this.deletedAt,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  dynamic stripPackageId;
  String name;
  String amount;
  String avail;
  int days;
  int membershipDays;
  String agreementLink;
  int status;
  int createdBy;
  int updatedBy;
  dynamic deletedAt;
  DateTime createdAt;
  DateTime updatedAt;

  factory Membership.fromJson(Map<String, dynamic> json) => Membership(
        id: json["id"],
        stripPackageId: json["strip_package_id"],
        name: json["name"],
        amount: json["amount"],
        avail: json["avail"],
        days: json["days"],
        membershipDays: json["membership_days"],
        agreementLink: json["agreement_link"],
        status: json["status"],
        createdBy: json["created_by"],
        updatedBy: json["updated_by"],
        deletedAt: json["deleted_at"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "strip_package_id": stripPackageId,
        "name": name,
        "amount": amount,
        "avail": avail,
        "days": days,
        "membership_days": membershipDays,
        "agreement_link": agreementLink,
        "status": status,
        "created_by": createdBy,
        "updated_by": updatedBy,
        "deleted_at": deletedAt,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
      };
}
