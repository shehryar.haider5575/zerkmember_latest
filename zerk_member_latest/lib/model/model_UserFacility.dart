class UserFacility {
  List<UserFacilityData> data;
  int code;
  String message;
  String imageUrl;

  UserFacility({this.data, this.code, this.message, this.imageUrl});

  UserFacility.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<UserFacilityData>();
      json['data'].forEach((v) {
        data.add(new UserFacilityData.fromJson(v));
      });
    }
    code = json['code'];
    message = json['message'];
    imageUrl = json['image_url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    data['code'] = this.code;
    data['message'] = this.message;
    data['image_url'] = this.imageUrl;
    return data;
  }
}

class UserFacilityData {
  int id;
  String bussinessName;
  dynamic abn;
  String firstName;
  String lastName;
  String avatar;
  String email;
  dynamic digitCode;
  String phone;
  String address;
  dynamic addressLat;
  dynamic addressLong;
  dynamic userType;
  dynamic verified;
  dynamic verifiedAt;
  dynamic createdBy;
  dynamic updatedBy;
  dynamic status;
  dynamic emailVerifiedAt;
  dynamic deletedAt;
  dynamic createdAt;
  dynamic updatedAt;

  UserFacilityData(
      {this.id,
      this.bussinessName,
      this.abn,
      this.firstName,
      this.lastName,
      this.avatar,
      this.email,
      this.digitCode,
      this.phone,
      this.address,
      this.addressLat,
      this.addressLong,
      this.userType,
      this.verified,
      this.verifiedAt,
      this.createdBy,
      this.updatedBy,
      this.status,
      this.emailVerifiedAt,
      this.deletedAt,
      this.createdAt,
      this.updatedAt});

  UserFacilityData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    bussinessName = json['bussiness_name'];
    abn = json['abn'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    avatar = json['avatar'];
    email = json['email'];
    digitCode = json['digit_code'];
    phone = json['phone'];
    address = json['address'];
    addressLat = json['address_lat'];
    addressLong = json['address_long'];
    userType = json['user_type'];
    verified = json['verified'];
    verifiedAt = json['verified_at'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    status = json['status'];
    emailVerifiedAt = json['email_verified_at'];
    deletedAt = json['deleted_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['bussiness_name'] = this.bussinessName;
    data['abn'] = this.abn;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['avatar'] = this.avatar;
    data['email'] = this.email;
    data['digit_code'] = this.digitCode;
    data['phone'] = this.phone;
    data['address'] = this.address;
    data['address_lat'] = this.addressLat;
    data['address_long'] = this.addressLong;
    data['user_type'] = this.userType;
    data['verified'] = this.verified;
    data['verified_at'] = this.verifiedAt;
    data['created_by'] = this.createdBy;
    data['updated_by'] = this.updatedBy;
    data['status'] = this.status;
    data['email_verified_at'] = this.emailVerifiedAt;
    data['deleted_at'] = this.deletedAt;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
