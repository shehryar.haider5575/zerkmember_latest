class AvailsLogs {
  List<AvailsLogsData> data;
  int code;
  String message;

  AvailsLogs({this.data, this.code, this.message});

  AvailsLogs.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<AvailsLogsData>();
      json['data'].forEach((v) {
        data.add(new AvailsLogsData.fromJson(v));
      });
    }
    code = json['code'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    data['code'] = this.code;
    data['message'] = this.message;
    return data;
  }
}

class AvailsLogsData {
  int id;
  int memberId;
  int facilityId;
  Null deletedAt;
  String createdAt;
  String updatedAt;
  Facility facility;

  AvailsLogsData(
      {this.id,
      this.memberId,
      this.facilityId,
      this.deletedAt,
      this.createdAt,
      this.updatedAt,
      this.facility});

  AvailsLogsData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    memberId = json['member_id'];
    facilityId = json['facility_id'];
    deletedAt = json['deleted_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    facility = json['facility'] != null
        ? new Facility.fromJson(json['facility'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['member_id'] = this.memberId;
    data['facility_id'] = this.facilityId;
    data['deleted_at'] = this.deletedAt;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.facility != null) {
      data['facility'] = this.facility.toJson();
    }
    return data;
  }
}

class Facility {
  int id;
  String firstName;
  String lastName;
  String bussinessName;

  Facility({this.id, this.firstName, this.lastName, this.bussinessName});

  Facility.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    bussinessName = json['bussiness_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['bussiness_name'] = this.bussinessName;
    return data;
  }
}
