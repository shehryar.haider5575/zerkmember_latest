// class UserSignup {
//   UserSignupData data;
//   int code;
//   String message;
//
//   UserSignup({this.data, this.code, this.message});
//
//   UserSignup.fromJson(Map<String, dynamic> json) {
//     data = json['data'] != null ? new UserSignupData.fromJson(json['data']) : null;
//     code = json['code'];
//     message = json['message'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     if (this.data != null) {
//       data['data'] = this.data.toJson();
//     }
//     data['code'] = this.code;
//     data['message'] = this.message;
//     return data;
//   }
// }
//
// class UserSignupData {
//   String token;
//   String name;
//
//   UserSignupData({this.token, this.name});
//
//   UserSignupData.fromJson(Map<String, dynamic> json) {
//     token = json['token'];
//     name = json['name'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['token'] = this.token;
//     data['name'] = this.name;
//     return data;
//   }
// }

class UserSignup {
  Data data;
  int code;
  String message;
  String imageUrl;

  UserSignup({this.data, this.code, this.message, this.imageUrl});

  UserSignup.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    code = json['code'];
    message = json['message'];
    imageUrl = json['image_url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['code'] = this.code;
    data['message'] = this.message;
    data['image_url'] = this.imageUrl;
    return data;
  }
}

class Data {
  String token;
  User user;

  Data({this.token, this.user});

  Data.fromJson(Map<String, dynamic> json) {
    token = json['token'];
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['token'] = this.token;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    return data;
  }
}

class User {
  String firstName;
  String lastName;
  String dob;
  String email;
  String deviceToken;
  String phone;
  String stripeCustomerId;
  String updatedAt;
  String createdAt;
  int id;

  User(
      {this.firstName,
      this.lastName,
      this.dob,
      this.email,
      this.deviceToken,
      this.phone,
      this.stripeCustomerId,
      this.updatedAt,
      this.createdAt,
      this.id});

  User.fromJson(Map<String, dynamic> json) {
    firstName = json['first_name'];
    lastName = json['last_name'];
    dob = json['dob'];
    email = json['email'];
    deviceToken = json['device_token'];
    phone = json['phone'];
    stripeCustomerId = json['stripe_customer_id'];
    updatedAt = json['updated_at'];
    createdAt = json['created_at'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['dob'] = this.dob;
    data['email'] = this.email;
    data['device_token'] = this.deviceToken;
    data['phone'] = this.phone;
    data['stripe_customer_id'] = this.stripeCustomerId;
    data['updated_at'] = this.updatedAt;
    data['created_at'] = this.createdAt;
    data['id'] = this.id;
    return data;
  }
}
