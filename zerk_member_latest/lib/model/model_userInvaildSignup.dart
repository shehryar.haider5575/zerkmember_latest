class UserInvaildSignup {
  UserInvaildSignupData data;
  int code;
  String message;

  UserInvaildSignup({this.data, this.code, this.message});

  UserInvaildSignup.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new UserInvaildSignupData.fromJson(json['data']) : null;
    code = json['code'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['code'] = this.code;
    data['message'] = this.message;
    return data;
  }
}

class UserInvaildSignupData {
  List<String> email;

  UserInvaildSignupData({this.email});

  UserInvaildSignupData.fromJson(Map<String, dynamic> json) {
    email = json['email'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this.email;
    return data;
  }
}
