class QrcodeScan {
  Data data;
  int code;
  String message;
  String imageUrl;

  QrcodeScan({this.data, this.code, this.message, this.imageUrl});

  QrcodeScan.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    code = json['code'];
    message = json['message'];
    imageUrl = json['image_url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['code'] = this.code;
    data['message'] = this.message;
    data['image_url'] = this.imageUrl;
    return data;
  }
}

class Data {
  int id;
  String firstName;
  String lastName;
  String dob;
  String email;
  String avatar;
  String licence;
  String address;
  int member;
  int verified;
  String verifiedAt;
  Null deletedAt;
  String createdAt;
  String updatedAt;
  int totalAvails;
  String memberAt;
  int status;
  List<Transactions> transactions;

  Data(
      {this.id,
      this.firstName,
      this.lastName,
      this.dob,
      this.email,
      this.avatar,
      this.licence,
      this.address,
      this.member,
      this.verified,
      this.verifiedAt,
      this.deletedAt,
      this.createdAt,
      this.updatedAt,
      this.totalAvails,
      this.memberAt,
      this.status,
      this.transactions});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    dob = json['dob'];
    email = json['email'];
    avatar = json['avatar'];
    licence = json['licence'];
    address = json['address'];
    member = json['member'];
    verified = json['verified'];
    verifiedAt = json['verified_at'];
    deletedAt = json['deleted_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    totalAvails = json['total_avails'];
    memberAt = json['member_at'];
    status = json['status'];
    if (json['transactions'] != null) {
      transactions = new List<Transactions>();
      json['transactions'].forEach((v) {
        transactions.add(new Transactions.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['dob'] = this.dob;
    data['email'] = this.email;
    data['avatar'] = this.avatar;
    data['licence'] = this.licence;
    data['address'] = this.address;
    data['member'] = this.member;
    data['verified'] = this.verified;
    data['verified_at'] = this.verifiedAt;
    data['deleted_at'] = this.deletedAt;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['total_avails'] = this.totalAvails;
    data['member_at'] = this.memberAt;
    data['status'] = this.status;
    if (this.transactions != null) {
      data['transactions'] = this.transactions.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Transactions {
  int id;
  int customerId;
  int membershipId;
  String pmId;
  String prId;
  String amount;
  String avails;
  String date;
  String status;
  String createdAt;
  String updatedAt;
  // String remainingAvails;

  Transactions({
    this.id,
    this.customerId,
    this.membershipId,
    this.pmId,
    this.prId,
    this.amount,
    this.avails,
    this.date,
    this.status,
    this.createdAt,
    this.updatedAt,
    // this.remainingAvails
  });

  Transactions.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    customerId = json['customer_id'];
    membershipId = json['membership_id'];
    pmId = json['pm_id'];
    prId = json['pr_id'];
    amount = json['amount'];
    avails = json['avails'];
    date = json['date'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    // remainingAvails = json['remaining_avails'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['customer_id'] = this.customerId;
    data['membership_id'] = this.membershipId;
    data['pm_id'] = this.pmId;
    data['pr_id'] = this.prId;
    data['amount'] = this.amount;
    data['avails'] = this.avails;
    data['date'] = this.date;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    // data['remaining_avails'] = this.remainingAvails;
    return data;
  }
}
