class CancelTransactions {
  List<Data> data;
  int code;
  String message;
  String imageUrl;

  CancelTransactions({this.data, this.code, this.message, this.imageUrl});

  CancelTransactions.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
    code = json['code'];
    message = json['message'];
    imageUrl = json['image_url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    data['code'] = this.code;
    data['message'] = this.message;
    data['image_url'] = this.imageUrl;
    return data;
  }
}

class Data {
  int id;
  int customerId;
  int membershipId;
  String pmId;
  String prId;
  String card;
  String cvv;
  String cardExpiryDate;
  int amount;
  String avails;
  String remainingAvails;
  String date;
  String cancelAt;
  String expiredAt;
  String membershipExpiredAt;
  int expire;
  String status;
  String paymentStatus;
  String createdAt;
  String updatedAt;
  Membership membership;

  Data(
      {this.id,
      this.customerId,
      this.membershipId,
      this.pmId,
      this.prId,
      this.card,
      this.cvv,
      this.cardExpiryDate,
      this.amount,
      this.avails,
      this.remainingAvails,
      this.date,
      this.cancelAt,
      this.expiredAt,
      this.membershipExpiredAt,
      this.expire,
      this.status,
      this.paymentStatus,
      this.createdAt,
      this.updatedAt,
      this.membership});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    customerId = json['customer_id'];
    membershipId = json['membership_id'];
    pmId = json['pm_id'];
    prId = json['pr_id'];
    card = json['card'];
    cvv = json['cvv'];
    cardExpiryDate = json['card_expiry_date'];
    amount = json['amount'];
    avails = json['avails'];
    remainingAvails = json['remaining_avails'];
    date = json['date'];
    cancelAt = json['cancel_at'];
    expiredAt = json['expired_at'];
    membershipExpiredAt = json['membership_expired_at'];
    expire = json['expire'];
    status = json['status'];
    paymentStatus = json['payment_status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    membership = json['membership'] != null
        ? new Membership.fromJson(json['membership'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['customer_id'] = this.customerId;
    data['membership_id'] = this.membershipId;
    data['pm_id'] = this.pmId;
    data['pr_id'] = this.prId;
    data['card'] = this.card;
    data['cvv'] = this.cvv;
    data['card_expiry_date'] = this.cardExpiryDate;
    data['amount'] = this.amount;
    data['avails'] = this.avails;
    data['remaining_avails'] = this.remainingAvails;
    data['date'] = this.date;
    data['cancel_at'] = this.cancelAt;
    data['expired_at'] = this.expiredAt;
    data['membership_expired_at'] = this.membershipExpiredAt;
    data['expire'] = this.expire;
    data['status'] = this.status;
    data['payment_status'] = this.paymentStatus;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.membership != null) {
      data['membership'] = this.membership.toJson();
    }
    return data;
  }
}

class Membership {
  int id;
  int stripPackageId;
  String name;
  String amount;
  String avail;
  int days;
  int membershipDays;
  String agreementLink;
  int status;
  int createdBy;
  int updatedBy;
  String deletedAt;
  String createdAt;
  String updatedAt;

  Membership(
      {this.id,
      this.stripPackageId,
      this.name,
      this.amount,
      this.avail,
      this.days,
      this.membershipDays,
      this.agreementLink,
      this.status,
      this.createdBy,
      this.updatedBy,
      this.deletedAt,
      this.createdAt,
      this.updatedAt});

  Membership.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    stripPackageId = json['strip_package_id'];
    name = json['name'];
    amount = json['amount'];
    avail = json['avail'];
    days = json['days'];
    membershipDays = json['membership_days'];
    agreementLink = json['agreement_link'];
    status = json['status'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    deletedAt = json['deleted_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['strip_package_id'] = this.stripPackageId;
    data['name'] = this.name;
    data['amount'] = this.amount;
    data['avail'] = this.avail;
    data['days'] = this.days;
    data['membership_days'] = this.membershipDays;
    data['agreement_link'] = this.agreementLink;
    data['status'] = this.status;
    data['created_by'] = this.createdBy;
    data['updated_by'] = this.updatedBy;
    data['deleted_at'] = this.deletedAt;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
