class MemberNotification {
  List<Data> data;
  int code;
  String message;
  String imageUrl;

  MemberNotification({this.data, this.code, this.message, this.imageUrl});

  MemberNotification.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
    code = json['code'];
    message = json['message'];
    imageUrl = json['image_url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    data['code'] = this.code;
    data['message'] = this.message;
    data['image_url'] = this.imageUrl;
    return data;
  }
}

class Data {
  int id;
  int from;
  int to;
  String message;
  int resend;
  int user;
  String readAt;
  String createdAt;
  String updatedAt;
  FromContact fromContact;

  Data(
      {this.id,
      this.from,
      this.to,
      this.message,
      this.resend,
      this.user,
      this.readAt,
      this.createdAt,
      this.updatedAt,
      this.fromContact});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    from = json['from'];
    to = json['to'];
    message = json['message'];
    resend = json['resend'];
    user = json['user'];
    readAt = json['read_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    fromContact = json['from_contact'] != null
        ? new FromContact.fromJson(json['from_contact'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['from'] = this.from;
    data['to'] = this.to;
    data['message'] = this.message;
    data['resend'] = this.resend;
    data['user'] = this.user;
    data['read_at'] = this.readAt;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.fromContact != null) {
      data['from_contact'] = this.fromContact.toJson();
    }
    return data;
  }
}

class FromContact {
  int id;
  String bussinessName;
  String abn;
  String phone;
  String firstName;
  String lastName;
  String avatar;
  String deviceToken;
  String email;
  String address;
  String addressLat;
  String addressLong;
  String emailVerifiedAt;
  String digitCode;
  int userType;
  int verified;
  String verifiedAt;
  String createdBy;
  String updatedBy;
  int status;
  String deletedAt;
  String createdAt;
  String updatedAt;

  FromContact(
      {this.id,
      this.bussinessName,
      this.abn,
      this.phone,
      this.firstName,
      this.lastName,
      this.avatar,
      this.deviceToken,
      this.email,
      this.address,
      this.addressLat,
      this.addressLong,
      this.emailVerifiedAt,
      this.digitCode,
      this.userType,
      this.verified,
      this.verifiedAt,
      this.createdBy,
      this.updatedBy,
      this.status,
      this.deletedAt,
      this.createdAt,
      this.updatedAt});

  FromContact.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    bussinessName = json['bussiness_name'];
    abn = json['abn'];
    phone = json['phone'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    avatar = json['avatar'];
    deviceToken = json['device_token'];
    email = json['email'];
    address = json['address'];
    addressLat = json['address_lat'];
    addressLong = json['address_long'];
    emailVerifiedAt = json['email_verified_at'];
    digitCode = json['digit_code'];
    userType = json['user_type'];
    verified = json['verified'];
    verifiedAt = json['verified_at'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    status = json['status'];
    deletedAt = json['deleted_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['bussiness_name'] = this.bussinessName;
    data['abn'] = this.abn;
    data['phone'] = this.phone;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['avatar'] = this.avatar;
    data['device_token'] = this.deviceToken;
    data['email'] = this.email;
    data['address'] = this.address;
    data['address_lat'] = this.addressLat;
    data['address_long'] = this.addressLong;
    data['email_verified_at'] = this.emailVerifiedAt;
    data['digit_code'] = this.digitCode;
    data['user_type'] = this.userType;
    data['verified'] = this.verified;
    data['verified_at'] = this.verifiedAt;
    data['created_by'] = this.createdBy;
    data['updated_by'] = this.updatedBy;
    data['status'] = this.status;
    data['deleted_at'] = this.deletedAt;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
