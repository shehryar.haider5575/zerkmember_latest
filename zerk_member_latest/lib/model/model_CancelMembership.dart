class CancelMembership {
  Data data;
  int code;
  String message;
  String imageUrl;

  CancelMembership({this.data, this.code, this.message, this.imageUrl});

  CancelMembership.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    code = json['code'];
    message = json['message'];
    imageUrl = json['image_url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['code'] = this.code;
    data['message'] = this.message;
    data['image_url'] = this.imageUrl;
    return data;
  }
}

class Data {
  int id;
  int customerId;
  int membershipId;
  String pmId;
  String prId;
  String card;
  String cvv;
  String cardExpiryDate;
  int amount;
  String avails;
  String remainingAvails;
  String date;
  String cancelAt;
  String expiredAt;
  int expire;
  String status;
  String createdAt;
  String updatedAt;

  Data(
      {this.id,
      this.customerId,
      this.membershipId,
      this.pmId,
      this.prId,
      this.card,
      this.cvv,
      this.cardExpiryDate,
      this.amount,
      this.avails,
      this.remainingAvails,
      this.date,
      this.cancelAt,
      this.expiredAt,
      this.expire,
      this.status,
      this.createdAt,
      this.updatedAt});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    customerId = json['customer_id'];
    membershipId = json['membership_id'];
    pmId = json['pm_id'];
    prId = json['pr_id'];
    card = json['card'];
    cvv = json['cvv'];
    cardExpiryDate = json['card_expiry_date'];
    amount = json['amount'];
    avails = json['avails'];
    remainingAvails = json['remaining_avails'];
    date = json['date'];
    cancelAt = json['cancel_at'];
    expiredAt = json['expired_at'];
    expire = json['expire'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['customer_id'] = this.customerId;
    data['membership_id'] = this.membershipId;
    data['pm_id'] = this.pmId;
    data['pr_id'] = this.prId;
    data['card'] = this.card;
    data['cvv'] = this.cvv;
    data['card_expiry_date'] = this.cardExpiryDate;
    data['amount'] = this.amount;
    data['avails'] = this.avails;
    data['remaining_avails'] = this.remainingAvails;
    data['date'] = this.date;
    data['cancel_at'] = this.cancelAt;
    data['expired_at'] = this.expiredAt;
    data['expire'] = this.expire;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
