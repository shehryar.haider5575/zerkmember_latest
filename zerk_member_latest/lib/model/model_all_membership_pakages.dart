// class AllMembershipPakages {
//   List<Data> data;
//   int code;
//   String message;
//   String imageUrl;
//
//   AllMembershipPakages({this.data, this.code, this.message, this.imageUrl});
//
//   AllMembershipPakages.fromJson(Map<String, dynamic> json) {
//     if (json['data'] != null) {
//       data = new List<Data>();
//       json['data'].forEach((v) {
//         data.add(new Data.fromJson(v));
//       });
//     }
//     code = json['code'];
//     message = json['message'];
//     imageUrl = json['image_url'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     if (this.data != null) {
//       data['data'] = this.data.map((v) => v.toJson()).toList();
//     }
//     data['code'] = this.code;
//     data['message'] = this.message;
//     data['image_url'] = this.imageUrl;
//     return data;
//   }
// }
//
// class Data {
//   int id;
//   String name;
//   double amount;
//   String avail;
//   int days;
//   String agreementLink;
//   int status;
//   int createdBy;
//   int updatedBy;
//   Null deletedAt;
//   String createdAt;
//   String updatedAt;
//
//   Data(
//       {this.id,
//       this.name,
//       this.amount,
//       this.avail,
//       this.days,
//       this.agreementLink,
//       this.status,
//       this.createdBy,
//       this.updatedBy,
//       this.deletedAt,
//       this.createdAt,
//       this.updatedAt});
//
//   Data.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     name = json['name'];
//     amount = json['amount'].toDouble();
//     avail = json['avail'];
//     days = json['days'];
//     agreementLink = json['agreement_link'];
//     status = json['status'];
//     createdBy = json['created_by'];
//     updatedBy = json['updated_by'];
//     deletedAt = json['deleted_at'];
//     createdAt = json['created_at'];
//     updatedAt = json['updated_at'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['id'] = this.id;
//     data['name'] = this.name;
//     data['amount'] = this.amount;
//     data['avail'] = this.avail;
//     data['days'] = this.days;
//     data['agreement_link'] = this.agreementLink;
//     data['status'] = this.status;
//     data['created_by'] = this.createdBy;
//     data['updated_by'] = this.updatedBy;
//     data['deleted_at'] = this.deletedAt;
//     data['created_at'] = this.createdAt;
//     data['updated_at'] = this.updatedAt;
//     return data;
//   }
// }

class AllMembershipPakages {
  List<Data> data;
  MembershipStatus membershipStatus;
  int code;
  String message;
  String imageUrl;

  AllMembershipPakages(
      {this.data,
      this.membershipStatus,
      this.code,
      this.message,
      this.imageUrl});

  AllMembershipPakages.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
    membershipStatus = json['membership_status'] != null
        ? new MembershipStatus.fromJson(json['membership_status'])
        : null;
    code = json['code'];
    message = json['message'];
    imageUrl = json['image_url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    if (this.membershipStatus != null) {
      data['membership_status'] = this.membershipStatus.toJson();
    }
    data['code'] = this.code;
    data['message'] = this.message;
    data['image_url'] = this.imageUrl;
    return data;
  }
}

class Data {
  int id;
  String name;
  double amount;
  dynamic days;
  String agreementLink;
  dynamic status;
  String createdAt;

  Data(
      {this.id,
      this.name,
      this.amount,
      this.days,
      this.agreementLink,
      this.status,
      this.createdAt});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    // amount = json['amount'];
    amount = json['amount'].toDouble();
    days = json['days'];
    agreementLink = json['agreement_link'];
    status = json['status'];
    createdAt = json['created_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['amount'] = this.amount;
    data['days'] = this.days;
    data['agreement_link'] = this.agreementLink;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    return data;
  }
}

class MembershipStatus {
  int membershipId;
  int cancelSubscription;
  String messageCanceled;

  MembershipStatus({this.membershipId, this.cancelSubscription});

  MembershipStatus.fromJson(Map<String, dynamic> json) {
    membershipId = json['membership_id'];
    cancelSubscription = json['cancel_subscription'];
    messageCanceled = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['membership_id'] = this.membershipId;
    data['cancel_subscription'] = this.cancelSubscription;
    data['message'] = this.messageCanceled;
    return data;
  }
}
