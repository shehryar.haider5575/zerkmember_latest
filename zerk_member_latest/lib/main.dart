import 'dart:core';

import 'package:Zerk/screens/CancllationHistory.dart';
import 'package:Zerk/screens/new_signup_screen.dart';
import 'package:Zerk/screens/scan_qr_code_screen.dart';
import 'package:Zerk/screens/splash_screen.dart';
import 'package:country_code_picker/country_localizations.dart';
import 'package:firebase_core/firebase_core.dart';
// import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:overlay_support/overlay_support.dart';

import 'screens/aceess_denied_qr_code_screen.dart';
import 'screens/aceess_granted_qr_code_screen.dart';
import 'screens/avails_History_Screen.dart';
import 'screens/bottom_menu.dart';
import 'screens/change_password_screen.dart';
import 'screens/facility_details.dart';
import 'screens/forgot_password_screen.dart';
import 'screens/login_screen.dart';
import 'screens/mapview_listing_screen.dart';
import 'screens/member_notification_screen.dart';
import 'screens/membership_screen.dart';
import 'screens/profile_management_screen.dart';
import 'screens/profile_viewdetails_screen.dart';
import 'screens/qr_scan_view.dart';
import 'screens/settings_screen.dart';
import 'screens/thankyou_login.dart';
import 'screens/transaction_history_screen.dart';

void main() async {
//  SharedPreferences.setMockInitialValues({});
  WidgetsFlutterBinding.ensureInitialized();
  // await Firebase.initializeApp();
  // FirebaseMessaging.onBackgroundMessage(_messageHandler);
  // FirebaseMessaging.onMessageOpenedApp;
  // FirebaseMessaging.onMessageOpenedApp(_messageHandler);
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return OverlaySupport(
      child: MaterialApp(
        supportedLocales: [
          Locale("af"),
          Locale("am"),
          Locale("ar"),
          Locale("az"),
          Locale("be"),
          Locale("bg"),
          Locale("bn"),
          Locale("bs"),
          Locale("ca"),
          Locale("cs"),
          Locale("da"),
          Locale("de"),
          Locale("el"),
          Locale("en"),
          Locale("es"),
          Locale("et"),
          Locale("fa"),
          Locale("fi"),
          Locale("fr"),
          Locale("gl"),
          Locale("ha"),
          Locale("he"),
          Locale("hi"),
          Locale("hr"),
          Locale("hu"),
          Locale("hy"),
          Locale("id"),
          Locale("is"),
          Locale("it"),
          Locale("ja"),
          Locale("ka"),
          Locale("kk"),
          Locale("km"),
          Locale("ko"),
          Locale("ku"),
          Locale("ky"),
          Locale("lt"),
          Locale("lv"),
          Locale("mk"),
          Locale("ml"),
          Locale("mn"),
          Locale("ms"),
          Locale("nb"),
          Locale("nl"),
          Locale("nn"),
          Locale("no"),
          Locale("pl"),
          Locale("ps"),
          Locale("pt"),
          Locale("ro"),
          Locale("ru"),
          Locale("sd"),
          Locale("sk"),
          Locale("sl"),
          Locale("so"),
          Locale("sq"),
          Locale("sr"),
          Locale("sv"),
          Locale("ta"),
          Locale("tg"),
          Locale("th"),
          Locale("tk"),
          Locale("tr"),
          Locale("tt"),
          Locale("uk"),
          Locale("ug"),
          Locale("ur"),
          Locale("uz"),
          Locale("vi"),
          Locale("zh"),
        ],
        localizationsDelegates: [
          CountryLocalizations.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          highlightColor: Colors.orange,
        ),
        initialRoute: '/',
        routes: {
          '/ScanQRCodeScreen': (context) => ScanQRCodeScreen(),
          // '/ScanQRCodeScreen': (context) => QRCodeForIOS(),
          '/AceessDenied': (context) => AceessDenied(),
          '/AceessGranted': (context) => AceessGranted(),
          '/BottomNavBar': (context) => BottomNavBar(),
          '/SignUpScreen': (context) => NewSignUpScreen(),
          '/LoginScreen': (context) => LoginScreen(),
          '/TransactionHistoryScreen': (context) => TransactionHistoryScreen(),
          '/ThankyouforLogin': (context) => ThankyouforLogin(),
          '/Settings': (context) => Settings(),
          '/ProfileViewDetails': (context) => ProfileViewDetails(),
          '/ProfileManagement': (context) => ProfileManagement(),
          '/Membership': (context) => Membership(),
          '/MapViewListing': (context) => MapViewListing(),
          '/ChangePasswordScreen': (context) => ChangePasswordScreen(),
          '/ForgotPasswordScreen': (context) => ForgotPasswordScreen(),
          '/QRViewExample': (context) => QRViewExample(),
          '/FacilityListDetails': (context) => FacilityListDetails(),
          '/AvailsHistoryScreen': (context) => AvailsHistoryScreen(),
          '/MemberNotificationScreen': (context) => MemberNotificationScreen(),
          '/CancellationHistory': (context) => CancellationHistory(),
        },
        // home: NewSignUpScreen(),
        home: SplashScreen(),
      ),
    );
  }
}
