import 'package:flutter/material.dart';
import 'package:passbase_flutter/passbase_flutter.dart';

void main() => runApp(PassbaseFlutterDemoApp());

class PassbaseFlutterDemoApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: PassbaseDemoHomePage(),
    );
  }
}

class PassbaseDemoHomePage extends StatefulWidget {
  PassbaseDemoHomePage({Key key}) : super(key: key);
  @override
  _PassbaseDemoHomePageState createState() {
    PassbaseSDK.initialize(
        publishableApiKey:
            "FP5xWQy10rjRM7n1ghzXzvaSrz8d1jlie3ccx2hqohdAt19fdH2mnMVokisdAZCv");
    PassbaseSDK.prefillUserEmail = "azeemkalwar51@gmail.com"; //optional
    return _PassbaseDemoHomePageState();
  }
}

class _PassbaseDemoHomePageState extends State<PassbaseDemoHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            PassbaseButton(
              onFinish: (identityAccessKey) {
                print('finished');
// do stuff in case of success
                print(identityAccessKey);
              },
              onSubmitted: (identityAccessKey) {
                print('submitted');
// do stuff in case of success
                print(identityAccessKey);
              },
              onError: (errorCode) {
// do stuff in case of cancel
                print(errorCode);
              },
              onStart: () {
// do stuff in case of start
                print("start");
              },
              width: 300,
              height: 70,
            ),
          ],
        ),
      ),
    );
  }
}
