import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:Zerk/model/model_Avails_History.dart';
import 'package:Zerk/utils/baseurl.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';
import 'package:localstorage/localstorage.dart';
// import 'package:zerk_app/model/model_Avails_History.dart';
// import 'package:zerk_app/utils/baseurl.dart';

class AvailsHistoryScreen extends StatefulWidget {
  AvailsHistoryScreen({Key key}) : super(key: key);

  @override
  _AvailsHistoryScreenState createState() => _AvailsHistoryScreenState();
}

class _AvailsHistoryScreenState extends State<AvailsHistoryScreen> {
  final LocalStorage storage = new LocalStorage('token');
  Baseurl baseurl = Baseurl();
  Future<AvailsLogs> futureAvailsLogs;
  String avatar = 'j7Y49Oel1tdp9TrfvlhBmNKNlzmT3GmiB1sz348G.jpeg';
  final ScrollController _scrollController = ScrollController();
  final GlobalKey<LiquidPullToRefreshState> _refreshIndicatorKey =
      GlobalKey<LiquidPullToRefreshState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  void initState() {
    super.initState();
    futureAvailsLogs = fetchAvailsLogs(storage.getItem('userid').toString());
    // avatar = storage.getItem('avatar');
  }

  Future<void> _handleRefresh() {
    final Completer<void> completer = Completer<void>();
    Timer(const Duration(seconds: 1), () {
      completer.complete();
    });
    setState(() {
      futureAvailsLogs = fetchAvailsLogs(storage.getItem('userid').toString());
      print("_handleRefresh............................");
      // refreshNum = new Random().nextInt(100);
    });
    return completer.future.then<void>((_) {
      _scaffoldKey.currentState?.showSnackBar(
        SnackBar(
          content: const Text('Refresh complete'),
          action: SnackBarAction(
              label: 'RETRY',
              onPressed: () {
                // _refreshIndicatorKey.currentState.show();
              }),
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Stack(
          children: [
            Positioned(
              //     top: Alignment.topCenter.y,
              top: Alignment.topCenter.x,
              left: Alignment.center.x,
              right: Alignment.center.x,
              child: Container(
                color: Colors.orange,
                height: MediaQuery.of(context).size.height * .20,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 25.0, bottom: 20.0, top: 10.0),
                      child: InkWell(
                        onTap: () {
                          Navigator.popAndPushNamed(context, '/BottomNavBar');
                        },
                        child: SizedBox(
                          child: Icon(
                            Icons.arrow_back,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 20.0, bottom: 20.0, top: 10.0),
                      child: Text(
                        "Member Access Logs",
                        style: TextStyle(
                          fontSize: 22.0,
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              //   top: 120,
              // top: Alignment.bottomCenter.x,

              bottom: Alignment.bottomCenter.x,
              // top: Alignment.center.x,
              child: Container(
                //    alignment: Alignment.center,
                width: MediaQuery.of(context).size.width,
                //    color: Colors.amber,
                height: MediaQuery.of(context).size.height * .85,
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 10.0, right: 10.0, bottom: 5.0),
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15),
                    ),
                    elevation: 20,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Scrollbar(
                        isAlwaysShown: true,

                        // thickness: 8,
                        controller: _scrollController,
                        child: LiquidPullToRefresh(
                          backgroundColor: Colors.orange[600],
                          color: Colors.transparent,
                          key: _refreshIndicatorKey,
                          onRefresh: _handleRefresh,
                          showChildOpacityTransition: false,
                          child: FutureBuilder<AvailsLogs>(
                            future: futureAvailsLogs,
                            builder: (context, snapshot) {
                              if (snapshot.hasData) {
                                if (snapshot.data.data.isEmpty) {
                                  return Center(
                                    child: Text(
                                      'No Access History',
                                      textScaleFactor: 1.2,
                                      textAlign: TextAlign.center,
                                      style:
                                          TextStyle(color: Colors.orange[800]),
                                    ),
                                  );
                                }
                                return ListView.builder(
                                    controller: _scrollController,
                                    itemCount: snapshot.data.data.length,
//                                    reverse: true,
                                    // separatorBuilder: (context, index) =>
                                    //     Divider(
                                    //       endIndent: 15,
                                    //       indent: 15,
                                    //       color: Colors.grey[400],
                                    //     ),
                                    itemBuilder: (context, index) {
                                      return Container(
                                        height: 90,
                                        margin: EdgeInsets.symmetric(
                                            vertical: 3.0, horizontal: 5.0),
                                        decoration: BoxDecoration(
                                          //color: appTheme.primaryColor,
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(10.0),
                                          ),
                                          // boxShadow: [
                                          //   BoxShadow(
                                          //     color: Colors.black12,
                                          //     blurRadius: 10.0,
                                          //     spreadRadius: 0.0,
                                          //     offset: Offset(0.0, 5.0),
                                          //   ),
                                          // ],
                                        ),
                                        child: FlatButton(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 10.0),
                                          //   rounded: 10.0,
                                          onPressed: () {
                                            print(snapshot.data.data[index]
                                                    .facility.firstName +
                                                "index---------------" +
                                                index.toString());
                                          },
                                          child: Row(
                                            children: <Widget>[
                                              ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(10.0),
                                                child: SizedBox(
                                                  height: 40,
                                                  width: 50,
                                                  child: Image.network(
                                                    'http://165.227.69.207/zerk/public/uploads/' +
                                                        avatar.toString(),
                                                  ),
                                                  //  Image.asset(
                                                  //     'assets/images/john_image.png'),
                                                ),
                                                // child: ImageAsAsset(
                                                //   image: 'item_image.png',
                                                //   width: 120.0,
                                                // ),
                                              ),
                                              SizedBox(
                                                width: 10.0,
                                              ),
                                              Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  //'John Smith'
                                                  Text(
                                                      snapshot
                                                          .data
                                                          .data[index]
                                                          .facility
                                                          .bussinessName,
                                                      style: TextStyle(
                                                        fontSize: 18.0,
                                                        fontWeight:
                                                            FontWeight.normal,
                                                      )
                                                      //  style: kCollectionItemName,
                                                      ),
                                                  Padding(
                                                    padding: EdgeInsets.only(
                                                        top: 10.0),
                                                  ),
                                                  Text(
                                                    snapshot
                                                            .data
                                                            .data[index]
                                                            .facility
                                                            .firstName +
                                                        " " +
                                                        snapshot
                                                            .data
                                                            .data[index]
                                                            .facility
                                                            .lastName,
                                                    //   '168 West Magnolia St. Outer\nNunavut, NU X0A 3Y7',
                                                    style: TextStyle(
                                                      fontSize: 14.0,
                                                      color: Colors.grey,
                                                      fontWeight:
                                                          FontWeight.normal,
                                                    ),
                                                    //style: kCollectionItemPrice,
                                                  ),
                                                  Padding(
                                                    padding: EdgeInsets.only(
                                                        top: 5.0),
                                                  ),
                                                  Text(
                                                    snapshot.data.data[index]
                                                        .createdAt,
                                                    //   '168 West Magnolia St. Outer\nNunavut, NU X0A 3Y7',
                                                    style: TextStyle(
                                                      fontSize: 14.0,
                                                      color: Colors.grey,
                                                      fontWeight:
                                                          FontWeight.normal,
                                                    ),
                                                    //style: kCollectionItemPrice,
                                                  ),
                                                ],
                                              )
                                            ],
                                          ),
                                        ),
                                      );
                                    });
                                // : Center(
                                //     child: Text("Member Avail Logs No Found"),
                                //     //  CircularProgressIndicator()
                                //   );
                                // Center(child: CircularProgressIndicator());
                              } else if (snapshot.hasError) {
                                return Text("${snapshot.error}");
                              }
                              return Center(child: showProgressLoader(context));
                            },
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget showProgressLoader(BuildContext context) {
    if (Platform.isAndroid) {
      return CircularProgressIndicator(color: Colors.orange[800]);
    }
    if (Platform.isIOS) {
      return CupertinoActivityIndicator();
    }
  }

  Future<AvailsLogs> fetchAvailsLogs(String id) async {
    final response = await http.get(
      Uri.parse(baseurl.baseurl + "member/avails-logs/$id"),
      headers: <String, String>{
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );

    if (response.statusCode == 200) {
      print(response.statusCode);
      print(response.body);
      return AvailsLogs.fromJson(json.decode(response.body));
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to Avails Logs');
    }
  }
}
