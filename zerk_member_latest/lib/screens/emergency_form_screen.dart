import 'dart:convert';

import 'package:Zerk/screens/bottom_menu.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:localstorage/localstorage.dart';

class EmergencyFormScreen extends StatefulWidget {
  @override
  _EmergencyFormScreenState createState() => _EmergencyFormScreenState();
}

class _EmergencyFormScreenState extends State<EmergencyFormScreen> {
  final GlobalKey<ScaffoldState> _key = GlobalKey<ScaffoldState>();
  final TextEditingController userName = TextEditingController();
  final TextEditingController relation = TextEditingController();
  final LocalStorage storage = new LocalStorage('token');
  final TextEditingController number = TextEditingController();
  String dialCode;
  bool isLoaderVisible = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/images/login_bg.png'),
              fit: BoxFit.cover,
              alignment: Alignment.topLeft
              //fit: BoxFit.contain
              //   alignment: Alignment.center
              ),
        ),
        child: SingleChildScrollView(
          child: Form(
            key: _key,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            child: Stack(
              clipBehavior: Clip.none,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: 80,
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 25.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: 60,
                                width: 60,
                                child: Image.asset(
                                    'assets/images/sign_up_icon.png'),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(
                          height: 25,
                        ),
                        Text(
                          "Emergency Contact \nForm",
                          textAlign: TextAlign.start,
                          maxLines: 2,
                          overflow: TextOverflow.visible,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 26.0,
                              fontWeight: FontWeight.bold,
                              fontFamily: "Poppins-Bold",
                              letterSpacing: 4),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 25.0, right: 25.0),
                      child: Column(
                        children: [
                          primaryTextField(
                            controller: userName,
                            hintText: 'Full Name',
                            onFieldSubmitted: (val) {},
                            textInputAction: TextInputAction.next,
                            validator: validateFullName,
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 8.0),
                          ),
                          primaryTextField(
                            controller: relation,
                            hintText: 'Relation',
                            onFieldSubmitted: (val) {},
                            textInputAction: TextInputAction.next,
                            validator: validateRelation,
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 8.0),
                          ),
                          Stack(
                            clipBehavior: Clip.none,
                            children: [
                              Align(
                                alignment: Alignment.topRight,
                                child: Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.75,
                                  child: primaryTextField(
                                      validator: validateMobile,
                                      onFieldSubmitted: (va) {
                                        validateOnSubmit();
                                      },
                                      inputLength: 10,
                                      controller: number,
                                      textInputAction: TextInputAction.done),
                                ),
                              ),
                              Positioned(
                                  left: -25,
                                  top: 0,
                                  child: CountryCodePicker(
                                      onChanged: (code) {
                                        dialCode = code.dialCode;
                                        print(
                                            "-------  ${code.name} ${code.dialCode} ${code.name}");
                                      },
                                      hideMainText: true,
                                      showFlagMain: true,
                                      showFlag: true,
                                      initialSelection: 'PK',
                                      hideSearch: false,
                                      showCountryOnly: true,
                                      showOnlyCountryWhenClosed: false,
                                      alignLeft: true,
                                      flagWidth: 30,
                                      onInit: (code) => dialCode = code.dialCode
                                      // print(
                                      //     "on init ${code.name} ${code.dialCode} ${code.name}"),
                                      )),
                            ],
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 40,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 30, top: 30),
                      child: Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            SizedBox(
                              width: 220.0,
                              height: 60.0,
                              child: RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(42.0),
                                    side: BorderSide(color: Colors.white)),
                                onPressed: () {
                                  validateOnSubmit();
                                },
                                color: Colors.white,
                                textColor: Colors.orange,
                                child: Text("SUBMIT".toUpperCase(),
                                    style: TextStyle(
                                      fontSize: 25.0,
                                      letterSpacing: 2,
                                      fontWeight: FontWeight.bold,
                                    )),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                Positioned(
                  top: MediaQuery.of(context).size.height * 0.5,
                  left: MediaQuery.of(context).size.width * 0.4,
                  child: Visibility(
                    visible: isLoaderVisible,
                    child: CircularProgressIndicator(
                      color: Colors.white,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  primaryTextField(
      {TextEditingController controller,
      Function(String) onFieldSubmitted,
      String hintText,
      int inputLength = 50,
      TextInputAction textInputAction,
      Function(String) validator}) {
    return TextFormField(
      controller: controller,
      autofocus: false,
      keyboardType: TextInputType.text,
      onFieldSubmitted: onFieldSubmitted,
      textInputAction: textInputAction,
      obscuringCharacter: '*',
      autovalidateMode: AutovalidateMode.always,
      onChanged: (val) {},
      validator: validator,
      inputFormatters: [
        new LengthLimitingTextInputFormatter(inputLength),
      ],
      style: TextStyle(color: Colors.white, fontSize: 19),
      decoration: InputDecoration(
          hintText: hintText,
          errorStyle: TextStyle(color: Colors.red[800]),
          hintStyle: TextStyle(color: Colors.white70, fontSize: 17),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          errorBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.orange[800]))),
    );
  }

  String validateFullName(String value) {
    String patttern = r'(^[a-zA-Z ]*$)';
    RegExp regExp = new RegExp(patttern);
    if (value.length == 0) {
      return "Last Name is Required";
    } else if (!regExp.hasMatch(value)) {
      return "Last Name must be a-z and A-Z";
    }
    return null;
  }

  String validateRelation(String value) {
    String patttern = r'(^[a-zA-Z ]*$)';
    RegExp regExp = new RegExp(patttern);
    if (value.length == 0) {
      return "Relation Name is Required";
    } else if (!regExp.hasMatch(value)) {
      return "Last Name must be a-z and A-Z";
    }
    return null;
  }

  String validateMobile(String value) {
    String pattern = r'(^(?:[+0]9)?[0-9]{10,12}$)';
    RegExp regExp = new RegExp(pattern);
    if (value.length == 0) {
      return 'Please enter mobile number';
    } else if (!regExp.hasMatch(value)) {
      return 'Please enter valid mobile number';
    }
    return null;
  }

  postEmergencyContact(
      [String name,
      String phone,
      String relationName,
      String customerId]) async {
    var response = await http.post(
        Uri.parse("http://zerk.com.au/panel/public/api/emergency-contacts"),
        headers: {
          'accept': 'application/json'
        },
        body: {
          "name": name,
          "phone": phone,
          "relation": relationName,
          "customer_id": customerId
        });
    setState(() {
      isLoaderVisible = false;
    });
    if (response.statusCode == 200) {
      // print(response.body);
      navigateTo();
      // Navigator.of(context).pop();
      emptyAllFields();
    } else {
      // print(response.body);
    }
    var jsonResponse = jsonDecode(response.body);
    var msg = jsonResponse['message'];
    Fluttertoast.showToast(msg: msg);
  }

  navigateTo() {
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => BottomNavBar()),
        ModalRoute.withName("/BottomNavBar"));
  }

  emptyAllFields() {
    userName.text = '';
    number.text = '';
    relation.text = '';
  }

  validateOnSubmit() async {
    if (userName.text.isEmpty) {
      Fluttertoast.showToast(msg: 'Full Name is Required');
    } else if (relation.text.isEmpty) {
      Fluttertoast.showToast(msg: 'Relation Name is Required');
    } else if (number.text.isEmpty) {
      Fluttertoast.showToast(msg: 'Number is Required');
    } else {
      //TODO implement submit form here
      String userId = storage.getItem('userid');
      // print('this is userId : $userId');
      setState(() {
        isLoaderVisible = true;
      });
      postEmergencyContact(userName.text, number.text, relation.text, userId);
      // Navigator.of(context)
      //     .push(MaterialPageRoute(builder: (context) => ProfileMenu()));
    }
  }
}
