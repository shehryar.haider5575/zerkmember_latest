import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:Zerk/model/model_UserFacility.dart';
import 'package:Zerk/utils/baseurl.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';
import 'package:shared_preferences/shared_preferences.dart';

// import 'package:zerk_app/model/model_UserFacility.dart';
// import 'package:zerk_app/utils/baseurl.dart';

import 'bottom_menu.dart';
import 'profile_viewdetails_screen.dart';

class MapViewListing extends StatefulWidget {
  @override
  _MapViewListingState createState() => _MapViewListingState();
}

class _MapViewListingState extends State<MapViewListing> {
  Baseurl baseUrl = Baseurl();
  Future<UserFacility> futureUserFacilityData;
  final ScrollController _scrollController = ScrollController();
  final GlobalKey<LiquidPullToRefreshState> _refreshIndicatorKey =
      GlobalKey<LiquidPullToRefreshState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final TextEditingController _search = TextEditingController();
  bool isSuggestionVisible = false;
  bool isCatFacilityVisible = false;
  bool isClearIconVisible = false;
  String catHint = 'Select Category';
  bool isSearchFocused = false;
  List<dynamic> catSuggestionList = [];
  int catId;
  @override
  void initState() {
    super.initState();
    futureUserFacilityData = fetchFacility();
    getCatMap();
  }

  Future<List<dynamic>> getCatMap() async {
    final response = await http
        .get(Uri.parse('http://zerk.com.au/panel/public/api/categories'));
    // print('this is get map cat list ${response.body}');
    final Map<String, dynamic> jsonResponse = jsonDecode(response.body);
    // print('this is converted response ${jsonResponse['data']}');
    // jsonResponse.forEach((key, value) { })
    catSuggestionList = jsonResponse['data'];
    print('this is list $catSuggestionList');
    return jsonResponse['data'];
  }

  Future<void> _handleRefresh() {
    final Completer<void> completer = Completer<void>();
    Timer(const Duration(seconds: 1), () {
      completer.complete();
    });
    setState(() {
      futureUserFacilityData = fetchFacility();
      print("_handleRefresh....");
    });
    return completer.future.then<void>((_) {
      _scaffoldKey.currentState?.showSnackBar(
        SnackBar(
          content: const Text('Refresh complete'),
          action: SnackBarAction(
              label: 'RETRY',
              onPressed: () {
                // _refreshIndicatorKey.currentState.show();
              }),
        ),
      );
    });
  }

  Widget showProgressLoader(BuildContext context) {
    if (Platform.isAndroid) {
      return CircularProgressIndicator(
        color: Colors.orange[800],
      );
    }
    if (Platform.isIOS) {
      return CupertinoActivityIndicator();
    }
  }

  bool facilityFound() {
    bool isFound = false;
    catSuggestionList.forEach((element) {
      if (element['name']
          .toString()
          .toLowerCase()
          .contains(_search.text.toLowerCase())) {
        isFound = true;
      }
    });
    return isFound;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // body: getBody(),
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        leading: InkWell(
          onTap: () {
            Navigator.popAndPushNamed(context, '/BottomNavBar');
          },
          child: SizedBox(
            child: Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
          ),
        ),
        backgroundColor: Colors.orange,
        centerTitle: false,
        title: Text(
          "View Listing",
          style: TextStyle(
            fontSize: 20.0,
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: WillPopScope(
          child: Stack(
            clipBehavior: Clip.none,
            children: [
              Column(
                children: [
                  const SizedBox(
                    height: 12,
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        isSuggestionVisible = !isSuggestionVisible;
                      });
                    },
                    child: AbsorbPointer(
                      absorbing: true,
                      child: Stack(
                        clipBehavior: Clip.none,
                        children: [
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 15.0),
                            child: TextFormField(
                              autocorrect: true,
                              controller: _search,
                              autofocus: isSearchFocused,
                              keyboardType: TextInputType.text,
                              onFieldSubmitted: (val) {},
                              textInputAction: TextInputAction.done,
                              // obscuringCharacter: '*',
                              autovalidateMode: AutovalidateMode.always,
                              onChanged: (val) {
                                setState(() {
                                  isSuggestionVisible =
                                      val.isEmpty ? false : true;
                                  isClearIconVisible =
                                      val.isEmpty ? false : true;
                                  if (val.isEmpty) {
                                    isCatFacilityVisible = false;
                                  }
                                });
                              },
                              style:
                                  TextStyle(color: Colors.orange, fontSize: 19),
                              decoration: InputDecoration(
                                  enabled: false,
                                  contentPadding: const EdgeInsets.symmetric(
                                      horizontal: 20),
                                  hintText: catHint,
                                  errorStyle: TextStyle(color: Colors.red),
                                  hintStyle: TextStyle(
                                      color: Colors.orange, fontSize: 17),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(30),
                                    borderSide:
                                        BorderSide(color: Colors.orange),
                                  ),
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(30),
                                    borderSide:
                                        BorderSide(color: Colors.orange),
                                  ),
                                  disabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(30),
                                    borderSide:
                                        BorderSide(color: Colors.black12),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(30),
                                    borderSide:
                                        BorderSide(color: Colors.orange),
                                  ),
                                  errorBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(30),
                                      borderSide:
                                          BorderSide(color: Colors.orange))),
                            ),
                          ),
                          Positioned(
                              top: 12,
                              right: 35,
                              child: Icon(
                                Icons.arrow_drop_down_circle,
                                color: Colors.orange,
                                size: 26,
                              )),
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 6,
                  ),
                  Expanded(
                    child: Container(
                      //    alignment: Alignment.center,
                      width: MediaQuery.of(context).size.width,
                      //    color: Colors.amber,
                      height: MediaQuery.of(context).size.height * .78,
                      child: Padding(
                        padding: const EdgeInsets.only(
                            left: 10.0, right: 10.0, bottom: 5.0),
                        child: Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15),
                          ),
                          elevation: 20,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Scrollbar(
                              isAlwaysShown: true,
                              // thickness: 8,
                              controller: _scrollController,
                              child: isCatFacilityVisible
                                  ? LiquidPullToRefresh(
                                      backgroundColor: Colors.orange[600],
                                      color: Colors.transparent,
                                      key: _refreshIndicatorKey,
                                      onRefresh: _handleRefresh,
                                      showChildOpacityTransition: false,
                                      child: FutureBuilder<UserFacility>(
                                        future: fetchCatWiseData(catId),
                                        builder: (context, snapshot) {
                                          if (snapshot.hasData) {
                                            if (snapshot.data.data.isEmpty) {
                                              return Container(
                                                color: Colors.transparent,
                                                child: Center(
                                                  child: Text(
                                                    'No Facility Is Available',
                                                    style: TextStyle(
                                                        color: Colors.orange,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 18.0),
                                                  ),
                                                ),
                                              );
                                            }
                                            return ListView.separated(
                                                controller: _scrollController,
                                                itemCount:
                                                    snapshot.data.data.length,
                                                separatorBuilder: (context,
                                                        index) =>
                                                    Divider(
                                                      endIndent: 15,
                                                      indent: 15,
                                                      color: Colors.grey[400],
                                                    ),
                                                itemBuilder: (context, index) {
                                                  return Container(
                                                    // height: 60,
                                                    height:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .height /
                                                            14,
                                                    margin:
                                                        EdgeInsets.symmetric(
                                                            vertical: 3.0,
                                                            horizontal: 5.0),
                                                    decoration: BoxDecoration(
                                                      //color: appTheme.primaryColor,
                                                      borderRadius:
                                                          BorderRadius.all(
                                                        Radius.circular(10.0),
                                                      ),
                                                    ),
                                                    child: FlatButton(
                                                      padding:
                                                          EdgeInsets.symmetric(
                                                              horizontal: 10.0),
                                                      //   rounded: 10.0,
                                                      onPressed: () {
                                                        Navigator.push(
                                                          context,
                                                          MaterialPageRoute(
                                                              fullscreenDialog:
                                                                  true,
                                                              builder: (context) =>
                                                                  ProfileViewDetails(
                                                                    facilityId: snapshot
                                                                        .data
                                                                        .data[
                                                                            index]
                                                                        .id
                                                                        .toString(),
                                                                    bussinessName: snapshot
                                                                        .data
                                                                        .data[
                                                                            index]
                                                                        .bussinessName,
                                                                    address: snapshot
                                                                        .data
                                                                        .data[
                                                                            index]
                                                                        .address,
                                                                    firstName: snapshot
                                                                        .data
                                                                        .data[
                                                                            index]
                                                                        .firstName,
                                                                  )),
                                                        );

                                                        // Navigator.push(
                                                        //   context,
                                                        //   MaterialPageRoute(
                                                        //     builder: (context) =>
                                                        //         FacilityListDetails(
                                                        //       bussiness_name: snapshot.data
                                                        //           .data[index].bussinessName,
                                                        //       first_name: snapshot
                                                        //           .data.data[index].firstName,
                                                        //       last_name: snapshot
                                                        //           .data.data[index].lastName
                                                        //           .toString(),
                                                        //       email: snapshot
                                                        //           .data.data[index].email
                                                        //           .toString(),
                                                        //       avatar: snapshot
                                                        //           .data.data[index].avatar
                                                        //           .toString(),
                                                        //       address: snapshot
                                                        //           .data.data[index].address
                                                        //           .toString(),
                                                        //       phone: snapshot
                                                        //           .data.data[index].phone
                                                        //           .toString(),
                                                        //       img_url: snapshot.data.imageUrl
                                                        //           .toString(),
                                                        //     ),
                                                        //   ),
                                                        // );
                                                        // Navigator.pushNamed(
                                                        //     context, '/FacilityListDetails');
                                                      },
                                                      child: Row(
                                                        children: <Widget>[
                                                          ClipRRect(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        10.0),
                                                            child: SizedBox(
                                                              height: 30,
                                                              width: 50,
                                                              child: snapshot
                                                                          .data
                                                                          .data[
                                                                              index]
                                                                          .avatar ==
                                                                      'null'
                                                                  ? Image
                                                                      .network(
                                                                      snapshot.data
                                                                              .imageUrl +
                                                                          snapshot
                                                                              .data
                                                                              .data[index]
                                                                              .avatar
                                                                              .toString(),
                                                                    )
                                                                  : Image
                                                                      .network(
                                                                      'https://leadagency.club/wp-content/uploads/2021/09/placeholder-660.png',
                                                                    ),
                                                              // child: Image.asset(
                                                              //     'assets/images/john_image.png'),
                                                            ),
                                                            // child: ImageAsAsset(
                                                            //   image: 'item_image.png',
                                                            //   width: 120.0,
                                                            // ),
                                                          ),
                                                          SizedBox(
                                                            width: 10.0,
                                                          ),
                                                          Column(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .center,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: <Widget>[
                                                              //'John Smith'
                                                              Text(
                                                                  snapshot
                                                                          .data
                                                                          .data[
                                                                              index]
                                                                          .bussinessName ??
                                                                      '',
                                                                  style:
                                                                      TextStyle(
                                                                    fontSize:
                                                                        18.0,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                  )
                                                                  //  style: kCollectionItemName,
                                                                  ),
                                                              Padding(
                                                                padding: EdgeInsets
                                                                    .only(
                                                                        top:
                                                                            2.0),
                                                              ),
                                                              Expanded(
                                                                  child:
                                                                      Container(
//                                                   color: Colors.red,
                                                                width: MediaQuery.of(
                                                                            context)
                                                                        .size
                                                                        .width *
                                                                    .6,
                                                                child: Text(
                                                                  snapshot
                                                                          .data
                                                                          .data[
                                                                              index]
                                                                          .address ??
                                                                      '',
                                                                  //   '168 West Magnolia St. Outer\nNunavut, NU X0A 3Y7',
                                                                  style:
                                                                      TextStyle(
                                                                    fontSize:
                                                                        12.0,
                                                                    color: Colors
                                                                        .grey,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .normal,
                                                                  ),
                                                                  //style: kCollectionItemPrice,
                                                                ),
                                                              )),
                                                            ],
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                  );
                                                });
                                          } else if (snapshot.hasError) {
                                            return Text("${snapshot.error}");
                                          }

                                          // By default, show a loading spinner.
                                          return Center(
                                              child:
                                                  showProgressLoader(context));
                                        },
                                      ),
                                    )
                                  : LiquidPullToRefresh(
                                      backgroundColor: Colors.orange[600],
                                      color: Colors.transparent,
                                      key: _refreshIndicatorKey,
                                      onRefresh: _handleRefresh,
                                      showChildOpacityTransition: false,
                                      child: FutureBuilder<UserFacility>(
                                        future: futureUserFacilityData,
                                        builder: (context, snapshot) {
                                          if (snapshot.hasData) {
                                            if (snapshot.data.data.isEmpty) {
                                              return Container(
                                                color: Colors.transparent,
                                                child: Center(
                                                  child: Text(
                                                    'No Facility Is Avaliable',
                                                    style: TextStyle(
                                                        color: Colors.orange,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 18.0),
                                                  ),
                                                ),
                                              );
                                            }
                                            return ListView.separated(
                                                controller: _scrollController,
                                                itemCount:
                                                    snapshot.data.data.length,
                                                separatorBuilder: (context,
                                                        index) =>
                                                    Divider(
                                                      endIndent: 15,
                                                      indent: 15,
                                                      color: Colors.grey[400],
                                                    ),
                                                itemBuilder: (context, index) {
                                                  return Container(
                                                    // height: 60,
                                                    height:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .height /
                                                            14,
                                                    margin:
                                                        EdgeInsets.symmetric(
                                                            vertical: 3.0,
                                                            horizontal: 5.0),
                                                    decoration: BoxDecoration(
                                                      //color: appTheme.primaryColor,
                                                      borderRadius:
                                                          BorderRadius.all(
                                                        Radius.circular(10.0),
                                                      ),
                                                    ),
                                                    child: FlatButton(
                                                      padding:
                                                          EdgeInsets.symmetric(
                                                              horizontal: 10.0),
                                                      //   rounded: 10.0,
                                                      onPressed: () {
                                                        Navigator.push(
                                                          context,
                                                          MaterialPageRoute(
                                                              fullscreenDialog:
                                                                  true,
                                                              builder: (context) =>
                                                                  ProfileViewDetails(
                                                                    facilityId: snapshot
                                                                        .data
                                                                        .data[
                                                                            index]
                                                                        .id
                                                                        .toString(),
                                                                    bussinessName: snapshot
                                                                        .data
                                                                        .data[
                                                                            index]
                                                                        .bussinessName,
                                                                    address: snapshot
                                                                        .data
                                                                        .data[
                                                                            index]
                                                                        .address,
                                                                    firstName: snapshot
                                                                        .data
                                                                        .data[
                                                                            index]
                                                                        .firstName,
                                                                  )),
                                                        );

                                                        // Navigator.push(
                                                        //   context,
                                                        //   MaterialPageRoute(
                                                        //     builder: (context) =>
                                                        //         FacilityListDetails(
                                                        //       bussiness_name: snapshot.data
                                                        //           .data[index].bussinessName,
                                                        //       first_name: snapshot
                                                        //           .data.data[index].firstName,
                                                        //       last_name: snapshot
                                                        //           .data.data[index].lastName
                                                        //           .toString(),
                                                        //       email: snapshot
                                                        //           .data.data[index].email
                                                        //           .toString(),
                                                        //       avatar: snapshot
                                                        //           .data.data[index].avatar
                                                        //           .toString(),
                                                        //       address: snapshot
                                                        //           .data.data[index].address
                                                        //           .toString(),
                                                        //       phone: snapshot
                                                        //           .data.data[index].phone
                                                        //           .toString(),
                                                        //       img_url: snapshot.data.imageUrl
                                                        //           .toString(),
                                                        //     ),
                                                        //   ),
                                                        // );
                                                        // Navigator.pushNamed(
                                                        //     context, '/FacilityListDetails');
                                                      },
                                                      child: Row(
                                                        children: <Widget>[
                                                          ClipRRect(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        10.0),
                                                            child: SizedBox(
                                                              height: 30,
                                                              width: 50,
                                                              child: snapshot
                                                                          .data
                                                                          .data[
                                                                              index]
                                                                          .avatar ==
                                                                      'null'
                                                                  ? Image
                                                                      .network(
                                                                      snapshot.data
                                                                              .imageUrl +
                                                                          snapshot
                                                                              .data
                                                                              .data[index]
                                                                              .avatar
                                                                              .toString(),
                                                                    )
                                                                  : Image
                                                                      .network(
                                                                      'https://leadagency.club/wp-content/uploads/2021/09/placeholder-660.png',
                                                                    ),
                                                              // child: Image.asset(
                                                              //     'assets/images/john_image.png'),
                                                            ),
                                                            // child: ImageAsAsset(
                                                            //   image: 'item_image.png',
                                                            //   width: 120.0,
                                                            // ),
                                                          ),
                                                          SizedBox(
                                                            width: 10.0,
                                                          ),
                                                          Column(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .center,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: <Widget>[
                                                              //'John Smith'
                                                              Text(
                                                                  snapshot
                                                                          .data
                                                                          .data[
                                                                              index]
                                                                          .bussinessName ??
                                                                      '',
                                                                  style:
                                                                      TextStyle(
                                                                    fontSize:
                                                                        18.0,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                  )
                                                                  //  style: kCollectionItemName,
                                                                  ),
                                                              Padding(
                                                                padding: EdgeInsets
                                                                    .only(
                                                                        top:
                                                                            2.0),
                                                              ),
                                                              Expanded(
                                                                  child:
                                                                      Container(
//                                                   color: Colors.red,
                                                                width: MediaQuery.of(
                                                                            context)
                                                                        .size
                                                                        .width *
                                                                    .6,
                                                                child: Text(
                                                                  snapshot
                                                                          .data
                                                                          .data[
                                                                              index]
                                                                          .address ??
                                                                      '',
                                                                  //   '168 West Magnolia St. Outer\nNunavut, NU X0A 3Y7',
                                                                  style:
                                                                      TextStyle(
                                                                    fontSize:
                                                                        12.0,
                                                                    color: Colors
                                                                        .grey,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .normal,
                                                                  ),
                                                                  //style: kCollectionItemPrice,
                                                                ),
                                                              )),
                                                            ],
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                  );
                                                });
                                          } else if (snapshot.hasError) {
                                            return Text("${snapshot.error}");
                                          }

                                          // By default, show a loading spinner.
                                          return Center(
                                              child:
                                                  showProgressLoader(context));
                                        },
                                      ),
                                    ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Visibility(
                visible: isSuggestionVisible,
                child: Positioned(
                  top: 60,
                  left: 12,
                  right: 12,
                  child: Container(
                    height: 180,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black12,
                            spreadRadius: 3,
                            blurRadius: 3,
                            offset: const Offset(3, 3),
                          )
                        ]),
                    width: MediaQuery.of(context).size.width,
                    child: Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: facilityFound()
                          ? ListView.separated(
                              itemBuilder: (context, index) {
                                return InkWell(
                                  onTap: () {
                                    print(
                                        'THIS IS CAT HIT : ${catSuggestionList[index]['name']}');
                                    print('THIS IS CAT ID OLD : $catId');
                                    setState(() {
                                      //update value of heading
                                      catHint =
                                          catSuggestionList[index]['name'];
                                      isCatFacilityVisible = true;
                                      catId = catSuggestionList[index]['id'];
                                      isSuggestionVisible = false;
                                      isClearIconVisible = true;
                                      isSearchFocused = false;
                                    });
                                    print('THIS IS CAT ID UPDATED : $catId');
                                  },
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 13.0, horizontal: 20),
                                    child: Text(
                                      catSuggestionList[index]['name'],
                                      textAlign: TextAlign.start,
                                      style: TextStyle(color: Colors.black),
                                    ),
                                  ),
                                );
                              },
                              separatorBuilder: (context, index) {
                                return Container(
                                  height: 1,
                                  width: 22,
                                  color: Colors.black45,
                                );
                              },
                              itemCount: catSuggestionList.length)
                          : Center(
                              child: Text(
                              'Not Found',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.normal,
                                  fontSize: 18),
                            )),
                    ),
                  ),
                ),
              )
            ],
          ),
          onWillPop: onWillPop),
    );
  }

  Future<UserFacility> fetchFacility() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String idFromLoginScreen = sharedPreferences.getString('id');
    print('THIS IS ID $idFromLoginScreen');
    final response = await http.get(
      Uri.parse(baseUrl.baseurl + "admin/facility/all/$idFromLoginScreen"),
      headers: <String, String>{
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );
    if (response.statusCode == 200) {
      print(response.statusCode);
      print('************* THIS IS FACILITY ALL DATA ******************');
      print('************* THIS IS FACILITY ALL DATA ******************');

      print(response.body);
      print('************* THIS IS FACILITY ALL DATA ******************');
      print('************* THIS IS FACILITY ALL DATA ******************');

      // If the server did return a 200 OK response,
      // then parse the JSON.
      return UserFacility.fromJson(json.decode(response.body));
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load fetch Facility');
    }
  }

  Future<UserFacility> fetchCatWiseData(catId) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String idFromLoginScreen = sharedPreferences.getString('id');
    print('THIS IS ID $idFromLoginScreen');
    final response = await http.get(
      Uri.parse(baseUrl.baseurl + "admin/facility/search/$catId"),
      headers: <String, String>{
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );
    if (response.statusCode == 200) {
      print(response.statusCode);
      print('************* THIS IS FACILITY ALL DATA ******************');
      print('************* THIS IS FACILITY ALL DATA ******************');

      print(response.body);
      print('************* THIS IS FACILITY ALL DATA ******************');
      print('************* THIS IS FACILITY ALL DATA ******************');

      // If the server did return a 200 OK response,
      // then parse the JSON.
      return UserFacility.fromJson(json.decode(response.body));
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load fetch Facility');
    }
  }

  Future<bool> onWillPop() {
    return Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => BottomNavBar()),
    );
    // return Navigator.pushNamed(context, '/BottomNavBar');
    // SystemNavigator.pop();
  }
}
