import 'dart:convert';
import 'dart:io';

import 'package:Zerk/model/PushNotificationModel.dart';
import 'package:Zerk/model/model_InvaildUserProfileUpdated.dart';
import 'package:Zerk/model/model_NotificationVideoUrl.dart';
import 'package:Zerk/model/model_StripeCustomersCreate.dart';
import 'package:Zerk/model/model_userInvaildSignup.dart';
import 'package:Zerk/model/model_userProfileUpdated.dart';
import 'package:Zerk/model/model_userSignup.dart';
import 'package:Zerk/utils/Utils.dart';
import 'package:Zerk/utils/baseurl.dart';
import 'package:Zerk/utils/dioExceptions.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:dio/dio.dart';
import 'package:fcm_config/fcm_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:localstorage/localstorage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

class NewSignUpScreen extends StatefulWidget {
  const NewSignUpScreen({Key key}) : super(key: key);

  @override
  _NewSignUpScreenState createState() => _NewSignUpScreenState();
}

class _NewSignUpScreenState extends State<NewSignUpScreen> {
  Baseurl baseurl = Baseurl();

  Future<File> imageFile;
  File imageFiles;
  File licenseImage;
  Image imageFromPreferences;
  bool _validate = false;

  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();
  TextEditingController dateController = TextEditingController();
  TextEditingController phoneNumberController = TextEditingController();

  String dialCode;

  Color primaryGreen = Color(0xffFF34E33A);
  int currentIndex = 0;
  bool visible = false;
  bool isChecked = false;
  DateFormat dateFormat = DateFormat('dd-MM-yyyy');
  GlobalKey<FormState> _key = new GlobalKey();
  bool isLoaderVisible = false;
  final PageController pageController = PageController(initialPage: 0);

  final FirebaseMessaging firebaseMessaging = FirebaseMessaging.instance;

  final LocalStorage storage = new LocalStorage('token');
  DateTime selectedDate = DateTime.now();
  List<UserSignup> userSignup;
  // UserSignupInvaild
  List<UserInvaildSignup> userInvaildSignup;
  Future<UserSignup> _futureUserSignup;
  String deviceToken = "Waiting for token...";

  @override
  void initState() {
    super.initState();
    userSignup = List<UserSignup>();
    print('[INFO] THIS IS SIGNUP INIT');

    firebaseMessaging.getToken().then((token) {
      print(token);
      if (token != null) {
        setState(() {
          deviceToken = token;
        });
      }
    });
    FirebaseMessaging.instance.getInitialMessage();

    print('------32452345------' + deviceToken);

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      // print("message recieved");
      // print('*******');
      PushNotification notification = PushNotification(
        title: message.notification?.title,
        body: message.notification?.body,
        dataTitle: message.data['title'],
        dataBody: message.data['body'],
      );

      print('on app opened notification $notification');

      var paredJson = jsonDecode(message.notification.body);
      var success = NotificationVideoUrl.fromJson(paredJson);
      _showDialog('Thank you for becoming a member', "");
//       // print(paredJson);
//       facilityID = success.facility.toString();
// //       // print('**********');
// //       // print(facilityID);
// //       // print('***********');
//       videoUrl = success.videoUrl;
// //       // print('*********');
// //       // print('vedio url' + videoUrl);
//       if (videoUrl != null) {
//         Navigator.push(
//           context,
//           MaterialPageRoute(
//             builder: (context) => VideoItems(
//               userid: userId,
//               facilityID: facilityID,
//               videoUrl: videoUrl,
//             ),
//           ),
//         );
//       } else {}
//       if (success.message == 'Access Granted Successfully') {
//         _showNotificationWithoutSound('Access Granted Successfully');
//         _showNotificationWithoutSound("The access has been granted");
//       }
//
//       firebaseMessaging.getToken().then((token) {
//         print(token);
//         if (token != null) {
//           setState(() {
//             deviceToken = token;
//           });
//         }
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage remoteMessage) {
      PushNotification notification = PushNotification(
        title: remoteMessage.notification?.title,
        body: remoteMessage.notification?.body,
        dataTitle: remoteMessage.data['title'],
        dataBody: remoteMessage.data['body'],
      );
      // print('lakjsdf');
      print('on app opened notification $notification');
    });

//
//       // grantedprint('????????????');
//       if (Platform.isIOS) {
//         message = _handleNotification(message);
//       }
//       // _showDialog(string.toString(), true);
// //          _showItemDialog(message);
// //       },
//       // onLaunch: (Map<String, dynamic> message) async {
//       //   print("onLaunch: $message");
//       //   if (Platform.isIOS) {
//       //     message = _handleNotification(message);
//       //   }
//       //   String string = message['notification']['body'];
//       //   _showDialog(string.toString(), true);
//       //   // _navigateToItemDetail(message);
//       //   myBackgroundMessageHandler(message);
//       // },
//       // onResume: (Map<String, dynamic> message) async {
//       //   print("onResume: $message");
//       //   if (Platform.isIOS) {
//       //     message = _handleNotification(message);
//       //   }
//       //   String string = message['notification']['body'];
//       //   _showDialog(string.toString(), true);
//       //   myBackgroundMessageHandler(message);
//       // },
//       // );
//       //   print(message.notification.body);
//     });

//     _firebaseMessaging.configure(
//       onMessage: (Map<String, dynamic> message) async {
//         print("onMessage: $message");
// //        _showItemDialog(message);
//       },
//       onLaunch: (Map<String, dynamic> message) async {
//         print("onLaunch: $message");
// //        _navigateToItemDetail(message);
//       },
//       onResume: (Map<String, dynamic> message) async {
//         print("onResume: $message");
// //        _navigateToItemDetail(message);
//       },
//     );
    // _firebaseMessaging.requestNotificationPermissions(
    //     const IosNotificationSettings(
    //         sound: true, badge: true, alert: true, provisional: true));
    // _firebaseMessaging.onIosSettingsRegistered
    //     .listen((IosNotificationSettings settings) {
    //   print("Settings registered: $settings");
    // });
    // _firebaseMessaging.getToken().then((String token) {
    //   assert(token != null);
    //   setState(() {
    //     deviceToken = token;
    //   });
    //   print('SignUpScreen : ' + deviceToken);
    // });

    if (imageFromPreferences != null) {
      print('imageFromPreferences------------------------IF');
      loadImageFromPreferences();
      // _showDialog(true);
    } else {
      loadImageFromPreferences();
      print('imageFromPreferences------------------------ELSE');
      // _showErrorDialog('false', true);
    }
  }

  submitForm() {
    if (passwordController.text != confirmPasswordController.text) {
      print(passwordController.text +
          " NotMatch " +
          confirmPasswordController.text);
      Fluttertoast.showToast(
          textColor: Colors.black,
          backgroundColor: Colors.white,
          msg: 'The password confirmation does not match');
      // _showErrorDialog("error", "The password confirmation does not match");
    } else if (isChecked == false) {
      Fluttertoast.showToast(
          textColor: Colors.black,
          backgroundColor: Colors.white,
          msg: 'Please accept terms and conditions');
      // Scaffold.of(context).showBottomSheet(
      //     (context) => Text('Please accept terms and conditions'));
      // _showErrorDialog("error", "Please accept terms and conditions");
    } else {
      print(passwordController.text + "Match" + confirmPasswordController.text);
      // if (licenseImage == null) {
      //   _showDialog('Identification Failed', 'Please select a valid license');
      // }
      // else {
      // loadProgress(true);
      _sendToServer();
      setState(() {
        isLoaderVisible = true;
      });
      // }
    }
  }

  // onSubmitted(value){
  Color primaryBlack = Color(0xff4a4848);
  @override
  Widget build(BuildContext context) {
    // double screenHeight = MediaQuery.of(context).size.height;
    // double screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.white24.withOpacity(0.15),
      resizeToAvoidBottomInset: false,
      bottomNavigationBar: buttonControls(),
      body: WillPopScope(
        onWillPop: () {
          if (isLoaderVisible) {
            Fluttertoast.showToast(
                msg: 'Please wait',
                textColor: Colors.black,
                backgroundColor: Colors.white);
          } else {
            return showDialog(
              context: context,
              builder: (ctx) => new AlertDialog(
                title: new Text('Confirm Exit?',
                    style: new TextStyle(
                        color: Colors.black,
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold)),
                content: new Text('Are you sure you want to exit the app?',
                    style: new TextStyle(
                        color: Colors.black,
                        fontSize: 18.0,
                        fontWeight: FontWeight.normal)),
                actions: <Widget>[
                  new FlatButton(
                    onPressed: () {
                      // this line exits the app.
                      SystemChannels.platform
                          .invokeMethod('SystemNavigator.pop');
                    },
                    child: new Text('Yes',
                        style:
                            new TextStyle(fontSize: 18.0, color: Colors.red)),
                  ),
                  new FlatButton(
                    onPressed: () =>
                        Navigator.pop(ctx), // this line dismisses the dialog
                    child: new Text('No', style: new TextStyle(fontSize: 18.0)),
                  )
                ],
              ),
            );
          }
          return null;
          // SystemChannels.platform.invokeMethod('SystemNavigator.pop');
        },
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15.0),
          child: Stack(
            clipBehavior: Clip.none,
            children: [
              Form(
                key: _key,
                child: PageView(
                  // physics: NeverScrollableScrollPhysics(),
                  controller: pageController,
                  // scrollDirection: Axis.horizontal,
                  onPageChanged: (val) {
                    setState(() {
                      currentIndex = val;
                    });
                  },
                  children: [
                    userInputAction(
                      label: 'What is your first name',
                      textEditingController: firstNameController,
                      validator: validateFirstName,
                      index: 1,
                      autoFocus: true,
                      onFieldSubmitted: (va) {
                        validateOnEachStep(
                          valid: validateFirstName(va),
                          toBeValidated: 'First Name is Required',
                          toBeValidatedOne: 'First Name must be a-z and A-Z',
                          pageIndex: 1,
                        );
                      },
                      onOkayButtonTap: () {
                        validateOnEachStep(
                          valid: validateFirstName(firstNameController.text),
                          toBeValidated: 'First Name is Required',
                          toBeValidatedOne: 'First Name must be a-z and A-Z',
                          pageIndex: 1,
                        );
                      },
                    ),

                    // ********** Last Name

                    userInputAction(
                      label: 'What is your last name',
                      textEditingController: lastNameController,
                      validator: validateLastName,
                      index: 2,
                      autoFocus: true,
                      onFieldSubmitted: (val) {
                        validateOnEachStep(
                          valid: validateLastName(val),
                          toBeValidated: 'Last Name is Required',
                          toBeValidatedOne: 'Last Name must be a-z and A-Z',
                          pageIndex: 2,
                        );
                      },
                      onOkayButtonTap: () {
                        validateOnEachStep(
                          valid: validateLastName(lastNameController.text),
                          toBeValidated: 'Last Name is Required',
                          toBeValidatedOne: 'Last Name must be a-z and A-Z',
                          pageIndex: 2,
                        );
                      },
                    ),

                    // ********** Email ***

                    userInputAction(
                      label: 'What is your email',
                      textEditingController: emailController,
                      validator: validateEmail,
                      autoFocus: true,
                      index: 3,
                      onFieldSubmitted: (val) {
                        validateOnEachStep(
                          valid: validateEmail(val),
                          toBeValidated: 'Email is Required',
                          toBeValidatedOne: 'Invalid Email : test@now.com',
                          pageIndex: 3,
                        );
                      },
                      onOkayButtonTap: () {
                        validateOnEachStep(
                          valid: validateEmail(emailController.text),
                          toBeValidated: 'Email is Required',
                          toBeValidatedOne: 'Invalid Email : test@now.com',
                          pageIndex: 3,
                        );
                      },
                    ),

                    // ******* DOB

                    userInputAction(
                      label: 'What is your date of birth',
                      textEditingController: dateController,
                      validator: validateDOB,
                      index: 4,
                      // prefixIcon: Image.asset(
                      //   'assets/images/dob_icon.png',
                      //   color: Colors.white54,
                      // ),
                      autoFocus: true,
                      onTap: () {
                        _selectDate(context);
                      },
                      // hintText: 'Click here to select dob..',
                      hintText: selectedDate == null
                          ? ""
                          : "${selectedDate.toLocal()}".split(' ')[0],
                      onOkayButtonTap: () {
                        validateOnEachStep(
                          valid: validateDOB(dateController.text),
                          toBeValidated: 'DOB is Required',
                          toBeValidatedOne: 'DOB is Required',
                          pageIndex: 4,
                        );
                      },
                      onFieldSubmitted: (val) {
                        validateOnEachStep(
                          valid: validateDOB(val),
                          toBeValidated: 'DOB is Required',
                          toBeValidatedOne: 'DOB is Required',
                          pageIndex: 4,
                        );
                      },
                    ),

                    // ****** Phone Number

                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        labelQuestion(
                            label: 'What is your phone number?*',
                            index: 5.toString()),
                        Stack(
                          clipBehavior: Clip.none,
                          children: [
                            Align(
                              alignment: Alignment.topRight,
                              child: Container(
                                width: MediaQuery.of(context).size.width * 0.8,
                                child: textInputField(
                                    validator: validateMobile,
                                    onFieldSubmitted: (val) {
                                      validateOnEachStep(
                                        valid: validateMobile(val),
                                        toBeValidated:
                                            'Please enter mobile number',
                                        toBeValidatedOne:
                                            'Please enter valid mobile number',
                                        pageIndex: 5,
                                      );
                                    },
                                    inputLength: 10,
                                    controller: phoneNumberController,
                                    textInputAction: TextInputAction.next),
                              ),
                            ),
                            Positioned(
                                left: -25,
                                top: 4,
                                child: CountryCodePicker(
                                    onChanged: (code) {
                                      dialCode = code.dialCode;
                                      print(
                                          "-------  ${code.name} ${code.dialCode} ${code.name}");
                                    },
                                    hideMainText: true,
                                    showFlagMain: true,
                                    showFlag: true,
                                    initialSelection: 'PK',
                                    hideSearch: false,
                                    showCountryOnly: true,
                                    showOnlyCountryWhenClosed: false,
                                    alignLeft: true,
                                    flagWidth: 30,
                                    onInit: (code) => dialCode = code.dialCode
                                    // print(
                                    //     "on init ${code.name} ${code.dialCode} ${code.name}"),
                                    )),
                          ],
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        doneButton(onOkayButtonTap: () {
                          validateOnEachStep(
                            valid: validateMobile(phoneNumberController.text),
                            toBeValidatedOne: 'Please enter mobile number',
                            toBeValidated: 'Please enter valid mobile number',
                            pageIndex: 5,
                          );
                        })
                      ],
                    ),

                    // ***** Phone Number

                    userInputAction(
                      label: 'What is your password',
                      textEditingController: passwordController,
                      index: 6,
                      obscureText: true,
                      autoFocus: true,
                      onFieldSubmitted: (val) {
                        validateOnEachStep(
                          valid: validatePassword(val),
                          toBeValidated: 'Please enter password',
                          toBeValidatedOne: 'Enter valid password : Pass@123',
                          pageIndex: 6,
                        );
                      },
                      validator: validatePassword,
                      onOkayButtonTap: () {
                        validateOnEachStep(
                          valid: validatePassword(passwordController.text),
                          toBeValidated: 'Please enter password',
                          toBeValidatedOne: 'Enter valid password : Pass@123',
                          pageIndex: 6,
                        );
                      },
                    ),
                    userInputAction(
                      label: 'Please confirm your password',
                      textEditingController: confirmPasswordController,
                      textInputAction: TextInputAction.done,
                      obscureText: true,
                      validator: validateConfirmPassword,
                      autoFocus: true,
                      onFieldSubmitted: (val) {
                        submitForm();
                      },
                      index: 7,
                      isCheckBoxVisible: true,
                      onOkayButtonTap: () {
                        submitForm();
                      },
                    ),
                  ],
                ),
              ),
              Visibility(
                visible: isLoaderVisible,
                child: Center(
                  child: CircularProgressIndicator(
                    color: Colors.orange[800],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  validateOnEachStep(
      {String valid,
      String toBeValidated,
      String toBeValidatedOne,
      pageIndex}) {
    if (valid == toBeValidated) {
      Fluttertoast.showToast(
          textColor: Colors.black,
          backgroundColor: Colors.white,
          msg: '$toBeValidated');
    } else if (valid == toBeValidatedOne) {
      Fluttertoast.showToast(
          textColor: Colors.black,
          backgroundColor: Colors.white,
          msg: '$toBeValidatedOne');
    } else
      // setState(() {
      pageController.animateToPage(
        pageIndex,
        duration: Duration(milliseconds: 350),
        curve: Curves.easeInOutSine,
      );
    // });
  }

  Widget userInputAction(
      {String label,
      TextEditingController textEditingController,
      int index,
      TextInputAction textInputAction = TextInputAction.next,
      bool isCheckBoxVisible = false,
      Function(String) onFieldSubmitted,
      int inputLength = 25,
      Function onTap,
      Widget prefixIcon,
      bool obscureText = false,
      bool autoFocus = true,
      String hintText = 'Write your answer here...',
      Function(String) validator,
      Function onOkayButtonTap}) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        labelQuestion(label: '$label? *', index: index.toString()),
        SizedBox(
          height: 15,
        ),
        textInputField(
            validator: validator,
            onTap: onTap,
            obscureText: obscureText,
            prefixIcon: prefixIcon,
            autoFocus: autoFocus,
            hintText: hintText,
            inputLength: inputLength,
            onFieldSubmitted: onFieldSubmitted,
            controller: textEditingController,
            textInputAction: textInputAction),
        SizedBox(
          height: 10,
        ),
        Visibility(
            visible: isCheckBoxVisible,
            child: Row(
              children: [
                Checkbox(
                  activeColor: Colors.orange[800],
                  checkColor: Colors.black,
                  value: isChecked,
                  // tristate: true,
                  side: BorderSide(color: Colors.white),
                  onChanged: (val) {
                    setState(() {
                      isChecked = isChecked == true ? false : true;
                    });
                    if (isChecked) {
                      // _launchURL(
                      //     "https://zerk.com.au/panel/public/privacy-policy");
                      _showMyDialog(isChecked);
                    } else {
                      // _launchURL(
                      //     "https://zerk.com.au/panel/public/privacy-policy");
                    }
                  },
                ),
                Expanded(
                  child: Text(
                    'I agree to terms and conditions & privacy policy',
                    textAlign: TextAlign.start,
                    maxLines: 2,
                    overflow: TextOverflow.visible,
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ],
            )),
        SizedBox(
          height: 10,
        ),
        doneButton(onOkayButtonTap: onOkayButtonTap),
      ],
    );
  }

  Widget labelQuestion({String label, String index}) {
    return Row(
      children: [
        Text(
          '$index',
          textAlign: TextAlign.center,
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.normal, fontSize: 17),
        ),
        Icon(
          Icons.arrow_forward,
          color: Colors.white,
          size: 15,
        ),
        Text(
          ' $label',
          textAlign: TextAlign.center,
          maxLines: 2,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.normal, fontSize: 19),
        ),
      ],
    );
  }

  Widget textInputField(
      {TextEditingController controller,
      TextInputAction textInputAction = TextInputAction.next,
      Function(String) onFieldSubmitted,
      Function(String) validator,
      Function onTap,
      bool obscureText = false,
      Widget prefixIcon,
      bool autoFocus = true,
      int inputLength = 25,
      String hintText = 'Type your answer here...'}) {
    return TextFormField(
      controller: controller,
      autofocus: autoFocus,
      obscureText: obscureText,
      keyboardType: TextInputType.text,
      onFieldSubmitted: onFieldSubmitted,
      textInputAction: textInputAction,
      obscuringCharacter: '*',
      onTap: onTap,
      autovalidateMode: AutovalidateMode.always,
      onChanged: (val) {},
      validator: validator,
      inputFormatters: [
        new LengthLimitingTextInputFormatter(inputLength),
      ],
      style: TextStyle(color: Colors.white, fontSize: 19),
      decoration: InputDecoration(
          prefixIcon: prefixIcon,
          hintText: hintText,
          errorStyle: TextStyle(color: Colors.orange[800]),
          hintStyle: TextStyle(color: Colors.white54, fontSize: 17),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          errorBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.orange[800]))),
    );
  }

  Widget doneButton({String buttonTitle = 'OK', Function onOkayButtonTap}) {
    return InkWell(
      onTap: onOkayButtonTap,
      child: Container(
        height: MediaQuery.of(context).size.height * 0.056,
        width: MediaQuery.of(context).size.width * 0.22,
        decoration: new BoxDecoration(
          borderRadius: BorderRadius.circular(6),
          color: Colors.orange[800],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(2.0),
              child: Text(
                buttonTitle.toUpperCase(),
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 17),
              ),
            ),
            Icon(
              Icons.done,
              color: Colors.black,
            )
          ],
        ),
      ),
    );
  }

  Widget primaryButton([IconData iconData, Function onPressed]) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        height: 35,
        width: 38,
        color: Colors.orange[800],
        child: Icon(
          iconData,
          color: Colors.black,
          size: 20,
        ),
      ),
    );
  }

  Widget buttonControls() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 3),
      child: Row(
        children: [
          primaryButton(Icons.arrow_downward_sharp,
              () => SystemChannels.textInput.invokeMethod('TextInput.hide')),
          Container(
            width: 1,
            height: 10,
            color: Colors.black,
          ),
          primaryButton(Icons.arrow_upward,
              () => SystemChannels.textInput.invokeMethod('TextInput.show')),
        ],
      ),
    );
  }
  //*********************************
  //*********************************
  //*********************************
  //*************** Faizan's Functions

  Future<void> _showMyDialog(bool check) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        if (isChecked) {
          return AlertDialog(
            title: InkWell(
              splashColor: Colors.orange[700],
              highlightColor: Colors.orange[700],
              child: Text(
                "Terms and conditions",
                style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.normal),
              ),
              onTap: () {
                _launchURL(
                    "https://zerk.com.au/panel/public/terms-and-condition");
              },
            ),
          );
        }
        return AlertDialog(
          title: Text('Terms & Conditions', style: TextStyle()),
          content: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              CircleAvatar(
                // radius: 18,
                backgroundColor: Colors.orange[700],
                child: SizedBox(
                  child: Icon(
                    //done_rounded
                    Icons.done,
                    //  size: 25,
                    color: Colors.white,
                    // color: Colors.orange[700],
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 12.0),
                  child: Text(
                    "I acknowledge and understand client to provide content.",
                    overflow: TextOverflow.visible,
                    textAlign: TextAlign.start,
                    // maxLines: 3,
                    style: TextStyle(wordSpacing: 0),
                    maxLines: 3,
                    // "Sign Up Successfully",
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  selectImageFromGallery() async {
    var image = await ImagePicker.pickImage(
      source: ImageSource.gallery,
    );
    if (image != null) {
      setState(() {
        imageFiles = image;
      });
    } else {
      throw Exception('Could not profile image');
    }
  }

  selectLicenseImage() async {
    var image = await ImagePicker.pickImage(
      source: ImageSource.gallery,
    );
    if (image != null) {
      setState(() {
        licenseImage = image;
      });
      _showDialog("Identification", "Driver licence is attached Successful");
    } else {
      _showDialog("Permission Denied", "Could not access your gallery");
      throw Exception('Could not license image');
    }
  }

  Future<UserSignup> userSignupApi(
    String first_name,
    String last_name,
    String email,
    String dob,
    String password,
    String password_confirmation,
    String device_token,
    String cellPhone,
  ) async {
    Dio dio = Dio();
    // String profile;
    // String license;
    // Image placeholderImageFile = Image.asset('assets/images/placeholderImage.jpg');

    // String profile = imageFiles.path.split('/').last;
    // String license = licenseImage.path.split('/').last;

    // if (imageFiles.path.split('/').last == null) {
    // } else {
    //   profile = imageFiles.path.split('/').last;
    //   license = licenseImage.path.split('/').last;
    // }
    // if (licenseImage.path.split('/').last == null) {
    // } else {
    //   profile = imageFiles.path.split('/').last;
    //   license = licenseImage.path.split('/').last;
    // }
    // File profile = 'assets/images/accessdenied.png' as File;
    // dio.options.headers['Content-Type'] = 'application/json; Charset=UTF-8';
    FormData formData = FormData.fromMap({
      'first_name': first_name,
      'last_name': last_name,
      // 'avatar': '',
      // 'licence':  '',
      // await MultipartFile.fromFile(licenseImage.path,
      //     filename: license.split('/').last)
      'dob': dob,
      'email': email,
      'password': password,
      'password_confirmation': password_confirmation,
      'device_token': device_token,
      'phone': cellPhone
    });
    // if (imageFiles != null) {
    //   String profile = imageFiles.path.split('/').last;
    //   formData = FormData.fromMap({
    //     'first_name': first_name,
    //     'last_name': last_name,
    //     'avatar': await MultipartFile.fromFile(imageFiles.path,
    //         filename: profile.split('/').last),
    //     // 'licence':  '',
    //     // await MultipartFile.fromFile(licenseImage.path,
    //     //     filename: license.split('/').last)
    //     'dob': dob,
    //     'email': email,
    //     'password': password,
    //     'password_confirmation': password_confirmation,
    //     'device_token': device_token,
    //     'phone': cellPhone
    //   });
    // }
    // if (licenseImage != null) {
    //   String license = licenseImage.path.split('/').last;
    //   formData = FormData.fromMap({
    //     'first_name': first_name,
    //     'last_name': last_name,
    //     'licence': await MultipartFile.fromFile(licenseImage.path,
    //         filename: license.split('/').last),
    //     // 'licence':  '',
    //     // await MultipartFile.fromFile(licenseImage.path,
    //     //     filename: license.split('/').last)
    //     'dob': dob,
    //     'email': email,
    //     'password': password,
    //     'password_confirmation': password_confirmation,
    //     'device_token': device_token,
    //     'phone': cellPhone
    //   });
    // }

    // if (licenseImage != null && imageFiles != null) {

    // String license = licenseImage.path.split('/').last;
    // String profile = imageFiles.path.split('/').last;

    formData = FormData.fromMap({
      'first_name': first_name,
      'last_name': last_name,
      //****************************** license and image done here******************************
      // 'avatar': await MultipartFile.fromFile(imageFiles.path,
      //     filename: profile.split('/').last),
      // 'licence': await MultipartFile.fromFile(licenseImage.path,
      //     filename: license.split('/').last),
      //****************************** license and image done here******************************
      'dob': dob,
      'email': email,
      'password': password,
      'password_confirmation': password_confirmation,
      'device_token': device_token,
      'phone': cellPhone
    });
    // }
    var response = await dio.post(
      'https://zerk.com.au/panel/public/api/user/register',
      data: formData,
      options: Options(
          followRedirects: false,
          validateStatus: (status) {
            return status < 500;
          }),
      // options: Options(
      //   headers: {"Content-Type": "application/json; Charset=UTF-8"},
      // ),
    );
    setState(() {
      isLoaderVisible = false;
    });
    print("[INFO] SIGN UP API FUNCTION");
    try {
      // print(response.toString());
      // print("jasdhasjdhjkashdjashdjksa");
    } catch (e) {
      final errorMessage = DioExceptions.fromDioError(e).toString();
      // print(errorMessage);
      // print("jasdhasjdhjkashdjashdjksa");
    }
    if (response.statusCode == 200) {
      // List<String> phoneNumber = [];
      print("-----------userSignupApi-------------");
      print("[INFO] SIGN UP API FUNCTION 200");
      // print(response.data);
      // print('This is final .. done with image path ${imagePath}');
      var parsedJson = response.data;
      var success = UserSignup.fromJson(parsedJson);
      userSignup.add(UserSignup(data: success.data));
      storage.setItem("token", success.data.token);
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString('email', emailController.text);
      // prefs.setString('userId', value);
      storage.setItem('userid', success.data.user.id.toString());
      print('This is token \n ${storage.getItem("token")}');

      String firstName = storage.getItem("firstnameController").toString();
      String lastName = storage.getItem("lastnameController").toString();
      String name = firstName.toString() + "" + lastName.toString();
      String phone = storage.getItem("cellPhoneController").toString();
      String email = storage.getItem("emailController").toString();
      String userId = storage.getItem('userid');
      // String dob storage.getItem("selectedDate");
      stripeCustomersCreate(userId, firstName, lastName, dob, password,
          password_confirmation, name, email, phone, "description");
      // Navigator.pushNamed(context, '/LoginScreen');
      Fluttertoast.showToast(
          msg: 'Sign Up Successful',
          textColor: Colors.black,
          backgroundColor: Colors.white);
      // phoneNumber.add(phone);
      // _showDialog(firstName, 'Sign Up Successfully');
      Future.delayed(Duration(seconds: 1), () {
        Navigator.pushReplacementNamed(context, '/LoginScreen');
      });
      return UserSignup.fromJson(response.data);
    } else {
      // print(json.decode(response.statusCode.toString()));
      // print(json.decode(response.body));
      print("[INFO] SIGN UP API FUNCTION !200");
      userInvaildSignup = List<UserInvaildSignup>();
      var parsedJson = response.data;
      var error = UserInvaildSignup.fromJson(parsedJson);
      userInvaildSignup
          .add(UserInvaildSignup(data: error.data, message: error.message));
      // print('*** this is error dialgoue ${error.message}');
      print(userInvaildSignup[0].data.email[0].toString());
      // _showErrorDialog(userInvaildSignup[0].message.toString(),
      //     userInvaildSignup[0].data.email[0].toString());
      Fluttertoast.showToast(
          msg: '${userInvaildSignup[0].data.email[0].toString()}',
          textColor: Colors.black,
          backgroundColor: Colors.white);
      // Future.delayed(Duration(seconds: 1), () {
      pageController.animateToPage(
        2,
        duration: Duration(milliseconds: 500),
        curve: Curves.easeInOutSine,
      );
      // });
      // if (userInvaildSignup[0]
      //     .data
      //     .email[0]
      //     .contains('Email has already been taken')) {
      //   pageController.animateToPage(
      //     2,
      //     duration: Duration(milliseconds: 350),
      //     curve: Curves.easeInOutSine,
      //   );
      // }
      print("[INFO] SIGN UP API FUNCTION EXIT");
      throw Exception('Failed to Register the Users');
    }
  }

  Future<StripeCustomersCreate> stripeCustomersCreate(
      String userId,
      String first_name,
      String last_name,
      String dob,
      String password,
      String password_confirmation,
      String name,
      String email,
      String phone,
      String description) async {
    print(userId);

    Map<String, dynamic> body = {
      'description': description,
      'name': name,
      'email': email,
      'phone': phone,
    };
    final http.Response response = await http.post(
      Uri.parse('https://api.stripe.com/v1/customers'),
      headers: <String, String>{
//       'Content-Type': 'application/json',
        'Authorization':
            'Bearer sk_test_51HbQCpJrvceP60phuzzvHaxtXprQv8DRJpvS97ie0Qx7CYhySy2rvdMYy8bTFlnLlH6bA7PfBGwrq1g71jbsSPbu00PNg2wv0Z',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: body,
      encoding: Encoding.getByName("utf-8"),
    );

    if (response.statusCode == 200) {
      print(body.toString());
      print('------stripe Customers Create-----------');
      print(response.statusCode.toString());
      print(response.body.toString());
      var parsedJson = json.decode(response.body);
      var success = StripeCustomersCreate.fromJson(parsedJson);
      print("stripeCustomerID" + success.id.toString());
      storage.setItem('stripeCustomerID', success.id);
      String stripeCustomerID = storage.getItem("stripeCustomerID");
      // int stripeCustomerID = int.parse(stripeCustomerId);
      print(userId);
      // print(stripeCustomerID);
      userProfileUpdated(
        first_name,
        last_name,
        email,
        dob,
        password,
        userId,
        stripeCustomerID,
      );
      return StripeCustomersCreate.fromJson(jsonDecode(response.body));
    } else {
      print('----Failed--stripe Customers Create----------');
      print(body.toString());
      print(response.statusCode.toString());
      print(response.body.toString());
      var parsedJson = json.decode(response.body);
      var success = StripeCustomersCreate.fromJson(parsedJson);
      _showDialog("Failed", 'Failed to stripe Customers Create');
      throw Exception('Failed to stripe Customers Create .');
    }
  }

  //------UserProfileUpdatedData------------------------------------
  Future<UserProfileUpdatedData> userProfileUpdated(
      String first_name,
      String last_name,
      String email,
      String dob,
      String password,
      // String token,
      String id,
      String stripeCustomerId
      //   String avatar,
      //   String address
      ) async {
    final http.Response response = await http.post(
      Uri.parse(baseurl.baseurl + "user/update/$id"),
      headers: <String, String>{
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=UTF-8',
        // 'Content-Type': 'application/json; charset=UTF-8',
      },
      //<String, String>
      body: jsonEncode(<String, String>{
        'first_name': first_name,
        'last_name': last_name,
        'dob': dob,
        'email': email,
        'password': password,
        'stripe_customer_id': stripeCustomerId
        // 'password_confirmation': password_confirmation,
      }),
    );
    if (response.statusCode == 200) {
      // Navigator.pushReplacementNamed(context, '/LoginScreen');
      print(json.decode(response.statusCode.toString()));
      print("------------UserProfileUpdatedData-------------");
      print(json.decode(response.body));
      var parsedJson = json.decode(response.body);
      var success = UserProfileUpdatedData.fromJson(parsedJson);
      // Navigator.pushNamed(context, '/LoginScreen');
      // storage.setItem("firstnameController", firstnameController.text);
      // storage.setItem("lastnameController", lastnameController.text);
      // storage.setItem("emailController", emailController.text);
      // storage.setItem("selectedDate", dobController.text);

      // setState(() {
      //   if (loaderVisible == true) {
      //     loaderVisible = false;
      //   }
      // });
      return UserProfileUpdatedData.fromJson(json.decode(response.body));
    } else {
      print(baseurl.baseurl + "user/update/" + id);
      print(json.decode(response.statusCode.toString()));
      print(json.decode(response.body));
      var parsedJson = json.decode(response.body.toString());
      var success = InvaildUserProfileUpdated.fromJson(parsedJson);
      _showErrorDialog(success.data.email.toString(), success.message);
      // setState(() {
      //   if (loaderVisible == true) {
      //     loaderVisible = false;
      //   }
      // });
      throw Exception('Failed to update the Users');
    }
  }

  _sendToServer() {
    setState(() {
      isLoaderVisible = true;
    });
    if (_key.currentState.validate()) {
      // No any error in validation
      _key.currentState.save();
      _futureUserSignup = userSignupApi(
        firstNameController.text,
        lastNameController.text,
        emailController.text,
        selectedDate == null
            ? dateController.text
            : "${selectedDate.toLocal()}".split(' ')[0],
        // dobController.text,
        passwordController.text,
        confirmPasswordController.text,
        deviceToken,
        dialCode + phoneNumberController.text.toString(),
      );
      // print('This is email ${emailController.text}');
      storage.setItem("firstnameController", firstNameController.text);
      storage.setItem("lastnameController", lastNameController.text);
      storage.setItem(
          "cellPhoneController", dialCode + phoneNumberController.text);
      storage.setItem("emailController", emailController.text);
      storage.setItem(
          "selectedDate", "${selectedDate.toLocal()}".split(' ')[0]);
      storage.setItem("password", passwordController.text.toString());

      loadProgress(true);

      firebaseMessaging.getToken().then((token) {
        print('TOKEN' + token);
        deviceToken = token;
      });
      //
      // print("Name $firstname");
      // print("Mobile $lastname");
      // print("Email $email");
      // print("DOB $dob");
      // print("Password $password");
      // print("Confirmpassword $confirmpassword");
      // print("deviceToken $deviceToken");
    } else {
      // validation error
      setState(() {
        isLoaderVisible = true;
        _validate = true;
      });
    }
  }

  //validateName
  String validateFirstName(String value) {
    String patttern = r'(^[a-zA-Z ]*$)';
    RegExp regExp = new RegExp(patttern);
    if (value.length == 0) {
      return "First Name is Required";
    } else if (!regExp.hasMatch(value)) {
      return "Last Name must be a-z and A-Z";
    }
    return null;
  }

  String validateLastName(String value) {
    String patttern = r'(^[a-zA-Z ]*$)';
    RegExp regExp = new RegExp(patttern);
    if (value.length == 0) {
      return "Last Name is Required";
    } else if (!regExp.hasMatch(value)) {
      return "Last Name must be a-z and A-Z";
    }
    return null;
  }

  //validateName
  String validateMobile(String value) {
    String pattern = r'(^(?:[+0]9)?[0-9]{10,12}$)';
    RegExp regExp = new RegExp(pattern);
    if (value.length == 0) {
      return 'Please enter mobile number';
    } else if (!regExp.hasMatch(value)) {
      return 'Please enter valid mobile number';
    }
    return null;
  }

  //validateName
  String validateDOB(String value) {
    String patttern =
        r'^([0]?[1-9]|[1|2][0-9]|[3][0|1])[./-]([0]?[1-9]|[1][0-2])[./-]([0-9]{4}|[0-9]{2})$';
    RegExp regExp = new RegExp(patttern);
    if (value.length == 0) {
      return "DOB is Required";
    } else if (!regExp.hasMatch(value)) {
      return "DOB must be in this pattern: 25-04-2017 ";
    }
    return null;
  }

//validateEmail
  String validateEmail(String value) {
    String pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = new RegExp(pattern);
    if (value.length == 0) {
      return "Email is Required";
    } else if (!regExp.hasMatch(value)) {
      return "Invalid Email : test@now.com";
    } else {
      return null;
    }
  }

  // bool validatePasswords(String value) {
  //   String pattern =
  //       r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
  //   RegExp regExp = new RegExp(pattern);
  //   return regExp.hasMatch(value);
  // }

  String signUpvalidatePassword(String value) {
    Pattern pattern = r'^(?=.*?[A-Z])(?=.*?[a-z])';
    //(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$

    RegExp regex = new RegExp(pattern);

    print(value);

    if (value.isEmpty) {
      return 'Please enter password';
    }
    // else {
    //   if (value.isNotEmpty)
    //     // if (!regex.hasMatch(value))
    //     return 'Enter valid password';
    //   // else
    //   return null;
    // }
  }

  String validatePassword(String value) {
    Pattern pattern =
        r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
    RegExp regex = new RegExp(pattern);
    print(value);
    if (value.isEmpty) {
      return 'Please enter password';
    }
    // else {
    //   if (!regex.hasMatch(value))
    //     return 'Enter valid password : Pass@123';
    else
      return null;
    // }
  }

  String validateConfirmPassword(String value) {
    // Pattern pattern =
    //     r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
    // RegExp regex = new RegExp(pattern);
    // print("===========" + value);

    if (value.isEmpty) {
      return 'Please enter confirm password';
    } else if (value != passwordController.text) {
      return 'Confirm password must be same as password';
    }
    // return null;
    // else {
    //   if (!regex.hasMatch(value))
    //     return 'Enter valid confirm password';
    else
      return null;
    // }
  }

  loadImageFromPreferences() {
    Utility.getImageFromPreferences('IMAGE_KEY').then((img) {
      if (null == img) {
        return;
      }
      imageFromPreferences = Utility.imageFromBase64String(img);
      // setState(() {});
    });
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url, forceWebView: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  void _showErrorDialog(String error, String string) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            "$error",
            style: TextStyle(
                color: Colors.black, fontSize: 18, fontWeight: FontWeight.bold),
            overflow: TextOverflow.ellipsis,
            maxLines: 3,
            textAlign: TextAlign.center,
          ),
          content: Text(
            "$string",
            textAlign: TextAlign.center,
          ),
        );
      },
    );
    loadProgress(false);
  }

  void _showDialog(String titlestring, String contentstring) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          elevation: 5,
          title: Text(
            "$titlestring",
            // "Sign Up Successfully",
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.orange[700],
            ),
          ),
          content: Text(
            "$contentstring",
            // textAlign: TextAlign.justify,
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
            overflow: TextOverflow.ellipsis,
            textAlign: TextAlign.center,
            maxLines: 2,
            // "Sign Up Successfully",
          ),
        );
      },
    );
    setState(() {
      isLoaderVisible = false;
    });
    loadProgress(false);
  }

  loadProgress(bool value) {
    if (visible == value) {
      setState(() {
        visible = value;
      });
    } else {
      setState(() {
        visible = value;
      });
    }
  }

  Future<void> _selectDate(BuildContext context) async {
    try {
      final DateTime picked = await showDatePicker(
          builder: (BuildContext context, Widget child) {
            return Theme(
              data: ThemeData(
                primarySwatch: Colors.grey,
                splashColor: Colors.black,
                textTheme: TextTheme(
                  subtitle1: TextStyle(color: Colors.black),
                  button: TextStyle(color: Colors.black),
                ),
                accentColor: Colors.black,
                colorScheme: ColorScheme.light(
                    primary: Colors.orange[800],
                    primaryVariant: Colors.black,
                    secondaryVariant: Colors.black,
                    onSecondary: Colors.black,
                    onPrimary: Colors.white,
                    surface: Colors.black,
                    onSurface: Colors.black,
                    secondary: Colors.black),
                dialogBackgroundColor: Colors.white,
              ),
              child: child ?? Text(""),
            );
          },
          cancelText: '',
          context: context,
          errorFormatText: 'Enter valid date',
          errorInvalidText: 'Enter date in valid range',
          //         firstDate: DateTime(2000),
          //   lastDate: DateTime(2025),
          //  initialDate: DateTime.now(),
          initialDate: selectedDate,
          firstDate: DateTime(1885),
          lastDate: DateTime(2101));
      if (picked != null && picked != selectedDate)

        // print('this is picked date $picked');
        // print('this is selected date $selectedDate');
        // DateFormat dateFormat = DateFormat('dd-mm-yyyy');
        // print('this is date format $dateFormat');
        //   print(DateFormat format = DateFormat('dd-mm-yyyy'));
        //   DateFormat()

        // print('this is converted date ${}')
        setState(() {
          selectedDate = picked;
          // dateController.text = "${selectedDate.toLocal()}".split(' ')[0];
          print('this is date $selectedDate');
          // print('${dateFormat.format(selectedDate)}');
          dateController.text = dateFormat.format(selectedDate);
          // print('this is converted date ${dateFor}')
          // print("${dobController.text}");
          // print("${selectedDate.toLocal()}".split(' ')[0]);
        });
      Fluttertoast.showToast(
          msg: 'dob picked successfully',
          textColor: Colors.black,
          backgroundColor: Colors.white);
    } catch (e) {
      print(e);
      Fluttertoast.showToast(
          msg: 'Please pick a valid date',
          textColor: Colors.black,
          backgroundColor: Colors.white);
      print('this is exception $e');
      // _selectDate(context);
    }

    loadProgress(false);
  }

  // void _sendSMS(String message, List<String> recipient) async {
  //   String _result = await sendSMS(message: message, recipients: recipient)
  //       .catchError((onError) {
  //     print(onError);
  //   });
  //   print('send sms function');
  //   print(_result);
  // }

}
