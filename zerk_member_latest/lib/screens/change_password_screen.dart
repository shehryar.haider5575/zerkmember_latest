import 'dart:convert';

import 'package:Zerk/model/model_UserChangePassword.dart';
import 'package:Zerk/model/model_invalidChangePassword.dart';
import 'package:Zerk/utils/baseurl.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:localstorage/localstorage.dart';
// import 'package:zerk_app/model/model_UserChangePassword.dart';
// import 'package:zerk_app/model/model_invalidChangePassword.dart';
// import 'package:zerk_app/utils/baseurl.dart';

class ChangePasswordScreen extends StatefulWidget {
  // ChangePasswordScreen({Key key}) : super(key: key);

  @override
  _ChangePasswordScreenState createState() => _ChangePasswordScreenState();
}

class _ChangePasswordScreenState extends State<ChangePasswordScreen> {
  final oldPasswordController = TextEditingController();
  final passwordController = TextEditingController();
  final passwordConfirmationController = TextEditingController();
  Future<ChangePassword> _futureChangePassword;
  bool _validate = false;
  List<InvalidChangePassword> invalidChangePasswordList;
  String oldPassword, password, passwordConfirmation;
  bool visible = false;
  GlobalKey<FormState> _key = new GlobalKey();
  Baseurl baseurl = Baseurl();
  final LocalStorage storage = new LocalStorage('token');
  String token;
  String details_id;
  @override
  void initState() {
    super.initState();

    token = storage.getItem('token');
    print(token);
    token = storage.getItem("token");
    details_id = storage.getItem('details_id');
    print(details_id);
  }

  loadProgress(bool value) {
    if (visible == value) {
      setState(() {
        visible = value;
      });
    } else {
      setState(() {
        visible = value;
      });
    }
  }

  void _showDialog() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return
//  ResponseStatus(responseText: widget.,);
            AlertDialog(
          title: Text(
            "Password Changed Successfully",
            textAlign: TextAlign.center,
          ),
        );
      },
    );

    loadProgress(false);
  }

  void _showErrorDialog(String error, String string) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            "$error",
            textAlign: TextAlign.center,
          ),
          content: Text(
            "$string",
            textAlign: TextAlign.center,
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/images/login_bg.png'),
              fit: BoxFit.cover,
              alignment: Alignment.topLeft
              //fit: BoxFit.contain
              //   alignment: Alignment.center
              ),
        ),
        child: SingleChildScrollView(
          child: Form(
            key: _key,
            autovalidate: _validate,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                //   Spacer(
                //     flex: 20,
                //   ),
                SizedBox(
                  height: 50,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 25.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: 60,
                            width: 60,
                            child:
                                Image.asset('assets/images/sign_up_icon.png'),
                          ),
                        ],
                      ),
                    ),
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(
                              top: 35, left: 20, right: 0, bottom: 0),
                          child: Text(
                            "Change Password",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 28.0,
                                fontWeight: FontWeight.bold,
                                fontFamily: "Poppins-Bold",
                                letterSpacing: 2),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                // Spacer(
                //   flex: 9,
                // ),
                Padding(
                  padding: const EdgeInsets.only(
                      top: 10, left: 0, right: 0, bottom: 0),
                ),
                //User Name
                Padding(
                  padding: EdgeInsets.only(left: 25.0, right: 25.0),
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: 5.0),
                      ),
                      TextFormField(
                        controller: oldPasswordController,
                        // validator: validateEmail,
                        keyboardType: TextInputType.visiblePassword,
                        autocorrect: true,
                        onSaved: (String val) {
                          oldPassword = val;
                        },
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                            hintText: "Previous Password",
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                            ),
                            contentPadding: const EdgeInsets.all(10.0),
                            hintStyle:
                                TextStyle(color: Colors.white, fontSize: 15.0)),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 15.0),
                      ),
                      TextFormField(
                        controller: passwordController,
                        // validator: validatePassword,
                        cursorColor: Colors.white,
                        keyboardType: TextInputType.visiblePassword,
                        autocorrect: true,
                        onSaved: (String val) {
                          password = val;
                        },
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                            hintText: "Password",
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                            ),
                            contentPadding: const EdgeInsets.all(10.0),
                            hintStyle:
                                TextStyle(color: Colors.white, fontSize: 15.0)),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 15.0),
                      ),
                      TextFormField(
                        controller: passwordConfirmationController,
                        // validator: validatePassword,
                        cursorColor: Colors.white,
                        keyboardType: TextInputType.visiblePassword,
                        autocorrect: true,

                        onSaved: (String val) {
                          passwordConfirmation = val;
                        },
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                            hintText: "Password Confirmation",
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                            ),
                            contentPadding: const EdgeInsets.all(10.0),
                            // prefixIcon: SizedBox(
                            //   height: 30,
                            //   width: 30,
                            //   child: Image.asset('assets/images/password_icon.png'),
                            // ),
                            hintStyle:
                                TextStyle(color: Colors.white, fontSize: 15.0)),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                    ],
                  ),
                ),

                Center(
                  child: Visibility(
//  maintainSize: true,
//               maintainAnimation: true,
//               maintainState: true,
                      visible: visible,
                      child: CircularProgressIndicator(
                        color: Colors.orange[800],
                      )),
                ),

                // Spacer(
                //   flex: 1,
                // ),
                SizedBox(
                  height: 40,
                ),
                //SIGN UP BUTTON
                Padding(
                  padding: const EdgeInsets.only(left: 30, top: 30),
                  child: Container(
                    //   color: Colors.black,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        SizedBox(
                          width: 220.0,
                          height: 60.0,
                          child: RaisedButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(42.0),
                                side: BorderSide(color: Colors.white)),
                            onPressed: () {
                              print(oldPasswordController.text);
                              print(passwordController.text);
                              print(passwordConfirmationController.text);

                              if (passwordController.text !=
                                  passwordConfirmationController.text) {
                                print(passwordController.text +
                                    " NotMatch " +
                                    passwordConfirmationController.text);
                                _showErrorDialog("error",
                                    "The password confirmation does not match");
                              } else {
                                print(passwordController.text +
                                    "Match" +
                                    passwordConfirmationController.text);
                                _sendToServer();
                              }
                            },
                            color: Colors.white,
                            textColor: Colors.orange,
                            child: Text("Change Password",
                                style: TextStyle(
                                  fontSize: 15.0,
                                  letterSpacing: 0,
                                  fontWeight: FontWeight.bold,
                                )),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  //------ChangePassword------------------------------------
  Future<ChangePassword> userChangePassword(
    String old_password,
    String password,
    String password_confirmation,
    String id,
  ) async {
    final http.Response response = await http.post(
      Uri.parse(baseurl.baseurl + "user/change-password/" + id),
      headers: <String, String>{
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'old_password': old_password,
        'password': password,
        'password_confirmation': password_confirmation,
      }),
    );

    if (response.statusCode == 200) {
      print(json.decode(response.statusCode.toString()));
      print("------------ChangePassword-------------");
      print(json.decode(response.body));
      _showDialog();
      // userDetailsDataApi(userSignup[0].token.toString());

      return ChangePassword.fromJson(json.decode(response.body));
    } else {
      print(json.decode(response.statusCode.toString()));
      print(json.decode(response.body));
      invalidChangePasswordList = List<InvalidChangePassword>();
      var parsedJson = json.decode(response.body);
      var error = InvalidChangePassword.fromJson(parsedJson);

      invalidChangePasswordList
          .add(InvalidChangePassword(data: error.data, message: error.message));
      _showErrorDialog(invalidChangePasswordList[0].message.toString(),
          invalidChangePasswordList[0].data.password[0].toString());
      throw Exception('Failed to Register the Users');
    }
  }

  _sendToServer() {
    if (_key.currentState.validate()) {
      // No any error in validation
      _key.currentState.save();

      _futureChangePassword = userChangePassword(
          oldPasswordController.text,
          passwordController.text,
          passwordConfirmationController.text,
          details_id);
      loadProgress(true);

      print("oldPassword $oldPassword");
      print("password $password");
      print("passwordConfirmation $passwordConfirmation");
      print("details_id $details_id");
    } else {
      // validation error
      setState(() {
        _validate = true;
      });
    }
  }
}
