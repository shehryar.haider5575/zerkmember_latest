import 'dart:core';

import 'package:flutter/material.dart';

import 'mapview_listing_screen.dart';
import 'profile_widget.dart';

class BottomNavBar extends StatefulWidget {
  BottomNavBar({Key key, this.title, this.tokenStorage}) : super(key: key);
  final String title, tokenStorage;

  @override
  _BottomNavBarState createState() => _BottomNavBarState();
}

class _BottomNavBarState extends State<BottomNavBar> {
  int bottomSelectedIndex = 0;

  List<BottomNavigationBarItem> buildBottomNavBarItems() {
    return [
      BottomNavigationBarItem(
        activeIcon: Image.asset('assets/images/profile_active.png'),
        icon: const ImageIcon(
          AssetImage('assets/images/profile_inactive.png'),
        ),
        title: Text('Profile', style: new TextStyle()),
      ),
      BottomNavigationBarItem(
        activeIcon: Image.asset('assets/images/calendar_active.png'),
        icon: const ImageIcon(
          AssetImage('assets/images/calendar_inactive.png'),
        ),
        title: Text(
          'List View',
          style: new TextStyle(),
        ),
      ),
    ];
  }

  PageController pageController = PageController(
    initialPage: 0,
    keepPage: true,
  );

  Widget buildPageView() {
    return PageView(
      physics: NeverScrollableScrollPhysics(),
      controller: pageController,
      onPageChanged: (index) {
        pageChanged(index);
      },
      children: <Widget>[
        Today(),
        Weekly(),
        Monthly(),
        Inbox(),
      ],
    );
  }

  @override
  void initState() {
    super.initState();
  }

  void pageChanged(int index) {
    setState(() {
      bottomSelectedIndex = index;
    });
  }

  void bottomTapped(int index) {
    setState(() {
      bottomSelectedIndex = index;

      pageController.animateToPage(index,
          duration: Duration(milliseconds: 500), curve: Curves.ease);
      //    duration: Duration(milliseconds: 500), curve: Curves.ease);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: buildPageView(),
      bottomNavigationBar: SizedBox(
        // height: MediaQuery.of(context).size.height * 0.1,
        child: BottomNavigationBar(
          iconSize: 30,
          selectedItemColor: Colors.red,
          type: BottomNavigationBarType.fixed,
          currentIndex: bottomSelectedIndex,
          onTap: (index) {
            bottomTapped(index);
          },
          items: buildBottomNavBarItems(),
        ),
      ),
    );
  }
}

class Today extends StatefulWidget {
  @override
  _TodayState createState() => _TodayState();
}

class _TodayState extends State<Today> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ProfileMenu(),
    );
  }
}

class Weekly extends StatefulWidget {
  @override
  _WeeklyState createState() => _WeeklyState();
}

class _WeeklyState extends State<Weekly> {
  @override
  Widget build(BuildContext context) {
    return MapViewListing();
  }
}

class Monthly extends StatefulWidget {
  @override
  _MonthlyState createState() => _MonthlyState();
}

class _MonthlyState extends State<Monthly> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.yellowAccent,
      // child: MyHomePage(),
    );
  }
}

class Inbox extends StatefulWidget {
  @override
  _InboxState createState() => _InboxState();
}

class _InboxState extends State<Inbox> {
  @override
  Widget build(BuildContext context) {
    return Container(
        //color: Colors.yellowAccent,
        child: Column(
      // crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text("Messages",
            style: new TextStyle(
                color: Colors.black,
                fontSize: 25.0,
                fontWeight: FontWeight.bold)),
      ],
    ));
  }
}
