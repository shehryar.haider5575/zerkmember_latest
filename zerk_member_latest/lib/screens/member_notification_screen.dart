import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:Zerk/model/model_memberNotification.dart';
import 'package:Zerk/utils/baseurl.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';
import 'package:localstorage/localstorage.dart';
// import 'package:zerk_app/model/model_memberNotification.dart';
// import 'package:zerk_app/utils/baseurl.dart';

class MemberNotificationScreen extends StatefulWidget {
  @override
  _MemberNotificationScreenState createState() =>
      _MemberNotificationScreenState();
}

class _MemberNotificationScreenState extends State<MemberNotificationScreen> {
  Baseurl baseurl = Baseurl();
  final LocalStorage storage = new LocalStorage('token');
  Future<MemberNotification> futureMemberNotification;
  String userId = '';
  final ScrollController _scrollController = ScrollController();
  final GlobalKey<LiquidPullToRefreshState> _refreshIndicatorKey =
      GlobalKey<LiquidPullToRefreshState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  void initState() {
    super.initState();
    userId = storage.getItem('userid');
    print(userId);

    futureMemberNotification = fetchMemberNotification(userId);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Stack(
          children: [
            Positioned(
              //     top: Alignment.topCenter.y,
              top: Alignment.topCenter.x,
              left: Alignment.center.x,
              right: Alignment.center.x,
              child: Container(
                color: Colors.orange,
                height: MediaQuery.of(context).size.height * .20,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 25.0, bottom: 20.0, top: 10.0),
                      child: InkWell(
                        onTap: () {
                          Navigator.popAndPushNamed(context, '/BottomNavBar');
                        },
                        child: SizedBox(
                          child: Icon(
                            Icons.arrow_back,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 20.0, bottom: 20.0, top: 10.0),
                      child: Text(
                        "Member Notification",
                        style: TextStyle(
                          fontSize: 22.0,
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              //   top: 120,
              // top: Alignment.bottomCenter.x,

              bottom: Alignment.bottomCenter.x,
              // top: Alignment.center.x,
              child: Container(
                //    alignment: Alignment.center,
                width: MediaQuery.of(context).size.width,
                //    color: Colors.amber,
                height: MediaQuery.of(context).size.height * .85,
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 10.0, right: 10.0, bottom: 5.0),
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15),
                    ),
                    elevation: 20,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: LiquidPullToRefresh(
                        backgroundColor: Colors.orange[600],
                        color: Colors.transparent,
                        key: _refreshIndicatorKey,
                        onRefresh: _handleRefresh,
                        showChildOpacityTransition: false,
                        child: FutureBuilder<MemberNotification>(
                          future: futureMemberNotification,
                          builder: (context, snapshot) {
                            if (snapshot.hasData) {
                              if (snapshot.data.data.isEmpty) {
                                return Container(
                                  color: Colors.transparent,
                                  child: Center(
                                    child: Text(
                                      'No Member Notification ',
                                      style: TextStyle(
                                          color: Colors.orange,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18.0),
                                    ),
                                  ),
                                );
                              }

                              return ListView.builder(
                                  controller: _scrollController,
                                  itemCount: snapshot.data.data.length,
//                                    reverse: true,
                                  // separatorBuilder: (context, index) =>
                                  //     Divider(
                                  //       endIndent: 15,
                                  //       indent: 15,
                                  //       color: Colors.grey[400],
                                  //     ),
                                  itemBuilder: (context, index) {
                                    return Container(
                                      height: 90,
                                      margin: EdgeInsets.symmetric(
                                          vertical: 3.0, horizontal: 5.0),
                                      decoration: BoxDecoration(
                                        //color: appTheme.primaryColor,
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(10.0),
                                        ),
                                        // boxShadow: [
                                        //   BoxShadow(
                                        //     color: Colors.black12,
                                        //     blurRadius: 10.0,
                                        //     spreadRadius: 0.0,
                                        //     offset: Offset(0.0, 5.0),
                                        //   ),
                                        // ],
                                      ),
                                      child: FlatButton(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 10.0),
                                        //   rounded: 10.0,
                                        onPressed: () {},
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
//                                                ClipRRect(
//                                                  borderRadius:
//                                                      BorderRadius.circular(
//                                                          10.0),
//                                                  child: SizedBox(
//                                                    height: 40,
//                                                    width: 50,
////                                                child: Image.network(
////                                                  'http://165.227.69.207/zerk/public/uploads/' +
////                                                      snapshot.toString(),
////                                                ),
////                                                    child: Image.asset(
////                                                        'assets/images/john_image.png'),
//                                                  ),
//                                                  // child: ImageAsAsset(
//                                                  //   image: 'item_image.png',
//                                                  //   width: 120.0,
//                                                  // ),
//                                                ),
//                                                SizedBox(
//                                                  width: 10.0,
//                                                ),

                                            Container(
//                                                  width: MediaQuery.of(context).size.width*0.5,
                                              child: Row(
                                                children: [
                                                  Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      Text(
                                                        'ZERK'.toUpperCase(),
                                                        style: TextStyle(
                                                          fontSize: 18.0,
                                                          fontWeight:
                                                              FontWeight.normal,
                                                        ),
                                                      ),
                                                      Padding(
                                                        padding:
                                                            EdgeInsets.only(
                                                                top: 10.0),
                                                      ),
                                                      Container(
                                                        height: 50,
                                                        width: MediaQuery.of(
                                                                    context)
                                                                .size
                                                                .width *
                                                            .4,
                                                        child: Text(
                                                          snapshot
                                                              .data
                                                              .data[index]
                                                              .message,
                                                          style: TextStyle(
                                                            fontSize: 14.0,
                                                            color: Colors.black,
                                                            fontWeight:
                                                                FontWeight
                                                                    .normal,
                                                          ),
                                                          //style: kCollectionItemPrice,
                                                        ),
                                                      ),
                                                      Padding(
                                                        padding:
                                                            EdgeInsets.only(
                                                                top: 5.0),
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),

                                            Container(
//                                                  width: MediaQuery.of(context).size.width*0.6,
//                                                  color: Colors.red,
                                                child: Row(
//                                                    mainAxisAlignment: MainAxisAlignment.end,
                                              children: [
                                                Text(
                                                    snapshot.data.data[index]
                                                        .createdAt
                                                        .toString(),
                                                    style: TextStyle(
                                                      fontSize: 14.0,
                                                      fontWeight:
                                                          FontWeight.normal,
                                                    )),
                                              ],
                                            )),
                                          ],
                                        ),
                                      ),
                                    );
                                  });
                              // : Center(
                              //     child: Text("Member Avail Logs No Found"),
                              //     //  CircularProgressIndicator()
                              //   );
                              // Center(child: CircularProgressIndicator());
                            } else if (snapshot.hasError) {
                              return Text("${snapshot.error}");
                            }

                            return Center(child: showProgressLoader(context));
                            // return Container(
                            //   // color: Colors.black,
                            //   child: Center(
                            //       // child: Text("Member Avail Logs No Found"),
                            //       // CircularProgressIndicator()
                            //       ),
                            // );
                          },
                        ),
                      ),
//                      child: Scrollbar(
//                        isAlwaysShown: true,
//                        thickness: 8,
//                        controller: _scrollController,
//
//                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget showProgressLoader(BuildContext context) {
    if (Platform.isAndroid) {
      return CircularProgressIndicator(
        color: Colors.orange[800],
      );
    }
    if (Platform.isIOS) {
      return CupertinoActivityIndicator();
    }
  }

  Future<void> _handleRefresh() {
    final Completer<void> completer = Completer<void>();
    Timer(const Duration(seconds: 1), () {
      completer.complete();
    });
    setState(() {
      futureMemberNotification =
          fetchMemberNotification(storage.getItem('userid').toString());
      print("_handleRefresh............................");
      // refreshNum = new Random().nextInt(100);
    });
    return completer.future.then<void>((_) {
      _scaffoldKey.currentState?.showSnackBar(
        SnackBar(
          content: const Text('Refresh complete'),
          action: SnackBarAction(
              label: 'RETRY',
              onPressed: () {
                // _refreshIndicatorKey.currentState.show();
              }),
        ),
      );
    });
  }

  Future<MemberNotification> fetchMemberNotification(String id) async {
    final response = await http.get(
      Uri.parse(baseurl.baseurl + "notification/all/$id"),
      headers: <String, String>{
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );

    if (response.statusCode == 200) {
      print(response.body.toString());
      return MemberNotification.fromJson(jsonDecode(response.body));
    } else {
      print(response.body.toString());
      throw Exception('Failed to load fetch Member Notification');
    }
  }
}
