import 'dart:convert';

import 'package:Zerk/model/model_forgot_password.dart';
import 'package:Zerk/model/model_invalid_forgot_password.dart';
import 'package:Zerk/utils/baseurl.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
// import 'package:zerk_app/model/model_forgot_password.dart';
// import 'package:zerk_app/model/model_invalid_forgot_password.dart';
// import 'package:zerk_app/utils/baseurl.dart';

class ForgotPasswordScreen extends StatefulWidget {
  ForgotPasswordScreen({Key key}) : super(key: key);

  @override
  _ForgotPasswordScreenState createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  final emailController = TextEditingController();
  Future<ForgotPassword> _futureForgotPassword;
  List<ForgotPassword> forgotPasswordList;
  List<InvalidForgotPassword> invalidForgotPasswordList;
  bool _validate = false;
  String email;
  bool visible = false;
  GlobalKey<FormState> _key = new GlobalKey();
  Baseurl baseurl = Baseurl();

  void _showErrorDialog(String error, String string) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
            title: Text(
              "$error",
              textAlign: TextAlign.center,
            ),
            content: Text(
              "$string",
              textAlign: TextAlign.center,
            )
            //     Text(
            //   "Please wait from verification",
            //   textAlign: TextAlign.center,
            // ),
            );
      },
    );

    loadProgress(false);
    // nameController.clear();
    // emailController.clear();
    // contactNumberController.clear();
    // passwordController.clear();
    // confirmpasswordController.clear();
  }

  void _showDialog(String contentstring) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          elevation: 5,

          // title: Text(
          //   "$titlestring",
          //   // "Sign Up Successfully",
          //   textAlign: TextAlign.center,
          //    style: TextStyle(
          //         color: Colors.orange[700],
          //       ),
          // ),
          content: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              // Padding(
              //   padding: const EdgeInsets.only(right: 3),
              //   child: CircleAvatar(
              //     radius: 15,
              //     backgroundColor: Colors.orange[700],
              //     child: SizedBox(
              //       child: Icon(
              //         Icons.login_rounded,
              //         //  size: 25,
              //         color: Colors.white,
              //         // color: Colors.orange[700],
              //       ),
              //     ),
              //   ),
              // ),
              Expanded(
                child: Text(
                  "$contentstring",
                  textAlign: TextAlign.start,
                  maxLines: 2,
                  style: TextStyle(
                      color: Colors.orange[700], fontWeight: FontWeight.bold),
                  // "Sign Up Successfully",
                ),
              ),
            ],
          ),
        );
      },
    );

    loadProgress(false);
//  getStringValuesSF("token");
    // nameController.clear();
    // emailController.clear();
    // contactNumberController.clear();
    // passwordController.clear();
    // confirmpasswordController.clear();
  }

  loadProgress(bool value) {
    if (visible == value) {
      setState(() {
        visible = value;
      });
    } else {
      setState(() {
        visible = value;
      });
    }

//                 Visibility(
//  maintainSize: true,
//               maintainAnimation: true,
//               maintainState: true,
//               visible: visible,

//               child: CircularProgressIndicator()
//               ),
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/images/login_bg.png'),
              fit: BoxFit.cover,
              alignment: Alignment.topLeft
              //fit: BoxFit.contain
              //   alignment: Alignment.center
              ),
        ),
        child: SingleChildScrollView(
          child: Form(
            key: _key,
            autovalidate: _validate,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                //   Spacer(
                //     flex: 20,
                //   ),
                SizedBox(
                  height: 50,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 25.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: 60,
                            width: 60,
                            child:
                                Image.asset('assets/images/sign_up_icon.png'),
                          ),
                        ],
                      ),
                    ),
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(
                              top: 35, left: 20, right: 0, bottom: 0),
                          child: Text(
                            "Forgot Password",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 28.0,
                                fontWeight: FontWeight.bold,
                                fontFamily: "Poppins-Bold",
                                letterSpacing: 2),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                // Spacer(
                //   flex: 9,
                // ),
                Padding(
                  padding: const EdgeInsets.only(
                      top: 10, left: 0, right: 0, bottom: 0),
                ),

                //User Name
                Padding(
                  padding: EdgeInsets.only(left: 25.0, right: 25.0),
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: 5.0),
                      ),
                      SizedBox(
                        height: 50,
                      ),
                      TextFormField(
                        controller: emailController,
                        validator: validateEmail,
                        keyboardType: TextInputType.emailAddress,
                        autocorrect: true,
                        onSaved: (String val) {
                          email = val;
                        },
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                            hintText: "Enter Email",
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                            ),
                            contentPadding: const EdgeInsets.all(10.0),

                            // prefixIcon: SizedBox(
                            //   height: 30,
                            //   width: 30,
                            //   child: Image.asset('assets/images/dob_icon.png'),
                            // ),
                            hintStyle:
                                TextStyle(color: Colors.white, fontSize: 15.0)),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                    ],
                  ),
                ),

                Center(
                  child: Visibility(
//  maintainSize: true,
//               maintainAnimation: true,
//               maintainState: true,
                      visible: visible,
                      child: CircularProgressIndicator()),
                ),

                //SIGN UP BUTTON
                Padding(
                  padding: const EdgeInsets.only(left: 30, top: 30),
                  child: Container(
                    //   color: Colors.black,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        SizedBox(
                          width: 220.0,
                          height: 60.0,
                          child: RaisedButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(42.0),
                                side: BorderSide(color: Colors.white)),
                            onPressed: () {
                              print(emailController.text);
                              _sendToServer();
                            },
                            color: Colors.white,
                            textColor: Colors.orange,
                            child: Text("Submit",
                                style: TextStyle(
                                  fontSize: 22.0,
                                  letterSpacing: 0,
                                  fontWeight: FontWeight.bold,
                                )),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  //------ForgotPassword------------------------------------
  Future<ForgotPassword> userForgotPassword(
    String email,
  ) async {
    final http.Response response = await http.post(
      Uri.parse(baseurl.baseurl + "user/password/create"),
      headers: <String, String>{
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'email': email,
      }),
    );

    if (response.statusCode == 200) {
      print(json.decode(response.statusCode.toString()));
      print("------------ForgotPassword-------------");
      print(json.decode(response.body));
      forgotPasswordList = List<ForgotPassword>();
      var parsedJson = json.decode(response.body);
      var success = InvalidForgotPassword.fromJson(parsedJson);
      forgotPasswordList.add(ForgotPassword(message: success.message));

      _showDialog(forgotPasswordList[0].message.toString());
      emailController.clear();
      return ForgotPassword.fromJson(json.decode(response.body));
    } else if (response.statusCode == 404) {
      forgotPasswordList = List<ForgotPassword>();
      var parsedJson = json.decode(response.body);
      var error = ForgotPassword.fromJson(parsedJson);

      forgotPasswordList.add(ForgotPassword(message: error.message));
      _showErrorDialog(forgotPasswordList[0].message.toString(), "");
    } else {
      print(json.decode(response.statusCode.toString()));
      print(json.decode(response.body));
      invalidForgotPasswordList = List<InvalidForgotPassword>();
      var parsedJson = json.decode(response.body);
      var error = InvalidForgotPassword.fromJson(parsedJson);

      invalidForgotPasswordList
          .add(InvalidForgotPassword(data: error.data, message: error.message));
      _showErrorDialog(invalidForgotPasswordList[0].message.toString(),
          invalidForgotPasswordList[0].data.email[0].toString());
      // _showErrorDialog("", "The email has already been taken.");
//       //   print(response.body.toString());
      throw Exception('Failed to Process the email');
    }
  }

  _sendToServer() {
    if (_key.currentState.validate()) {
      // No any error in validation
      _key.currentState.save();

      _futureForgotPassword = userForgotPassword(emailController.text);
      loadProgress(true);

      print("email $email");
    } else {
      // validation error
      setState(() {
        _validate = true;
      });
    }
  }

  //validateEmail
  String validateEmail(String value) {
    String pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = new RegExp(pattern);
    if (value.length == 0) {
      return "Email is Required";
    } else if (!regExp.hasMatch(value)) {
      return "Invalid Email";
    } else {
      return null;
    }
  }
}
