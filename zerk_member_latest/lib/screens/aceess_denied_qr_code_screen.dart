import 'package:flutter/material.dart';

class AceessDenied extends StatefulWidget {
  @override
  _AceessDeniedState createState() => _AceessDeniedState();
}

class _AceessDeniedState extends State<AceessDenied> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/images/login_bg.png'),
              fit: BoxFit.cover,
              alignment: Alignment.topLeft),
        ),
        child: Stack(
          children: [
// Settings Title
            Positioned(
              top: Alignment.topCenter.x,
              child: Container(
                // color: Colors.orange,
                height: MediaQuery.of(context).size.height * .22,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width / 6,
                      // color: Colors.red,
                      child: InkWell(
                        onTap: () {
                          Navigator.pop(context, '/ScanQRCodeScreen');
                        },
                        child: SizedBox(
                          child: Icon(
                            Icons.arrow_back,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width / 1.5,

                      //   color: Colors.black,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            "Scan QR Code",
                            style: TextStyle(
                              fontSize: 22.0,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),

// Settings Card Widget
            Positioned(
              //   top: 110,
              left: Alignment.center.y,
              right: Alignment.center.y,
              top: Alignment.center.y,
              bottom: Alignment.center.y,

              child: Padding(
                padding: const EdgeInsets.only(top: 40.0),
                child: Align(
                  alignment: Alignment.center,
                  child: SingleChildScrollView(
                    child: Container(
                      //  alignment: Alignment.center,
                      width: MediaQuery.of(context).size.width,

                      //    color: Colors.amber,
                      //   height: MediaQuery.of(context).size.height * 80,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20.0),
                        child: SingleChildScrollView(
                          child: Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20),
                            ),
                            // elevation: 5,
                            shadowColor: Colors.grey,
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 30.0),
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  SizedBox(
                                    height: 40,
                                  ),
                                  SizedBox(
                                    width:
                                        MediaQuery.of(context).size.width / 7,
                                    height:
                                        MediaQuery.of(context).size.height / 14,
                                    child: Container(
                                      width:
                                          MediaQuery.of(context).size.width / 7,
                                      height:
                                          MediaQuery.of(context).size.height /
                                              14,
                                      //   color: Colors.black,
                                      child: Image.asset(
                                          'assets/images/aceessdenied.png'),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  Align(
                                    alignment: Alignment.center,
                                    child: Container(
                                      child: Text(
                                        "Access Denied",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          fontSize: 28.0,
                                          color: Colors.orange[700],
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  Container(
                                    //   color: Colors.black,
                                    height:
                                        MediaQuery.of(context).size.width / 2,
                                    width:
                                        MediaQuery.of(context).size.width / 2,
                                    child: SizedBox(
                                      height:
                                          MediaQuery.of(context).size.width /
                                              2.5,
                                      width:
                                          MediaQuery.of(context).size.width / 2,
                                      child: Image.asset(
                                          'assets/images/aceessdenied_artwork.png'),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 50,
                                  ),
                                  Align(
                                    alignment: Alignment.center,
                                    child: Container(
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          InkWell(
                                            onTap: () {
                                              Navigator.pushNamed(
                                                  context, '/AceessGranted');
                                            },
                                            child: Text("Payment Failed",
                                                style: TextStyle(
                                                  fontSize: 20.0,
                                                  letterSpacing: 1,
                                                  fontWeight: FontWeight.bold,
                                                )),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 40,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
