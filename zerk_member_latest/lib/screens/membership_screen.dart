import 'dart:convert';
import 'dart:io';

import 'package:Zerk/CancellationNetwork.dart';
import 'package:Zerk/model/model_CancelMembership.dart';
import 'package:Zerk/model/model_InvaildCreateCardToken.dart';
import 'package:Zerk/model/model_InvaildTransactionStore.dart';
import 'package:Zerk/model/model_TransactionStore.dart';
import 'package:Zerk/model/model_all_membership_pakages.dart';
import 'package:Zerk/model/model_stripeAPIChargeModel.dart';
import 'package:Zerk/model/model_userDetails.dart';
import 'package:Zerk/screens/profile_widget.dart';
import 'package:Zerk/utils/baseurl.dart';
import 'package:Zerk/utils/checkbox_group.dart';
import 'package:Zerk/utils/grouped_buttons_orientation.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:localstorage/localstorage.dart';
import 'package:passbase_flutter/passbase_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

// import 'package:zerk_app/CancellationNetwork.dart';
// import 'package:zerk_app/model/model_CancelMembership.dart';
// import 'package:zerk_app/model/model_InvaildCreateCardToken.dart';
// import 'package:zerk_app/model/model_InvaildTransactionStore.dart';
// import 'package:zerk_app/model/model_TransactionStore.dart';
// import 'package:zerk_app/model/model_all_membership_pakages.dart';
// import 'package:zerk_app/model/model_stripeAPIChargeModel.dart';
// import 'package:zerk_app/model/model_userDetails.dart';
// import 'package:zerk_app/screens/profile_widget.dart';
// import 'package:zerk_app/utils/baseurl.dart';
// import 'package:zerk_app/utils/checkbox_group.dart';
// import 'package:zerk_app/utils/grouped_buttons_orientation.dart';

import 'settings_screen.dart';

class Membership extends StatefulWidget {
  Membership({Key key}) : super(key: key);

  @override
  _MembershipState createState() => _MembershipState();
}

class _MembershipState extends State<Membership> {
  final ScrollController _scrollController = ScrollController();
  Future<AllMembershipPakages> futureAllMembershipPakages;
  Future<TransactionStore> _futureTransactionStore;
  Future<UserDetails> _futureUserDetailsData;
  List<UserDetails> userDetailsData;
  List<InvaildTransactionStore> invaildTransactionStoreList;
  Future<CancelMembership> _futureCancelMembership;
  List<AllMembershipPakages> allMembershipPakagesList;
  bool isStatusButtonVisible = false;
  Baseurl baseurl = Baseurl();
  String payamount = '';
  String userid = "",
      membershipid = "",
      status = "",
      userMembershipId = "",
      cancelSubscription = "",
      yearlyCancel = "",
      messageCanceled = "";
  String cancelMembershipDate = '2021-05-04';
  final LocalStorage storage = new LocalStorage('token');
  String passBaseStatus = 'Pending';
  String cancelSubscriptionMember,
      memberCheckSubscription,
      createCustomersCardId,
      stripeCustomerID;
  bool visible;
  bool isPassBaseButtonVisible = true;
  bool cancelVisible = true;
  String token, expiredAt;
  // bool isUserVerified = true;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool circularProgressIndicatorVisible = false;
  bool isUserVerified = false;
  void _settingModalBottomSheet(context, contentstring) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: Wrap(
              children: <Widget>[
                // Row(
                //   mainAxisAlignment: MainAxisAlignment.center,
                //   crossAxisAlignment: CrossAxisAlignment.center,
                //   children: [
                //     Text(
                //       "$contentstring",
                //       textAlign: TextAlign.justify,
                //       // maxLines: 2,
                //       style: TextStyle(
                //           color: Colors.orange[700],
                //           fontWeight: FontWeight.bold),
                //
                //       // "Sign Up Successfully",
                //     ),
                //   ],
                // ),
                ListTile(
                    leading: Icon(
                      Icons.add_alert,
                      color: Colors.orange[500],
                    ),
                    title: Text(
                      "$contentstring",
                      textAlign: TextAlign.justify,
                      // maxLines: 2,
                      style: TextStyle(
                          color: Colors.orange[700],
                          fontWeight: FontWeight.bold),

                      // "Sign Up Successfully",
                    ),
                    onTap: () => {}),
              ],
            ),
          );
        });
  }

  void showInSnackBar(BuildContext context) {
    Scaffold(
      key: _scaffoldKey,
      body: GestureDetector(
        onTap: () {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: const Text('snack'),
            backgroundColor: Colors.red,
            duration: const Duration(seconds: 1),
            action: SnackBarAction(
              label: 'ACTION',
              onPressed: () {},
            ),
          ));
        },
        child: const Text('SHOW SNACK'),
      ),
    );
  }

  String userVerificationStatus;

  Future<void> dataRetrieved;

  Future getPassBaseVerificationResponse(String key) async {
    print('this is key $key');
    var response = await http.get(
        Uri.parse('https://api.passbase.com/verification/v1/identities/$key'),
        headers: {
          'X-API-KEY':
              'fJwSf6GHidhMymGj1sNlT1Ko0H6uXl1aSEvHlwV1YEL0VXa9BdRVDYR5P0yK4d5l0OJDnGLHxRJBWk0dN66T5awqep3JHJk7mO73Kcs2CY17QyFDe5joVRRaW8rfv0fT'
        });
    var jsonBody = jsonDecode(response.body);
    print('PASSBASE RESPONSE ${response.body}');
    var status = jsonBody['status'];
    userVerificationStatus = jsonBody['status'];
    // userVerificationStatus = status;
    if (userVerificationStatus == 'approved') {
      setState(() {
        isUserVerified = true;
        isPassBaseButtonVisible = false;
        userVerificationStatus = 'Verification Done.';
      });
      // Fluttertoast.showToast(msg: 'Congratulations!');
    } else if (userVerificationStatus == '' ||
        userVerificationStatus == 'processing') {
      setState(() {
        isUserVerified = false;
        isPassBaseButtonVisible = false;
        userVerificationStatus =
            'Your verification is in process, please wait.';
      });
    } else if (userVerificationStatus == 'declined') {
      setState(() {
        isUserVerified = false;
        isPassBaseButtonVisible = true;
        userVerificationStatus = 'Verification declined, try again';
      });
    }
    setState(() {
      isStatusButtonVisible = true;
    });
    Fluttertoast.showToast(msg: 'verification $userVerificationStatus');
    var score = jsonBody['score'];
    print('this is status $status');
    // print('this is score $score');
    print('this is response ${response.body}');
    if (response.statusCode == 200) {
      print('this is passbase response ${response.body}');
    } else {
      print('this is failed response ${response.body}');
    }
  }

  String userEmail;

  getPassBaseData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var key = prefs.getString('verificationKeyPB');
    print('this is key $key');
    getPassBaseVerificationResponse(key);
  }

  getEmail() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    userEmail = preferences.getString('email');
    return;
  }

  @override
  void initState() {
    super.initState();
    getEmail();
    // userVerificationStatus =
    //     'You are not verified user. To verify click on button below !';
    // getMapCategories();
    // SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    getPassBaseData();
    PassbaseSDK.initialize(
        publishableApiKey:
            "FP5xWQy10rjRM7n1ghzXzvaSrz8d1jlie3ccx2hqohdAt19fdH2mnMVokisdAZCv");
    PassbaseSDK.prefillUserEmail = userEmail.toString();

    token = storage.getItem('token');
    userid = storage.getItem('userid').toString();
    // userMembershipId = storage.getItem('userMembershipId').toString();

    _futureUserDetailsData = userDetailsDataApi(token);
    futureAllMembershipPakages = fetchAllMembershipPakages(userid);
    // StripeService.init();
    // checkMemberSubscription();

    // Network network = Network();
    // dataRetrieved = network.getRetrieveData(userid);
  }

  // checkMemberSubscription() {
  //   setState(() {
  //     member = storage.getItem('member');
  //     cancelSubscriptionMember = storage.getItem('cancelSubscription');
  //   });
  // }

  onItemPress(BuildContext context, int index, String amount,
      String membershipid) async {
    switch (index) {
      case 0:
        // payViaNewCard(context, amount, membershipid);
        break;
      // case 1:
      //   Navigator.pushNamed(context, '/existing-cards');
      //   break;
    }
  }

  bool _isChecked = false;
  _onReset() {
    setState(() {
      _isChecked = false;
    });
  }

  setStringValuesSF(String key, String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(key, value);
    print("getStringValuesSF: $value");
    //  prefs.setString('stringValue', "abc");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(80.0), // here the desired height
        child: Container(
          color: Colors.orange[800],
          height: MediaQuery.of(context).size.height * .22,
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  width: MediaQuery.of(context).size.width / 8,
                  //   color: Colors.red,
                  child: InkWell(
                    onTap: () {
                      Navigator.pop(context, '/BottomNavBar');
                    },
                    child: SizedBox(
                      child: Icon(
                        Icons.arrow_back,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width / 1.5,
                  //   color: Colors.black,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        "Membership",
                        style: TextStyle(
                          fontSize: 22.0,
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
                InkWell(
                  onTap: () {
                    Navigator.of(context).pushReplacement(
                        MaterialPageRoute(builder: (context) => Membership()));
                  },
                  child: Icon(
                    Icons.refresh,
                    color: Colors.white,
                  ),
                )
              ],
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 20.0, left: 20.0),
                    child: Text(
                      " Select a membership",
                      style: TextStyle(
                        fontSize: 22.0,
                        color: Colors.orange[700],
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 15, vertical: 8),
                child: FutureBuilder<AllMembershipPakages>(
                  future: futureAllMembershipPakages,
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      if (snapshot.data.data.isEmpty) {
                        return Container(
                          color: Colors.transparent,
                          child: Center(
                            child: Text(
                              'No Membership Package is Available ',
                              style: TextStyle(
                                  color: Colors.orange,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18.0),
                            ),
                          ),
                        );
                      }
                      AllMembershipPakages allMembershipPakages = snapshot.data;
                      print(snapshot.data.data[0].name.toString());
                      return
                          // Text(snapshot.data.data[0].name.toString());
                          ListView.builder(
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              controller: _scrollController,
                              itemCount: snapshot.data.data.length,
                              itemBuilder: (context, index) {
                                return Container(
                                  height:
                                      MediaQuery.of(context).size.height * .130,
                                  width: MediaQuery.of(context).size.width,
                                  margin: EdgeInsets.symmetric(
                                      vertical: 3.0, horizontal: 5.0),
                                  decoration: BoxDecoration(
                                    //color: appTheme.primaryColor,
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(30.0),
                                    ),
                                    // boxShadow: [
                                    //   BoxShadow(
                                    //     color: Colors.white,
                                    //     blurRadius: 10.0,
                                    //     spreadRadius: 10.0,
                                    //
                                    //     offset: Offset(0.0, 5.0),
                                    //   ),
                                    // ],
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.only(top: 10.0),
                                    child: RaisedButton(
                                      color: Colors.white,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(30.0),
                                          side: BorderSide(
                                            color: Colors.grey,
                                          )),

                                      padding:
                                          EdgeInsets.symmetric(horizontal: 0.0),
                                      //   rounded: 10.0,
                                      onPressed: () {
                                        print(index);
                                        // Navigator.pushNamed(context, '/itemPage');
                                      },
                                      child: Row(
                                        children: <Widget>[
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: <Widget>[
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 18.0),
                                                child: userMembershipId ==
                                                        snapshot
                                                            .data.data[index].id
                                                            .toString()
                                                    ? Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .only(
                                                                bottom: 10.0,
                                                                left: 10.0),
                                                        child: Checkbox(
                                                            value: true,
                                                            checkColor: Colors
                                                                .white, // color of tick Mark
                                                            activeColor: Colors
                                                                .orange[600],
                                                            onChanged:
                                                                (bool value) {
                                                              setState(() {
                                                                // if (cancelVisible ==
                                                                //     false) {
                                                                //   value =
                                                                //       cancelVisible;
                                                                // }
                                                              });
                                                            }),
                                                      )
                                                    : CheckboxGroup(
                                                        orientation:
                                                            GroupedButtonsOrientation
                                                                .VERTICAL,
                                                        activeColor:
                                                            Colors.orange[600],
                                                        checkColor:
                                                            Colors.white,
                                                        labels: <String>[
                                                          "",
                                                        ],
                                                        onChange:
                                                            (bool isChecked,
                                                                String label,
                                                                int indexs) {
                                                          _isChecked =
                                                              isChecked;
                                                          // print(
                                                          //     "isChecked: $isChecked   label: $label  index: $index" +
                                                          //         index.toString());
                                                          if (userMembershipId ==
                                                              "0") {
                                                            // _showDialog(
                                                            //     "if", false);

                                                            if (isChecked) {
                                                              print(
                                                                  '<-isChecked----------------------->');
                                                              print(isChecked);

                                                              _showMemberDialog(
                                                                  snapshot
                                                                      .data
                                                                      .data[
                                                                          index]
                                                                      .name
                                                                      .toString(),
                                                                  snapshot
                                                                      .data
                                                                      .data[
                                                                          index]
                                                                      .agreementLink
                                                                      .toString(),
                                                                  index,
                                                                  userid,
                                                                  allMembershipPakages
                                                                      .data[
                                                                          index]
                                                                      .id
                                                                      .toString(),
                                                                  allMembershipPakages
                                                                      .data[
                                                                          index]
                                                                      .amount
                                                                      .toString()
                                                                      // '00'
                                                                      .toString(),
                                                                  allMembershipPakages
                                                                      .data[
                                                                          index]
                                                                      .name
                                                                      .toString());
                                                            } else {
                                                              print(isChecked);
                                                            }
                                                          } else {
                                                            // _showDialog(
                                                            //     "Already Have Subscription\nYour membership is expiring \n on $cancelMembershipDate",
                                                            //     false);
                                                            _showDialog(
                                                                "You currently have an active membership subscription",
                                                                false);
                                                          }
                                                        },
                                                        onSelected:
                                                            (List<String>
                                                                checked) {
                                                          print(
                                                              '<-onSelected----------------------->');
                                                          storage.setItem(
                                                              "amount",
                                                              double.parse(
                                                                  allMembershipPakages
                                                                      .data[
                                                                          index]
                                                                      .amount
                                                                      .toString()
                                                                  // +
                                                                  // '00'
                                                                  )
                                                              // .toStringAsFixed(
                                                              //     1)
                                                              // '00'.toString()
                                                              );
                                                          storage.setItem(
                                                              "membershipid",
                                                              allMembershipPakages
                                                                  .data[index]
                                                                  .id
                                                                  .toString());
                                                          storage.setItem(
                                                              "membershipName",
                                                              allMembershipPakages
                                                                  .data[index]
                                                                  .name
                                                                  .toString());

                                                          print(index);

                                                          print(
                                                              allMembershipPakages
                                                                  .data[index]
                                                                  .amount
                                                                  .toString());
                                                          print(
                                                              allMembershipPakages
                                                                  .data[index]
                                                                  .name
                                                                  .toString());
//                                                      Navigator.popAndPushNamed(context, '/Settings');
                                                        }),
                                              ),
                                              Container(
                                                // color: Colors.yellow,
                                                child: ConstrainedBox(
                                                  constraints: BoxConstraints(
                                                    minWidth: 40.0,
                                                    minHeight: 40.0,
                                                    maxHeight: 40.0,
                                                    maxWidth: 70.0,
                                                  ),
                                                  child: Center(
                                                    child: AutoSizeText(
                                                        '\$' +
                                                            double.parse(allMembershipPakages
                                                                    .data[index]
                                                                    .amount
                                                                    .toString())
                                                                .toString() +
                                                            '0',
                                                        textAlign:
                                                            TextAlign.center,
                                                        style: TextStyle(
                                                          fontSize: 22.0,
                                                          color: Colors
                                                              .orange[800],
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        )
                                                        //  style: kCollectionItemName,
                                                        ),
                                                  ),
                                                ),
                                              ),
                                              // AutoSizeText(
                                              //     '\$' +
                                              //         double.parse(
                                              //                 allMembershipPakages
                                              //                     .data[index]
                                              //                     .amount
                                              //                     .toString())
                                              //             .toString(),
                                              //     textAlign: TextAlign.center,
                                              //     style: TextStyle(
                                              //       fontSize: 25.0,
                                              //       color: Colors.orange[800],
                                              //       fontWeight: FontWeight.bold,
                                              //     )
                                              //     //  style: kCollectionItemName,
                                              //     ),
                                              Padding(
                                                padding: EdgeInsets.only(
                                                    // left: 10.0,
                                                    // right: 20.0,
                                                    top: 0.0),
                                                child: Container(
                                                  // color: Colors.red,
                                                  child: ConstrainedBox(
                                                    constraints: BoxConstraints(
                                                      minWidth: 100.0,
                                                      minHeight: 100.0,
                                                      maxHeight: 200.0,
                                                      maxWidth: 150.0,
                                                    ),
                                                    child: Center(
                                                      child: AutoSizeText(
                                                        // 'WEEKLY X1 DAILY ACCESS\n(NO CONTRACT)',
                                                        allMembershipPakages
                                                            .data[index].name
                                                            .toString(),
                                                        textAlign:
                                                            TextAlign.center,

                                                        // maxLines: 3,
                                                        style: TextStyle(
                                                          fontSize: 14.0,
                                                          color: Colors.grey,
                                                          fontWeight:
                                                              FontWeight.normal,
                                                        ),
                                                        //style: kCollectionItemPrice,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                );
                              });
                    } else if (snapshot.hasError) {
                      return Text("${snapshot.error}");
                    }

                    // By default, show a loading spinner.
                    return Center(child: showProgressLoader(context));
                  },
                ),
              ),
              SizedBox(
                height: 0,
              ),
              Container(
                alignment: Alignment.center,
                // top: Alignment.center.y,
                // bottom: Alignment.center.x,
                // left: Alignment.center.x,
                // right: Alignment.center.y,
                child: Visibility(
                  visible: circularProgressIndicatorVisible,
                  child: Center(
                    child: CircularProgressIndicator(
                      color: Colors.orange[800],
                    ),
                  ),
                ),
              ),
              // Padding( padding: const EdgeInsets.only(top: 20.0),),
              Container(
                // color: Colors.black,
                alignment: Alignment.bottomCenter,
                height: MediaQuery.of(context).size.height / 3,
                width: MediaQuery.of(context).size.width,
                child: Column(
                  children: [
                    // Container(
                    //   // alignment: Alignment.bottomCenter,
                    //   // color: Colors.indigo,
                    //   margin: const EdgeInsets.only(
                    //       left: 35.0, right: 35.0, top: 0.0, bottom: 10.0),
                    //   width: MediaQuery.of(context).size.width,
                    //   height: MediaQuery.of(context).size.height / 12,
                    //   child: RaisedButton(
                    //     shape: RoundedRectangleBorder(
                    //         borderRadius: BorderRadius.circular(28.0),
                    //         side: BorderSide(color: Colors.orange[800])),
                    //     onPressed: () {
                    //       Navigator.push(
                    //         context,
                    //         MaterialPageRoute(
                    //             fullscreenDialog: true,
                    //             builder: (context) =>
                    //                 TransactionHistoryScreen()),
                    //       );
                    //     },
                    //     color: Colors.orange[800],
                    //     textColor: Colors.white,
                    //     child: Text("View transaction history",
                    //         style: TextStyle(fontSize: 20)),
                    //   ),
                    // ),

                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 26.0),
                      child: Text(
                        userVerificationStatus ?? '',
                        style: TextStyle(
                            color: Colors.black,
                            // fontSize: 18,
                            fontWeight: FontWeight.normal),
                        textAlign: TextAlign.start,
                      ),
                    ),
                    Visibility(
                      visible: isPassBaseButtonVisible,
                      child: PassbaseButton(
                        onFinish: (identityAccessKey) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          getPassBaseVerificationResponse(identityAccessKey);
                          prefs.setString(
                              'verificationKeyPB', identityAccessKey);
                          print(
                              'this is stored key ${prefs.getString(prefs.getString('verificationKeyPB'))}');
                          /////////////////////////
                          // Fluttertoast.showToast(msg: 'value $isUserNotVerified');
                          print(
                              'THIS IS ON FINISH PASS BASE RESPONSE $identityAccessKey');
                          print(identityAccessKey);
                          setState(() {
                            isPassBaseButtonVisible = false;
                          });
                          setState(() {
                            // isUserVerified = true;
                            // SharedPreferences prefs =
                            //     await SharedPreferences.getInstance();
                            // prefs.setBool('isUserVerified', isUserVerified);
                          });
                          // Navigator.of(context).push(MaterialPageRoute(
                          //     builder: (context) => Membership()));
                        },
                        onSubmitted: (identityAccessKey) {
                          print(
                              'THIS IS SUBMIT PASS BASE RESPONSE $identityAccessKey');
                          print(identityAccessKey);
                        },
                        onError: (errorCode) {
                          print('THIS IS ERROR $errorCode');
                          Fluttertoast.showToast(
                              msg: 'verification failed. try again');
                          print(errorCode);
                        },
                        onStart: () {
                          print("start");
                        },
                        width: 300,
                        height: 70,
                      ),
                    ),
                    cancelSubscription == '1' &&
                            userMembershipId == '0' &&
                            yearlyCancel == '0'
                        // Purchase Membership
                        ? Visibility(
                            visible: isUserVerified,
                            child: Container(
                              margin: const EdgeInsets.only(
                                  left: 35.0,
                                  right: 35.0,
                                  top: 10.0,
                                  bottom: 10.0),
                              width: MediaQuery.of(context).size.width,
                              height: MediaQuery.of(context).size.height / 12,
                              child: RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(28.0),
                                    side: BorderSide(
                                      color: Colors.orange[800],
                                    )),
                                onPressed: () {
                                  // if (isUserVerified == true) {

                                  //////////////////
                                  if (memberCheckSubscription == '1' &&
                                      cancelSubscriptionMember == '0') {
                                    print(
                                        "updatestripecard---------------------");

                                    print(memberCheckSubscription);
                                    print(cancelSubscriptionMember);

                                    // _showDialog("hahahahahha", false);
                                    String memberShipName = storage
                                        .getItem('membershipName')
                                        .toString();
                                    // double memberShipAmount =
                                    //     // storage.getItem('amount').toString();
                                    //     double.parse(storage.getItem('amount'));

                                    int stripeAmount =
                                        storage.getItem('amount').toInt();

                                    createCustomersCardId = storage
                                        .getItem("CreateCustomersCardId")
                                        .toString();
                                    if (storage.getItem("stripeCustomerID") !=
                                        null) {
                                      stripeCustomerID =
                                          storage.getItem("stripeCustomerID");
                                    } else {
                                      stripeCustomerID = '';
                                    }
                                    print(stripeAmount);
                                    print(memberShipName);
                                    print(createCustomersCardId);
                                    print(stripeCustomerID);
                                    loadProgress(true);
                                    stripeAPICharge(
                                        stripeAmount.toString() + "00",
                                        'USD',
                                        memberShipName,
                                        createCustomersCardId,
                                        stripeCustomerID);
                                  } else {
                                    myMaterialPageRoute(
                                      userid,
                                      storage
                                          .getItem('membershipid')
                                          .toString(),
                                      storage.getItem('amount'),
                                      // storage.getItem('amount').toString(),
                                      storage
                                          .getItem('membershipName')
                                          .toString(),
                                    );
                                  }
                                },
                                // else {
                                //   Fluttertoast.showToast(
                                //       msg: 'verify your self first');
                                //   }
                                // },
                                color: Colors.orange[800],
                                textColor: Colors.white,
                                child: Text("Purchase Membership",
                                    style: TextStyle(fontSize: 20)),
                              ),
                            ),
                          )
                        : Container(),

                    cancelSubscription == '0' &&
                            userMembershipId == '0' &&
                            yearlyCancel == '0'
                        // Purchase Membership
                        ? Visibility(
                            visible: isUserVerified,
                            child: Container(
                              margin: const EdgeInsets.only(
                                  left: 35.0,
                                  right: 35.0,
                                  top: 10.0,
                                  bottom: 10.0),
                              width: MediaQuery.of(context).size.width,
                              height: MediaQuery.of(context).size.height / 12,
                              child: RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(28.0),
                                    side: BorderSide(
                                      color: Colors.orange[800],
                                    )),
                                onPressed: () {
                                  //////////////////////
                                  // if (isUserNotVerified) {
                                  //   Fluttertoast.showToast(
                                  //       msg: 'verify yourself first');
                                  // }
                                  //////////////////////
                                  // if (isUserVerified == true) {
                                  if (memberCheckSubscription == '1' &&
                                      cancelSubscriptionMember == '0') {
                                    print(
                                        "updatestripecard---------------------1");

                                    print(memberCheckSubscription);
                                    print(cancelSubscriptionMember);

                                    // _showDialog("hahahahahha", false);
                                    String memberShipName = storage
                                        .getItem('membershipName')
                                        .toString();
                                    // double memberShipAmount =
                                    //     // storage.getItem('amount').toString();
                                    //     double.parse(storage.getItem('amount'));

                                    int stripeAmount =
                                        storage.getItem('amount').toInt();
                                    createCustomersCardId = storage
                                        .getItem("CreateCustomersCardId")
                                        .toString();
                                    if (storage.getItem("stripeCustomerID") !=
                                        null) {
                                      stripeCustomerID =
                                          storage.getItem("stripeCustomerID");
                                    } else {
                                      stripeCustomerID = '';
                                    }
                                    print(stripeAmount);
                                    print(memberShipName);
                                    print(createCustomersCardId);
                                    print(stripeCustomerID);
                                    loadProgress(true);
                                    stripeAPICharge(
                                        stripeAmount.toString() + "00",
                                        'USD',
                                        memberShipName,
                                        createCustomersCardId,
                                        stripeCustomerID);
                                  } else {
                                    myMaterialPageRoute(
                                      userid,
                                      storage
                                          .getItem('membershipid')
                                          .toString(),
                                      storage.getItem('amount'),
                                      // storage.getItem('amount').toString(),
                                      storage
                                          .getItem('membershipName')
                                          .toString(),
                                    );
                                  }
                                },
                                // else {
                                //   Fluttertoast.showToast(
                                //       msg: 'verify yourself first');
                                // }
                                // },
                                color: Colors.orange[800],
                                textColor: Colors.white,
                                child: Text("Purchase Membership",
                                    style: TextStyle(fontSize: 20)),
                              ),
                            ),
                          )
                        : Container(),

                    cancelSubscription == '0' &&
                            userMembershipId != '0' &&
                            yearlyCancel == '0'
                        ?
                        // Membership Cancel
                        Visibility(
                            visible: cancelVisible,
                            child: Container(
                              margin: const EdgeInsets.only(
                                  left: 35.0,
                                  right: 35.0,
                                  top: 10.0,
                                  bottom: 10.0),
                              width: MediaQuery.of(context).size.width,
                              height: MediaQuery.of(context).size.height / 12,
                              child: RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(25.0),
                                    side: BorderSide(
                                      // color: Colors.orange[800],
                                      color: Colors.orange[800],
                                    )),
                                onPressed: () {
                                  print('Here is what u need');
                                  showDialog(
                                      barrierDismissible: true,
                                      context: context,
                                      builder: (context) {
                                        return AlertDialog(
                                          title: Text(
                                            'Confirmation',
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          content: Text(
                                              'Are you sure you want to cancel subscription ??'),
                                          actions: [
                                            InkWell(
                                                onTap: () {
                                                  Navigator.of(context).pop();
                                                },
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.all(8.0),
                                                  child: Text(
                                                    'Cancel',
                                                    style: TextStyle(
                                                        color: Colors.blue),
                                                  ),
                                                )),
                                            InkWell(
                                                onTap: () {
                                                  if (userid != null) {
                                                    setState(() {
                                                      _futureCancelMembership =
                                                          cancelMembership(
                                                              userid);
                                                    });
                                                  }
                                                  Navigator.of(context).pop();
                                                },
                                                child: Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 10),
                                                  child: Text(
                                                    'Ok',
                                                    style: TextStyle(
                                                        color: Colors.red),
                                                  ),
                                                ))
                                          ],
                                        );
                                      });
                                  // if (userid != null) {
                                  //   setState(() {
                                  //     _futureCancelMembership =
                                  //         cancelMembership(userid);
                                  //   });
                                  // }
                                },
                                // color: Colors.orange[800],
                                textColor: Colors.white,
                                color: Colors.orange[800],
                                child: Text("Membership Cancel",
                                    style: TextStyle(fontSize: 20)),
                              ),
                            ),
                          )
                        : Container(),

                    cancelSubscription != '0' &&
                            userMembershipId != '0' &&
                            yearlyCancel == '0'
                        //  Canceled a membership
                        ? Container(
                            margin: const EdgeInsets.only(
                                left: 35.0, right: 35.0, top: 0.0, bottom: 0.0),
                            width: MediaQuery.of(context).size.width,
                            // height: MediaQuery.of(context).size.height / 12,
                            height: MediaQuery.of(context).size.height / 5,
                            // color: Colors.redAccent,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  // color: Colors.redAccent,
                                  height:
                                      MediaQuery.of(context).size.height / 14,
                                  width: MediaQuery.of(context).size.width,
                                  // margin: const EdgeInsets.only(
                                  //     left: 25.0,
                                  //     right: 25.0,
                                  //     top: 0.0,
                                  //     bottom: 0.0),
                                  child: RaisedButton(
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(25.0),
                                        side: BorderSide(
                                          // color: Colors.orange[800],
                                          color: Colors.grey[400],
                                        )),
                                    onPressed: () {
                                      // _showDialog(
                                      //     "Your membership is expiring\n on $cancelMembershipDate",
                                      //     false);
                                      // showInSnackBar(context);

                                      // you have Canceled your Membership and you can't subscribe any Membership till
                                      // _settingModalBottomSheet(context,
                                      //     "Your membership is expiring\n on $expiredAt");

                                      // SnackBarPage(
                                      //   text:
                                      //       "Your membership is expiring\n on $expiredAt",
                                      // );

                                      // _showDialog(
                                      //     "Your membership is expiring\n on $expiredAt",
                                      //     false);
                                    },
                                    // color: Colors.orange[800],
                                    textColor: Colors.white,
                                    color: Colors.grey[400],
                                    child: Text("Membership Canceled",
                                        style: TextStyle(fontSize: 20)),
                                  ),
                                ),
                                // Padding(padding: EdgeInsets.only(top: 5.0)),
                                Container(
                                  // color: Colors.redAccent,
                                  // height:
                                  //     MediaQuery.of(context).size.height / 15,
                                  child: ListTile(
                                      leading: Icon(
                                        Icons.add_alert,
                                        color: Colors.orange[500],
                                      ),
                                      // title: Text(
                                      //   yearlyCancel == '1'
                                      //       // ? "You have canceled your Membership and you can't subscribe any Membership till $expiredAt"
                                      //       ? "fhalsjflakjhflaksdjhfdl-$yearlyCancel"
                                      //       : yearlyCancel,
                                      //   textAlign: TextAlign.start,
                                      //   // maxLines: 2,
                                      //   style: TextStyle(
                                      //       color: Colors.orange[700],
                                      //       fontSize: 12,
                                      //       fontWeight: FontWeight.normal),
                                      //
                                      //   // "Sign Up Successfully",
                                      // ),
                                      onTap: () => {}),
                                ),
                                // Container(
                                //   color: Colors.redAccent,
                                //   height:
                                //       MediaQuery.of(context).size.height / 15,
                                //   width: MediaQuery.of(context).size.width,
                                //   child: ListTile(
                                //       leading: Icon(
                                //         Icons.add_alert,
                                //         color: Colors.orange[500],
                                //       ),
                                //       title: Text(
                                //         "you have Canceled your Membership and you can't subscribe any Membership till $expiredAt",
                                //         textAlign: TextAlign.start,
                                //         // maxLines: 2,
                                //         style: TextStyle(
                                //             color: Colors.orange[700],
                                //             fontSize: 12,
                                //             fontWeight: FontWeight.normal),
                                //
                                //         // "Sign Up Successfully",
                                //       ),
                                //       onTap: () => {}),
                                // ),
                              ],
                            ),
                          )
                        : Container(),

                    cancelSubscription != '0' &&
                            userMembershipId != '0' &&
                            yearlyCancel == '1'
                        //  Canceled a membership
                        ? Container(
                            margin: const EdgeInsets.only(
                                left: 35.0, right: 35.0, top: 0.0, bottom: 0.0),
                            width: MediaQuery.of(context).size.width,
                            // height: MediaQuery.of(context).size.height / 12,
                            height: MediaQuery.of(context).size.height / 5,
                            // color: Colors.redAccent,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  // color: Colors.redAccent,
                                  height:
                                      MediaQuery.of(context).size.height / 14,
                                  width: MediaQuery.of(context).size.width,
                                  // margin: const EdgeInsets.only(
                                  //     left: 25.0,
                                  //     right: 25.0,
                                  //     top: 0.0,
                                  //     bottom: 0.0),
                                  child: RaisedButton(
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(25.0),
                                        side: BorderSide(
                                          // color: Colors.orange[800],
                                          color: Colors.grey[400],
                                        )),
                                    onPressed: () {
                                      Network network = Network();
                                      network.getRetrieveData(userid);
                                      Navigator.of(context).push(
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  ProfileMenu()));

                                      // _showDialog(
                                      //     "Your membership is expiring\n on $cancelMembershipDate",
                                      //     false);
                                      // showInSnackBar(context);

                                      // you have Canceled your Membership and you can't subscribe any Membership till
                                      // _settingModalBottomSheet(context,
                                      //     "Your membership is expiring\n on $expiredAt");

                                      // SnackBarPage(
                                      //   text:
                                      //       "Your membership is expiring\n on $expiredAt",
                                      // );

                                      // _showDialog(
                                      //     "Your membership is expiring\n on $expiredAt",
                                      //     false);
                                    },
                                    // color: Colors.orange[800],
                                    color: Colors.orange[800],
                                    textColor: Colors.white,
                                    child: Text("Membership Retrieve",
                                        style: TextStyle(fontSize: 20)),
                                  ),
                                ),
                                // Padding(padding: EdgeInsets.only(top: 5.0)),
                                Container(
                                  // color: Colors.redAccent,
                                  // height:
                                  //     MediaQuery.of(context).size.height / 15,
                                  child: ListTile(
                                      leading: Icon(
                                        Icons.add_alert,
                                        color: Colors.orange[500],
                                      ),
                                      title: Text(
                                        yearlyCancel == '1'
                                            ? messageCanceled
                                            : "you have Canceled your Membership and you can't subscribe any Membership till $expiredAt",
                                        textAlign: TextAlign.start,
                                        // maxLines: 2,
                                        style: TextStyle(
                                            color: Colors.orange[700],
                                            fontSize: 12,
                                            fontWeight: FontWeight.normal),

                                        // "Sign Up Successfully",
                                      ),
                                      onTap: () => {}),
                                ),
                                // Container(
                                //   color: Colors.redAccent,
                                //   height:
                                //       MediaQuery.of(context).size.height / 15,
                                //   width: MediaQuery.of(context).size.width,
                                //   child: ListTile(
                                //       leading: Icon(
                                //         Icons.add_alert,
                                //         color: Colors.orange[500],
                                //       ),
                                //       title: Text(
                                //         "you have Canceled your Membership and you can't subscribe any Membership till $expiredAt",
                                //         textAlign: TextAlign.start,
                                //         // maxLines: 2,
                                //         style: TextStyle(
                                //             color: Colors.orange[700],
                                //             fontSize: 12,
                                //             fontWeight: FontWeight.normal),
                                //
                                //         // "Sign Up Successfully",
                                //       ),
                                //       onTap: () => {}),
                                // ),
                              ],
                            ),
                          )
                        : Container(),

                    cancelVisible == false
                        ? Container(
                            margin: const EdgeInsets.only(
                                left: 35.0,
                                right: 35.0,
                                top: 10.0,
                                bottom: 10.0),
                            width: MediaQuery.of(context).size.width,
                            height: MediaQuery.of(context).size.height / 12,
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(25.0),
                                  side: BorderSide(
                                    // color: Colors.orange[800],
                                    color: Colors.grey[400],
                                  )),
                              onPressed: () {},
                              // color: Colors.orange[800],
                              textColor: Colors.white,
                              color: Colors.grey[400],
                              child: Text("Membership Canceled",
                                  style: TextStyle(fontSize: 20)),
                            ),
                          )
                        : Container(),
                    //-----------------------------------------------------

                    //Membership Cancel
                    // Visibility(
                    //     visible: cancelVisible,
                    //     child: Container(
                    //       margin: const EdgeInsets.only(
                    //           left: 35.0,
                    //           right: 35.0,
                    //           top: 10.0,
                    //           bottom: 10.0),
                    //       width: MediaQuery.of(context).size.width,
                    //       height: MediaQuery.of(context).size.height / 12,
                    //       child: RaisedButton(
                    //         shape: RoundedRectangleBorder(
                    //             borderRadius: BorderRadius.circular(25.0),
                    //             side: BorderSide(
                    //               // color: Colors.orange[800],
                    //               color: Colors.orange[800],
                    //             )),
                    //         onPressed: () {
                    //           if (userid != null) {
                    //             setState(() {
                    //               _futureCancelMembership =
                    //                   cancelMembership(userid);
                    //             });
                    //           }
                    //         },
                    //         // color: Colors.orange[800],
                    //         textColor: Colors.white,
                    //         color: Colors.orange[800],
                    //         child: Text("Membership Cancel $userid",
                    //             style: TextStyle(fontSize: 20)),
                    //       ),
                    //     ),
                    //   ),

                    // should be show a Canceled a membership
                    // Container(
                    //     margin: const EdgeInsets.only(
                    //         left: 35.0,
                    //         right: 35.0,
                    //         top: 10.0,
                    //         bottom: 10.0),
                    //     width: MediaQuery.of(context).size.width,
                    //     height: MediaQuery.of(context).size.height / 12,
                    //     child: RaisedButton(
                    //       shape: RoundedRectangleBorder(
                    //           borderRadius: BorderRadius.circular(25.0),
                    //           side: BorderSide(
                    //             // color: Colors.orange[800],
                    //             color: Colors.grey[400],
                    //           )),
                    //       onPressed: () {},
                    //       // color: Colors.orange[800],
                    //       textColor: Colors.white,
                    //       color: Colors.grey[400],
                    //       child: Text("Membership Canceled",
                    //           style: TextStyle(fontSize: 20)),
                    //     ),
                    //   ),
                    // cancelVisible == false
                    //     ? Container(
                    //         margin: const EdgeInsets.only(
                    //             left: 35.0,
                    //             right: 35.0,
                    //             top: 10.0,
                    //             bottom: 10.0),
                    //         width: MediaQuery.of(context).size.width,
                    //         height: MediaQuery.of(context).size.height / 12,
                    //         child: RaisedButton(
                    //           shape: RoundedRectangleBorder(
                    //               borderRadius: BorderRadius.circular(25.0),
                    //               side: BorderSide(
                    //                 // color: Colors.orange[800],
                    //                 color: Colors.grey[400],
                    //               )),
                    //           onPressed: () {},
                    //           // color: Colors.orange[800],
                    //           textColor: Colors.white,
                    //           color: Colors.grey[400],
                    //           child: Text("Membership Canceled",
                    //               style: TextStyle(fontSize: 20)),
                    //         ),
                    //       )
                    //     : Container()
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  myMaterialPageRoute(String userId, String membershipId, double amount,
      String membershipName) {
    print(userId);
    print(membershipId);
    print(amount);
    if (visible == true) {
      print('true : ' + visible.toString());
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (context) => Settings(
                    userId: userid,
                    membershipId: membershipId,
                    amount: amount,
                    membershipName: membershipName,
                  )),
          ModalRoute.withName("/BottomNavBar"));
      _onReset();
    } else if (visible == false) {
      _showDialog(
          "Please view terms and conditions\nthen select membership", false);
    } else {
      _showDialog("Please select the membership first ", false);
      print('false : ' + visible.toString());
    }
    // return Navigator.push(
    //   context,
    //   MaterialPageRoute(
    //       builder: (context) => Settings(
    //             userId: userId,
    //             membershipId: membershipId,
    //             amount: amount,
    //           )),
    // );
  }

  loadProgress(bool value) {
    if (circularProgressIndicatorVisible == value) {
      setState(() {
        circularProgressIndicatorVisible = value;
      });
    } else {
      setState(() {
        circularProgressIndicatorVisible = value;
      });
    }
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url, forceWebView: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  Widget showProgressLoader(BuildContext context) {
    if (Platform.isAndroid) {
      return CircularProgressIndicator(
        color: Colors.orange[800],
      );
    }
    if (Platform.isIOS) {
      return CupertinoActivityIndicator();
    }
  }

  Future<CancelMembership> cancelMembership(String userId) async {
    final http.Response response = await http.post(
      Uri.parse(baseurl.baseurl + "member/cancel"),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'memberId': userId,
      }),
    );

    if (response.statusCode == 200) {
      var parsedJson = json.decode(response.body);
      var success = CancelMembership.fromJson(parsedJson);
      if (cancelVisible == true) {
        setState(() {
          cancelVisible = false;
          // _showDialog(
          //     "Your Current Membership will be\nCancelled on $cancelMembershipDate",
          //     false);
          _showDialog(
              "Your Current Membership will be\nCancelled on $expiredAt",
              false);
          // _showDialog("Membership Cancel\nSuccessfully", false);
        });
      } else {
        setState(() {
          cancelVisible = true;
        });
      }

      return CancelMembership.fromJson(jsonDecode(response.body));
    } else {
      _showDialog("Failed to cancel Membership", false);
      throw Exception('Failed to cancel Membership');
    }
  }

  Future<AllMembershipPakages> fetchAllMembershipPakages(String userId) async {
    final response = await http.get(
      Uri.parse(baseurl.baseurl + "memberships/all?customer_id=$userId"),
      headers: <String, String>{
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );

    if (response.statusCode == 200) {
      var parsedJson = json.decode(response.body);
      var success = AllMembershipPakages.fromJson(parsedJson);
      messageCanceled = success.membershipStatus.messageCanceled.toString();

      setState(() {
        storage.setItem('membership_status_membership_id',
            success.membershipStatus.membershipId.toString());
        storage.setItem('membership_status_cancel_subscription',
            success.membershipStatus.cancelSubscription.toString());

        userMembershipId =
            storage.getItem('membership_status_membership_id').toString();
        cancelSubscription =
            storage.getItem('membership_status_cancel_subscription').toString();
        expiredAt = storage.getItem('expired_at');
      });

      allMembershipPakagesList = List<AllMembershipPakages>();
      allMembershipPakagesList.add(AllMembershipPakages(data: success.data));

      print(response.statusCode);
      print(response.body);
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return AllMembershipPakages.fromJson(json.decode(response.body));
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load album');
    }
  }

  Future<StripeAPIChargeModel> stripeAPICharge(String amount, String currency,
      String description, String source, String stripeCustomerID) async {
    Map<String, dynamic> body = {
      'amount': amount,
      'currency': currency,
      'description': description,
      'source': source,
      'customer': stripeCustomerID,
    };
    final http.Response response = await http.post(
      Uri.parse('https://api.stripe.com/v1/charges'),
      headers: <String, String>{
//       'Content-Type': 'application/json',
        'Authorization':
            'Bearer sk_test_51HbQCpJrvceP60phuzzvHaxtXprQv8DRJpvS97ie0Qx7CYhySy2rvdMYy8bTFlnLlH6bA7PfBGwrq1g71jbsSPbu00PNg2wv0Z',
        'Content-Type': 'application/x-www-form-urlencoded',
      },

      body: body,
      encoding: Encoding.getByName("utf-8"),
//      jsonEncode(<String, String>{
//        'card[number]': number,
//        'card[exp_month]': exp_month,
//        'card[exp_year]': exp_year,
//        '': cvc,
//      }),
    );

    if (response.statusCode == 200) {
      print(body.toString());
      print('------StripeAPIChargeModel-----------');
      print(response.statusCode.toString());
      print(response.body.toString());
      var parsedJson = json.decode(response.body);
      var success = StripeAPIChargeModel.fromJson(parsedJson);
      storage.setItem("ch_id", success.id.toString());

      storage.setItem(
          "balance_transaction", success.balanceTransaction.toString());

      String membershipId = storage.getItem('membershipid').toString();

      print(success.balanceTransaction);
      print(success.id.toString());
      print(userid);
      print(membershipId);
      print(success.amount.toString());
      print(success.status.toString());
      print("===========storeTransaction==============");

      storeTransaction(
        // storage.getItem("tok_id"),
        // storage.getItem("ch_id"),
        success.balanceTransaction,
        success.id.toString(),
        userid,
        membershipId,
        success.amount.toString(),
        // "successful",
        // 'succeeded'
        success.status.toString(),
        // 'failed',
        // success.balanceTransaction,
        // cardnumberController.text.replaceAll(new RegExp(r"\s+"), "").toString(),
        // cvcController.text.toString(),
        // expirydateController.text.toString(),
      );

//      Scaffold.of(context).showSnackBar(SnackBar(
//        content: Text(success.status.toString()),
//        duration:
//        new Duration(milliseconds: success.status == 'succeeded' ? 1200 : 3000),
//      ));
      return StripeAPIChargeModel.fromJson(jsonDecode(response.body));
    } else {
      print('----Failed--stripeAPICharge-----------');
      print(body.toString());
      loadProgress(false);
      print(response.statusCode.toString());
      print(response.body.toString());
      var parsedJson = json.decode(response.body);
      var success = InvaildCreateCardTokenModel.fromJson(parsedJson);
      _showDialog(success.error.message, true);
      throw Exception('Failed to stripeAPICharge .');
    }
  }

  //----------------------storeTransaction-------------------------------
  Future<TransactionStore> storeTransaction(
    String pm_id,
    String pr_id,
    String customer_id,
    String membership_id,
    String amount,
    String status,
    // String balanceTransaction,
    // String card,
    // String cvc,
    // String expiryDate
  ) async {
    final http.Response response = await http.post(
      Uri.parse(baseurl.baseurl + "transaction/store"),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'pm_id': pm_id,
        'pr_id': pr_id,
        'customer_id': customer_id,
        'membership_id': membership_id,
        'amount': amount,
        'status': status,
        // 'card': card,
        // 'cvv': cvc,
        // 'card_expiry_date': expiryDate,
        // 'balance_transaction': balanceTransaction,
      }),
    );

    if (response.statusCode == 200) {
      var parsedJson = json.decode(response.body);
      var success = TransactionStore.fromJson(parsedJson);
//      ProgressDialog dialog = new ProgressDialog(context);
      print(
          '---------------------storeTransaction-------------------------------');
      print(response.statusCode.toString());
      print(response.body.toString());
      if (token != null) {
        userDetailsDataApi(token);
      } else {
        print('userDetailsDataApiiiiiiiiiiiiiiii');
      }
//      await dialog.hide();
//      _showDialog(success.message.toString(), true);

      loadProgress(false);

      // _showNotificationWithoutSound("Transaction Status Successful");

      String selectedDate = storage.getItem("selectedDate");
      String firstName = storage.getItem("firstnameController").toString();
      String lastName = storage.getItem("lastnameController").toString();
      String name = firstName.toString() + "" + lastName.toString();
      String phone = storage.getItem("cellPhoneController").toString();
      String email = storage.getItem("emailController").toString();
      String userId = storage.getItem('userid');
      String password = storage.getItem("password");
      String stripeCustomerID = storage.getItem("stripeCustomerID");
      String createCustomersCardId =
          storage.getItem("CreateCustomersCardId").toString();

      _showDialog('Transaction Store Successful', false);
      // Navigator.pushAndRemoveUntil(
      //     context,
      //     MaterialPageRoute(builder: (context) => BottomNavBar()),
      //     ModalRoute.withName("/BottomNavBar"));
      return TransactionStore.fromJson(jsonDecode(response.body));
    } else {
      loadProgress(false);
      print(
          '-------------------Failed--storeTransaction-------------------------------');
      print(response.statusCode.toString());
      print(response.body.toString());
      invaildTransactionStoreList = List<InvaildTransactionStore>();
      var parsedJson = json.decode(response.body);
      var success = InvaildTransactionStore.fromJson(parsedJson);
      // _showNotificationWithoutSound("Transaction status Failed");
      _showDialog('Transaction status Failed', false);
      throw Exception('Failed to Store Transaction');
    }
  }
  // Future<TransactionStore> storeTransaction(
  //     String pm_id,
  //     String pr_id,
  //     String customer_id,
  //     String membership_id,
  //     String amount,
  //     String status) async {
  //   final http.Response response = await http.post(
  //     baseurl.baseurl + "transaction/store",
  //     headers: <String, String>{
  //       'Content-Type': 'application/json; charset=UTF-8',
  //     },
  //     body: jsonEncode(<String, String>{
  //       'pm_id': pm_id,
  //       'pr_id': pr_id,
  //       'customer_id': customer_id,
  //       'membership_id': membership_id,
  //       'amount': amount,
  //       'status': status,
  //     }),
  //   );
  //
  //   if (response.statusCode == 200) {
  //     var parsedJson = json.decode(response.body);
  //     var success = TransactionStore.fromJson(parsedJson);
  //     _showDialog(success.message.toString(), false);
  //
  //     return TransactionStore.fromJson(jsonDecode(response.body));
  //   } else {
  //     throw Exception('Failed to Store Transaction');
  //   }
  // }

  //-------------UserDetailsDataAPI--------------------------------
  Future<UserDetails> userDetailsDataApi(String token) async {
    final response = await http.get(
      Uri.parse(baseurl.baseurl + "user/details"),
      headers: <String, String>{
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + token,
      },
    );

    if (response.statusCode == 200) {
      print(json.decode(response.body));
      userDetailsData = List<UserDetails>();
      storage.setItem("UserDetailsData", storage.getItem('token'));
      var parsedJson = json.decode(response.body);
      var success = UserDetails.fromJson(parsedJson);
      userDetailsData = List<UserDetails>();
      userDetailsData.add(UserDetails(data: success.data));
      print('||||||||' + userDetailsData[0].data.id.toString());
      print('||||||||' + userDetailsData[0].data.firstName.toString());
      print('||||||||' + userDetailsData[0].data.lastName.toString());
      print('||||||||' + userDetailsData[0].data.email.toString());
      print('||||||||' + userDetailsData[0].data.avatar.toString());

      setState(() {
        yearlyCancel = success.data.yearlyCancel.toString();
      });
      print('||||||||' + success.data.id.toString());
      print(
          '||||||||yearlyCancelyearlyCancelyearlyCancelyearlyCancelyearlyCancelyearlyCancelyearlyCancelyearlyCancelyearlyCancel' +
              yearlyCancel);
      print('||||||||' + success.data.firstName.toString());
      print('||||||||' + success.data.lastName.toString());
      print('||||||||' + success.data.email.toString());
      print('||||||||' + success.data.avatar.toString());
      print('||||||||' + success.data.expiredAt.toString());

      storage.setItem('userid', success.data.id.toString());
      storage.setItem('firstName', success.data.firstName.toString());
      storage.setItem('lastName', success.data.lastName.toString());
      storage.setItem('email', success.data.email.toString());
      storage.setItem('avatar', success.data.avatar.toString());
      storage.setItem('expired_at', success.data.expiredAt.toString());
      cancelSubscriptionMember = success.data.cancelSubscription.toString();
      memberCheckSubscription = success.data.member.toString();
      storage.setItem('member', success.data.member.toString());
      print(memberCheckSubscription);
      print(cancelSubscriptionMember);
      print("checkkasdjklasjdksajdklasjdkljaskdl");

      storage.setItem(
          'cancelSubscription', success.data.cancelSubscription.toString());
      expiredAt = storage.getItem('expired_at');
      // storage.setItem('totalAvails', success.data.totalAvails.toString());
      if (success.data.totalAvails != null) {
        print("isNOTequaltoNULL");
        storage.setItem('totalAvails', success.data.totalAvails.toString());
      } else {
        storage.setItem('totalAvails', "0");
        print("isEqualtoNULL");
      }
      setStringValuesSF('userid', success.data.id.toString());
      setStringValuesSF('firstName', success.data.firstName.toString());
      setStringValuesSF('lastName', success.data.lastName.toString());
      setStringValuesSF('email', success.data.avatar.toString());
      setStringValuesSF('avatar', success.data.avatar.toString());

      print(storage.getItem("avatar").toString() +
          storage.getItem("email").toString());

      return UserDetails.fromJson(json.decode(response.body));
    } else if (response.statusCode == 401) {
      String messageData = json.decode(response.body)['message'];
      print('$messageData');
      Navigator.popAndPushNamed(context, '/BottomNavBar');
    } else {
      print(response.statusCode.toString());
      print(response.body.toString());
      String messageData = json.decode(response.body)['message'] ?? '';
      String data = json.decode(response.body)['data'] ?? '';
      if (messageData == 'The access token is invalid.') {
        Navigator.popAndPushNamed(context, '/LoginScreen');
      } else {
        Navigator.popAndPushNamed(context, '/BottomNavBar');
      }
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load User Details Data Api');
    }
  }

  // loadProgress(bool value) {
  //   if (visible == value) {
  //     setState(() {
  //       visible = value;
  //     });
  //   } else {
  //     setState(() {
  //       visible = value;
  //     });
  //   }
  // }

  void _showMemberDialog(String name, String link, int index, String userId,
      String membershipId, String amount, String membershipName) {
    String string = "Please view terms and conditions";
    // loadProgress(false);
    visible = false;
    // flutter defined function
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        // return object of type Dialog
        return WillPopScope(
          onWillPop: () {
            if (!_isChecked) {
              Navigator.of(context).pop();
            } else {
              Fluttertoast.showToast(
                  msg: 'You are bound to agree with terms and conditions');
            }
            return;
          },
          child: AlertDialog(
            elevation: 5,

            // title: Text(
            //   "$titlestring",
            //   // "Sign Up Successfully",
            //   textAlign: TextAlign.center,
            //    style: TextStyle(
            //         color: Colors.orange[700],
            //       ),
            // ),
            content: Container(
              height: 220,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  // Container(
                  //   height: 20,
                  //   color: Colors.grey,
                  //   child:
                  // ),
                  ListTile(
                    // leading: Text('leading'),
                    title: Text(
                      "$string",
                      // "Click Details $name",
                      style: TextStyle(
                          fontSize: 15.0,
                          decoration: TextDecoration.underline,
                          color: Colors.orange),
                    ),
                    onTap: () {
                      _launchURL(link);
                      setState(() {
                        visible = true;
                      });
                    },
                  ),

                  // Visibility(
                  //   visible: visible,
                  //   child: ListTile(
                  //     // leading: Text('leading'),
                  //     title: Text(
                  //       " string",
                  //       // "Click Details $name",
                  //       style: TextStyle(
                  //           fontSize: 15.0,
                  //           decoration: TextDecoration.none,
                  //           color: Colors.orange),
                  //     ),
                  //     onTap: () {
                  //       // _launchURL(link);
                  //     },
                  //   ),
                  // ),

                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: CheckboxGroup(
                        orientation: GroupedButtonsOrientation.VERTICAL,
                        activeColor: Colors.orange[600],
                        checkColor: Colors.white,
                        labels: <String>[
                          "I agree to the \nterms and conditions",
                        ],
                        onChange: (bool isChecked, String label, int indexs) {
                          // print(
                          //     "isChecked: $isChecked   label: $label  index: $index" +
                          //         index.toString());
                          print(visible.toString());
                          if (isChecked) {
                            Navigator.pop(context);
                            // if (visible == true) {
                            //   print(visible);
                            //   // Navigator.push(
                            //   //   context,
                            //   //   MaterialPageRoute(
                            //   //       builder: (context) => Settings(
                            //   //             userId: userid,
                            //   //             membershipId: membershipid,
                            //   //             amount: amount,
                            //   //             membershipName: membershipName,
                            //   //           )),
                            //   // );
                            // } else {}
                          } else {
                            Navigator.of(context).pop();
                            print('this is checked');
                            print(isChecked);
                          }
                        },
                        onSelected: (List<String> checked) {}),
                  ),
                  Text(
                    "Note: “If you do not agree to being bound by the terms and conditions of the membership agreement, please leave the website”.",
                    textAlign: TextAlign.justify,
                    // maxLines: 2,
                    style: TextStyle(
                        color: Colors.orange[700], fontWeight: FontWeight.bold),

                    // "Sign Up Successfully",
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  void _showDialog(String contentstring, bool value) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        if (value == false) {
          print(value);
          Future.delayed(Duration(seconds: 3), () {
            Navigator.of(context).pop(true);
          });
        } else {}
        // return object of type Dialog
        return AlertDialog(
          elevation: 5,
          content: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                "$contentstring",
                textAlign: TextAlign.justify,
                // maxLines: 2,
                style: TextStyle(
                    color: Colors.orange[700], fontWeight: FontWeight.bold),
              ),
            ],
          ),
        );
      },
    );
  }
}

class SnackBarPage extends StatelessWidget {
  SnackBarPage({
    Key key,
    this.text,
  }) : super(key: key);

  String text;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ElevatedButton(
        onPressed: () {
          final snackBar = SnackBar(
            content: Text('Yay! A SnackBar!'),
            action: SnackBarAction(
              label: 'Undo',
              onPressed: () {
                // Some code to undo the change.
              },
            ),
          );

          // Find the ScaffoldMessenger in the widget tree
          // and use it to show a SnackBar.
          ScaffoldMessenger.of(context).showSnackBar(snackBar);
        },
        child: Text('Show SnackBar'),
      ),
    );
  }

  // buyMembership(){
  //   if (memberCheckSubscription == '1' &&
  //       cancelSubscriptionMember == '0') {
  //     print(
  //         "updatestripecard---------------------1");
  //
  //     print(memberCheckSubscription);
  //     print(cancelSubscriptionMember);
  //
  //     // _showDialog("hahahahahha", false);
  //     String memberShipName = storage
  //         .getItem('membershipName')
  //         .toString();
  //     // double memberShipAmount =
  //     //     // storage.getItem('amount').toString();
  //     //     double.parse(storage.getItem('amount'));
  //
  //     int stripeAmount =
  //     storage.getItem('amount').toInt();
  //     createCustomersCardId = storage
  //         .getItem("CreateCustomersCardId")
  //         .toString();
  //     if (storage.getItem("stripeCustomerID") !=
  //         null) {
  //       stripeCustomerID =
  //           storage.getItem("stripeCustomerID");
  //     } else {
  //       stripeCustomerID = '';
  //     }
  //     print(stripeAmount);
  //     print(memberShipName);
  //     print(createCustomersCardId);
  //     print(stripeCustomerID);
  //     loadProgress(true);
  //     stripeAPICharge(
  //         stripeAmount.toString() + "00",
  //         'USD',
  //         memberShipName,
  //         createCustomersCardId,
  //         stripeCustomerID);
  //   } else {
  //     myMaterialPageRoute(
  //       userid,
  //       storage
  //           .getItem('membershipid')
  //           .toString(),
  //       storage.getItem('amount'),
  //       // storage.getItem('amount').toString(),
  //       storage
  //           .getItem('membershipName')
  //           .toString(),
  //     );
  //   }
  // }

}
