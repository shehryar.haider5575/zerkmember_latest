import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:Zerk/model/model_FacilitySearch.dart';
import 'package:Zerk/model/model_UserFacility.dart';
import 'package:Zerk/model/model_userDetails.dart';
import 'package:Zerk/screens/QRCodeForIOS.dart';
import 'package:Zerk/screens/emergency_form_screen.dart';
import 'package:Zerk/screens/member_notification_screen.dart';
import 'package:Zerk/screens/profile_viewdetails_screen.dart';
import 'package:Zerk/screens/settings_screen.dart';
import 'package:Zerk/utils/Utils.dart';
import 'package:Zerk/utils/baseurl.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_vibrate/flutter_vibrate.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:localstorage/localstorage.dart';
import 'package:location/location.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfileMenu extends StatefulWidget {
  ProfileMenu({Key key}) : super(key: key);
  // final List<String> list = List.generate(10, (index) => "Text $index");
  @override
  _ProfileMenuState createState() => _ProfileMenuState();
}

class _ProfileMenuState extends State<ProfileMenu> {
  Baseurl baseurl = Baseurl();

  bool visible = false;
  GlobalKey<FormState> _key = new GlobalKey();
  bool _validate = false;
  final ScrollController _scrollController = ScrollController();
  Future<UserFacility> futureUserFacilityData;
  Future<FacilitySearch> futureFacilitySearch;

  Future<UserDetailsData> futureUserDetailsData;
  List<dynamic> catSuggestionList = [];

  //Details
  List<UserDetails> userDetailsData;
  Image imageFromPreferences;
  Completer<GoogleMapController> _controller = Completer();
  final findfacilityController = TextEditingController();
  final LocalStorage storage = new LocalStorage('token');
  // static const LatLng _center = const LatLng(24.8603136, 67.0645627);
  LatLng _center = const LatLng(-33.865143, 151.209900);
  // bool isAutoSuggestionVisible = false;
  // bool isFacilityFound = false;
  bool isSuggestionVisible = false;

  // LatLng defaultLocation;

  //  25.2744° S, 133.7751° E
  String token, findfacility;
  List<FacilitySearch> facilitySearchList;
  final Set<Marker> _markers = {};
  List<Marker> allMarkers = [];
  List<Marker> findMarkers = [];
  List<Marker> catWiseMarker = [];
  List<String> facilityName = [];
  List<String> facilityAddress = [];
  bool isSearchFacilityNotAddedToMarkerList = true;
  List<String> facilityImage = [];
  List<String> suggestedCategoryList = [];
  // double lat = 0.0;
  String catHint = 'Select Category';
  bool autoFocus = true;
  // double long = 0.0;
  LatLng _lastMapPosition;
  LatLng defaultLocation = LatLng(-33.865143, 151.209900);

  MapType _currentMapType = MapType.normal;
  // MapType.normal;

  // final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  String _homeScreenText = "Waiting for token...";
  String bussinessName = '';
  String address = '';
  String facilityId = '';
  String first_name = '';
  String last_name = '';
  String firstName = '',
      lastName = '',
      email = '',
      avatar = '',
      firstname,
      lastname,
      totalAvails = '0';

  String remainingAvailsKey;
  Future<File> imageFile;
  void _onMapTypeButtonPressed() {
    setState(() {
      _currentMapType = _currentMapType == MapType.normal
          ? MapType.satellite
          : MapType.normal;
    });
  }

  final Map<String, Item> _items = <String, Item>{};

  Item _itemForMessage(Map<String, dynamic> message) {
    String notification = '';
    notification = message['notification']['body'];
    print(notification);
    final dynamic data = message['data'] ?? message;
    print(data);
    final String itemId = data['id'];
    print(itemId);
    final Item item = _items.putIfAbsent(
        itemId, () => Item(itemId: itemId, data: notification))
      ..status = data['status'];
    return item;
  }

  void _onCameraMove(CameraPosition position) {
    _lastMapPosition = position.target;
  }

  _onSearchClearMovement() async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(bearing: 0, zoom: 14.0, target: defaultLocation),
      ),
    );
  }

  void _onSearchCameraMovement(List<Marker> findMarkers) async {
    print('on search camera camera movement changed');
    final GoogleMapController controller = await _controller.future;
    print("_onSearchCameraMove----------------------------------------");
    LatLng _lastSearchMapPosition = LatLng(
        findMarkers[0].position.latitude, findMarkers[0].position.longitude);
    // LatLng _lastSearchMapPosition = LatLng(latitude, longitude);
    print(_lastSearchMapPosition);
    // center = _lastSearchMapPosition;
    // defaultLocation = _lastSearchMapPosition;
    controller.animateCamera(CameraUpdate.newCameraPosition(
      CameraPosition(
          bearing: 0,
          // target: LatLng(latitude, longitude),
          zoom: 10.0,
          target: _lastSearchMapPosition),
    ));
  }
//

  void _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
    getList();
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  removeValuesSF() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.clear();
  }

  getStringValuesSF(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String stringValue = prefs.getString(key);

    print("getStringValuesSF: $stringValue");
    return stringValue;
  }

  setStringValuesSF(String key, String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(key, value);
    print("getStringValuesSF: $value");
    //  prefs.setString('stringValue', "abc");
  }

  loadProgress(bool value) {
    if (visible == value) {
      setState(() {
        visible = value;
      });
    } else {
      setState(() {
        visible = value;
      });
    }
  }

  Widget showProgressLoader(BuildContext context) {
    if (Platform.isAndroid) {
      return CircularProgressIndicator(
        color: Colors.orange[800],
      );
    }
    if (Platform.isIOS) {
      return CupertinoActivityIndicator();
    }
  }

  Widget primaryMap() {
    return Stack(
      clipBehavior: Clip.none,
      children: <Widget>[
        findfacilityController.text.isEmpty
            ? GoogleMap(
                myLocationEnabled: false,
                zoomControlsEnabled: false,
                zoomGesturesEnabled: true,
                onMapCreated: _onMapCreated,
                myLocationButtonEnabled: false,
                initialCameraPosition: CameraPosition(
                  target: defaultLocation == null ? _center : defaultLocation,
                  zoom: 14.0,
                  tilt: 45.0,
                ),
                mapType: _currentMapType,
                markers: Set.from(allMarkers),
                onCameraMove: _onCameraMove,
              )
            : GoogleMap(
                myLocationEnabled: false,
                zoomControlsEnabled: false,
                zoomGesturesEnabled: true,
                onMapCreated: _onMapCreated,
                myLocationButtonEnabled: false,
                initialCameraPosition: CameraPosition(
                  target: defaultLocation == null ? _center : defaultLocation,
                  zoom: 14.0,
                  tilt: 45.0,
                ),
                mapType: _currentMapType,
                markers: Set.from(findMarkers),
                onCameraMove: _onCameraMove,
              ),
      ],
    );
  }

  void startServiceInPlatform() async {
    if (Platform.isAndroid) {
      var methodChannel = MethodChannel("com.example.zerkapp");
      String data = await methodChannel.invokeMethod("startService");
      debugPrint(data);
    }
    if (Platform.isIOS) {
      var methodChannel = MethodChannel("com.example.zerkapp");
      String data = await methodChannel.invokeMethod("startService");
      debugPrint(data);
    }
  }

  void _showDialog(String contentString, bool value) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        if (value == false) {
          print(value);
          Future.delayed(Duration(seconds: 1), () {
            Navigator.of(context).pop(true);
          });
        } else {}
        return AlertDialog(
          elevation: 5,
          content: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Text(
                  contentString,
                  // "$contentstring",
                  textAlign: TextAlign.left,
                  maxLines: 4,
                  style: TextStyle(
                      color: Colors.orange[700], fontWeight: FontWeight.bold),
                ),
              )
            ],
          ),
        );
      },
    );
  }

  hideKeyboard() {
    FocusScope.of(context).requestFocus(FocusNode());
  }

  Widget showImage() {
    return FadeInImage.assetNetwork(
      placeholder: 'assets/images/place_holder.png',
      fit: BoxFit.cover,
      image: baseurl.imgurl + avatar.toString(),
    );
  }

  // Widget _buildDialog(BuildContext context, Item item) {
  //   return AlertDialog(
  //     actions: <Widget>[
  //       SizedBox(
  //         width: 250,
  //         //          width:MediaQuery.of(context).size.width/4,
  //         height: 100,
  //         child: Stack(
  //           children: <Widget>[
  //             Container(
  //               padding: EdgeInsets.all(5.0),
  //               alignment: Alignment.center,
  //               child: Center(
  //                   child: Align(
  //                 alignment: Alignment.center,
  //                 child: Text(
  //                   item.data.toString(),
  //                   maxLines: 4,
  //                   style: TextStyle(color: Colors.black, fontSize: 20.0),
  //                 ),
  //               )),
  //             ),
  //           ],
  //         ),
  //       ),
  //     ],
  //   );
  // }

  // void _showItemDialog(Map<String, dynamic> message) {
  //   showDialog<bool>(
  //     context: context,
  //     builder: (_) => _buildDialog(
  //       context,
  //       _itemForMessage(message),
  //     ),
  //   ).then((bool shouldNavigate) {
  //     if (shouldNavigate == true) {
  //       _navigateToItemDetail(message);
  //     }
  //   });
  // }

  void _navigateToItemDetail(Map<String, dynamic> message) {
    final Item item = _itemForMessage(message);
    // Clear away dialogs
    Navigator.popUntil(context, (Route<dynamic> route) => route is PageRoute);
    if (!item.route.isCurrent) {
      Navigator.push(context, item.route);
    }
  }

  // _handleNotification(Map<String, dynamic> message) async {
  //   message['data'] = Map.from(message ?? {});
  //   message['notification'] = message['apps']['alert'];
  //   return message;
  //
  //   /// [...]
  // }

  _handleNotification(Map<dynamic, dynamic> message) async {
    var data = message['data'] ?? message;
    String expectedAttribute = data['expectedAttribute'];

    /// [...]
  }

  bool _canVibrate = true;
  final Iterable<Duration> pauses = [
    const Duration(milliseconds: 500),
    const Duration(milliseconds: 1000),
    const Duration(milliseconds: 500),
  ];
  init() async {
    bool canVibrate = await Vibrate.canVibrate;
    setState(() {
      _canVibrate = canVibrate;
      _canVibrate
          ? print("This device can vibrate")
          : print("This device cannot vibrate");
    });
  }

  void _currentLocation() async {
    final GoogleMapController controller = await _controller.future;
    LocationData currentLocation;
    var location = new Location();
    try {
      currentLocation = await location.getLocation();
    } on Exception {
      currentLocation = null;
    }

    controller.animateCamera(CameraUpdate.newCameraPosition(
      CameraPosition(
        bearing: 0,
        target: LatLng(currentLocation.latitude, currentLocation.longitude),
        zoom: 12.0,
      ),
    ));
  }

  @override
  void initState() {
    print('int state function executed');
    // getMapCategories();
    getCatMap();
    super.initState();
    setState(() {
      autoFocus = true;
    });

    facilitySearchList = List<FacilitySearch>();
    findMarkers = List<Marker>();
    token = storage.getItem('token');
    init();
    print(
        '----------------------------------------------------------------------------------------' +
            token.toString());
    print(token);
    storage.setItem("token", token);
    setState(() {
      if (token != null) {
        print('111111111111111111111111111');
        userDetailsDataApi(token);
      } else {
        print('userDetailsDataApiiiiiiiiiiiiiiii');
      }
      futureUserFacilityData = fetchFacility();
    });

    print('???????????????????????????????????????????????');
    print(firstName.toString());
    print(lastName.toString());
    print(email.toString());
    print(avatar.toString());

    firstname = storage.getItem("firstnameController");
    lastname = storage.getItem("lastnameController");
    email = storage.getItem("emailController");

    if (storage.getItem('totalAvails').toString() != null) {
      totalAvails = storage.getItem('totalAvails').toString();
    } else {
      totalAvails = '0';
    }
//    showImage();
//    startServiceInPlatform();
  }

  Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) {
    if (message.containsKey('data')) {
      // Handle data message
      final dynamic data = message['data'];
      if (Platform.isIOS) {
        message = _handleNotification(message);
      }
      print(data.toString());
    }

    if (message.containsKey('notification')) {
      // Handle notification message
      final dynamic notification = message['notification'];
      print(notification.toString());
    }
  }

  onItemChanged(String value) {
    print('on item changed item executed');
    // print('this is list of markers');
    // print('$findMarkers');
    // print('this is search facility list $facilitySearchList');
    setState(() {
      findMarkers = allMarkers
          .where((string) => string.infoWindow.title
              .toLowerCase()
              .contains(value.toLowerCase()))
          .toList();
      _onSearchCameraMovement(findMarkers);
      // _onCameraMove(CameraPosition(target: LatLng(lat, long)));
    });
    print('THIS IS ALL MARKERS');
    // print('${findMarkers.forEach((element) {element.markerId})}');
    // allMarkers.forEach((element) {
    // print('****************');
    //   print(element.markerId);
    //   if (element.infoWindow.title
    //       .toString()
    //       .toLowerCase()
    //       .contains((value.toLowerCase()))) {
    //     print('****************');
    //     print('this is match id ${element.markerId}');
    //     print('this is match name ${element.infoWindow.title}');
    //     print('****************');
    //   }
    //   // print('***************');
    // });
    // allMarkers.forEach((element) {
    //   if (element.infoWindow.title
    //       .toString()
    //       .toLowerCase()
    //       .contains(value.toLowerCase())) {}
    // });
    print('this is markers $allMarkers');
  }

  Widget imageFromGallery() {
    return FutureBuilder<File>(
      future: imageFile,
      builder: (BuildContext context, AsyncSnapshot<File> snapshot) {
        if (snapshot.connectionState == ConnectionState.done &&
            null != snapshot.data) {
          //print(snapshot.data.path);
          Utility.saveImageToPreferences('IMAGE_KEY',
              Utility.base64String(snapshot.data.readAsBytesSync()));
          // return Image.file(
          //   snapshot.data,
          // );
          return Container(
              width: 190.0,
              height: 190.0,
              decoration: new BoxDecoration(
                  shape: BoxShape.circle,
                  image: new DecorationImage(
                      fit: BoxFit.fill, image: new FileImage(snapshot.data))));
        } else if (null != snapshot.error) {
          return const Text(
            'Error Picking Image',
            textAlign: TextAlign.center,
          );
        } else {
          // return const Text(
          //   'No Image Selected',
          //   textAlign: TextAlign.center,
          // );
          return SizedBox(
            height: 50,
            width: 50,
            child: Image.asset('assets/images/sign_up_icon.png'),
          );
        }
      },
    );
  }

  loadImageFromPreferences() {
    Utility.getImageFromPreferences('IMAGE_KEY').then((img) {
      if (null == img) {
        return;
      }
      imageFromPreferences = Utility.imageFromBase64String(img);
      // setState(() {
      //
      // });
    });
  }

  @override
  Widget build(BuildContext context) {
    double _screenHeight = MediaQuery.of(context).size.height;
    MediaQuery.of(context).size.width;
    double searchFieldGap = 115;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      drawerEnableOpenDragGesture: false,
      key: _scaffoldKey,
      drawer: Drawer(
        elevation: 30,
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/images/side_drawer_bg.png'),
              fit: BoxFit.cover,
            ),
          ),
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            //   backgroundColor: Colors.transparent,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  height: MediaQuery.of(context).size.height / 3.5,
                  width: MediaQuery.of(context).size.width,
                  //   color: Colors.black,
                  child: ListView(
                    physics: NeverScrollableScrollPhysics(),
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(right: 12),
                            child: InkWell(
                                onTap: () {
                                  _scaffoldKey.currentState.openEndDrawer();
                                  setState(() {
                                    loadImageFromPreferences();
                                  });
                                },
                                child: InkWell(
                                  onTap: () {
                                    Navigator.of(context).pop();
                                  },
                                  child: Icon(
                                    Icons.arrow_back,
                                    color: Colors.white,
                                  ),
                                )),
                          ),
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.fromLTRB(35, 5, 10, 0),
                            child: Align(
                              alignment: Alignment.topRight,
                              child: Column(
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Container(
                                        height: 50,
                                        width: 50,
                                        decoration: new BoxDecoration(
                                          shape: BoxShape.circle,
                                        ),
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(25.0),
                                            topRight: Radius.circular(25.0),
                                            bottomLeft: Radius.circular(25.0),
                                            bottomRight: Radius.circular(25.0),
                                          ),
                                          child: FadeInImage.assetNetwork(
                                              placeholder:
                                                  'assets/images/sign_up_icon.png',
                                              fit: BoxFit.cover,
                                              image: avatar == 'null'
                                                  ? "https://s.afl.com.au/staticfile/AFL%20Tenant/AFL/Players/ChampIDImages/XLarge2021/1007049.png"
                                                  : baseurl.imgurl +
                                                      avatar.toString()),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 15,
                                      ),
                                      Padding(
                                          padding:
                                              const EdgeInsets.only(top: 5.0),
                                          child: Row(
                                            children: [
                                              ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(10.0),
                                                child: SizedBox(
                                                  height: 45,
                                                  width: 45,
                                                  child: Image.asset(
                                                      'assets/images/zerk.png'),
                                                  // showImage(),
                                                ),
                                              ),
                                              SizedBox(
                                                width: 15,
                                              ),
                                              Text(
                                                // " 3",
                                                storage
                                                            .getItem(
                                                                'totalAvails')
                                                            .toString() ==
                                                        null
                                                    ? '0'
                                                    : storage
                                                        .getItem('totalAvails')
                                                        .toString(),
                                                textAlign: TextAlign.start,
                                                // totalAvails == null ? "0" : totalAvails,
                                                // storage.getItem(
                                                //           "remainingAvailsKey",
                                                //         ) !=
                                                //         null
                                                //     ? remainingAvailsKey
                                                //     : " 3",
                                                style: TextStyle(
                                                  fontSize: 24.0,
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.normal,
                                                ),
                                              ),
                                              // Padding(
                                              //   padding: const EdgeInsets.only(left: 0.0),
                                              //   child:
                                              // )
                                            ],
                                          )),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 15.0),
                                child: Container(
                                  width: 170,
                                  child: Text(
                                    storage.getItem('firstName').toString() ??
                                        '',
                                    textAlign: TextAlign.start,
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      fontSize: 15.0,
                                      color: Colors.white,
                                      fontWeight: FontWeight.normal,
                                    ),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 5.0),
                                child: Container(
                                  width: 170,
                                  child: Text(
                                    email ?? "",
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.normal,
                                    ),
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20),
                  child: Container(
                    height: MediaQuery.of(context).size.height / 2,
                    width: MediaQuery.of(context).size.width,
                    // color: Colors.red,
                    child: ListView(
                      children: [
                        ListTile(
                          leading: SizedBox(
                            height: 100.0,
                            width: 50.0,
                            child: Image.asset(
                                'assets/images/sidedrawer_homeicon.png'),
                          ),
                          title: Text(
                            'Home',
                            style: TextStyle(
                              fontSize: 18.0,
                              color: Colors.grey,
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          onTap: () {
                            Navigator.pop(context);
                            getList();
                            setState(() {
                              primaryMap();
                            });
                            print('doing it');
                          },
                        ),
                        ListTile(
                          leading: SizedBox(
                            height: 100.0,
                            width: 50.0,
                            child: Image.asset(
                                'assets/images/sidedrawer_profilemanagementicon.png'),
                          ),
                          title: Text(
                            'Profile Management',
                            style: TextStyle(
                              fontSize: 18.0,
                              color: Colors.grey,
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          onTap: () {
                            Navigator.pushNamed(context, '/ProfileManagement');
                            _scaffoldKey.currentState.openEndDrawer();
                          },
                        ),
                        ListTile(
                          leading: SizedBox(
                            height: 100.0,
                            width: 50.0,
                            child: Image.asset(
                                'assets/images/sidedrawer_membershipicon.png'),
                          ),
                          title: Text(
                            'Membership',
                            style: TextStyle(
                              fontSize: 18.0,
                              color: Colors.grey,
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          onTap: () {
                            Navigator.popAndPushNamed(context, '/Membership');
                          },
                        ),
                        ListTile(
                          leading: SizedBox(
                            height: 100.0,
                            width: 50.0,
                            child: Image.asset(
                                'assets/images/sidedrawer_settingsicon.png'),
                          ),
                          title: Text(
                            'Settings',
                            style: TextStyle(
                              fontSize: 18.0,
                              color: Colors.grey,
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => Settings(
                                      isVisible: false,
                                    )));
                            // Navigator.popAndPushNamed(context, '/Settings');
                          },
                        ),
                        ExpansionTile(
                          leading: SizedBox(
                            height: 100.0,
                            width: 50.0,
                            child: Image.asset(
                                'assets/images/sidedrawer_settingsicon.png'),
                          ),
                          title: Text(
                            // ' Access Credits',
                            'History',
                            // 'History',
                            style: TextStyle(
                              fontSize: 18.0,
                              color: Colors.grey,
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          children: <Widget>[
                            ListTile(
                              leading: SizedBox(
                                height: 100.0,
                                width: 50.0,
                                child: Image.asset(
                                    'assets/images/sidedrawer_settingsicon.png'),
                              ),
                              contentPadding: EdgeInsets.only(left: 40.0),
                              title: Text(
                                // 'Credits Available',
                                'Access History',
                                style: TextStyle(
                                  fontSize: 18.0,
                                  color: Colors.grey,
                                  fontWeight: FontWeight.normal,
                                ),
                              ),
                              onTap: () {
                                Navigator.popAndPushNamed(
                                    context, '/AvailsHistoryScreen');
                                // Navigator.popAndPushNamed(
                                //     context, '/TransactionHistoryScreen');
                              },
                            ),
                            ListTile(
                              leading: SizedBox(
                                height: 100.0,
                                width: 50.0,
                                child: Image.asset(
                                    'assets/images/sidedrawer_settingsicon.png'),
                              ),
                              contentPadding: EdgeInsets.only(left: 40.0),
                              title: Text(
                                // 'Access History',
                                'Transaction History',
                                style: TextStyle(
                                  fontSize: 18.0,
                                  color: Colors.grey,
                                  fontWeight: FontWeight.normal,
                                ),
                              ),
                              onTap: () {
                                // Navigator.popAndPushNamed(
                                //     context, '/AvailsHistoryScreen');
                                Navigator.popAndPushNamed(
                                    context, '/TransactionHistoryScreen');
                              },
                            ),
                            ListTile(
                              leading: SizedBox(
                                height: 100.0,
                                width: 50.0,
                                child: Image.asset(
                                    'assets/images/sidedrawer_settingsicon.png'),
                              ),
                              contentPadding: EdgeInsets.only(left: 40.0),
                              title: Text(
                                // 'Access History',
                                'Cancellation History',
                                style: TextStyle(
                                  fontSize: 18.0,
                                  color: Colors.grey,
                                  fontWeight: FontWeight.normal,
                                ),
                              ),
                              onTap: () {
                                // Navigator.popAndPushNamed(
                                //     context, '/AvailsHistoryScreen');
                                Navigator.popAndPushNamed(
                                    context, '/CancellationHistory');
                              },
                            ),
                          ],
                        ),
                        ListTile(
                          leading: SizedBox(
                            height: 100.0,
                            width: 50.0,
                            child: Icon(
                              Icons.announcement_rounded,
                              color: Colors.orange[800],
                            ),
                          ),
                          title: Text(
                            'Emergency Form',
                            style: TextStyle(
                              fontSize: 18.0,
                              color: Colors.grey,
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => EmergencyFormScreen()));
                          },
                        ),
                        ListTile(
                          leading: SizedBox(
                            height: 100.0,
                            width: 50.0,
                            child: Image.asset(
                                'assets/images/sidedrawer_logout_icon.png'),
                          ),
                          title: Text(
                            'Log out',
                            style: TextStyle(
                              fontSize: 18.0,
                              color: Colors.grey,
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          onTap: () {
                            removeValuesSF();
                            getStringValuesSF("token");
                            print(storage.getItem("token"));
                            storage.clear();
                            print(storage.getItem("token"));
                            Navigator.pushNamedAndRemoveUntil(
                                context, "/LoginScreen", (r) => false);
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      //Drawer end widget
//      body: getBody(),
      body: WillPopScope(child: getBody(), onWillPop: onWillPop),
    );
  }

  getList() {
    print('get facility list function executed');
    for (int i = 0; i < facilitySearchList.length; i++) {
      facilityImage.add(facilitySearchList[i].imageUrl.toString() +
          facilitySearchList[i].data[i].avatar.toString());
      // print("-------------------" + i.toString());
      if (facilitySearchList[i].data[i].bussinessName != null) {
        print('************************************');
        print('**** FACILITY LIST ****');
        print(facilitySearchList);
        print('************************************');
        facilityName
            .add((facilitySearchList[i].data[i].bussinessName.toString()));
        // facilityAddress.add(userFacility.data[i].address.toString());
        // print("-------------------" + facilityName.length.toString());
      } else {
        print("---------ELSE----userFacility----------");
      }
      if ((facilitySearchList[i].data[i].address != null)) {
        // facilityName
        //     .add(userFacility.data[i].bussinessName.toString());
        facilityAddress.add((facilitySearchList[i].data[i].address.toString()));
        // print("-------------------" + facilityName.length.toString());
      } else {
        print("---------ELSE----userFacility----------");
      }
      if (facilitySearchList[i].data[i].addressLat != null &&
          facilitySearchList[i].data[i].addressLong != null) {
        _center = LatLng(facilitySearchList[0].data[0].addressLat,
            facilitySearchList[0].data[0].addressLong);
        // print('this is center location $_center');
        print('************************************');
        print('**** FACILITY LIST ****');
        print(facilitySearchList);
        print('************************************');
        setState(() {
          print('************************************');
          print('**** FACILITY LIST ****');
          print(facilitySearchList);
          print('************************************');
          // catWiseMarker.add(
          //
          // );
          findMarkers.add(Marker(
            markerId: MarkerId(facilitySearchList[i].data[i].id.toString()),
            draggable: false,
            infoWindow: InfoWindow(
              onTap: () {
                if (findMarkers[i].infoWindow.title.toString() !=
                    facilitySearchList[i].data[i].bussinessName.toString()) {
                  print("MATCH" + i.toString());

                  print(findMarkers[i].infoWindow.title.toString());

                  print(facilitySearchList[i].data[i].bussinessName.toString());

                  print(facilitySearchList[i].data[i].id.toString());
                  print(findMarkers[i].markerId.value.toString());
                } else {
                  print("NOTMATCH");
                  print(facilitySearchList[i].data[i].id.toString());
                  print(findMarkers[i].markerId.value.toString());
                }
              },
              title: facilitySearchList[i].data[i].bussinessName.toString(),
              //  + " " + userFacility.data[i].id.toString(),
              snippet: facilitySearchList[i].data[i].address.toString(),
            ),
            onTap: () {
              setState(() {
                bussinessName = findMarkers[i].infoWindow.title.toString();
                address = findMarkers[i].infoWindow.snippet.toString();
                facilityId = findMarkers[i].markerId.value;
                print(facilityImage.toString());
              });
            },
            position: LatLng(facilitySearchList[i].data[i].addressLat,
                facilitySearchList[i].data[i].addressLong),
            //    position: LatLng(24.9107, 67.0311),
          ));
        });
      } else {
        // Utils.displayToast("Search Facility Not Found");
      }
      print('****************************************');
      print(
          '***** THIS IS FACILITY SEARCH LIST $facilitySearchList **********');
      print('****************************************');
    }
    // allMarkers.clear();

    // print(facilitySearchList.length.toString());
    // print("bussinessName1 " +
    //     facilitySearchList[0].data[0].bussinessName.toString());
    // print("bussinessName2 " +
    //     facilitySearchList[1].data[1].bussinessName.toString());
    // print(facilitySearchList[0].data[0].address.toString());
    // print(facilitySearchList[0].data[0].phone.toString());
  }

  bool facilityFound() {
    bool isFound = false;
    catSuggestionList.forEach((element) {
      if (element['name']
          .toString()
          .toLowerCase()
          .contains(findfacilityController.text.toLowerCase())) {
        isFound = true;
      }
    });
    return isFound;
  }

  getBody() {
    return Stack(
      clipBehavior: Clip.none,
      children: [
        Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: FutureBuilder<UserFacility>(
            future: futureUserFacilityData,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                if (snapshot.data.data.isEmpty) {
                  return Stack(
                    clipBehavior: Clip.none,
                    children: [
                      Container(
                        color: Colors.orange,
                        height: MediaQuery.of(context).size.height * .22,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 25.0),
                              child: InkWell(
                                onTap: () {
                                  if (token != null) {
                                    print('2222222222222222');
                                  } else {
                                    print('userDetailsDataApiiiiiiiiiiiiiiii');
                                  }
                                  setState(() {
                                    firstName = storage.getItem('firstName');
                                    lastName = storage.getItem('lastName');
                                    email = storage.getItem('email');
                                    avatar =
                                        storage.getItem('avatar').toString();
                                    token = storage.getItem('token').toString();
                                    totalAvails = storage
                                        .getItem('totalAvails')
                                        .toString();
                                    print("sadlkjaskdjaklsjklsad");
                                    print(totalAvails);
                                    print(avatar);
                                    if (storage
                                            .getItem('totalAvails')
                                            .toString() !=
                                        null) {
                                      totalAvails = storage
                                          .getItem('totalAvails')
                                          .toString();
                                    } else {
                                      totalAvails = '0';
                                    }
                                    if (storage.getItem(
                                          "remainingAvailsKey",
                                        ) !=
                                        null) {
                                      remainingAvailsKey = storage.getItem(
                                        "remainingAvailsKey",
                                      );
                                    } else {
                                      remainingAvailsKey = "-";
                                    }
                                    loadImageFromPreferences();
                                  });
                                  _scaffoldKey.currentState.openDrawer();
                                  print('33333333333333333');
                                },
                                child: SizedBox(
                                  width: 30,
                                  height: 30,
                                  child: Image.asset("assets/images/menu2.png",
                                      fit: BoxFit.cover, color: Colors.white),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 0.0),
                              child: SizedBox(
                                width: 100,
                                height: 80,
                                child: Image.asset(
                                    "assets/images/zerklogo_homepage.png"),
                              ),
                            ),
                            SizedBox(
                              // width: 80
                              width: MediaQuery.of(context).size.width * .15,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 20.0),
                              child: InkWell(
                                onTap: () {
                                  print("asdsaasdsaasdsaasdsa");

                                  // Navigator.of(context).push(MaterialPageRoute(context) => MemberNotificationScreen());
                                  // startServiceInPlatform();
                                  // _showDialog("No Notification", false);

                                  // Vibrate.vibrate();
                                  // Vibration.vibrate();
                                  // Vibration.vibrate(duration: 1000);
                                  // Vibration.vibrate(
                                  //   pattern: [500, 1000, 500, 2000, 500, 3000, 500, 500],
                                  // );
                                  // Navigator.pushNamed(
                                  //     context, '/MemberNotificationScreen');
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (context) =>
                                          MemberNotificationScreen()));
                                  print('hit');

//                                 Utils.displayToast("This is toast message");
//                               Navigator.pushNamed(context, '/HomePage');
                                  // Navigator.pop(context);
                                },
                                child: Image.asset(
                                    "assets/images/notification_icon.png"),
                              ),
                            ),
                          ],
                        ),
                      ),
                      // body MAP
                      Positioned(
                        bottom: AlignmentDirectional.bottomEnd.start,
                        child: Container(
                          alignment: Alignment.bottomCenter,
                          height: MediaQuery.of(context).size.height * .70,
                          width: MediaQuery.of(context).size.width,
                          child: primaryMap(),
                        ),
                      ),
                      // TextField Find facility Search box ...................
                      searchFieldWidget(),
                      Positioned(
                        bottom: AlignmentDirectional.bottomEnd.start,
                        right: AlignmentDirectional.bottomEnd.start,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            alignment: Alignment.bottomCenter,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Card(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(30),
                                  ),
                                  shadowColor: Colors.orange[800],
                                  elevation: 50,
                                  child: SizedBox(
                                    height:
                                        MediaQuery.of(context).size.height / 9,
                                    width:
                                        MediaQuery.of(context).size.height / 9,

                                    //   height: 80,
                                    //   width: 80,
                                    child: InkWell(
                                      onTap: () {
                                        storage.setItem(
                                            "facilityAccessAd", true);
                                        if (Platform.isIOS) {
                                          Navigator.of(context).push(
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      QRCodeForIOS()));
                                        } else
                                          Navigator.pushNamed(
                                              context, '/ScanQRCodeScreen');
                                      },
                                      child: CircleAvatar(
                                          backgroundColor: Colors.white,
                                          child: SizedBox(
                                            height: 50,
                                            width: 60,
                                            child: Image.asset(
                                              'assets/images/qrcode.png',
                                              height: 35,
                                              width: 65,
                                            ),
                                          )),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(right: 8.0),
                                  child: Text(
                                    // "Tap & Scan",
                                    "",
                                    style: TextStyle(
                                        fontSize: 16.0,
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      //Tap & Scan
                      // filter list
                    ],
                  );
                }
                UserFacility userFacility = snapshot.data;
                facilityImage = List<String>();
                avatar = userFacility.data[0].avatar.toString();

                for (int i = 0; i < userFacility.data.length; i++) {
                  double lat = userFacility.data[i].addressLat == null
                      ? 24.9872
                      : double.parse(
                          userFacility.data[i].addressLat.toString());
                  double long = userFacility.data[i].addressLong == null
                      ? 67.07568
                      : double.parse(
                          userFacility.data[i].addressLong.toString());

                  print('this is lat $lat');
                  print('this is long $long');

                  facilityImage.add(userFacility.imageUrl +
                      userFacility.data[i].avatar.toString());
                  // print("-------------------" + i.toString());
                  if (userFacility.data[i].bussinessName != null) {
                    facilityName
                        .add(userFacility.data[i].bussinessName.toString());
                    // facilityAddress.add(userFacility.data[i].address.toString());
                    // print("-------------------" + facilityName.length.toString());
                  } else {
                    print("---------ELSE----userFacility----------");
                  }
                  if (userFacility.data[i].address != null) {
                    // facilityName
                    //     .add(userFacility.data[i].bussinessName.toString());
                    facilityAddress
                        .add(userFacility.data[i].address.toString());
                  } else {
                    print("---------ELSE----userFacility----------");
                  }
                  if (lat != null && long != null) {
                    allMarkers.add(
                      Marker(
                        markerId: MarkerId(userFacility.data[i].id.toString()),
                        draggable: false,
                        infoWindow: InfoWindow(
                          onTap: () {
                            print((userFacility.data[i].bussinessName
                                .toString()));
                          },
                          title: userFacility.data[i].bussinessName.toString(),
                          snippet: userFacility.data[i].address.toString(),
                        ),
                        onTap: () {
                          setState(() {
                            bussinessName =
                                userFacility.data[i].bussinessName.toString();
                            address = userFacility.data[i].address.toString();
                            facilityId = userFacility.data[i].id.toString();
                            print(bussinessName);
                          });
                        },
                        position: LatLng(lat, long),
                      ),
                    );
                    defaultLocation = LatLng(lat, long);
                    print('this is updated default location $defaultLocation');
                  } else {}
                }
                return Stack(
                  children: [
                    //top zerk side widget
                    Container(
                      color: Colors.orange,
                      height: MediaQuery.of(context).size.height * .22,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 25.0),
                            child: InkWell(
                              onTap: () {
                                if (token != null) {
                                  print('444444444444444444');
                                } else {
                                  print('userDetailsDataApiiiiiiiiiiiiiiii');
                                }
                                setState(() {
                                  firstName = storage.getItem('firstName');
                                  lastName = storage.getItem('lastName');
                                  email = storage.getItem('email');
                                  print("sadlkjaskdjashdkjsahdjashaklsjklsad");
                                  avatar = storage.getItem('avatar');

                                  totalAvails =
                                      storage.getItem('totalAvails').toString();

                                  print(totalAvails);
                                  print(avatar);
                                  if (storage
                                          .getItem('totalAvails')
                                          .toString() !=
                                      null) {
                                    totalAvails = storage
                                        .getItem('totalAvails')
                                        .toString();
                                  } else {
                                    totalAvails = '0';
                                  }
                                  if (storage.getItem(
                                        "remainingAvailsKey",
                                      ) !=
                                      null) {
                                    remainingAvailsKey = storage.getItem(
                                      "remainingAvailsKey",
                                    );
                                  } else {
                                    remainingAvailsKey = "-";
                                  }
                                  loadImageFromPreferences();
                                });
                                _scaffoldKey.currentState.openDrawer();
                              },
                              child: SizedBox(
                                width: 30,
                                height: 30,
                                child: Image.asset("assets/images/menu2.png",
                                    fit: BoxFit.cover, color: Colors.white),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 0.0),
                            child: SizedBox(
                              width: 100,
                              height: 80,
                              child: Image.asset(
                                  "assets/images/zerklogo_homepage.png"),
                            ),
                          ),
                          SizedBox(
                            width: MediaQuery.of(context).size.width * .15,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right: 20.0),
                            child: InkWell(
                              onTap: () {
                                Navigator.pushNamed(
                                    context, '/MemberNotificationScreen');
                              },
                              child: SizedBox(
                                width: 30,
                                height: 30,
                                child: Image.asset(
                                    "assets/images/notification_icon.png"),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    // body MAP
                    Positioned(
                      bottom: AlignmentDirectional.bottomEnd.start,
                      child: Container(
                        alignment: Alignment.bottomCenter,
                        height: MediaQuery.of(context).size.height * .70,
                        width: MediaQuery.of(context).size.width,
                        child: Stack(
                          clipBehavior: Clip.none,
                          children: <Widget>[
                            findfacilityController.text.isEmpty
                                ? GoogleMap(
                                    myLocationEnabled: false,
                                    zoomControlsEnabled: false,
                                    zoomGesturesEnabled: true,
                                    onMapCreated: _onMapCreated,
                                    myLocationButtonEnabled: false,
                                    initialCameraPosition: CameraPosition(
                                      target: defaultLocation == null
                                          ? _center
                                          : defaultLocation,
                                      zoom: 14.0,
                                    ),
                                    mapType: _currentMapType,
                                    markers: Set.from(allMarkers),
                                    onCameraMove: _onCameraMove,
                                  )
                                : GoogleMap(
                                    myLocationEnabled: false,
                                    zoomControlsEnabled: false,
                                    zoomGesturesEnabled: true,
                                    onMapCreated: _onMapCreated,
                                    myLocationButtonEnabled: false,
                                    initialCameraPosition: CameraPosition(
                                      target: defaultLocation == null
                                          ? _center
                                          : defaultLocation,
                                      zoom: 14.0,
                                    ),
                                    mapType: _currentMapType,
                                    markers: Set.from(findMarkers),
                                    onCameraMove: _onCameraMove,
                                  ),
                          ],
                        ),
                      ),
                    ),
                    // TextField Find facility Search box ...................
                    searchFieldWidget(),
                    // View Details Re- Center widget
                    Positioned(
                      bottom: AlignmentDirectional.bottomStart.start,
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 30, left: 5),
                        child: Container(
                          height: 450.0,
                          alignment: Alignment.bottomLeft,
                          child: Stack(
                            alignment: Alignment.centerLeft,
                            children: <Widget>[
                              Card(
                                // color: Colors.black,
                                shadowColor: Colors.orange[700],
                                elevation: 30,
                                margin: EdgeInsets.symmetric(horizontal: 15.0),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(10.0),
                                  ),
                                ),
                                child: Container(
                                  width: 220.0,
                                  decoration: BoxDecoration(
                                    //   color: appTheme.primaryColor,
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(10.0),
                                    ),
                                  ),
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 15.0, horizontal: 20.0),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 10.0),
                                        child: Padding(
                                          padding:
                                              const EdgeInsets.only(left: 20.0),
                                          child: Text(
                                            bussinessName.toString(),
                                            style: TextStyle(
                                                fontSize: 16.0,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(left: 20.0),
                                        child: Text(
                                          address.toString(),
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(top: 5.0),
                                      ),
                                      InkWell(
                                        onTap: () {
                                          print(facilityId);
                                          if (facilityId != '') {
                                            Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  fullscreenDialog: true,
                                                  builder: (context) =>
                                                      ProfileViewDetails(
                                                        facilityId: facilityId,
                                                        bussinessName:
                                                            bussinessName,
                                                        address: address,
                                                        firstName: first_name,
                                                      )),
                                            );
                                            findfacilityController.clear();
                                            facilitySearchList.clear();
                                          } else {
                                            _showDialog(
                                                "Please Select Facility First",
                                                false);
                                          }
                                        },
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 20.0),
                                          child: Text(
                                            'View Details',
                                            style: TextStyle(
                                                fontSize: 14.0,
                                                decoration:
                                                    TextDecoration.underline,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Container(
                                child: CircleAvatar(
                                  backgroundColor: Colors.transparent,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(80),
                                    child: SizedBox(
                                      child: snapshot.data.data[0].avatar !=
                                              'null'
                                          ? Image.network(
                                              'https://s.afl.com.au/staticfile/AFL%20Tenant/AFL/Players/ChampIDImages/XLarge2021/1007049.png')
                                          : Image.network(
                                              snapshot.data.imageUrl +
                                                  snapshot.data.data[1].avatar
                                                      .toString(),
                                            ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      bottom: 5,
                      right: AlignmentDirectional.bottomEnd.start,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          alignment: Alignment.bottomCenter,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30),
                                ),
                                shadowColor: Colors.orange[800],
                                elevation: 50,
                                child: SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height / 9,
                                  width: MediaQuery.of(context).size.height / 9,
                                  child: InkWell(
                                    onTap: () {
                                      storage.setItem("facilityAccessAd", true);
                                      if (Platform.isIOS) {
                                        Navigator.of(context).push(
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    QRCodeForIOS()));
                                      } else
                                        Navigator.pushNamed(
                                            context, '/ScanQRCodeScreen');
                                    },
                                    child: CircleAvatar(
                                        backgroundColor: Colors.white,
                                        child: SizedBox(
                                          height: 50,
                                          width: 60,
                                          child: Image.asset(
                                            'assets/images/qrcode.png',
                                            height: 35,
                                            width: 65,
                                          ),
                                        )),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(right: 8.0),
                                child: Text(
                                  // "Tap & Scan",
                                  "",
                                  style: TextStyle(
                                      fontSize: 16.0,
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    //Tap & Scan
                  ],
                );
              } else if (snapshot.hasError) {
                return Center(child: Text("${snapshot.error}"));
              }

              // By default, show a loading spinner.
              return Center(child: showProgressLoader(context));
            },
          ),
        ),
        Padding(
          padding:
              EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.175),
          child: GestureDetector(
            onTap: () {
              setState(() {
                isSuggestionVisible = !isSuggestionVisible;
              });
            },
            child: AbsorbPointer(
              absorbing: true,
              child: Stack(
                clipBehavior: Clip.none,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 25.0),
                    child: TextFormField(
                      autocorrect: true,
                      // controller: _search,
                      // autofocus: isSearchFocused,
                      keyboardType: TextInputType.text,
                      onFieldSubmitted: (val) {},
                      textInputAction: TextInputAction.done,
                      // obscuringCharacter: '*',
                      autovalidateMode: AutovalidateMode.always,
                      onChanged: (val) {
                        setState(() {
                          isSuggestionVisible = val.isEmpty ? false : true;
                          // isClearIconVisible =
                          val.isEmpty ? false : true;
                          if (val.isEmpty) {
                            // isCatFacilityVisible = false;
                          }
                        });
                      },
                      style: TextStyle(color: Colors.orange, fontSize: 19),
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          enabled: false,
                          contentPadding:
                              const EdgeInsets.symmetric(horizontal: 25),
                          hintText: catHint,
                          errorStyle: TextStyle(color: Colors.orange),
                          hintStyle:
                              TextStyle(color: Colors.orange, fontSize: 17),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30),
                            borderSide: BorderSide(color: Colors.orange),
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30),
                            borderSide: BorderSide(color: Colors.orange),
                          ),
                          disabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30),
                            borderSide: BorderSide(color: Colors.black12),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30),
                            borderSide: BorderSide(color: Colors.orange),
                          ),
                          errorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30),
                              borderSide: BorderSide(color: Colors.orange))),
                    ),
                  ),
                  Positioned(
                      top: 12,
                      right: 35,
                      child: Icon(
                        Icons.arrow_drop_down_circle,
                        color: Colors.orange,
                        size: 26,
                      )),
                ],
              ),
            ),
          ),
        ),
        Visibility(
          visible: isSuggestionVisible,
          child: Positioned(
            top: 60,
            left: 12,
            right: 12,
            child: Container(
              height: 170,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black12,
                      spreadRadius: 3,
                      blurRadius: 3,
                      offset: const Offset(3, 3),
                    )
                  ]),
              width: MediaQuery.of(context).size.width,
              child: facilityFound()
                  ? ListView.separated(
                      itemBuilder: (context, index) {
                        return InkWell(
                          onTap: () {
                            setState(() {
                              //update hint value
                              catHint = catSuggestionList[index]['name'];
                              isSuggestionVisible = false;
                              fetchCatWiseData(catSuggestionList[index]['id']);
                            });
                          },
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 13.0, horizontal: 20),
                            child: Text(
                              catSuggestionList[index]['name'],
                              textAlign: TextAlign.start,
                              style: TextStyle(color: Colors.black),
                            ),
                          ),
                        );
                      },
                      separatorBuilder: (context, index) {
                        return Container(
                          height: 1,
                          width: 22,
                          color: Colors.black45,
                        );
                      },
                      itemCount: catSuggestionList.length)
                  : Center(
                      child: Text(
                      'Not Found',
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.normal,
                          fontSize: 18),
                    )),
            ),
          ),
        )
      ],
    );
  }

  // _sendToServer() {
  //   print('_send to server function executed');
  //   if (_key.currentState.validate()) {
  //     // No any error in validation
  //     _key.currentState.save();
  //     print('this is find facility value $findfacility');
  //     setState(() {
  //       futureFacilitySearch = fetchFacilitySearch(findfacility
  //           // findfacilityController.text.trim().toLowerCase().toString()
  //           );
  //     });
  //     print("Search:  $findfacility");
  //   } else {}
  // }

  // validateName
  String validateName(String value) {
    String patttern = r'(^[a-zA-Z ]*$)';
    RegExp regExp = new RegExp(patttern);
    if (value.length == 0) {
      return "Name is Required";
    } else if (!regExp.hasMatch(value)) {
      return "Name must be a-z and A-Z";
    }
    return null;
  }

  // Future getMapCategories() async {
  //   final http.Response response = await http
  //       .get(Uri.parse('http://zerk.com.au/panel/public/api/categories'));
  //   print('this is response get map categories');
  //   print(response.body);
  // }

  Future<List<dynamic>> getCatMap() async {
    final response = await http
        .get(Uri.parse('http://zerk.com.au/panel/public/api/categories'));
    // print('this is get map cat list ${response.body}');
    final Map<String, dynamic> jsonResponse = jsonDecode(response.body);
    // print('this is converted response ${jsonResponse['data']}');
    // jsonResponse.forEach((key, value) { })
    catSuggestionList = jsonResponse['data'];
    print('this is list $catSuggestionList');
    return jsonResponse['data'];
  }

  searchFieldWidget() {
    return Positioned(
      top: MediaQuery.of(context).size.height * 0.175 + 60,
      // bottom: 500,
      child: Form(
        key: _key,
        autovalidate: _validate,
        child: Container(
          // color: Colors.red,
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height * 1 / 13,
          child: Padding(
            padding: const EdgeInsets.only(
              left: 30.0,
              right: 30.0,
            ),
            child: new Container(
              //   color: Colors.red,
              padding: const EdgeInsets.all(8.0),
              alignment: Alignment.center,
              // height: 60.0,
              //  height: MediaQuery.of(context).size.height * .9,
              decoration: new BoxDecoration(
                  color: Colors.white,
                  border: new Border.all(color: Colors.white, width: 4.0),
                  borderRadius: new BorderRadius.circular(32.0)),
              child: Row(
                children: <Widget>[
                  new Icon(
                    Icons.search,
                    color: Colors.orange[800],
                  ),
                  new SizedBox(
                    width: 10.0,
                  ),
                  new Expanded(
                    child: new Stack(clipBehavior: Clip.none,
                        // fit: StackFit.expand,
                        // alignment: const Alignment(1.0, 1.0),
                        children: <Widget>[
                          TextField(
                            onTap: () {
                              print('this is hit');
                            },
                            autofocus: autoFocus,
                            decoration: InputDecoration(
                              suffixText: '',
                              //fillColor: Colors.black,
                              border: InputBorder.none,
                              contentPadding: const EdgeInsets.only(
                                  left: 10, top: 5.0, bottom: 12.0),
                              hintText: "Find facility",
                              hintStyle: TextStyle(
                                color: Colors.grey,
                                fontSize: 15.0,
                              ),
                            ),
                            onChanged: (value) {
                              facilityFound();
                              onItemChanged(value);
                              // findfacility =
                              //     value.trim().toLowerCase();
                              // print(
                              //     "onChanged___________________________");
                              // print(findfacility);
                              // _sendToServer();
                            },
                            // onSubmitted: (value) {
                            //   findfacility =
                            //       value.trim().toLowerCase();
                            //   print(
                            //       "onChanged___________________________");
                            //   print(findfacility);
                            //   _sendToServer();
                            // },
                            controller: findfacilityController,
                          ),
                          findfacilityController.text.length > 0
                              ? Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    IconButton(
                                        icon: new Icon(
                                          Icons.center_focus_strong,
                                          color: Colors.orange[800],
                                        ),
                                        onPressed: () {
                                          setState(() {
                                            // isAutoSuggestionVisible = false;
                                            findfacilityController.clear();
                                            facilitySearchList.clear();
                                            bussinessName = '';
                                            address = '';
                                            facilityId = '';
                                            if (findfacilityController
                                                    .text.length ==
                                                0) {
                                              _onSearchClearMovement();
                                            }
                                          });
                                        })
                                  ],
                                )
                              : new Container(
                                  height: 0.0,
                                )
                        ]),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<UserFacility> fetchCatWiseData(catId) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String idFromLoginScreen = sharedPreferences.getString('id');
    print('THIS IS ID $idFromLoginScreen');
    final response = await http.get(
      Uri.parse(Baseurl().baseurl + "admin/facility/search/$catId"),
      headers: <String, String>{
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );
    if (response.statusCode == 200) {
      // print(response.statusCode);
      print('this is cat wise facility');
      // print(response.body);
      var json = jsonDecode(response.body);
      print(json['data']);
      // clear the marker list first
      setState(() {
        allMarkers.clear();
      });
      print('this is marker length ${allMarkers.length}');
      // now add cat wise markers
      for (int i = 0; i < json['data'].length; i++) {
        var lat = json['data'][i]['address_lat'] == null
            ? 27.9898
            : double.parse(json['data'][i]['address_lat']);
        var long = json['data'][i]['address_long'] == null
            ? 67.07568
            : double.parse(json['data'][i]['address_long']);
        var id = json['data'][i]['id'];
        var name = json['data'][i]['bussiness_name'];
        var address = json['data'][i]['address'];
        var img = json['data'][i]['avatar'];
        allMarkers.add(
          Marker(
            onTap: () {
              setState(() {
                bussinessName = name.toString();
                address = address.toString();
                facilityId = id.toString();
                print(bussinessName);
              });
            },
            markerId: MarkerId(id.toString()),
            position: LatLng(lat, long),
            draggable: false,
            infoWindow: InfoWindow(
              onTap: () {
                // print((userFacility.data[i].bussinessName
                //     .toString()));
              },
              title: name.toString(),
              snippet: address.toString(),
            ),
          ),
        );
      }
      return UserFacility.fromJson(json.decode(response.body));
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load fetch Facility');
    }
  }

  Future<UserFacility> fetchFacility() async {
    String idFromHomeScreen = storage.getItem('userid').toString();
    // print('**********************');
    // print('**********************');
    // print('**********************');
    // print('This is id from home screen $idFromHomeScreen');
    // print('**********************');
    // print('**********************');
    // print('**********************');
    final response = await http.get(
      Uri.parse(baseurl.baseurl + "admin/facility/all/$idFromHomeScreen"),
      headers: <String, String>{
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );

    if (response.statusCode == 200) {
      print('//////////////fetchFacility///////////////////');
      print(response.statusCode);
      print('this is fetch facility function ${response.body}');
      print('*****************This is response ***********');
      print('*****************This is response ***********');
      print('this is fetch facility response ${response.body}');
      // print(response.b)
      Map<String, dynamic> data = jsonDecode(response.body);
      print('*****************This is response ***********');
      print('${data['data']}');
      print('*****************This is response ***********');
      print('*****************This is response ***********');

      // If the server did return a 200 OK response,
      // then parse the JSON.
      return UserFacility.fromJson(json.decode(response.body));
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      Navigator.pushNamedAndRemoveUntil(context, "/LoginScreen", (r) => false);
      throw Exception('Failed to load Server is Not Responding.....');
    }
  }

  //-------------UserDetailsDataAPI--------------------------------
  Future<UserDetailsData> userDetailsDataApi(String token) async {
    print('user details api executed');
    try {
      final response = await http.get(
        Uri.parse(baseurl.baseurl + "user/details"),
        headers: <String, String>{
          'Accept': 'application/json',
          'Authorization': 'Bearer ' + token,
        },
      );

      if (response.statusCode == 200) {
        print(response.statusCode);
        print(json.decode(response.body));
        userDetailsData = List<UserDetails>();
        storage.setItem("UserDetailsData", storage.getItem('token'));
        var parsedJson = json.decode(response.body);
        var success = UserDetails.fromJson(parsedJson);
        userDetailsData = List<UserDetails>();
        userDetailsData.add(UserDetails(data: success.data));
        print('||||||||' + userDetailsData[0].data.id.toString());
        print('||||||||' + userDetailsData[0].data.firstName.toString());
        print('||||||||' + userDetailsData[0].data.lastName.toString());
        print('||||||||' + userDetailsData[0].data.email.toString());
        print('||||||||' + userDetailsData[0].data.avatar.toString());

        print('||||||||' + success.data.id.toString());
        print('||||||||' + success.data.firstName.toString());
        print('||||||||' + success.data.lastName.toString());
        print('||||||||' + success.data.email.toString());
        print('||||||||' + success.data.avatar.toString());

        storage.setItem('userid', success.data.id.toString());
        storage.setItem('firstName', success.data.firstName.toString());
        storage.setItem('lastName', success.data.lastName.toString());
        storage.setItem('email', success.data.email.toString());
        storage.setItem('avatar', success.data.avatar.toString());
        // storage.setItem('totalAvails', success.data.totalAvails.toString());
        if (success.data.totalAvails != null) {
          print("isNOTequaltoNULL");
          // setState(() {
          //
          // });
          storage.setItem('totalAvails', success.data.totalAvails.toString());
        } else {
          storage.setItem('totalAvails', "0");
          print("isEqualtoNULL");
        }
        setStringValuesSF('userid', success.data.id.toString());
        setStringValuesSF('firstName', success.data.firstName.toString());
        setStringValuesSF('lastName', success.data.lastName.toString());
        setStringValuesSF('email', success.data.avatar.toString());
        setStringValuesSF('avatar', success.data.avatar.toString());

        print(storage.getItem("avatar").toString() +
            storage.getItem("email").toString());

        storage.getItem('member');

        return UserDetailsData.fromJson(json.decode(response.body));
      } else if (response.statusCode == 401) {
        String messageData = json.decode(response.body)['message'];
        print('$messageData');
        Navigator.popAndPushNamed(context, '/BottomNavBar');
      } else {
        print(response.statusCode.toString());
        print(response.body.toString());
        String messageData = json.decode(response.body)['message'] ?? '';
        String data = json.decode(response.body)['data'] ?? '';
        if (messageData == 'The access token is invalid.') {
          Navigator.popAndPushNamed(context, '/LoginScreen');
        } else {
          Navigator.popAndPushNamed(context, '/BottomNavBar');
        }
        // If the server did not return a 200 OK response,
        // then throw an exception.
        // throw Exception('Failed to load User Details Data Api');
      }
    } catch (err) {
      print(err);
    }
  }

  // ___________________FacilitySearch__________________________________

  Future<FacilitySearch> fetchFacilitySearch(String value) async {
    print('fetch facility function executed');
    print('this is the value we get $value');
    final response = await http.get(
      Uri.parse(baseurl.baseurl + "admin/facility/search/$value"),
      headers: <String, String>{
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );

    if (response.statusCode == 200) {
      print(response.statusCode);
      print('**** FACILITY LIST *******');
      print('THIS IS FACILITY LIST ${response.body}');
      print('***** FACILITY LIST *******');

      var parsedJson = json.decode(response.body);
      var success = FacilitySearch.fromJson(parsedJson);
      // print('this is success $success');
      print('this is out of success');
      // if (success.data.length > 0) {
      print('this is inside success');
      print('this is success data ${success.data}');
      // _showDialog(success.data.length.toString(), false);
      facilitySearchList.add(FacilitySearch(data: success.data));
      if (success.data.isEmpty) {
        print('${success.message}');
        Fluttertoast.showToast(
            msg: 'no facility found in selected category',
            textColor: Colors.black,
            backgroundColor: Colors.white);
      } else if (success.data.isNotEmpty) {
        print('*******        *****');
        print('${success.data[0].id}');
        print('*****        *******');
        // getList();
        print('this is previous facility length ${findMarkers.length}');
        // if (isSearchFacilityNotAddedToMarkerList) {
        success.data.forEach((element) {
          findMarkers.add(Marker(
            markerId: MarkerId('${element.id}'),
            infoWindow: InfoWindow(
                title: '${element.firstName}', snippet: '${element.address}'),
            position: LatLng(
                element.addressLat ??
                    -19.978199246248408371684490703046321868896484375,
                element.addressLong ??
                    129.759492479687509103314368985593318939208984375),
          ));
        });
        isSearchFacilityNotAddedToMarkerList = false;
        // }
        print('this is previous facility length ${findMarkers.length}');
      } else {
        Fluttertoast.showToast(msg: '${success.message}');
      }
      // setState(() {});
      // }
      return FacilitySearch.fromJson(json.decode(response.body));
    } else {
      setState(() {
        // Utils.displayToast("Search Facility Not Found");
      });

      print(response.statusCode);
      print(response.body);

      facilitySearchList.clear();
      throw Exception('Failed to load fetch Facility Search ');
    }
  }

  Future<bool> onWillPop() {
    return SystemNavigator.pop();
  }
}

class Item {
  Item({this.itemId, this.data});
  final String itemId;
  final dynamic data;

  StreamController<Item> _controller = StreamController<Item>.broadcast();
  Stream<Item> get onChanged => _controller.stream;

  String _status;
  String get status => _status;
  set status(String value) {
    _status = value;
    _controller.add(this);
  }

  static final Map<String, Route<void>> routes = <String, Route<void>>{};
  Route<void> get route {
    final String routeName = '/detail/$itemId';
    return routes.putIfAbsent(
      routeName,
      () => MaterialPageRoute<void>(
        settings: RouteSettings(name: routeName),
        //        builder: (BuildContext context) => DetailPage(itemId),
      ),
    );
  }
}
