import 'dart:async';
import 'dart:convert';

import 'package:Zerk/model/model_userDetails.dart';
import 'package:Zerk/utils/baseurl.dart';
import 'package:Zerk/utils/my_navigator.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:localstorage/localstorage.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final LocalStorage storage = new LocalStorage('token');
  String token, tokenStorage;
  Baseurl baseurl = Baseurl();
  List<UserDetails> userDetailsData;
  void initState() {
    super.initState();
//    print(tokenStorage);
    checkingTheSavedData();

//    checkingTheSaved();
//    tokenStorage = storage.getItem('token');
    // print(storage.getItem('token'));

//    token = getStringValuesSF("token") .toString();

//     if (getStringValuesSF("token") != null) {
//
//    if (tokenStorage == null) {
//      Timer(Duration(seconds: 2), () => MyNavigator.goToLoginScreen(context));
//    } else {
//      Timer(Duration(seconds: 2), () => MyNavigator.goToBottomNavBar(context));
//    }
  }

//  Future<void> checkingTheSaved() async {
//    SharedPreferences prefs = await SharedPreferences.getInstance();
//    var token = prefs.getString('token');
//    print(token);
//
//    runApp(MaterialApp(home: token == null ? LoginScreen() : BottomNavBar()));
//  }

  checkingTheSavedData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print('++++++++++++++++++++++++++++++++');
    String username = prefs.getString('token');
    print(storage.getItem('token'));
    print(username);
    token = getStringValuesSF("token").toString();
    print(token);
  }

  getStringValuesSF(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String stringValue = prefs.getString(key);

    print("getStringValuesSF: $stringValue");
    print('---------------------');
    print("username " + prefs.getString('token').toString());
    print('---------------------');
    if (prefs.getString('token') == null) {
      print('IF');
      print(token);

      Timer(Duration(seconds: 5), () => MyNavigator.goToLoginScreen(context));
    } else {
      // print('ELSE');
      // print(token);
      userDetailsDataApi(prefs.getString('token').toString());

//      Timer(Duration(seconds: 2), () => MyNavigator.goToBottomNavBar(context));
    }

    return stringValue.toString();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/splash_bg.png'),
                fit: BoxFit.cover,
              ),
            ),
            // decoration: BoxDecoration(color:  Color.fromRGBO(252, 132, 20, 10),),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset(
                'assets/images/zerklogo_splash.png',
                alignment: Alignment.center,
                height: 300,
                width: 300,
              ),
            ],
          ),
        ],
      ),
    );
  }

  //-------------UserDetailsDataAPI--------------------------------
  Future<UserDetailsData> userDetailsDataApi(String token) async {
    print('this is user detail function');
    final response = await http.get(
      Uri.parse(baseurl.baseurl + "user/details"),
      headers: <String, String>{
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + token,
      },
    );

    if (response.statusCode == 200) {
      print(json.decode(response.body));
      userDetailsData = List<UserDetails>();
//      _showDialog('loginnnn');
      // setStringValuesSF("UserDetailsData", response.body.toString());

      storage.setItem("UserDetailsData", storage.getItem('token'));

      var parsedJson = json.decode(response.body);
      var success = UserDetails.fromJson(parsedJson);

      userDetailsData = List<UserDetails>();
      userDetailsData.add(UserDetails(data: success.data));

      // print('||||||||' + userDetailsData[0].data.id.toString());
      // print('||||||||' + userDetailsData[0].data.firstName.toString());
      // print('||||||||' + userDetailsData[0].data.lastName.toString());
      // print('||||||||' + userDetailsData[0].data.email.toString());
      // print('||||||||' + userDetailsData[0].data.avatar.toString());
      //
      // print('||||||||' + success.data.id.toString());
      // print('||||||||' + success.data.firstName.toString());
      // print('||||||||' + success.data.lastName.toString());
      // print('||||||||' + success.data.email.toString());
      // print('||||||||' + success.data.avatar.toString());
      // print('sajdksajkdljaskldjsakld' + success.data.totalAvails.toString());

      storage.setItem('userid', success.data.id.toString());
      storage.setItem('firstName', success.data.firstName.toString());
      storage.setItem('lastName', success.data.lastName.toString());
      storage.setItem('email', success.data.email.toString());
      storage.setItem('avatar', success.data.avatar.toString());
      storage.setItem('member', success.data.member.toString());
      storage.setItem(
          'cancelSubscription', success.data.cancelSubscription.toString());
      // storage.setItem('totalAvails', success.data.totalAvails.toString());
      if (success.data.totalAvails != null) {
        // print("isNOTequaltoNULL");
        storage.setItem('totalAvails', success.data.totalAvails.toString());
      } else {
        storage.setItem('totalAvails', "0");
        // print("isEqualtoNULL");
      }
      print(storage.getItem("avatar").toString() +
          storage.getItem("email").toString());
      print("-------///////////////////////////////////////////-------ID:" +
          userDetailsData[0].data.id.toString() +
          success.data.id.toString());
      Navigator.popAndPushNamed(context, '/BottomNavBar');
      return UserDetailsData.fromJson(json.decode(response.body));
    } else {
      print(response.statusCode.toString());
      print(response.body.toString());
      String message = json.decode(response.body)['message'] ?? '';
      String data = json.decode(response.body)['data'] ?? '';
      _showDialog(message);
      if (message == 'The access token is invalid.') {
        Navigator.popAndPushNamed(context, '/LoginScreen');
      } else {
        Navigator.popAndPushNamed(context, '/BottomNavBar');
      }

      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load album');
    }
  }

  void _showDialog(String contentstring) {
    // flutter defined function
    showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          elevation: 5,
          content: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                "$contentstring",
                textAlign: TextAlign.justify,
                // maxLines: 2,
                style: TextStyle(
                    color: Colors.orange[700], fontWeight: FontWeight.bold),

                // "Sign Up Successfully",
              ),
            ],
          ),
        );
      },
    );
  }
}

// import 'dart:async';
// import 'dart:convert';
//
// import 'package:flutter/material.dart';
// import 'package:http/http.dart' as http;
// import 'package:localstorage/localstorage.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:Zerk/model/model_userDetails.dart';
// import 'package:Zerk/utils/baseurl.dart';
// import 'package:Zerk/utils/my_navigator.dart';
//
// class SplashScreen extends StatefulWidget {
//   @override
//   _SplashScreenState createState() => _SplashScreenState();
// }
//
// class _SplashScreenState extends State<SplashScreen> {
//   final LocalStorage storage = new LocalStorage('token');
//   String token, tokenStorage;
//   Baseurl baseurl = Baseurl();
//   List<UserDetails> userDetailsData;
//   void initState() {
//     super.initState();
// //    print(tokenStorage);
//     checkingTheSavedData();
//
// //    checkingTheSaved();
// //    tokenStorage = storage.getItem('token');
//     // print(storage.getItem('token'));
//
// //    token = getStringValuesSF("token") .toString();
//
// //     if (getStringValuesSF("token") != null) {
// //
// //    if (tokenStorage == null) {
// //      Timer(Duration(seconds: 2), () => MyNavigator.goToLoginScreen(context));
// //    } else {
// //      Timer(Duration(seconds: 2), () => MyNavigator.goToBottomNavBar(context));
// //    }
//   }
//
// //  Future<void> checkingTheSaved() async {
// //    SharedPreferences prefs = await SharedPreferences.getInstance();
// //    var token = prefs.getString('token');
// //    print(token);
// //
// //    runApp(MaterialApp(home: token == null ? LoginScreen() : BottomNavBar()));
// //  }
//
//   checkingTheSavedData() async {
//     SharedPreferences prefs = await SharedPreferences.getInstance();
//     print('++++++++++++++++++++++++++++++++');
//     String username = prefs.getString('token');
//     print(storage.getItem('token'));
//     print(username);
//     token = getStringValuesSF("token").toString();
//     print(token);
//   }
//
//   getStringValuesSF(String key) async {
//     SharedPreferences prefs = await SharedPreferences.getInstance();
//     String stringValue = prefs.getString(key);
//
//     print("getStringValuesSF: $stringValue");
//     print('---------------------');
//     print("username " + prefs.getString('token').toString());
//     print('---------------------');
//     if (prefs.getString('token') == null) {
//       print('IF');
//       print(token);
//
//       Timer(Duration(seconds: 2), () => MyNavigator.goToLoginScreen(context));
//     } else {
//       print('ELSE');
//       print(token);
//       userDetailsDataApi(prefs.getString('token').toString());
//
// //      Timer(Duration(seconds: 2), () => MyNavigator.goToBottomNavBar(context));
//     }
//
//     return stringValue.toString();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: Stack(
//         fit: StackFit.expand,
//         children: <Widget>[
//           Container(
//             decoration: BoxDecoration(
//               image: DecorationImage(
//                 image: AssetImage('assets/images/splash_bg.png'),
//                 fit: BoxFit.cover,
//               ),
//             ),
//             // decoration: BoxDecoration(color:  Color.fromRGBO(252, 132, 20, 10),),
//           ),
//           Column(
//             crossAxisAlignment: CrossAxisAlignment.center,
//             mainAxisSize: MainAxisSize.min,
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: <Widget>[
//               Image.asset(
//                 'assets/images/zerklogo_splash.png',
//                 alignment: Alignment.center,
//                 height: 300,
//                 width: 300,
//               ),
//             ],
//           ),
//         ],
//       ),
//     );
//   }
//
//   //-------------UserDetailsDataAPI--------------------------------
//   Future<UserDetailsData> userDetailsDataApi(String token) async {
//     final response = await http.get(
//       baseurl.baseurl + "user/details",
//       headers: <String, String>{
//         'Accept': 'application/json',
//         'Authorization': 'Bearer ' + token,
//       },
//     );
//
//     if (response.statusCode == 200) {
//       print(json.decode(response.body));
//       userDetailsData = List<UserDetails>();
// //      _showDialog('loginnnn');
//       // setStringValuesSF("UserDetailsData", response.body.toString());
//
//       storage.setItem("UserDetailsData", storage.getItem('token'));
//
//       var parsedJson = json.decode(response.body);
//       var success = UserDetails.fromJson(parsedJson);
//
//       userDetailsData = List<UserDetails>();
//       userDetailsData.add(UserDetails(data: success.data));
//       print('||||||||' + userDetailsData[0].data.id.toString());
//       print('||||||||' + userDetailsData[0].data.firstName.toString());
//       print('||||||||' + userDetailsData[0].data.lastName.toString());
//       print('||||||||' + userDetailsData[0].data.email.toString());
//       print('||||||||' + userDetailsData[0].data.avatar.toString());
//
//       print('||||||||' + success.data.id.toString());
//       print('||||||||' + success.data.firstName.toString());
//       print('||||||||' + success.data.lastName.toString());
//       print('||||||||' + success.data.email.toString());
//       print('||||||||' + success.data.avatar.toString());
//       print('sajdksajkdljaskldjsakld' + success.data.totalAvails.toString());
//
//       storage.setItem('userid', success.data.id.toString());
//       storage.setItem('firstName', success.data.firstName.toString());
//       storage.setItem('lastName', success.data.lastName.toString());
//       storage.setItem('email', success.data.email.toString());
//       storage.setItem('avatar', success.data.avatar.toString());
//       storage.setItem('member', success.data.member.toString());
//       storage.setItem(
//           'cancelSubscription', success.data.cancelSubscription.toString());
//       // storage.setItem('totalAvails', success.data.totalAvails.toString());
//       if (success.data.totalAvails != null) {
//         print("isNOTequaltoNULL");
//         storage.setItem('totalAvails', success.data.totalAvails.toString());
//       } else {
//         storage.setItem('totalAvails', "0");
//         print("isEqualtoNULL");
//       }
//       print(storage.getItem("avatar").toString() +
//           storage.getItem("email").toString());
//       print("-------///////////////////////////////////////////-------ID:" +
//           userDetailsData[0].data.id.toString() +
//           success.data.id.toString());
//       Navigator.popAndPushNamed(context, '/BottomNavBar');
//       return UserDetailsData.fromJson(json.decode(response.body));
//     } else {
//       print(response.statusCode.toString());
//       print(response.body.toString());
//       String message = json.decode(response.body)['message'] ?? '';
//       String data = json.decode(response.body)['data'] ?? '';
//       _showDialog(message);
//       if (message == 'The access token is invalid.') {
//         Navigator.popAndPushNamed(context, '/LoginScreen');
//       } else {
//         Navigator.popAndPushNamed(context, '/BottomNavBar');
//       }
//
//       // If the server did not return a 200 OK response,
//       // then throw an exception.
//       throw Exception('Failed to load album');
//     }
//   }
//
//   void _showDialog(String contentstring) {
//     // flutter defined function
//     showDialog(
//       barrierDismissible: true,
//       context: context,
//       builder: (BuildContext context) {
//         // return object of type Dialog
//         return AlertDialog(
//           elevation: 5,
//           content: Row(
//             mainAxisAlignment: MainAxisAlignment.center,
//             crossAxisAlignment: CrossAxisAlignment.center,
//             children: [
//               Text(
//                 "$contentstring",
//                 textAlign: TextAlign.justify,
//                 // maxLines: 2,
//                 style: TextStyle(
//                     color: Colors.orange[700], fontWeight: FontWeight.bold),
//
//                 // "Sign Up Successfully",
//               ),
//             ],
//           ),
//         );
//       },
//     );
//   }
// }
