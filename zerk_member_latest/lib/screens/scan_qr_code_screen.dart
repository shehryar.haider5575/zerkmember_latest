import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:Zerk/model/PushNotificationModel.dart';
import 'package:Zerk/model/model_NotificationVideoUrl.dart';
import 'package:Zerk/model/model_userDetails.dart';
import 'package:Zerk/screens/profile_widget.dart';
import 'package:Zerk/utils/baseurl.dart';
// import 'package:Zerk/utils/video_items.dart';
import 'package:fcm_config/fcm_config.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:localstorage/localstorage.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:qrscan/qrscan.dart' as scanner;
import 'package:video_player/video_player.dart';

import 'duplicate_video_item_class.dart';

class ScanQRCodeScreen extends StatefulWidget {
  bool facilityAccessAd;
  ScanQRCodeScreen({
    this.facilityAccessAd,
    Key key,
  }) : super(key: key);
  @override
  _ScanQRCodeScreenState createState() => _ScanQRCodeScreenState();
}

class _ScanQRCodeScreenState extends State<ScanQRCodeScreen> {
  Uint8List bytes = Uint8List(0);
  GlobalKey globalKey = new GlobalKey();
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
  String _dataString = "Hello from this QR";
  TextEditingController _inputController;
  TextEditingController _outputController;
  String userid, token;
  String qrCodeId;
  Baseurl baseurl = Baseurl();
  final FirebaseMessaging firebaseMessaging = FirebaseMessaging.instance;
  PushNotification _notificationInfo;
  final LocalStorage storage = new LocalStorage('token');
  bool visible = false;
  bool facilityAccessAd;
  String videoUrl, facilityID;

  VideoPlayerController _videoPlayerController;
  _handleNotification(Map<dynamic, dynamic> message) async {
    var data = message['data'] ?? message;
    String expectedAttribute = data['expectedAttribute'];
    // print(data);
    // print(expectedAttribute);

    // String dataMap =
    //     json.decode(message['notification']['body']['message']);
    print("onMessage--------------1111");
    print("onMessage----: $message");
    // print("onMessage----: $dataMap");
    print('---------------');
    print(message['notification']['body']);
    print('---------------');

    var parsedJson = json.decode(message['notification']['body']);
    print(parsedJson['message'].toString());
    print('mmmmmmmmm');
    var success = NotificationVideoUrl.fromJson(parsedJson);
    print('this is success ' + success.message);
    // _showDialog(success.message.toString(), false);
    print(success.videoUrl);
    print('****** THIS IS HANDLE NOTIFICATION METHOD');
    print('THIS IS TOTAL RESPONSE $success');
    print('THIS IS VIDEO URL ${success.videoUrl}');
    print('HANDLE RESPONSE OVER');
    facilityID = success.facility.toString();
    videoUrl = success.videoUrl;
    print('this is old video url');
    // print(
    //     'this is new video url ${message['notification']['body']['video_url']}');
    print(videoUrl);

    Map<dynamic, dynamic> newBody =
        jsonDecode(message['notification']['body']['video_url']);
    print('this is new body ${newBody.values.first}');
    String updatedUrl = newBody.values.first;
    print('this is new url $updatedUrl}');
    _showDialog(success.message, true);

    if (videoUrl != null) {
      print('this is video url not null');
      _showDialog(success.message, true);
      Future.delayed(Duration(seconds: 2), () {
        // final chewieController = ChewieController(
        //   videoPlayerController: _videoPlayerController,
        //   autoPlay: true,
        //   looping: true,
        // );
        //
        // final playerWidget = Chewie(
        //   controller: chewieController,
        // );
        print('[INFO] VIDEO PLAY BEGIN');
        print('[INFO] THIS IS video url ${success.videoUrl}');
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => DuplicateVideoItems(
              // videoPlayerController: _videoPlayerController,
              userid: userid,
              facilityID: facilityID,
              videoUrl: success.videoUrl,
            ),
          ),
        );
        print('VIDEO PLAY END');
      });
      loadProgress(true);
      setState(() {
        storage.setItem("facilityAccessAd", true);
      });
    } else {
      // if(success.message.isNotEmpty){
      //
      // }
      _showDialog("videoUrlIsInValid", false);
      widget.facilityAccessAd == false
          ? loadProgress(false)
          : loadProgress(false);
    }
    if (success.message == 'Access Granted Successfully') {
      _showNotificationWithoutSound('Access Granted Successfully');
      _showNotificationWithoutSound("The access has been granted");
    }

    /// [...]
  }

  final Map<String, Item> _items = <String, Item>{};

  Item _itemForMessage(Map<String, dynamic> message) {
    String notification = '';
    notification = message['notification']['body'];
    print(notification);
    final dynamic data = message['data'] ?? message;
    print(data);
    final String itemId = data['id'];
    print(itemId);
    final Item item = _items.putIfAbsent(
        itemId, () => Item(itemId: itemId, data: notification))
      ..status = data['status'];
    return item;
  }

  void _navigateToItemDetail(Map<String, dynamic> message) {
    final Item item = _itemForMessage(message);
    // Clear away dialogs
    Navigator.popUntil(context, (Route<dynamic> route) => route is PageRoute);
    if (!item.route.isCurrent) {
      Navigator.push(context, item.route);
    }
  }

  void _showDialog(String contentstring, bool value) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        if (value == false) {
          print(value);
          Future.delayed(Duration(seconds: 1), () {
            Navigator.of(context).pop(true);
          });
        } else {}
        return AlertDialog(
          content: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Text(
                  contentstring,
                  textAlign: TextAlign.left,
                  maxLines: 4,
                  style: TextStyle(
                      color: Colors.orange[700], fontWeight: FontWeight.bold),
                ),
              )
            ],
          ),
        );
      },
    );
  }

  Future _showNotificationWithoutSound(String string) async {
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        'your channel id', 'your channel name', 'your channel description',
        playSound: false, importance: Importance.max, priority: Priority.high);
    var iOSPlatformChannelSpecifics =
        new IOSNotificationDetails(presentSound: false);
    var platformChannelSpecifics = new NotificationDetails(
        android: androidPlatformChannelSpecifics,
        iOS: iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
      0,
      'ZERK',
      '$string',
      platformChannelSpecifics,
      payload: '$string',
    );
  }

  Future onSelectNotification(String payload) async {
    showDialog(
      context: context,
      builder: (_) {
        return new AlertDialog(
          title: Text("ZERK"),
          content: Text("$payload"),
        );
      },
    );
  }

  Future<void> _firebaseMessagingBackgroundHandler(
      RemoteMessage message) async {
    print("Handling a background message: ${message.messageId}");
  }

  void registerNotification() async {
    print('this is register notification function');
    FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
    NotificationSettings settings = await firebaseMessaging.requestPermission(
      alert: true,
      badge: true,
      provisional: false,
      sound: true,
    );
    if (settings.authorizationStatus == AuthorizationStatus.authorized) {
      print('User granted permission');

      FirebaseMessaging.onMessage.listen((RemoteMessage message) {
        print(
            'Message title: ${message.notification?.title}, body: ${message.notification?.body}, data: ${message.data}');

        // Parse the message received
        PushNotification notification = PushNotification(
          title: message.notification?.title,
          body: message.notification?.body,
          dataTitle: message.data['title'],
          dataBody: message.data['body'],
        );
        //
        setState(() {
          _notificationInfo = notification;
        });

        if (_notificationInfo != null) {
          Map<dynamic, dynamic> newBody = jsonDecode(message.notification.body);
          print('this is new body ${newBody.values.first}');
          print('this is listen notification not null');
          print('[INFO] THIS IS VIDEO PLAY BEGIN');
          print('[INFO] THIS IS FINAL URL $videoUrl');
          Future.delayed(Duration(seconds: 2), () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => DuplicateVideoItems(
                  // videoPlayerController: _videoPlayerController,
                  userid: userid,
                  facilityID: facilityID,
                  videoUrl: newBody.values.first,
                ),
              ),
            );
            print('[INFO] THIS IS VIDEO PLAY END');
          });
          // For displaying the notification as an overlay
          _showNotificationWithoutSound(_notificationInfo.body);
          print('****** HERE INSTANCE IS PRINTED ********');
          var msg;
          newBody.forEach((key, value) {
            if (key == 'message') {
              msg = value;
            }
          });
          _showDialog(msg, true);
          print('[INFO] THIS IS MESSAGE ITSELF $message');
          print('[INFO] THIS IS VIDEO URL OLD $videoUrl');

          print('this is new body ${newBody.keys}');
          print('this is new body ${newBody[0]}');
          print('${newBody.runtimeType}');
          print('${message.notification.body.runtimeType}');
          print('****** END OF INSTANCE *****');
        }
      });
    } else {
      _showDialog("Please give required permissions", true);
      print('User declined or has not accepted permission');
    }
  }

  checkForInitialMessage() async {
    // await Firebase.initializeApp();
    RemoteMessage initialMessage =
        await FirebaseMessaging.instance.getInitialMessage();

    if (initialMessage != null) {
      PushNotification notification = PushNotification(
        title: initialMessage.notification?.title,
        body: initialMessage.notification?.body,
        dataTitle: initialMessage.data['title'],
        dataBody: initialMessage.data['body'],
      );
      setState(() {
        _notificationInfo = notification;
        print('this is check for initial message');
        // _totalNotifications++;
      });
    }
  }

  // final FlutterLocalNotificationsPlugin _flutterLocalNotificationsPlugin =
  //     FlutterLocalNotificationsPlugin();

  @override
  initState() {
    super.initState();
    checkForInitialMessage();
    registerNotification();
    VideoPlayerController.network(videoUrl);
    print('this is scan screen');
    // loadProgress(false);
    this._inputController = new TextEditingController();
    this._outputController = new TextEditingController();
    userid = storage.getItem('userid');
    token = storage.getItem('token');

    print("--------------userid:" +
        storage.getItem('userid') +
        userid.toString());
    _generateBarCode(userid);
    qrCodeId = userid;
    userDetailsDataApi(token);
    print(".--------------" + facilityAccessAd.toString());
    print(".--------------" + facilityAccessAd.toString());

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      PushNotification notification = PushNotification(
        title: message.notification?.title,
        body: message.notification?.body,
        dataTitle: message.data['title'],
        dataBody: message.data['body'],
      );
      print('*** THIS IS NOTIFICATION ****');
      print('$notification');
      print('this is notificationn info $_notificationInfo');
      print('this is message body ${message.notification.body}');
      print('**** THIS IS NOTIFICATION ***');
      _handleNotification(message.toMap());
      _showDialog(message.toString(), false);
      Future.delayed(Duration(seconds: 2), () {
        print('IN THE VIDEO PLAY CONDITION');
        if (videoUrl != null) {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => DuplicateVideoItems(
                userid: userid,
                facilityID: facilityID,
                videoUrl: videoUrl,
              ),
            ),
          );
        } else {
          print('this is video not played condition');
        }
      });
    });

//     _firebaseMessaging.configure(
//       onMessage: (Map<String, dynamic> message) async {
//         // String dataMap =
//         //     json.decode(message['notification']['body']['message']);
//         print("onMessage--------------1111");
//         print("onMessage----: $message");
//         // print("onMessage----: $dataMap");
//         print('---------------');
//         print(message['notification']['body']);
//         print('---------------');
//
//         var parsedJson = json.decode(message['notification']['body']);
//         print(parsedJson['message'].toString());
//         print('mmmmmmmmm');
//         var success = NotificationVideoUrl.fromJson(parsedJson);
//         print(success.message);
//         // _showDialog(success.message.toString(), false);
//         print(success.videoUrl);
//         facilityID = success.facility.toString();
//         videoUrl = success.videoUrl;
//         print(videoUrl);
//
//         if (videoUrl != null) {
//           Navigator.push(
//             context,
//             MaterialPageRoute(
//               builder: (context) => DuplicateDuplicateVideoItems(
//                 userid: userid,
//                 facilityID: facilityID,
//                 videoUrl: videoUrl,
//               ),
//             ),
//           );
//           // loadProgress(true);
//           // setState(() {
//           //   // storage.setItem("facilityAccessAd", true);
//           // });
//         } else {
//           // if(success.message.isNotEmpty){
//           //
//           // }
//           // _showDialog("videoUrlIsInValid", false);
//           // widget.facilityAccessAd == false
//           //     ? loadProgress(false)
//           //     :
//           // loadProgress(false);
//         }
//         if (success.message == 'Access Granted Successfully') {
//           _showNotificationWithoutSound('Access Granted Successfully');
//           _showNotificationWithoutSound("The access has been granted");
//         }
//
//         print('????????????');
//         if (Platform.isIOS) {
//           message = _handleNotification(message);
//         }
//         // _showDialog(string.toString(), true);
// //          _showItemDialog(message);
//       },
//       onLaunch: (Map<String, dynamic> message) async {
//         print("onLaunch: $message");
//         if (Platform.isIOS) {
//           message = _handleNotification(message);
//         }
//         String string = message['notification']['body'];
//         _showDialog(string.toString(), true);
//         // _navigateToItemDetail(message);
//         myBackgroundMessageHandler(message);
//       },
//       onResume: (Map<String, dynamic> message) async {
//         print("onResume: $message");
//         if (Platform.isIOS) {
//           message = _handleNotification(message);
//         }
//         String string = message['notification']['body'];
//         _showDialog(string.toString(), true);
//         myBackgroundMessageHandler(message);
//       },
//     );
    var initializationSettingsAndroid =
        new AndroidInitializationSettings('zerk');
    var initializationSettingsIOS = new IOSInitializationSettings();
    var initializationSettings = new InitializationSettings(
        android: initializationSettingsAndroid, iOS: initializationSettingsIOS);
    flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);
  }

  Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) {
    if (message.containsKey('data')) {
      // Handle data message
      final dynamic data = message['data'];
      if (Platform.isIOS) {
        message = _handleNotification(message);
      }
      print(data.toString());
    }

    if (message.containsKey('notification')) {
      // Handle notification message
      final dynamic notification = message['notification'];
      print(notification.toString());
    }
//     _firebaseMessaging.configure(
//       onMessage: (Map<String, dynamic> message) async {
//         print("onMessage: $message");
//         print("--------------------notification--------------------");
//         String string = message['notification']['body'];
// //        String newbody = message['notification']['body']['title'];
// //        print(newbody);
//         _showDialog('onMessage' + string.toString(), true);
// //          _showItemDialog(message);
//       },
//       onLaunch: (Map<String, dynamic> message) async {
//         print("onLaunch: $message");
//         String string = message['notification']['body'];
//         _showDialog('onLaunch $string', true);
//         _navigateToItemDetail(message);
// //          myBackgroundMessageHandler(message);
//       },
//       onResume: (Map<String, dynamic> message) async {
//         print("onResume: $message");
//         String string = message['notification']['body'];
//         _showDialog('onResume' + string.toString(), true);
// //          _navigateToItemDetail(message);
// //          myBackgroundMessageHandler(message);
//       },
//     );
//     _firebaseMessaging
//         .requestNotificationPermissions(const IosNotificationSettings(
//       sound: true,
//       badge: true,
//       alert: true,
//       provisional: false,
//     ));
//     _firebaseMessaging.onIosSettingsRegistered
//         .listen((IosNotificationSettings settings) {
//       print("Settings registered: $settings");
//     });
//
//     _firebaseMessaging.getToken().then((String token) {
//       assert(token != null);
//       setState(() {});
//     });

    // Or do other work.
  }

  Future _scan() async {
    String barcode = await scanner.scan();
    this._outputController.text = barcode;
  }

  Future _scanPhoto() async {
    String barcode = await scanner.scanPhoto();
    this._outputController.text = barcode;
  }

  Future _scanPath(String path) async {
    String barcode = await scanner.scanPath(path);
    this._outputController.text = barcode;
  }

  Future _scanBytes() async {
    File file = await ImagePicker.pickImage(source: ImageSource.camera);
    Uint8List bytes = file.readAsBytesSync();
    String barcode = await scanner.scanBytes(bytes);
    this._outputController.text = barcode;
  }

  Future _generateBarCode(String inputCode) async {
    print(inputCode);
    print('inputCode--------------------');
    Uint8List result = await scanner.generateBarCode(inputCode);
    print('*********');
    print('THIS IS QR CODE ID: ' + result.toString());
    print('**********');

    this.setState(() => this.bytes = result);
    print(bytes.toString());
    print(result.toString() + 'result--------------------');
    print('*********');
    print('THIS IS QR CODE ID: ' + result.toString());
    print('**********');
  }

  loadProgress(bool value) {
    if (visible == value) {
      setState(() {
        visible = value;
      });
    } else {
      setState(() {
        visible = value;
      });
    }
  }

  @override
  void dispose() {
    super.dispose();
    VideoPlayerController.network(videoUrl).dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/images/login_bg.png'),
              fit: BoxFit.cover,
              alignment: Alignment.topLeft),
        ),
        child: Stack(
          children: [
            //VideoPlayerWidget

            // widget.facilityAccessAd == null

// Settings TitleL
            Positioned(
              top: Alignment.topCenter.x,
              child: Container(
                // color: Colors.black,
                height: MediaQuery.of(context).size.height * .22,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width / 6,
                      // color: Colors.red,
                      child: InkWell(
                        onTap: () {
                          // loadProgress(true);
                          Navigator.popAndPushNamed(context, '/BottomNavBar');
                          // Navigator.pop(
                          //     context,
                          //     MaterialPageRoute(
                          //         fullscreenDialog: true,
                          //         builder: (context) => BottomNavBar()),
                          //   );
                        },
                        child: SizedBox(
                          child: Icon(
                            Icons.arrow_back,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width / 1.5,
                      //   color: Colors.black,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            "Scan QR Code ",
                            // "Scan QR Code ${widget.facilityAccessAd.toString()}",
                            style: TextStyle(
                              fontSize: 22.0,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),

            // icons
            Positioned(
              // top: Alignment.bottomRight.x,
              right: Alignment.centerRight.x,
              left: Alignment.centerLeft.x,
              bottom: MediaQuery.of(context).size.height -
                  MediaQuery.of(context).size.height * .25,
              // bottom: Alignment.centerLeft.x,
              child: Container(
                // color: Colors.red,
                height: MediaQuery.of(context).size.height * .10,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    // Container(
                    //   // width: MediaQuery.of(context).size.width / 1.5,
                    //   // color: Colors.black,
                    //   child: Row(
                    //     crossAxisAlignment: CrossAxisAlignment.center,
                    //     mainAxisAlignment: MainAxisAlignment.center,
                    //     children: [
                    //       Padding(
                    //         padding: EdgeInsets.only(left: 10.0),
                    //         child: SizedBox(
                    //           child: Icon(
                    //             Icons.hourglass_full,
                    //             color: Colors.white,
                    //           ),
                    //         ),
                    //       ),
                    //     ],
                    //   ),
                    // ),
                    Container(
                      // width: MediaQuery.of(context).size.width / 6,
                      // color: Colors.brown,
                      child: InkWell(
                        onTap: () {
                          // loadProgress(true);
                          // Navigator.popAndPushNamed(context, '/BottomNavBar');
                          // Navigator.pop(
                          //     context,
                          //     MaterialPageRoute(
                          //         fullscreenDialog: true,
                          //         builder: (context) => BottomNavBar()),
                          //   );
                        },
                        child: SizedBox(
                          width: MediaQuery.of(context).size.width / 6,
                          height: MediaQuery.of(context).size.height / 11,
                          child: Container(
                            // color: Colors.black,
                            // child: Image.asset('assets/images/wine-bottle.png',color: Colors.orange[500],),
                            child: CircleAvatar(
                                backgroundColor: Colors.white,
                                child: Padding(
                                  padding: EdgeInsets.all(10.0),
                                  child: Image.asset(
                                    'assets/images/water-bottle.png',
                                    color: Colors.orange[500],
                                  ),
                                )),
                          ),
                        ),
                        // SizedBox(
                        //   child: Icon(
                        //     Icons.arrow_back,
                        //     color: Colors.white,
                        //   ),
                        // ),
                      ),
                    ),
                    Container(
                      // width: MediaQuery.of(context).size.width / 6,
                      // color: Colors.brown,
                      child: InkWell(
                        onTap: () {
                          // loadProgress(true);
                          // Navigator.popAndPushNamed(context, '/BottomNavBar');
                          // Navigator.pop(
                          //     context,
                          //     MaterialPageRoute(
                          //         fullscreenDialog: true,
                          //         builder: (context) => BottomNavBar()),
                          //   );
                        },
                        child: SizedBox(
                          width: MediaQuery.of(context).size.width / 6,
                          height: MediaQuery.of(context).size.height / 11,
                          child: Container(
                            // color: Colors.black,
                            // child: Image.asset('assets/images/wine-bottle.png',color: Colors.orange[500],),
                            child: CircleAvatar(
                                backgroundColor: Colors.white,
                                child: Padding(
                                  padding: EdgeInsets.all(10.0),
                                  child: Image.asset(
                                    'assets/images/headphone.png',
                                    color: Colors.orange[500],
                                  ),
                                )),
                          ),
                        ),
                        // SizedBox(
                        //   child: Icon(
                        //     Icons.arrow_back,
                        //     color: Colors.white,
                        //   ),
                        // ),
                      ),
                    ),
                    Container(
                      // width: MediaQuery.of(context).size.width / 6,
                      // color: Colors.brown,
                      child: InkWell(
                        onTap: () {
                          // loadProgress(true);
                          // Navigator.popAndPushNamed(context, '/BottomNavBar');
                          // Navigator.pop(
                          //     context,
                          //     MaterialPageRoute(
                          //         fullscreenDialog: true,
                          //         builder: (context) => BottomNavBar()),
                          //   );
                        },
                        child: SizedBox(
                          width: MediaQuery.of(context).size.width / 6,
                          height: MediaQuery.of(context).size.height / 11,
                          child: Container(
                            // color: Colors.black,
                            // child: Image.asset('assets/images/wine-bottle.png',color: Colors.orange[500],),
                            child: CircleAvatar(
                                backgroundColor: Colors.white,
                                child: Padding(
                                  padding: EdgeInsets.all(10.0),
                                  child: Image.asset(
                                    'assets/images/towel.png',
                                    color: Colors.orange[500],
                                  ),
                                )),
                          ),
                        ),
                        // SizedBox(
                        //   child: Icon(
                        //     Icons.arrow_back,
                        //     color: Colors.white,
                        //   ),
                        // ),
                      ),
                    ),
                  ],
                ),
              ),
            ),

            // Settings Card Widget
            Positioned(
              //   top: 110,
              left: Alignment.center.y,
              right: Alignment.center.y,
              top: Alignment.center.y,
              bottom: Alignment.center.y,
              // top: Alignment.center.x,
              child: Padding(
                padding: const EdgeInsets.only(top: 60.0),
                child: Align(
                  alignment: Alignment.center,
                  child: SingleChildScrollView(
                    child: Container(
                      margin: const EdgeInsets.only(top: 30.0),
                      //  alignment: Alignment.center,
                      width: MediaQuery.of(context).size.width,

                      //    color: Colors.amber,
                      // height: MediaQuery.of(context).size.height * 80,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20.0),
                        child: SingleChildScrollView(
                          child: Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20),
                            ),
                            // elevation: 5,
                            shadowColor: Colors.grey,
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 30.0),
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  SizedBox(
                                    height: 20,
                                  ),
                                  SizedBox(
                                    width:
                                        MediaQuery.of(context).size.width / 6,
                                    height:
                                        MediaQuery.of(context).size.height / 11,
                                    child: Container(
                                      // color: Colors.black,
                                      child: Image.asset(
                                          'assets/images/qrcodecamera_icon.png'),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  Container(
                                    child: Text(
                                      "Please scan QRCode\nwith Required device",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontSize: 18.0,
                                        color: Colors.grey,
                                        fontWeight: FontWeight.normal,
                                      ),
                                    ),
                                  ),
                                  // _contentWidget(),
                                  // _qrCodeWidgetIos(this.bytes, context),
                                  // Platform.isIOS
                                  //     ? QrImage(
                                  //         data: qrCodeId,
                                  //         size: MediaQuery.of(context)
                                  //                 .size
                                  //                 .height /
                                  //             4,
                                  //         backgroundColor: Colors.white,
                                  //       )
                                  //     :
                                  showQR(context),
                                  // Align(
                                  //   alignment: Alignment.center,
                                  //   child: Container(
                                  //     child: Row(
                                  //       mainAxisAlignment:
                                  //           MainAxisAlignment.center,
                                  //       crossAxisAlignment:
                                  //           CrossAxisAlignment.end,
                                  //       children: [
                                  //         SizedBox(
                                  //           // width: 220.0,
                                  //           // height: 60.0,
                                  //           height: MediaQuery.of(context)
                                  //                   .size
                                  //                   .width /
                                  //               8,
                                  //           width: MediaQuery.of(context)
                                  //                   .size
                                  //                   .width /
                                  //               2,
                                  //
                                  //           child: RaisedButton(
                                  //             shape: RoundedRectangleBorder(
                                  //                 borderRadius:
                                  //                     BorderRadius.circular(
                                  //                         42.0),
                                  //                 side: BorderSide(
                                  //                     color: Colors.white)),
                                  //             onPressed: () {
                                  //               // Navigator.pushNamed(
                                  //               //     context, '/DuplicateDuplicateVideoItems');
                                  //               Navigator.push(
                                  //                 context,
                                  //                 MaterialPageRoute(
                                  //                   builder: (context) =>
                                  //                       DuplicateDuplicateVideoItems(
                                  //                     userid: userid,
                                  //                     facilityID: facilityID,
                                  //                     videoUrl: videoUrl,
                                  //                   ),
                                  //                 ),
                                  //               );
                                  //               // loadProgress(false);
                                  //             },
                                  //             color: Colors.white,
                                  //             textColor: Colors.orange[300],
                                  //             child: Text("SCAN",
                                  //                 style: TextStyle(
                                  //                   fontSize: 20.0,
                                  //                   letterSpacing: 1,
                                  //                   fontWeight: FontWeight.bold,
                                  //                 )),
                                  //           ),
                                  //         ),
                                  //       ],
                                  //     ),
                                  //   ),
                                  // ),
                                  SizedBox(
                                    height: 40,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
            // storage.getItem("facilityAccessAd") == true
            //  ?
            Visibility(
              visible: visible,
              child: Positioned(
                child: DuplicateVideoItems(
                  // videoPlayerController: VideoPlayerController.network(
                  //   'https://www.youtube.com/watch?v=9xwazD5SyVg&ab_channel=applekids',
                  // ),
                  looping: false,
                  autoplay: true,
                  facilityID: '6',
                  userid: '1',
                ),
                // ListView(
                //   children: [
                //
                //   ],
                // ),
              ),
            ),

            // : Container(
            //     // child: Text("asdsadasdasd"),
            //     ),
          ],
        ),
      ),
    );
  }

  Future<NotificationVideoUrl> notificationVideoUrl(
      Map<String, dynamic> message) async {
    final response = await http.get(Uri.parse(message.toString()));
    print(response.statusCode.toString());
    print(response.body.toString());
    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      print(response.statusCode.toString());
      print(response.body.toString());
      var parsedJson = json.decode(message.toString());
      var success = NotificationVideoUrl.fromJson(parsedJson);
      print(success.videoUrl.toString());
      // storage.setItem('totalAvails', success.videoUrl.toString());
      return NotificationVideoUrl.fromJson(
          json.decode(message['notification']['body']));
      // return NotificationVideoUrl.fromJson(jsonDecode(response.body));
    } else {
      print(response.statusCode.toString());
      print(response.body.toString());
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load Notification Video Url');
    }
  }

  Future<UserDetails> userDetailsDataApi(String token) async {
    final response = await http.get(
      Uri.parse(baseurl.baseurl + "user/details"),
      headers: <String, String>{
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + token,
      },
    );

    if (response.statusCode == 200) {
      print("----------------userDetailsDataApi---------");
      print(json.decode(response.body));
      var parsedJson = json.decode(response.body);
      var success = UserDetails.fromJson(parsedJson);
      storage.setItem('totalAvails', success.data.totalAvails.toString());
      print('******************');
      print('THIS IS TOTAL AVAILS ${storage.getItem('totalAvails')}');
      print('******************');
      return UserDetails.fromJson(json.decode(response.body));
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load data');
    }
  }

  _contentWidget() {
    final bodyHeight = MediaQuery.of(context).size.height -
        MediaQuery.of(context).viewInsets.bottom;
    return Container(
      color: const Color(0xFFFFFFFF),
      child: Column(
        children: <Widget>[
          Expanded(
            child: Center(
              child: RepaintBoundary(
                key: globalKey,
                child: QrImage(
                  data: _dataString,
                  size: 0.5 * bodyHeight,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget showQR(BuildContext context) {
    if (Platform.isAndroid) {
      return _qrCodeWidgetAndroid(this.bytes, context);
    }
    if (Platform.isIOS) {
      return _qrCodeWidgetIos(this.bytes, context);
    }
  }

  Widget _qrCodeWidgetAndroid(Uint8List bytes, BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(5),
      child: Center(
        // elevation: 6,
        child: Column(
          children: <Widget>[
            Padding(
              padding:
                  EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 190,
                    child: bytes.isEmpty
                        ? Center(
                            child: Text('Empty code ... ',
                                style: TextStyle(color: Colors.black38)),
                          )
                        : Image.memory(bytes),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _qrCodeWidgetIos(Uint8List bytes, BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(5),
      child: Center(
        // elevation: 6,
        child: Column(
          children: <Widget>[
            Padding(
              padding:
                  EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: MediaQuery.of(context).size.height / 3.5,
                    child: QrImage(
                      data: bytes.toString(),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
