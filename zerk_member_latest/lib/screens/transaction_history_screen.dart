import 'dart:convert';
import 'dart:io';

import 'package:Zerk/model/model_TransactionHistory.dart';
import 'package:Zerk/utils/baseurl.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';
import 'package:http/http.dart' as http;
import 'package:localstorage/localstorage.dart';

class TransactionHistoryScreen extends StatefulWidget {
  TransactionHistoryScreen({Key key}) : super(key: key);

  @override
  _TransactionHistoryScreenState createState() =>
      _TransactionHistoryScreenState();
}

class _TransactionHistoryScreenState extends State<TransactionHistoryScreen> {
  final LocalStorage storage = new LocalStorage('token');
  Baseurl baseurl = Baseurl();
  Future<TransactionHistory> futureTransactionHistory;
  final ScrollController _scrollController = ScrollController();

  static const int sortName = 0;
  static const int sortStatus = 1;
  bool isAscending = true;
  int sortType = sortName;

  @override
  void initState() {
    super.initState();
    futureTransactionHistory =
        fetchTransactionHistoryLogs(storage.getItem('userid').toString());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // body: _getBodyWidget()

      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/sign_up_bg.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: Stack(
          children: [
            Positioned(
              //     top: Alignment.topCenter.y,
              top: Alignment.topCenter.x,
              left: Alignment.center.x,
              right: Alignment.center.x,
              child: Container(
                color: Colors.orange,
                height: MediaQuery.of(context).size.height * .20,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 25.0, bottom: 20.0, top: 10.0),
                      child: InkWell(
                        onTap: () {
                          Navigator.popAndPushNamed(context, '/BottomNavBar');
                        },
                        child: SizedBox(
                          child: Icon(
                            Icons.arrow_back,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 20.0, bottom: 20.0, top: 10.0),
                      child: Text(
                        "Transaction History",
                        style: TextStyle(
                          fontSize: 22.0,
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              //   top: Alignment.center.x,
              bottom: Alignment.center.x,
              right: Alignment.center.x,
              left: Alignment.center.x,
              height: MediaQuery.of(context).size.height * 0.88,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  alignment: Alignment.bottomCenter,
                  width: MediaQuery.of(context).size.width,
                  //    color: Colors.amber,
                  height: MediaQuery.of(context).size.height * .9,
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15),
                    ),
                    elevation: 20,
                    child: Padding(
                      padding: const EdgeInsets.all(0.0),
                      child: Scrollbar(
                          isAlwaysShown: true,
                          // thickness: 0,
                          controller: _scrollController,
                          child: _getBodyWidget()),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget showProgressLoader(BuildContext context) {
    if (Platform.isAndroid) {
      return CircularProgressIndicator(
        color: Colors.orange[800],
      );
    }
    if (Platform.isIOS) {
      return CupertinoActivityIndicator();
    }
  }

  Future<TransactionHistory> fetchTransactionHistoryLogs(String id) async {
    final response = await http.get(
      Uri.parse(baseurl.baseurl + "transaction/all/$id"),
      headers: <String, String>{
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );

    if (response.statusCode == 200) {
      print(response.body.toString());
      return TransactionHistory.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to TransactionHistory Logs');
    }
  }

  Widget _getBodyWidget() {
    return Container(
        height: MediaQuery.of(context).size.height,
        child: FutureBuilder<TransactionHistory>(
          future: futureTransactionHistory,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data.data.isEmpty) {
                print('this is best case');
                return Center(
                  child: Text(
                    'No Transaction History',
                    textAlign: TextAlign.center,
                    textScaleFactor: 1.2,
                    style: TextStyle(
                        color: Colors.orange[800],
                        fontWeight: FontWeight.normal),
                  ),
                );
              }
              return HorizontalDataTable(
                leftHandSideColumnWidth: 0,
                rightHandSideColumnWidth: 1100,
                isFixedHeader: true,
                headerWidgets: _getTitleWidget(),
                leftSideItemBuilder: generateFirstColumnRow,
                rightSideItemBuilder: _generateRightHandSideColumnRow,
                // itemCount: user.userInfo.length,
                itemCount: snapshot.data.data.length,
                rowSeparatorWidget: const Divider(
                  color: Colors.black54,
                  height: 1.0,
                  thickness: 0.0,
                ),
                // leftHandSideColBackgroundColor: Color(0xFF2A2A2A),
                // rightHandSideColBackgroundColor: Color(0xFFFFFFFF),
              );
            } else if (snapshot.hasError) {
              return Text("${snapshot.error}" + "ccccc");
            }

            // By default, show a loading spinner.
            return Center(child: showProgressLoader(context));
          },
        ));
  }

  List<Widget> _getTitleWidget() {
    return [
      FlatButton(
        color: Colors.red,
        padding: EdgeInsets.all(0),
        child: _getTitleItemWidget(
            'Name' + (sortType == sortName ? (isAscending ? '↓' : '↑') : ''),
            50),
        onPressed: () {
          sortType = sortName;
          isAscending = !isAscending;
          // user.sortName(isAscending);
          setState(() {});
        },
      ),
      // FlatButton(
      //   padding: EdgeInsets.all(0),
      //   child: _getTitleItemWidget(
      //       'Credits' +
      //           (sortType == sortStatus ? (isAscending ? '↓' : '↑') : ''),
      //       100),
      //   onPressed: () {
      //     sortType = sortStatus;
      //     isAscending = !isAscending;
      //     user.sortStatus(isAscending);
      //     setState(() {});
      //   },
      // ),

      // _getTitleItemWidget('        Name ', 200),
      // _getTitleItemWidget('Credits ', 80),
      // _getTitleItemWidget('Remaining Avails ', 150),
      // _getTitleItemWidget('Amount', 100),
      // _getTitleItemWidget('Date', 100),
      // _getTitleItemWidget('Status', 100),

      _getTitleItemWidget('Name ', MediaQuery.of(context).size.width * 0.35),
      _getTitleItemWidget(
        'Credits ',
        MediaQuery.of(context).size.width * 0.35,
      ),
      _getTitleItemWidget(
        'Remaining Avails ',
        MediaQuery.of(context).size.width * 0.35,
      ),
      _getTitleItemWidget(
        'Amount',
        MediaQuery.of(context).size.width * 0.35,
      ),
      _getTitleItemWidget(
        'Date',
        MediaQuery.of(context).size.width * 0.35,
      ),
      // _getTitleItemWidget(
      //   'Renew At',
      //   MediaQuery.of(context).size.width * 0.35,
      // ),
      _getTitleItemWidget(
        'Expired At',
        MediaQuery.of(context).size.width * 0.35,
      ),
      _getTitleItemWidget(
        'Status',
        MediaQuery.of(context).size.width * 0.35,
      ),
    ];
  }

  Widget _getTitleItemWidget(String label, double width) {
    return Container(
      child: Text(label, style: TextStyle(fontWeight: FontWeight.bold)),
      width: width,
      height: 50,
      padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
      alignment: Alignment.center,
    );
  }

  Widget generateFirstColumnRow(BuildContext context, int index) {
    // return Container(
    //   child: Text(snapshot.data.data[index].membership.name),
    //   width: 100,
    //   height: 52,
    //   padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
    //   alignment: Alignment.centerLeft,
    // );
  }

  Widget _generateRightHandSideColumnRow(BuildContext context, int index) {
    return FutureBuilder<TransactionHistory>(
      future: futureTransactionHistory,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          // String remainingAvails = snapshot.data.data[index].remainingAvails;
          storage.setItem("remainingAvailsKey",
              snapshot.data.data[index].remainingAvails.toString());
          return Row(
            children: <Widget>[
              Container(
                // color: Colors.red,
                child: Text(snapshot.data.data[index].membershipName == null
                    ? ""
                    : snapshot.data.data[index].membershipName),
                // width: 150,
                // height: 52,
                width: MediaQuery.of(context).size.width * 0.35,
                padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                alignment: Alignment.center,
              ),
              Container(
                // color: Colors.pink,
                child: Text(snapshot.data.data[index].avails + "/day" == null
                    ? ''
                    : snapshot.data.data[index].avails + "/day"),
                // width: 80,
                // height: 52,
                width: MediaQuery.of(context).size.width * 0.35,
                padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                alignment: Alignment.center,
              ),
              Container(
                // color: Colors.amber,

                child: Text(snapshot.data.data[index].remainingAvails == null
                    ? ''
                    : snapshot.data.data[index].remainingAvails),
                // width: 150,
                // height: 52,
                width: MediaQuery.of(context).size.width * 0.35,
                padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                alignment: Alignment.center,
              ),
              Container(
                // color: Colors.blue,
                child: Text(snapshot.data.data[index].amount.toString() == null
                    ? ''
                    : snapshot.data.data[index].amount.toString()),
                // width: 80,
                // height: 52,
                width: MediaQuery.of(context).size.width * 0.35,
                padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                alignment: Alignment.center,
              ),
              Container(
                // color: Colors.blueGrey,
                child: Text(snapshot.data.data[index].date.toString()),
                // width: 100,
                // height: 52,
                width: MediaQuery.of(context).size.width * 0.35,
                padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                alignment: Alignment.center,
              ),
              // Container(
              //   // color: Colors.blueGrey,
              //   child:
              //       // Text("expiredAt"),
              //       Text(snapshot.data.data[index].renewAt == null
              //           ? ''
              //           : snapshot.data.data[index].renewAt),
              //   // width: 100,
              //   // height: 52,
              //   width: MediaQuery.of(context).size.width * 0.35,
              //   padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
              //   alignment: Alignment.center,
              // ),
              Container(
                // color: Colors.blueGrey,
                child:
                    // Text("expiredAt"),
                    Text(snapshot.data.data[index].expiredAt == null
                        ? ''
                        : snapshot.data.data[index].expiredAt),
                // width: 100,
                // height: 52,
                width: MediaQuery.of(context).size.width * 0.35,
                padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                alignment: Alignment.center,
              ),
              Container(
                // color: Colors.green,
                child: Text(snapshot.data.data[index].status == null
                    ? ''
                    : snapshot.data.data[index].status),
                // width: 100,
                // height: 52,
                width: MediaQuery.of(context).size.width * 0.35,
                padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                alignment: Alignment.center,
              ),
            ],
          );
        } else if (snapshot.hasError) {
          return Text("${snapshot.error}" + "ppppp");
        }

        // By default, show a loading spinner.
        return Center(child: CircularProgressIndicator());
      },
    );
  }
}
