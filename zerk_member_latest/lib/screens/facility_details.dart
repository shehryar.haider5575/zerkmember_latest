import 'dart:ui';

import 'package:flutter/material.dart';

class FacilityListDetails extends StatefulWidget {
  FacilityListDetails({
    Key key,
    @required this.bussiness_name,
    // ignore: non_constant_identifier_names
    this.first_name,
    // ignore: non_constant_identifier_names
    this.last_name,
    this.email,
    this.avatar,
    this.address,
    this.phone,
    // ignore: non_constant_identifier_names
    this.img_url,
  }) : super(key: key);

  // FacilityListDetails({Key key}) : super(key: key);
  // ignore: non_constant_identifier_names
  String bussiness_name = "";
  // ignore: non_constant_identifier_names
  String first_name = "";
  // ignore: non_constant_identifier_names
  String last_name = "";
  String email = "";
  String avatar = "";
  String address = "";
  String phone = "";
  // ignore: non_constant_identifier_names
  String img_url = "";

  @override
  _FacilityListDetailsState createState() => _FacilityListDetailsState();
}

class _FacilityListDetailsState extends State<FacilityListDetails> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/images/sign_up_bg.png'),
              fit: BoxFit.cover,
            ),
          ),
          child: Stack(
            children: [
// Title  Profile Management
              Positioned(
                top: Alignment.topCenter.x,
                child: Container(
                  // color: Colors.orange,
                  height: MediaQuery.of(context).size.height * .22,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width / 6,
                        // color: Colors.red,
                        child: InkWell(
                          onTap: () {
                            Navigator.pop(context, '/BottomNavBar');
                          },
                          child: SizedBox(
                            child: Icon(
                              Icons.arrow_back,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width / 1.5,

                        //   color: Colors.black,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text(
                              "Facility Details",
                              style: TextStyle(
                                fontSize: 22.0,
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),

              // Form Card Widget Profile Management
              Positioned(
                top: 110,
                // top: Alignment.center.x,
                child: Container(
                  //  alignment: Alignment.center,
                  width: MediaQuery.of(context).size.width,
                  //  color: Colors.amber,
                  height: MediaQuery.of(context).size.height * .8,
                  child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20),
                      ),
                      elevation: 30,
                      shadowColor: Colors.grey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            height: MediaQuery.of(context).size.height / 4,
                            width: MediaQuery.of(context).size.width,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Container(
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 30),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        SizedBox(
                                          height: 70,
                                          width: 70,
                                          child: Stack(
                                            children: [
                                              Positioned(
                                                child: widget.avatar == null
                                                    ? Image.network(
                                                        'https://leadagency.club/wp-content/uploads/2021/09/placeholder-660.png')
                                                    : Image.network(
                                                        widget.img_url +
                                                            widget.avatar
                                                                .toString(),
                                                      ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      top: 20.0, left: 20.0),
                                  child: Container(
                                    // color: Colors.blue,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(top: 5.0),
                                          child: Expanded(
                                            child: Container(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  .5,
                                              // color: Colors.green,
                                              child: Text(
                                                widget.bussiness_name
                                                    .toString(),
                                                // 'John Smith',
                                                style: TextStyle(
                                                  fontSize: 20.0,
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(top: 8.0),
                                          child: Expanded(
                                            child: Container(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  .5,
                                              child: Text(
                                                widget.address.toString(),
                                                // snapshot.data.data.address
                                                //     .toString(),
                                                //    snapshot.data.address.toString(),
                                                // '168 West Magnolia St.\nOuter Nunavut, NU \nX0A3Y7',
                                                style: TextStyle(
                                                  // letterSpacing: .0,
                                                  fontSize: 16.0,
                                                  color: Colors.grey,
                                                  fontWeight: FontWeight.normal,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            height: MediaQuery.of(context).size.height / 2,
                            width: MediaQuery.of(context).size.width,
                            // color: Colors.red,
                            child: Column(
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Container(
                                          // color: Colors.red,
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              .05,
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              .4,
                                          child: TextFormField(
                                            enableInteractiveSelection: false,
                                            enabled: false,
                                            decoration: InputDecoration(
                                              // hintText: snapshot
                                              //     .data.data.firstName
                                              //     .toString(),
                                              //    snapshot.data.firstName.toString(),
                                              hintText:
                                                  widget.first_name.toString(),
                                              prefixIcon: SizedBox(
                                                height: 30,
                                                width: 30,
                                                child: Image.asset(
                                                  'assets/images/first_name.png',
                                                  color: Colors.orange,
                                                ),
                                              ),
                                              hintStyle: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 13.0),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(
                                              left: 10.0,
                                              right: 10.0,
                                              top: 20.0),
                                        ),
                                        Container(
                                          // color: Colors.white,
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              .05,
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              .4,
                                          //color: Colors.white
                                          child: TextFormField(
                                            enableInteractiveSelection: false,
                                            enabled: false,
                                            decoration: InputDecoration(
                                                // hintText: snapshot
                                                //     .data.data.lastName
                                                //     .toString(),
                                                hintText:
                                                    widget.last_name.toString(),
                                                prefixIcon: SizedBox(
                                                  height: 30,
                                                  width: 30,
                                                  child: Image.asset(
                                                    'assets/images/last_name.png',
                                                    color: Colors.orange,
                                                  ),
                                                ),
                                                hintStyle: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 12.0)),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                Padding(
                                  padding:
                                      EdgeInsets.only(left: 20.0, right: 25.0),
                                  child: Column(
                                    children: [
                                      Padding(
                                        padding: EdgeInsets.only(top: 5.0),
                                      ),
                                      TextField(
                                        enableInteractiveSelection: false,
                                        enabled: false,
                                        decoration: InputDecoration(
                                            // hintText: snapshot.data.data.dob
                                            //     .toString(),
                                            hintText: "27, Sep , 2020",
                                            prefixIcon: SizedBox(
                                              height: 30,
                                              width: 30,
                                              child: Image.asset(
                                                'assets/images/dob_icon.png',
                                                color: Colors.orange,
                                              ),
                                            ),
                                            hintStyle: TextStyle(
                                                color: Colors.black,
                                                fontSize: 12.0)),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(top: 5.0),
                                      ),
                                      TextField(
                                        enableInteractiveSelection: false,
                                        enabled: false,
                                        decoration: InputDecoration(
                                            // hintText: snapshot.data.data.email
                                            //     .toString(),
                                            hintText: widget.email.toString(),
                                            prefixIcon: SizedBox(
                                              height: 30,
                                              width: 30,
                                              child: Image.asset(
                                                'assets/images/last_name.png',
                                                color: Colors.orange,
                                              ),
                                            ),
                                            hintStyle: TextStyle(
                                                color: Colors.black,
                                                fontSize: 12.0)),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(top: 5.0),
                                      ),
                                      TextField(
                                        enableInteractiveSelection: false,
                                        enabled: false,
                                        decoration: InputDecoration(
                                            hintText: widget.phone.toString(),
                                            //  snapshot.data.data.pa.toString(),
                                            // "**************",
                                            prefixIcon: SizedBox(
                                              height: 30,
                                              width: 30,
                                              child: Image.asset(
                                                'assets/images/password_icon.png',
                                                color: Colors.orange,
                                              ),
                                            ),
                                            hintStyle: TextStyle(
                                                color: Colors.black,
                                                fontSize: 12.0)),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(top: 5.0),
                                      ),
                                      TextField(
                                        enableInteractiveSelection: false,
                                        enabled: false,
                                        decoration: InputDecoration(
                                            hintText: widget.address.toString(),
                                            // snapshot.data.data.address
                                            //     .toString(),
                                            // "168 West Magnolia St. Outer Nunavut, NU X0A 3Y7",
                                            prefixIcon: SizedBox(
                                              height: 30,
                                              width: 30,
                                              child: Image.asset(
                                                'assets/images/password_icon.png',
                                                color: Colors.orange,
                                              ),
                                            ),
                                            hintStyle: TextStyle(
                                                color: Colors.black,
                                                fontSize: 12.0)),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
