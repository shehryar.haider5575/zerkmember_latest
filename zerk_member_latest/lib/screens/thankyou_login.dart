import 'package:flutter/material.dart';

class ThankyouforLogin extends StatefulWidget {
  ThankyouforLogin({Key key}) : super(key: key);

  @override
  _ThankyouforLoginState createState() => _ThankyouforLoginState();
}

class _ThankyouforLoginState extends State<ThankyouforLogin> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          image: DecorationImage(
              fit: BoxFit.contain,
              image: AssetImage('assets/images/sign_up_bg.png'),
              //   fit: BoxFit.contain,
              //   scale: 5.0,
              alignment: Alignment.topCenter),
        ),
        child: Padding(
          padding: const EdgeInsets.only(top: 20.0, left: 22.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(
                height: 80,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Thank you",
                  style: TextStyle(
                    fontSize: 40.0,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "for Login",
                  style: TextStyle(
                    fontSize: 24.0,
                    color: Colors.white,
                    fontWeight: FontWeight.normal,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Your Verification is under\nreview by Admin",
                  style: TextStyle(
                    fontSize: 25.0,
                    color: Colors.white,
                    fontWeight: FontWeight.normal,
                  ),
                ),
              ),
              Container(
                height: 400,
                alignment: Alignment.bottomLeft,
                // color: Colors.amber,
                child: Row(
                  children: [
                    Text(
                      "Continue Browsing",
                      style: TextStyle(
                        fontSize: 18.0,
                        color: Colors.grey[700],
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Icon(
                        Icons.arrow_forward_ios,

                        // Icons.arrow_forward_ios_rounded,
                        color: Colors.orange[800],
                        size: 22.0,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
