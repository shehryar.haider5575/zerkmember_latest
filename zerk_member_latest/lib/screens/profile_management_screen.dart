import 'dart:convert';
import 'dart:io';

import 'package:Zerk/model/model_InvaildUserProfileUpdated.dart';
import 'package:Zerk/model/model_userDetails.dart';
import 'package:Zerk/model/model_userProfileUpdated.dart';
import 'package:Zerk/utils/Utils.dart';
import 'package:Zerk/utils/baseurl.dart';
import 'package:Zerk/utils/dioExceptions.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:localstorage/localstorage.dart';
import 'package:location/location.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfileManagement extends StatefulWidget {
  ProfileManagement({Key key}) : super(key: key);

  @override
  _ProfileManagementState createState() => _ProfileManagementState();
}

const TIMEOUT = const Duration(seconds: 5);

class _ProfileManagementState extends State<ProfileManagement> {
  // final firstnameController = TextEditingController();
  // final TextEditingController firstnameController = TextEditingController()
  //   ..text = '';
  final firstnameController = TextEditingController();
  final lastnameController = TextEditingController();
  final dobController = TextEditingController();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  bool loaderVisible = false;
  LocalStorage storage;
  Future<UserDetails> _futureUserDetailsData;
  Future<File> imageFile;
  Future<File> licenseImageFile;
  List<UserDetails> userDetailsData;
  File licenseImage;
  File imageFiles;
  bool isSwitched;
  double mLongitude, mLatitude;
  Location location = new Location();
  DateTime selectedDate = DateTime.now();
  bool _serviceEnabled;
  PermissionStatus _permissionGranted;
  LocationData _locationData;

  // PermissionStatus _permissionStatus = PermissionStatus.unknown;
  // LocationPermissionLevel _permissionLevel;
  // var profileImage;

//   final LocalStorage storage = new LocalStorage('token');

  asyncFunc() async {
    // Async func to handle Futures easier; or use Future.then
    SharedPreferences prefs = await SharedPreferences.getInstance();
  }

  Future<UserProfileUpdatedData> _futureUserProfileUpdatedData;
  // UserProfileUpdatedData
  List<UserProfileUpdatedData> userProfileUpdatedData;
  // UserSignupInvaild
//   List<UserInvaildSignup> userInvaildSignup;
  String token;

  bool _validate = false;
  String firstname,
      lastname,
      avatar,
      mylicense,
      email,
      dob,
      password,
      confirmpassword;
  double addressLat, addressLong;
  bool visible = false;
  GlobalKey<FormState> _key = new GlobalKey();
  Image imageFromPreferences,
      loadFilesImagePreferences,
      loadLicenseImagePreferences;

  Baseurl baseurl = Baseurl();
  String details_id, img_path, userID, membership_id;
  File _image;
  bool _isEditingText = false;
  TextEditingController _editingController;
  String initialText = "Initial Text";

  getStringValuesSF(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String stringValue = prefs.getString(key);
    print("getStringValuesSF: $stringValue");
    return stringValue;
  }

  //Open gallery
  pickImageFromGallery(ImageSource source) {
    setState(() {
      imageFile = ImagePicker.pickImage(source: source);
    });
  }

  loadImageFromPreferences() {
    Utility.getImageFromPreferences('IMAGE_KEY').then((img) {
      if (null == img) {
        return;
      }
      imageFromPreferences = Utility.imageFromBase64String(img);
      // setState(() {});
    });
  }

  isSwitchCheck(addressLat, addressLong) {
    print('------------------In Method Address Lat ${addressLat}');

    if (addressLat == null || addressLat == 0) {
      isSwitched = false;
      print("isSwitchCheck------------true" + addressLat.toString());
    } else {
      isSwitched = true;
      print("isSwitchCheck------------false" + addressLat.toString());
    }
  }

  @override
  void initState() {
    super.initState();

    storage = new LocalStorage('token');
    token = storage.getItem('token');
    print(token);
    token = storage.getItem("token");

    details_id = storage.getItem('details_id');
    print(details_id);
    _futureUserDetailsData = userDetailsDataApi(token);

    password = storage.getItem("password");
    // img_path = storage.getItem("img");
    // imageFile = storage.getItem("imageFile");

    firstname = storage.getItem("firstnameController");
    lastname = storage.getItem("lastnameController");
    email = storage.getItem("emailController");
    dob = storage.getItem("selectedDate");

    firstnameController..text = firstname;
    lastnameController..text = lastname;
    dobController..text = dob;
    emailController..text = email;
    // passwordController..text = password;

    if (imageFromPreferences != null) {
      print('imageFromPreferences------------------------IF');
      loadImageFromPreferences();
      // _showDialog(true);
    } else {
      loadImageFromPreferences();
      print('imageFromPreferences------------------------ELSE');
      // _showErrorDialog('false', true);
    }

    // checkPermissionStatus();

    // if (loadFilesImagePreferences != null) {
    //   print('imageFromPreferences------------------------IF');
    //
    //   loadFilesImageFromPreferences();
    //   // _showDialog(true);
    // } else {
    //   loadFilesImageFromPreferences();
    //
    //   print('imageFromPreferences------------------------ELSE');
    //   // _showErrorDialog('false', true);
    // }
    // if (loadLicenseImagePreferences != null) {
    //   print('imageFromPreferences------------------------IF');
    //
    //   loadLicenseImageFromPreferences();
    //   // _showDialog(true);
    // } else {
    //   loadLicenseImageFromPreferences();
    //
    //   print('imageFromPreferences------------------------ELSE');
    //   // _showErrorDialog('false', true);
    // }
  }

  void _showDialog(bool value) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        if (value == false) {
          print(value);
          Future.delayed(Duration(seconds: 1), () {
            Navigator.of(context).pop(true);
          });
        } else {}
        return AlertDialog(
          title: Text(
            "Profile Updated Successfully",
            textAlign: TextAlign.center,
          ),
        );
      },
    );

    loadProgress(false);
  }

  void _showDialogs(String titlestring, String contentstring) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return
//  ResponseStatus(responseText: widget.,);
            AlertDialog(
          elevation: 5,
          title: Text(
            "$titlestring",
            // "Sign Up Successfully",
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.orange[700],
            ),
          ),
          content: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              // Padding(
              //   padding: const EdgeInsets.only(right: 3),
              //   child: CircleAvatar(
              //     radius: 15,
              //     backgroundColor: Colors.orange[700],
              //     child: SizedBox(
              //       child: Icon(
              //         Icons.login_rounded,
              //         //  size: 25,
              //         color: Colors.white,
              //         // color: Colors.orange[700],
              //       ),
              //     ),
              //   ),
              // ),

              Text(
                "$contentstring",
                textAlign: TextAlign.justify,
                // maxLines: 2,
                style:
                    TextStyle(color: Colors.black, fontWeight: FontWeight.bold),

                // "Sign Up Successfully",
              ),
            ],
          ),
        );
      },
    );

    loadProgress(false);

    // nameController.clear();
    // emailController.clear();
    // contactNumberController.clear();
    // passwordController.clear();
    // confirmpasswordController.clear();
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        errorFormatText: 'Enter valid date',
        errorInvalidText: 'Enter date in valid range',
        //         firstDate: DateTime(2000),
        //   lastDate: DateTime(2025),
        //  initialDate: DateTime.now(),
        initialDate: selectedDate,
        firstDate: DateTime(1885),
        lastDate: DateTime(2101));

    if (picked != null && picked != selectedDate)
      //   print("$selectedDate" + picked.toString());
      setState(() {
        selectedDate = picked;
        dobController.text = "${selectedDate.toLocal()}".split(' ')[0];

        print("${dobController.text}");
        print("${selectedDate.toLocal()}".split(' ')[0]);
      });

    loadProgress(false);
  }

  void _showErrorDialog(String error, bool value) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        if (value == false) {
          print(value);
          Future.delayed(Duration(seconds: 1), () {
            Navigator.of(context).pop(true);
          });
        } else {}
        return AlertDialog(
          title: Text(
            "$error",
            textAlign: TextAlign.center,
          ),
          //   content: Text(
          //     "$string",
          //     textAlign: TextAlign.center,
          //   ),
        );
      },
    );

    loadProgress(false);
  }

  loadProgress(bool value) {
    if (visible == value) {
      setState(() {
        visible = value;
      });
    } else {
      setState(() {
        visible = value;
      });
    }
  }

  // Widget _editTitleTextField() {
  //   if (_isEditingText)
  //     return Center(
  //       child: TextFormField(
  //         // onSubmitted: (newValue) {
  //         //   setState(() {
  //         //     initialText = newValue;
  //         //     _isEditingText = false;
  //         //   });
  //         // },
  //         onSaved: (String val) {
  //           setState(() {
  //             firstname = val;
  //             initialText = val;
  //             _isEditingText = false;
  //           });
  //         },
  //
  //         autofocus: true,
  //         controller: _editingController,
  //       ),
  //     );
  //   return InkWell(
  //       onTap: () {
  //         setState(() {
  //           _isEditingText = true;
  //         });
  //       },
  //       child: Text(
  //         initialText,
  //         style: TextStyle(
  //           color: Colors.black,
  //           fontSize: 18.0,
  //         ),
  //       ));
  // }

  // getProfileImage() {
  //   var image = new FileImage(imageFiles);
  //   setState(() {});
  // }

  checkPermissionStatus() async {
    print("checkPermissionStatus");
    var _serviceEnabled = await location.serviceEnabled();

    if (_serviceEnabled) {
      print("serviceEnabled");

      setState(() {
        isSwitched = true;
      });

      _serviceEnabled = await location.requestService();
      var _locationData = await location.getLocation();
      print(_locationData.longitude);
      print(_locationData.latitude);
      mLongitude = _locationData.longitude;
      mLatitude = _locationData.latitude;
      print(mLongitude.toString() + "," + mLongitude.toString());
      return _locationData = await location.getLocation();
    }

    if (!_serviceEnabled) {
      print("serviceIsNotEnabled1");
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        print("serviceIsNotEnabled2");
        _permissionGranted = await location.hasPermission();
        // if (_permissionGranted == PermissionStatus.denied) {
        //   _permissionGranted = await location.requestPermission();
        //   if (_permissionGranted != PermissionStatus.granted) {
        //     var _locationData = await location.getLocation();
        //     mLongitude = _locationData.longitude;
        //     mLatitude = _locationData.latitude;
        //     print(mLongitude.toString() + "," + mLongitude.toString());
        //     return _locationData = await location.getLocation();
        //     // return _locationData = await location.getLocation();
        //   }
        // }
        // return _locationData = await location.getLocation();
      }
    }
  }

  askPermissionStatus(isSwitched) async {
    print("askPermissionStatus");
    var _serviceEnabled = await location.serviceEnabled();

    if (_serviceEnabled) {
      print("serviceEnabled");
      _serviceEnabled = await location.requestService();
      var _locationData = await location.getLocation();
      print(_locationData.longitude);
      print(_locationData.latitude);
      mLongitude = _locationData.longitude;
      mLatitude = _locationData.latitude;
      print(mLongitude.toString() + "," + mLongitude.toString());
      return _locationData = await location.getLocation();
    }

    if (!_serviceEnabled) {
      print("serviceIsNotEnabled1");
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        print("serviceIsNotEnabled2");
        _permissionGranted = await location.hasPermission();
        // if (_permissionGranted == PermissionStatus.denied) {
        //   _permissionGranted = await location.requestPermission();
        //   if (_permissionGranted != PermissionStatus.granted) {
        //     var _locationData = await location.getLocation();
        //     mLongitude = _locationData.longitude;
        //     mLatitude = _locationData.latitude;
        //     print(mLongitude.toString() + "," + mLongitude.toString());
        //     return _locationData = await location.getLocation();
        //     // return _locationData = await location.getLocation();
        //   }
        // }
        // return _locationData = await location.getLocation();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/images/sign_up_bg.png'),
              fit: BoxFit.cover,
            ),
          ),
          child: Stack(
            children: [
// Title  Profile Management
              Positioned(
                top: Alignment.topCenter.x,
                child: Container(
                  // color: Colors.orange,
                  height: MediaQuery.of(context).size.height * .22,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width / 6,
                        // color: Colors.red,
                        child: InkWell(
                          onTap: () {
                            // Navigator.pop(context, '/BottomNavBar');
                            // Navigator.pop(
                            //     context,
                            //     MaterialPageRoute(
                            //         fullscreenDialog: true,
                            //         builder: (context) => BottomNavBar()),
                            //   );
                            Navigator.of(context).pop();
                          },
                          child: SizedBox(
                            child: Icon(
                              Icons.arrow_back,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        // width: MediaQuery.of(context).size.width / 1.5,
                        width: MediaQuery.of(context).size.width / 1.8,

                        //   color: Colors.black,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Profile Management",
                              style: TextStyle(
                                fontSize: 18.0,
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),

// Form Card Widget Profile Management
              Positioned(
                // top: 110,
                // top: MediaQuery.of(context).size.height / 7,
                top: MediaQuery.of(context).size.height / 8,
                // bottom: Alignment.center.x,
                // top: Alignment.bottomCenter.y,
                child: FutureBuilder<UserDetails>(
                  future: _futureUserDetailsData,
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return Container(
                        alignment: Alignment.center,
                        width: MediaQuery.of(context).size.width,
                        //    color: Colors.amber,
                        height: MediaQuery.of(context).size.height * .8,
                        child: Padding(
                          padding: const EdgeInsets.all(10),
                          child: Form(
                            key: _key,
                            autovalidate: _validate,
                            child: Card(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20),
                              ),
                              elevation: 30,
                              shadowColor: Colors.grey,
                              child: FutureBuilder<UserDetails>(
                                future: _futureUserDetailsData,
                                builder: (context, snapshot) {
                                  // profileImage = snapshot.data.imageUrl +
                                  //     snapshot.data.data.avatar;
                                  if (snapshot.hasData) {
                                    return Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Container(
                                          // color: Colors.red,
                                          decoration: new BoxDecoration(
                                            color: Colors.grey[100],
                                            borderRadius: new BorderRadius.only(
                                              bottomLeft:
                                                  const Radius.circular(40.0),
                                              bottomRight:
                                                  const Radius.circular(40.0),
                                              topLeft:
                                                  const Radius.circular(20.0),
                                              topRight:
                                                  const Radius.circular(20.0),
                                            ),
                                          ),

                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height /
                                              4,
                                          width:
                                              MediaQuery.of(context).size.width,
                                          // color: Colors.grey[100],
                                          //child: Text("ASD"),
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Container(
                                                // color: Colors.black,
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          left: 30),
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    children: [
                                                      SizedBox(
                                                        height: 70,
                                                        width: 60,
                                                        child: Stack(
                                                          children: [
                                                            Positioned(
                                                              child: Container(
                                                                // width: 190.0,
                                                                // height: 190.0,
                                                                decoration:
                                                                    new BoxDecoration(
                                                                  shape: BoxShape
                                                                      .circle,
                                                                ),
                                                                // child: null ==
                                                                //         imageFromPreferences
                                                                //     ? imageFromGallery()
                                                                //     : imageFromPreferences,
                                                                child:
                                                                    ClipRRect(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .only(
                                                                    topLeft: Radius
                                                                        .circular(
                                                                            8.0),
                                                                    topRight: Radius
                                                                        .circular(
                                                                            8.0),
                                                                  ),
                                                                  child: imageFiles ==
                                                                          null
                                                                      ? FadeInImage
                                                                          .assetNetwork(
                                                                          placeholder:
                                                                              'assets/images/sign_up_icon.png',
                                                                          fit: BoxFit
                                                                              .contain,
                                                                          image: snapshot.data.data.avatar == null
                                                                              ? 'https://s.afl.com.au/staticfile/AFL%20Tenant/AFL/Players/ChampIDImages/XLarge2021/1007049.png'
                                                                              : snapshot.data.imageUrl + snapshot.data.data.avatar.toString(),
                                                                        )
                                                                      : Container(
                                                                          // width: 190.0,
                                                                          // height: 190.0,
                                                                          height:
                                                                              50,
                                                                          width:
                                                                              50,
                                                                          decoration:
                                                                              new BoxDecoration(
                                                                            shape:
                                                                                BoxShape.circle,
                                                                            image:
                                                                                new DecorationImage(
                                                                              fit: BoxFit.contain,
                                                                              image: new FileImage(imageFiles),
                                                                            ),
                                                                          ),
                                                                        ),
                                                                ),
                                                              ),
                                                            ),
                                                            Positioned(
                                                              top: Alignment
                                                                  .centerRight
                                                                  .y,
                                                              right: Alignment
                                                                  .centerRight
                                                                  .y,
                                                              child: Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                            .only(
                                                                        left:
                                                                            10.0),
                                                                child:
                                                                    CircleAvatar(
                                                                  backgroundColor:
                                                                      Colors.orange[
                                                                          600],
                                                                  radius: 10,
                                                                  child:
                                                                      IconButton(
                                                                    alignment:
                                                                        Alignment
                                                                            .center,
                                                                    padding:
                                                                        EdgeInsets
                                                                            .zero,
                                                                    icon: Icon(
                                                                      Icons
                                                                          .edit,
                                                                      size: 18,
                                                                    ),
                                                                    color: Colors
                                                                        .white,
                                                                    onPressed:
                                                                        () {
                                                                      // pickImageFromGallery(
                                                                      //     ImageSource
                                                                      //         .gallery);
                                                                      selectImageFromGallery();
                                                                      setState(
                                                                          () {
                                                                        showImages();
                                                                        imageFromPreferences =
                                                                            null;
                                                                      });
                                                                    },
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .only(top: 5.0),
                                                        child: Text(
                                                          snapshot.data.data
                                                                  .firstName +
                                                              " " +
                                                              snapshot.data.data
                                                                  .lastName,
                                                          // 'Michael Smith',
                                                          style: TextStyle(
                                                            fontSize: 20.0,
                                                            color:
                                                                Colors.orange,
                                                            fontWeight:
                                                                FontWeight
                                                                    .normal,
                                                          ),
                                                        ),
                                                      ),
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .only(top: 8.0),
                                                        child: Text(
                                                          snapshot
                                                              .data.data.email,
                                                          // 'Michael@gmail.com',
                                                          style: TextStyle(
                                                            fontSize: 15,
                                                            color: Colors.grey,
                                                            fontWeight:
                                                                FontWeight
                                                                    .normal,
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                // color: Colors.black,
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          right: 10),
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    children: [
                                                      //enable location access
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .only(top: .0),
                                                        child: Text(
                                                          'Enable location',
                                                          style: TextStyle(
                                                            fontSize: 10,
                                                            color: Colors
                                                                .orange[500],
                                                            fontWeight:
                                                                FontWeight
                                                                    .normal,
                                                          ),
                                                        ),
                                                      ),
                                                      // addressLat != null
                                                      //     ?
                                                      SizedBox(
                                                        // height: 70,
                                                        // width: 60,

                                                        child: Switch(
                                                          value: isSwitched,
                                                          onChanged: (value) {
                                                            setState(() {
                                                              isSwitched =
                                                                  value;
                                                              print(isSwitched);
                                                              // askPermissionStatus();
                                                              if (isSwitched) {
                                                                print(
                                                                    isSwitched);
                                                                askPermissionStatus(
                                                                    isSwitched);
                                                              } else {
                                                                print(
                                                                    isSwitched);
                                                              }
                                                            });
                                                          },
                                                          activeTrackColor:
                                                              Colors
                                                                  .orange[500],
                                                          activeColor: Colors
                                                              .orange[500],
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Container(
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height /
                                              3,
                                          width:
                                              MediaQuery.of(context).size.width,
                                          // color: Colors.red,
                                          child: Column(
                                            children: [
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                children: [
                                                  Row(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .center,
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    children: [
                                                      Container(
                                                        // color: Colors.red,
                                                        height: MediaQuery.of(
                                                                    context)
                                                                .size
                                                                .height *
                                                            .05,
                                                        width: MediaQuery.of(
                                                                    context)
                                                                .size
                                                                .width *
                                                            .4,
                                                        child:
                                                            //  if (_isEditingText)
                                                            _isEditingText
                                                                ? Center(
                                                                    child:
                                                                        TextFormField(
                                                                      // onSubmitted: (newValue) {
                                                                      //   setState(() {
                                                                      //     initialText = newValue;
                                                                      //     _isEditingText = false;
                                                                      //   });
                                                                      // },
                                                                      onSaved:
                                                                          (String
                                                                              val) {
                                                                        setState(
                                                                            () {
                                                                          firstname =
                                                                              val;
                                                                          initialText =
                                                                              val;
                                                                          _isEditingText =
                                                                              false;
                                                                        });
                                                                      },
                                                                      decoration:
                                                                          InputDecoration(
                                                                        hintText: snapshot
                                                                            .data
                                                                            .data
                                                                            .firstName,
                                                                        prefixIcon:
                                                                            SizedBox(
                                                                          height:
                                                                              30,
                                                                          width:
                                                                              30,
                                                                          child:
                                                                              Image.asset(
                                                                            'assets/images/first_name.png',
                                                                            color:
                                                                                Colors.orange,
                                                                          ),
                                                                        ),
                                                                        hintStyle: TextStyle(
                                                                            color:
                                                                                Colors.black,
                                                                            fontSize: 13.0),
                                                                      ),
                                                                      autofocus:
                                                                          true,
                                                                      controller:
                                                                          _editingController,
                                                                    ),
                                                                  )
                                                                : InkWell(
                                                                    onTap: () {
                                                                      setState(
                                                                          () {
                                                                        _isEditingText =
                                                                            true;
                                                                      });
                                                                    },
                                                                    child:
                                                                        TextFormField(
                                                                      controller:
                                                                          firstnameController,
                                                                      onSaved:
                                                                          (String
                                                                              val) {
                                                                        firstname =
                                                                            val;
                                                                      },
                                                                      decoration:
                                                                          InputDecoration(
                                                                        hintText:
                                                                            firstname,
                                                                        prefixIcon:
                                                                            SizedBox(
                                                                          height:
                                                                              30,
                                                                          width:
                                                                              30,
                                                                          child:
                                                                              Image.asset(
                                                                            'assets/images/first_name.png',
                                                                            color:
                                                                                Colors.orange,
                                                                          ),
                                                                        ),
                                                                        hintStyle: TextStyle(
                                                                            color:
                                                                                Colors.black,
                                                                            fontSize: 13.0),
                                                                      ),
                                                                    ),
                                                                  ),
                                                      ),
                                                      Padding(
                                                        padding:
                                                            EdgeInsets.only(
                                                                left: 10.0,
                                                                right: 10.0,
                                                                top: 10.0),
                                                      ),
                                                      Container(
                                                        // color: Colors.white,
                                                        height: MediaQuery.of(
                                                                    context)
                                                                .size
                                                                .height *
                                                            .05,
                                                        width: MediaQuery.of(
                                                                    context)
                                                                .size
                                                                .width *
                                                            .4,
                                                        //color: Colors.white
                                                        child: TextFormField(
                                                          controller:
                                                              lastnameController,
                                                          onSaved:
                                                              (String val) {
                                                            lastname = val;
                                                          },
                                                          decoration:
                                                              InputDecoration(
                                                                  hintText:
                                                                      lastname,
                                                                  prefixIcon:
                                                                      SizedBox(
                                                                    height: 30,
                                                                    width: 30,
                                                                    child: Image
                                                                        .asset(
                                                                      'assets/images/last_name.png',
                                                                      color: Colors
                                                                          .orange,
                                                                    ),
                                                                  ),
                                                                  hintStyle: TextStyle(
                                                                      color: Colors
                                                                          .black,
                                                                      fontSize:
                                                                          12.0)),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                              Padding(
                                                padding: EdgeInsets.only(
                                                    left: 20.0, right: 25.0),
                                                child: Column(
                                                  children: [
                                                    Padding(
                                                      padding: EdgeInsets.only(
                                                          top: 5.0),
                                                    ),
                                                    TextFormField(
                                                      textAlign:
                                                          TextAlign.start,
                                                      // onTap: () {
                                                      //   _selectDate(context);
                                                      // },
                                                      onTap: () =>
                                                          _selectDate(context),
                                                      controller: dobController,
                                                      onSaved: (String val) {
                                                        setState(() {
                                                          dob = val;
                                                        });
                                                      },
                                                      decoration:
                                                          InputDecoration(
                                                        hintText: dob,
                                                        // hintText:
                                                        //     "${selectedDate.toLocal()}"
                                                        //         .split(' ')[0],
                                                        prefixIcon: SizedBox(
                                                          height: 30,
                                                          width: 30,
                                                          child: Image.asset(
                                                            'assets/images/dob_icon.png',
                                                            color:
                                                                Colors.orange,
                                                          ),
                                                        ),
                                                        hintStyle: TextStyle(
                                                            color: Colors.black,
                                                            fontSize: 11.0),
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding: EdgeInsets.only(
                                                          top: 5.0),
                                                    ),
                                                    TextFormField(
                                                      textAlign:
                                                          TextAlign.start,
                                                      controller:
                                                          emailController,
                                                      onSaved: (String val) {
                                                        email = val;
                                                      },
                                                      decoration:
                                                          InputDecoration(
                                                              alignLabelWithHint:
                                                                  true,
                                                              hintText: email,
                                                              prefixIcon:
                                                                  SizedBox(
                                                                height: 30,
                                                                width: 30,
                                                                child: Icon(
                                                                  Icons.email,
                                                                  color: Colors
                                                                      .orange,
                                                                ),
                                                                // child:
                                                                //     Image.asset(
                                                                //   'assets/images/password_icon.png',
                                                                //   color: Colors
                                                                //       .orange,
                                                                // ),
                                                              ),
                                                              hintStyle: TextStyle(
                                                                  color: Colors
                                                                      .black,
                                                                  fontSize:
                                                                      11.0)),
                                                    ),
                                                    Padding(
                                                      padding: EdgeInsets.only(
                                                          top: 5.0),
                                                    ),
                                                    TextFormField(
                                                      textAlign:
                                                          TextAlign.start,
                                                      controller:
                                                          passwordController,
                                                      //   validator: validatePassword,
                                                      onSaved: (String val) {
                                                        // password = val;
                                                      },
                                                      decoration:
                                                          InputDecoration(
                                                              hintText:
                                                                  '******',
                                                              prefixIcon:
                                                                  SizedBox(
                                                                height: 30,
                                                                width: 30,
                                                                child:
                                                                    Image.asset(
                                                                  'assets/images/password_icon.png',
                                                                  color: Colors
                                                                      .orange,
                                                                ),
                                                              ),
                                                              hintStyle: TextStyle(
                                                                  color: Colors
                                                                      .black,
                                                                  fontSize:
                                                                      12.0)),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Container(
                                          // color: Colors.red,
                                          // margin:
                                          //     const EdgeInsets.only(left: 20.0, right: 20.0),
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height /
                                              6,
                                          width:
                                              MediaQuery.of(context).size.width,
                                          // color: Colors.amber,
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceAround,
                                            children: [
                                              Stack(
                                                children: [
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            left: 20,
                                                            bottom: 20),
                                                    child: Container(
                                                      // width: 190.0,
                                                      // height: 190.0,
                                                      decoration:
                                                          new BoxDecoration(
                                                        shape: BoxShape.circle,
                                                      ),
                                                      // child: null ==
                                                      //         imageFromPreferences
                                                      //     ? imageFromGallery()
                                                      //     : imageFromPreferences,
                                                      child: ClipRRect(
                                                        borderRadius:
                                                            BorderRadius.only(
                                                          topLeft:
                                                              Radius.circular(
                                                                  8.0),
                                                          topRight:
                                                              Radius.circular(
                                                                  8.0),
                                                        ),
                                                        child: licenseImage ==
                                                                null
                                                            ? FadeInImage
                                                                .assetNetwork(
                                                                placeholder:
                                                                    'assets/images/sign_up_icon.png',
                                                                fit: BoxFit
                                                                    .contain,
                                                                image: snapshot
                                                                            .data
                                                                            .data
                                                                            .licence ==
                                                                        null
                                                                    ? 'https://leadagency.club/wp-content/uploads/2021/09/placeholder-660.png'
                                                                    : snapshot
                                                                            .data
                                                                            .imageUrl +
                                                                        snapshot
                                                                            .data
                                                                            .data
                                                                            .licence
                                                                            .toString(),
                                                                height: 80,
                                                                width: 60,
                                                                // snapshot.data.imageUrl + snapshot.data.data.avatar.toString(),
                                                              )
                                                            : Container(
                                                                // width: 190.0,
                                                                // height: 190.0,
                                                                height: 60,
                                                                width: 60,
                                                                decoration:
                                                                    new BoxDecoration(
                                                                  shape: BoxShape
                                                                      .circle,
                                                                  image:
                                                                      new DecorationImage(
                                                                    fit: BoxFit
                                                                        .contain,
                                                                    image: new FileImage(
                                                                        licenseImage),
                                                                  ),
                                                                ),
                                                              ),
                                                      ),
                                                    ),
                                                  ),
                                                  Positioned(
                                                    top:
                                                        Alignment.centerRight.y,
                                                    right:
                                                        Alignment.centerRight.y,
                                                    child: Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              left: 10.0,
                                                              top: 0),
                                                      child: CircleAvatar(
                                                        backgroundColor:
                                                            Colors.orange[600],
                                                        radius: 10,
                                                        child: IconButton(
                                                          alignment:
                                                              Alignment.center,
                                                          padding:
                                                              EdgeInsets.zero,
                                                          icon: Icon(
                                                            Icons.edit,
                                                            size: 18,
                                                          ),
                                                          color: Colors.white,
                                                          onPressed: () {
                                                            // pickImageFromGallery(
                                                            //     ImageSource
                                                            //         .gallery);
                                                            selectLicenseImage();
                                                            setState(() {
                                                              showLicenseImage();
                                                              imageFromPreferences =
                                                                  null;
                                                            });
                                                          },
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),

                                              // InkWell(
                                              //   child: SizedBox(
                                              //     height: 40,
                                              //     width: 50,
                                              //     child: Image.asset(
                                              //       'assets/images/identification_icon.png',
                                              //       color: Colors.orange,
                                              //     ),
                                              //   ),
                                              //   onTap: () {
                                              //     selectLicenseImage();
                                              //     setState(() {
                                              //       imageFromPreferences = null;
                                              //     });
                                              //   },
                                              // ),

                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 10.0),
                                                child: Text(
                                                  "Change a photo of\n driver licence",
                                                  style: TextStyle(
                                                      color: Colors.grey,
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.normal,
                                                      letterSpacing: 2),
                                                ),
                                              ),
                                              Card(
                                                // shape: RoundedRectangleBorder(
                                                //   borderRadius:
                                                //       BorderRadius.circular(30),
                                                // ),
                                                // elevation: 20,
                                                // shadowColor: Colors.grey[600],
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  children: [
                                                    SizedBox(
                                                        // width: 100.0,
                                                        // height: 40.0,
                                                        // child: FlatButton(
                                                        //   disabledColor:
                                                        //       Colors.white,
                                                        //   shape: RoundedRectangleBorder(
                                                        //       borderRadius:
                                                        //           BorderRadius
                                                        //               .circular(
                                                        //                   22.0),
                                                        //       side: BorderSide(
                                                        //           color: Colors
                                                        //               .white)),
                                                        //   onPressed: () {
                                                        //     //   Navigator.push(
                                                        //     //     context,
                                                        //     //     MaterialPageRoute(
                                                        //     //         fullscreenDialog: true,
                                                        //     //         builder: (context) => BottomNavBar()),
                                                        //     //   );
                                                        //   },
                                                        //   color: Colors.white,
                                                        //   textColor:
                                                        //       Colors.orange,
                                                        //   child: Text("Edit",
                                                        //       style: TextStyle(
                                                        //         fontSize: 18.0,
                                                        //         fontWeight:
                                                        //             FontWeight
                                                        //                 .bold,
                                                        //       )),
                                                        // ),
                                                        ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Expanded(
                                          child: SizedBox(
                                            height: 20,
                                          ),
                                        ),
                                      ],
                                    );
                                  } else if (snapshot.hasError) {
                                    return Text("${snapshot.error}");
                                  }

                                  // By default, show a loading spinner.
                                  return Positioned(
                                      top: Alignment.center.x,
                                      bottom: Alignment.center.y,
                                      left: Alignment.center.x,
                                      right: Alignment.center.x,
                                      child: Center(
                                          child: showProgressLoader(context)));
                                },
                              ),
                            ),
                          ),
                        ),
                      );
                    } else if (snapshot.hasError) {
                      return Text("${snapshot.error}");
                    }

                    // _asyncLoader
                    // By default, show a loading spinner.
                    return Positioned(
                        top: Alignment.center.x,
                        bottom: Alignment.center.y,
                        left: Alignment.center.x,
                        right: Alignment.center.x,
                        child: Center(
                            child: showProgressLoader(context, Colors.white)));
                  },
                ),
              ),

              Visibility(
                visible: loaderVisible,
                child: Positioned(
                    top: Alignment.center.x,
                    bottom: Alignment.center.y,
                    left: Alignment.center.x,
                    right: Alignment.center.x,
                    child: Center(child: showProgressLoader(context))),
              ),

//Form Card Widget InnerField Profile Management

// S U B M I T Button  Widget Profile Management
              Positioned(
                // bottom: Alignment.bottomLeft.x,
                // b: Alignment.bottomLeft.x,
                bottom: MediaQuery.of(context).size.width / 20,
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 30, left: 20),
                  child: Container(
                    // alignment: Alignment.bottomLeft,
                    child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(35),
                      ),
                      elevation: 20,
                      shadowColor: Colors.grey[600],
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          ConstrainedBox(
                            constraints: BoxConstraints(
                              minWidth: 180.0,
                              maxWidth: 180.0,
                              minHeight: 50.0,
                              maxHeight: 50.0,
                            ),
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(22.0),
                                  side: BorderSide(color: Colors.white)),
                              onPressed: () {
                                _sendToServer();

                                //   Navigator.push(
                                //     context,
                                //     MaterialPageRoute(
                                //         fullscreenDialog: true,
                                //         builder: (context) => BottomNavBar()),
                                //   );
                              },
                              color: Colors.white,
                              textColor: Colors.orange,
                              child: Text("S U B M I T".toUpperCase(),
                                  style: TextStyle(
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.bold,
                                  )),
                            ),
                          ),
                          // SizedBox(
                          //   width: 180.0,
                          //   height: 50.0,
                          //
                          // ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget showProgressLoader(BuildContext context,
      [Color color = Colors.orange]) {
    if (Platform.isAndroid) {
      return CircularProgressIndicator(
        color: color,
      );
    }
    if (Platform.isIOS) {
      return CupertinoActivityIndicator();
    }
  }

  Widget showImages() {
    return FutureBuilder<File>(
      future: imageFile,
      builder: (BuildContext context, AsyncSnapshot<File> snapshot) {
        if (snapshot.connectionState == ConnectionState.done &&
            snapshot.data != null) {
          return Image.file(
            snapshot.data,
            width: 300,
            height: 300,
          );
        } else if (snapshot.error != null) {
          return const Text(
            'Error Picking Image',
            textAlign: TextAlign.center,
          );
        } else {
          return const Text(
            'No Image Selected',
            textAlign: TextAlign.center,
          );
        }
      },
    );
  }

  Widget showLicenseImage() {
    return FutureBuilder<File>(
      future: licenseImageFile,
      builder: (BuildContext context, AsyncSnapshot<File> snapshot) {
        if (snapshot.connectionState == ConnectionState.done &&
            snapshot.data != null) {
          return Image.file(
            snapshot.data,
            width: 300,
            height: 300,
          );
        } else if (snapshot.error != null) {
          return const Text(
            'Error Picking Image',
            textAlign: TextAlign.center,
          );
        } else {
          return const Text(
            'No Image Selected',
            textAlign: TextAlign.center,
          );
        }
      },
    );
  }

  // Widget imageFromGallery() {
  //   return FutureBuilder<File>(
  //     future: imageFile,
  //     builder: (BuildContext context, AsyncSnapshot<File> snapshot) {
  //       if (snapshot.connectionState == ConnectionState.done &&
  //           null != snapshot.data) {
  //         //print(snapshot.data.path);
  //         Utility.saveImageToPreferences('IMAGE_KEY',
  //             Utility.base64String(snapshot.data.readAsBytesSync()));
  //         // return Image.file(
  //         //   snapshot.data,
  //         // );
  //         return Container(
  //             width: 190.0,
  //             height: 190.0,
  //             decoration: new BoxDecoration(
  //                 shape: BoxShape.circle,
  //                 image: new DecorationImage(
  //                     fit: BoxFit.fill, image: new FileImage(snapshot.data))));
  //       } else if (null != snapshot.error) {
  //         return const Text(
  //           'Error Picking Image',
  //           textAlign: TextAlign.center,
  //         );
  //       } else {
  //         // return const Text(
  //         //   '\nNo Image ',
  //         //   textAlign: TextAlign.center,
  //         // );
  //         return SizedBox(
  //           height: 50,
  //           width: 50,
  //           child: Image.asset('assets/images/sign_up_icon.png'),
  //         );
  //       }
  //     },
  //   );
  // }

  // Widget showImage(String avatar) {
  //   return FutureBuilder<File>(
  //     future: imageFile,
  //     builder: (BuildContext context, AsyncSnapshot<File> snapshot) {
  //       if (snapshot.connectionState == ConnectionState.done &&
  //           snapshot.data != null) {
  //         storage.setItem("img", snapshot.data.path.toString());
  //         // return
  //         snapshot.data != null
  //             ? Image.file(
  //                 snapshot.data,
  //                 width: 300,
  //                 height: 300,
  //               )
  //             : Image.network(
  //                 baseurl.imgurl + avatar.toString(),
  //                 width: 300,
  //                 height: 300,
  //               );
  //       } else if (snapshot.error != null) {
  //         return const Text(
  //           'Error Picking Image',
  //           textAlign: TextAlign.center,
  //         );
  //       } else {
  //         return Image.asset('assets/images/john_image.png');
  //         // return const Text(
  //         //   'No Image Selected',
  //         //   textAlign: TextAlign.center,
  //         // );
  //       }
  //     },
  //   );
  // }

  selectImageFromGallery() async {
    var image = await ImagePicker.pickImage(
      source: ImageSource.gallery,
    );
    if (image != null) {
      setState(() {
        imageFiles = image;
        // profileImage = imageFiles;
        // Utility.saveImageToPreferences(
        //     'imageFiles', Utility.base64String(imageFiles.readAsBytesSync()));
      });
    } else {
      throw Exception('Could not profile image');
    }
  }

  selectLicenseImage() async {
    var image = await ImagePicker.pickImage(
      source: ImageSource.gallery,
    );
    if (image != null) {
      setState(() {
        licenseImage = image;
        // Utility.saveImageToPreferences('licenseImage',
        //     Utility.base64String(licenseImage.readAsBytesSync()));
      });
      _showDialogs("Identification", "Driver licence is\nattached Successful");
    } else {
      throw Exception('Could not license image');
    }
  }

  loadLicenseImageFromPreferences() {
    Utility.getImageFromPreferences('licenseImage').then((img) {
      if (null == img) {
        return;
      }
      loadLicenseImagePreferences = Utility.imageFromBase64String(img);
      // setState(() {});
    });
  }

  loadFilesImageFromPreferences() {
    Utility.getImageFromPreferences('imageFiles').then((img) {
      if (null == img) {
        return;
      }
      loadFilesImagePreferences = Utility.imageFromBase64String(img);
      // setState(() {});
    });
  }

  Future<UserProfileUpdatedData> userProfileUpdated(
    String first_name,
    String last_name,
    String email,
    String dob,
    String password,
    // String token,
    String id,
    File licenseImage,
    File imageFiles,
    double address_lat,
    double address_long,
    //   String avatar,
  ) async {
    print("jasdhjksadkashdjhasjkdhasjdsahkads");
    print(address_lat);
    print(address_long);
    Dio dio = Dio();

    // String profile;
    // String license;

    // String profile = imageFiles.path.split('/').last;
    // String license = licenseImage.path.split('/').last;

    // if (imageFiles.path.split('/').last == null) {
    // } else {
    //   profile = imageFiles.path.split('/').last;
    //   license = licenseImage.path.split('/').last;
    // }
    // if (licenseImage.path.split('/').last == null) {
    // } else {
    //   profile = imageFiles.path.split('/').last;
    //   license = licenseImage.path.split('/').last;
    // }
    // File profile = 'assets/images/accessdenied.png' as File;
    // dio.options.headers['Content-Type'] = 'application/json; Charset=UTF-8';
    FormData formData = FormData.fromMap({
      'first_name': first_name,
      'last_name': last_name,
      // 'avatar': await MultipartFile.fromFile(imageFiles.path,
      //     filename: profile.split('/').last),
      // 'licence': await MultipartFile.fromFile(licenseImage.path,
      //     filename: license.split('/').last),
      'dob': dob,
      'email': email,
      'password': password,
      'address_lat': address_lat,
      'address_long': address_long,
      // 'password_confirmation': password_confirmation,
      // 'device_token': device_token,
      // 'phone': cellPhone,
    });
    if (imageFiles != null) {
      String profile = imageFiles.path.split('/').last;
      formData = FormData.fromMap({
        'first_name': first_name,
        'last_name': last_name,
        'avatar': await MultipartFile.fromFile(imageFiles.path,
            filename: profile.split('/').last),
        // 'licence': await MultipartFile.fromFile(licenseImage.path,
        //     filename: license.split('/').last),
        'dob': dob,
        'email': email,
        'password': password,
        'address_lat': address_lat,
        'address_long': address_long,
        // 'password_confirmation': password_confirmation,
        // 'device_token': device_token,
        // 'phone': cellPhone,
      });
    }
    if (licenseImage != null) {
      String license = licenseImage.path.split('/').last;
      formData = FormData.fromMap({
        'first_name': first_name,
        'last_name': last_name,
        // 'avatar': await MultipartFile.fromFile(imageFiles.path,
        //     filename: profile.split('/').last),
        'licence': await MultipartFile.fromFile(licenseImage.path,
            filename: license.split('/').last),
        'dob': dob,
        'email': email,
        'password': password,
        'address_lat': address_lat,
        'address_long': address_long,
        // 'password_confirmation': password_confirmation,
        // 'device_token': device_token,
        // 'phone': cellPhone,
      });
    }

    if (licenseImage != null && _image != null) {
      String profile = imageFiles.path.split('/').last;
      String license = licenseImage.path.split('/').last;
      formData = FormData.fromMap({
        'first_name': first_name,
        'last_name': last_name,
        'avatar': await MultipartFile.fromFile(imageFiles.path,
            filename: profile.split('/').last),
        'licence': await MultipartFile.fromFile(licenseImage.path,
            filename: license.split('/').last),
        'dob': dob,
        'email': email,
        'password': password,
        'address_lat': address_lat,
        'address_long': address_long,
        // 'password_confirmation': password_confirmation,
        // 'device_token': device_token,
        // 'phone': cellPhone,
      });
    }

    var response = await dio.post(
      baseurl.baseurl + "user/update/" + id,
      data: formData,
      options: Options(
          followRedirects: false,
          validateStatus: (status) {
            return status < 500;
          }),
      // options: Options(
      //   headers: {"Content-Type": "application/json; Charset=UTF-8"},
      // ),
    );
    print("787897897897897987987");
    try {
      print(response.toString());
      print("jasdhasjdhjkashdjashdjksa");
    } catch (e) {
      final errorMessage = DioExceptions.fromDioError(e).toString();
      print(errorMessage);
      print("jasdhasjdhjkashdjashdjksa");
    }
    if (response.statusCode == 200) {
      print("-----------userSignupApi-------------");

      // print(response.data);
      // print('This is final .. done with image path ${imagePath}');
      // var parsedJson = response.data;
      // var success = UserProfileUpdatedData.fromJson(parsedJson);
      // userSignup.add(UserProfileUpdatedData(data: success.data));
      // storage.setItem("token", success.data.token);
      // storage.setItem('userid', success.data.user.id.toString());
      // print('This is token \n ${storage.getItem("token")}');
      //
      // String firstName = storage.getItem("firstnameController").toString();
      // String lastName = storage.getItem("lastnameController").toString();
      // String name = firstName.toString() + "" + lastName.toString();
      // String phone = storage.getItem("cellPhoneController").toString();
      // String email = storage.getItem("emailController").toString();
      // String userId = storage.getItem('userid');
      //
      // // Navigator.pushNamed(context, '/LoginScreen');
      // _showDialogs(firstnameController.text, 'Sign Up Successfully');

      print(json.decode(response.statusCode.toString()));
      print("------------UserProfileUpdatedData-------------");
      _showDialog(false);
      setState(() {
        if (loaderVisible == true) {
          loaderVisible = false;
        }
      });
      // print(json.decode(response.data));
      var parsedJson = response.data;
      var success = UserProfileUpdatedData.fromJson(parsedJson);
      // setState(() {
      //   avatar = success.avatar;
      //   license = success.licence;
      // });
      storage.setItem("firstnameController", firstnameController.text);
      storage.setItem("lastnameController", lastnameController.text);
      storage.setItem("emailController", emailController.text);
      storage.setItem("selectedDate", dobController.text);
      userDetailsDataApi(token);
      return UserProfileUpdatedData.fromJson(response.data);
    } else {
      // // print(json.decode(response.statusCode.toString()));
      // // print(json.decode(response.body));
      //
      // userInvaildSignup = List<InvaildUserProfileUpdated>();
      // var parsedJson = response.data;
      // var error = InvaildUserProfileUpdated.fromJson(parsedJson);
      //
      // userInvaildSignup
      //     .add(InvaildUserProfileUpdated(data: error.data, message: error.message));
      // // print(userInvaildSignup[0].message.toString() +
      // //     "" +
      // //     userInvaildSignup[0].data.email[0].toString());
      // _showErrorDialog(userInvaildSignup[0].message.toString(),
      //     userInvaildSignup[0].data.email[0].toString());
      // throw Exception('Failed to Register the Uers');

      print(baseurl.baseurl + "user/update/" + id);
      print(json.decode(response.statusCode.toString()));
      print(json.decode(response.data));
      var parsedJson = json.decode(response.data.toString());
      var success = InvaildUserProfileUpdated.fromJson(parsedJson);
      _showErrorDialog(success.data.email.toString(), false);
      setState(() {
        if (loaderVisible == true) {
          loaderVisible = false;
        }
      });
      throw Exception('Failed to Register the Users');
    }
  }

  //------UserProfileUpdatedData------------------------------------
  // Future<UserProfileUpdatedData> userProfileUpdated(
  //   String first_name,
  //   String last_name,
  //   String email,
  //   String dob,
  //   String password,
  //   // String token,
  //   String id,
  //   //   String avatar,
  //   //   String address
  // ) async {
  //   final http.Response response = await http.post(
  //     baseurl.baseurl + "user/update/" + id,
  //     headers: <String, String>{
  //       'Accept': 'application/json',
  //       'Content-Type': 'application/json; charset=UTF-8',
  //       // 'Content-Type': 'application/json; charset=UTF-8',
  //     },
  //     body: jsonEncode(<String, String>{
  //       'first_name': first_name,
  //       'last_name': last_name,
  //       'dob': dob,
  //       'email': email,
  //       'password': password,
  //       // 'licence': await MultipartFile.fromFile(licenseImage.path,
  //       //     filename: license.split('/').last),
  //       // 'password_confirmation': password_confirmation,
  //     }),
  //   );
  //
  //   if (response.statusCode == 200) {
  //     print(json.decode(response.statusCode.toString()));
  //     print("------------UserProfileUpdatedData-------------");
  //     print(json.decode(response.body));
  //     var parsedJson = json.decode(response.body);
  //     var success = UserProfileUpdatedData.fromJson(parsedJson);
  //     storage.setItem("firstnameController", firstnameController.text);
  //     storage.setItem("lastnameController", lastnameController.text);
  //     storage.setItem("emailController", emailController.text);
  //     storage.setItem("selectedDate", dobController.text);
  //
  //     _showDialog(false);
  //     setState(() {
  //       if (loaderVisible == true) {
  //         loaderVisible = false;
  //       }
  //     });
  //     return UserProfileUpdatedData.fromJson(json.decode(response.body));
  //   } else {
  //     print(baseurl.baseurl + "user/update/" + id);
  //     print(json.decode(response.statusCode.toString()));
  //     print(json.decode(response.body));
  //     var parsedJson = json.decode(response.body.toString());
  //     var success = InvaildUserProfileUpdated.fromJson(parsedJson);
  //     _showErrorDialog(success.data.email.toString(), false);
  //     setState(() {
  //       if (loaderVisible == true) {
  //         loaderVisible = false;
  //       }
  //     });
  //     throw Exception('Failed to Register the Users');
  //   }
  // }

  Future<UserDetails> userDetailsDataApi(String token) async {
    final response = await http.get(
      Uri.parse(baseurl.baseurl + "user/details"),
      headers: <String, String>{
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + token,
      },
    );

    if (response.statusCode == 200) {
      print("----------------userDetailsDataApi---------");
      print(json.decode(response.body));
      var parsedJson = json.decode(response.body);
      var success = UserDetails.fromJson(parsedJson);
      print("9812390389128391283901283098192");
      print(success.data.sourceCardId);
      storage.setItem("source_card_id", success.data.sourceCardId);
      userID = success.data.id.toString();
      firstname = success.data.firstName;
      lastname = success.data.lastName;
      dob = success.data.dob;
      email = success.data.email;
      avatar = success.data.avatar;
      storage.setItem('avatar', success.data.avatar.toString());
      mylicense = success.data.licence;
      membership_id = success.data.membershipId.toString();
      storage.setItem("userMembershipId", membership_id);
      firstnameController..text = firstname;
      lastnameController..text = lastname;
      dobController..text = dob;
      emailController..text = email;
      // passwordController..text = password;
      addressLat = success.data.addressLat.toDouble();
      addressLong = success.data.addressLong.toDouble();

      print('------------------Address Lat ${addressLat}');
      if (isSwitched == false) {
        isSwitchCheck(0, 0);
      } else {
        isSwitchCheck(addressLat, addressLong);
      }

      return UserDetails.fromJson(json.decode(response.body));
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load data');
    }
  }

  getMessage() async {
    return new Future.delayed(TIMEOUT, () => 'Welcome to your async screen');
  }

  _sendToServer() {
    if (_key.currentState.validate()) {
      // No any error in validation
      _key.currentState.save();
      setState(() {
        loaderVisible = true;
      });
      print(firstnameController.text);
      print(lastnameController.text);
      print(emailController.text);
      print(
        dobController.text,
      );
      print(passwordController.text);
      //   print(token);
      print(details_id);

      if (isSwitched == false) {
        print("isSwitchedIs-----------------------${isSwitched.toString()}");
        _futureUserProfileUpdatedData = userProfileUpdated(
          firstnameController.text.isEmpty
              ? firstname
              : firstnameController.text,
          lastnameController.text.isEmpty ? lastname : lastnameController.text,
          emailController.text.isEmpty ? email : emailController.text,
          dobController.text.isEmpty ? dob : dobController.text,
          passwordController.text.isEmpty ? password : passwordController.text,
          userID,
          // licenseImage == null ? loadLicenseImagePreferences : licenseImage,
          // imageFiles == null ? loadFilesImagePreferences : imageFiles,
          licenseImage,
          imageFiles,
          0,
          0,
          // mLongitude,
          // mLatitude
        );
      } else {
        print("isSwitchedIss-----------------------${isSwitched.toString()}");
        _futureUserProfileUpdatedData = userProfileUpdated(
            firstnameController.text.isEmpty
                ? firstname
                : firstnameController.text,
            lastnameController.text.isEmpty
                ? lastname
                : lastnameController.text,
            emailController.text.isEmpty ? email : emailController.text,
            dobController.text.isEmpty ? dob : dobController.text,
            passwordController.text.isEmpty
                ? password
                : passwordController.text,
            userID,
            // licenseImage == null ? loadLicenseImagePreferences : licenseImage,
            // imageFiles == null ? loadFilesImagePreferences : imageFiles,
            licenseImage,
            imageFiles,
            mLatitude,
            mLongitude
            // mLongitude,
            // mLatitude

            );
      }
      // _futureUserProfileUpdatedData = userProfileUpdated(
      //     firstnameController.text.isEmpty
      //         ? firstname
      //         : firstnameController.text,
      //     lastnameController.text.isEmpty ? lastname : lastnameController.text,
      //     emailController.text.isEmpty ? email : emailController.text,
      //     dobController.text.isEmpty ? dob : dobController.text,
      //     passwordController.text.isEmpty ? password : passwordController.text,
      //     userID,
      //     // licenseImage == null ? loadLicenseImagePreferences : licenseImage,
      //     // imageFiles == null ? loadFilesImagePreferences : imageFiles,
      //     licenseImage,
      //     imageFiles,
      //     isSwitched == false ? null : mLatitude,
      //     isSwitched == false ? null : mLongitude
      //     // mLongitude,
      //     // mLatitude
      //
      //     );
    } else {
      // validation error
      setState(() {
        _validate = true;
      });
    }
  }

  //validateName
  String validateName(String value) {
    String patttern = r'(^[a-zA-Z ]*$)';
    RegExp regExp = new RegExp(patttern);
    if (value.length == 0) {
      return "Name is Required";
    } else if (!regExp.hasMatch(value)) {
      return "Name must be a-z and A-Z";
    }
    return null;
  }

  //validateName
  String validateDOB(String value) {
    String patttern =
        r'^([0]?[1-9]|[1|2][0-9]|[3][0|1])[./-]([0]?[1-9]|[1][0-2])[./-]([0-9]{4}|[0-9]{2})$';
    RegExp regExp = new RegExp(patttern);
    if (value.length == 0) {
      return "DOB is Required";
    } else if (!regExp.hasMatch(value)) {
      return "DOB must be 25-04-2017 ";
    }
    return null;
  }

//validateEmail
  String validateEmail(String value) {
    String pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = new RegExp(pattern);
    if (value.length == 0) {
      return "Email is Required";
    } else if (!regExp.hasMatch(value)) {
      return "Invalid Email";
    } else {
      return null;
    }
  }

  bool validatePasswords(String value) {
    String pattern =
        r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
    RegExp regExp = new RegExp(pattern);
    return regExp.hasMatch(value);
  }

  String validatePassword(String value) {
    Pattern pattern =
        r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
    RegExp regex = new RegExp(pattern);
    print(value);
    if (value.isEmpty) {
      return 'Please enter password';
    } else {
      if (!regex.hasMatch(value))
        return 'Enter valid password';
      else
        return null;
    }
  }
}
