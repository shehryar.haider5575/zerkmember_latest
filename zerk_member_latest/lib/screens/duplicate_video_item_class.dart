import 'dart:convert';

import 'package:Zerk/model/model_qrcode_scan_facillity.dart';
import 'package:Zerk/screens/scan_qr_code_screen.dart';
import 'package:Zerk/utils/baseurl.dart';
import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:http/http.dart' as http;
import 'package:localstorage/localstorage.dart';
// import 'package:vibration/vibration.dart';
import 'package:video_player/video_player.dart';

class DuplicateVideoItems extends StatefulWidget {
  VideoPlayerController videoPlayerController;
  final bool looping;
  final bool autoplay;
  bool facilityAccessAd;
  String facilityID, userid, videoUrl;

  DuplicateVideoItems({
    // @required this.videoPlayerController,
    this.looping,
    this.autoplay,
    this.facilityAccessAd,
    this.facilityID,
    this.userid,
    this.videoUrl,
    Key key,
  }) : super(key: key);
  @override
  _DuplicateVideoItemsState createState() => _DuplicateVideoItemsState();
}

class _DuplicateVideoItemsState extends State<DuplicateVideoItems> {
  ChewieController _chewieController;
  final LocalStorage storage = new LocalStorage('token');
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
  Baseurl baseurl = Baseurl();
  List<QrcodeScan> qrcodeScanMemberList;
  bool visible = false;

  TargetPlatform _platform;
  VideoPlayerController _videoPlayerController1;
  VideoPlayerController _videoPlayerController2;

  @override
  void initState() {
    super.initState();
    print('[INFO] THIS IS VIDEO INIT METHOD');
    initializePlayer();
    var initializationSettingsAndroid =
        new AndroidInitializationSettings('zerk');
    var initializationSettingsIOS = new IOSInitializationSettings();
    var initializationSettings = new InitializationSettings(
        android: initializationSettingsAndroid, iOS: initializationSettingsIOS);
    flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);
  }

  Future<void> initializePlayer() async {
    print('[INFO] THIS SI INITIALIZE PLAYER FUNCTION');
    print('this is url ${widget.videoUrl}');
    _videoPlayerController1 = VideoPlayerController.network(
      widget.videoUrl,
      // 'https://assets.mixkit.co/videos/preview/mixkit-forest-stream-in-the-sunlight-529-large.mp4'
    );
    _videoPlayerController2 = VideoPlayerController.network(
        'https://assets.mixkit.co/videos/preview/mixkit-a-girl-blowing-a-bubble-gum-at-an-amusement-park-1226-large.mp4');
    await Future.wait([
      _videoPlayerController1.initialize(),
      _videoPlayerController2.initialize()
    ]);
    _chewieController = ChewieController(
      videoPlayerController: _videoPlayerController1,
      autoPlay: true,
      fullScreenByDefault: true,
      allowFullScreen: true,
      showControls: false,
      deviceOrientationsOnEnterFullScreen: [
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown,
        // DeviceOrientation.landscapeLeft,
        // DeviceOrientation.landscapeRight,
      ],
      deviceOrientationsAfterFullScreen: [
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown,
      ],
      // looping: true,
      // Try playing around with some of these other options:

      // showControls: false,
      // materialProgressColors: ChewieProgressColors(
      //   playedColor: Colors.red,
      //   handleColor: Colors.blue,
      //   backgroundColor: Colors.grey,
      //   bufferedColor: Colors.lightGreen,
      // ),
      // placeholder: Container(
      //   color: Colors.grey,
      // ),
      // autoInitialize: true,
    );

    setState(() {
      _videoPlayerController1.addListener(() {
        if (_videoPlayerController1.value.position ==
            _videoPlayerController1.value.duration) {
          print('video Ended');

          // Navigator.popAndPushNamed(
          //     context, '/AvailsHistoryScreen');
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                  builder: (context) => ScanQRCodeScreen(
                      // facilityAccessAd: true,
                      )),
              ModalRoute.withName("/BottomNavBar"));
          // _showNotificationWithoutSound('Access Granted Successfully');
          // _showNotificationWithoutSound(
          //     "thanks for watching video. The access has been granted");

          getMemberInfoQrcodeScan(widget.userid, widget.facilityID);
          // Vibration.vibrate();
        }
      });
    });
  }

  Future onSelectNotification(String payload) async {
    showDialog(
      context: context,
      builder: (_) {
        return new AlertDialog(
          title: Text("ZERK"),
          content: Text("$payload"),
        );
      },
    );
  }

  Future _showNotificationWithoutSound(String string) async {
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        'your channel id', 'your channel name', 'your channel description',
        playSound: false, importance: Importance.max, priority: Priority.high);
    var iOSPlatformChannelSpecifics =
        new IOSNotificationDetails(presentSound: false);
    var platformChannelSpecifics = new NotificationDetails(
        android: androidPlatformChannelSpecifics,
        iOS: iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
      0,
      'ZERK',
      '$string',
      platformChannelSpecifics,
      payload: '$string',
    );
  }

  @override
  void dispose() {
    _videoPlayerController1.dispose();
    _videoPlayerController2.dispose();
    _chewieController.dispose();
    super.dispose();
  }

  loadProgress(bool value) {
    if (visible == value) {
      setState(() {
        visible = value;
      });
    } else {
      setState(() {
        visible = value;
      });
    }
  }

  void _showDialog(String contentstring, bool value) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        if (value == false) {
          print(value);
          Future.delayed(Duration(seconds: 1), () {
            Navigator.of(context).pop(true);
          });
        } else {}
        return AlertDialog(
          content: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Text(
                  contentstring,
                  textAlign: TextAlign.left,
                  maxLines: 4,
                  style: TextStyle(
                      color: Colors.orange[700], fontWeight: FontWeight.bold),
                ),
              )
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    // return MaterialApp(
    //   theme: ThemeData.light().copyWith(
    //     platform: _platform ?? Theme.of(context).platform,
    //   ),
    return Scaffold(
      body: Column(
        children: <Widget>[
          Expanded(
            child: Center(
              child: _chewieController != null
                  // &&
                  // _chewieController.videoPlayerController.value.initialized
                  ? Chewie(
                      controller: _chewieController,
                    )
                  : Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        CircularProgressIndicator(),
                        SizedBox(height: 20),
                        Text('Loading'),
                      ],
                    ),
            ),
          ),
          // TextButton(
          //   onPressed: () {
          //     _chewieController.enterFullScreen();
          //   },
          //   child: const Text('Fullscreen'),
          // ),
          // Row(
          //   children: <Widget>[
          //     Expanded(
          //       child: TextButton(
          //         onPressed: () {
          //           setState(() {
          //             _chewieController.dispose();
          //             _videoPlayerController1.pause();
          //             _videoPlayerController1.seekTo(const Duration());
          //             _chewieController = ChewieController(
          //               videoPlayerController: _videoPlayerController1,
          //               autoPlay: true,
          //               looping: true,
          //             );
          //           });
          //         },
          //         child: const Padding(
          //           padding: EdgeInsets.symmetric(vertical: 16.0),
          //           child: Text("Landscape Video"),
          //         ),
          //       ),
          //     ),
          //     Expanded(
          //       child: TextButton(
          //         onPressed: () {
          //           setState(() {
          //             _chewieController.dispose();
          //             _videoPlayerController2.pause();
          //             _videoPlayerController2.seekTo(const Duration());
          //             _chewieController = ChewieController(
          //               videoPlayerController: _videoPlayerController2,
          //               autoPlay: true,
          //               looping: true,
          //             );
          //           });
          //         },
          //         child: const Padding(
          //           padding: EdgeInsets.symmetric(vertical: 16.0),
          //           child: Text("Portrait Video"),
          //         ),
          //       ),
          //     )
          //   ],
          // ),
          // Row(
          //   children: <Widget>[
          //     Expanded(
          //       child: TextButton(
          //         onPressed: () {
          //           setState(() {
          //             _platform = TargetPlatform.android;
          //           });
          //         },
          //         child: const Padding(
          //           padding: EdgeInsets.symmetric(vertical: 16.0),
          //           child: Text("Android controls"),
          //         ),
          //       ),
          //     ),
          //     Expanded(
          //       child: TextButton(
          //         onPressed: () {
          //           setState(() {
          //             _platform = TargetPlatform.iOS;
          //           });
          //         },
          //         child: const Padding(
          //           padding: EdgeInsets.symmetric(vertical: 16.0),
          //           child: Text("iOS controls"),
          //         ),
          //       ),
          //     )
          //   ],
          // ),
        ],
      ),
    );
  }

  getBody() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Chewie(
        controller: _chewieController,
      ),
    );
  }

  Future<QrcodeScan> getMemberInfoQrcodeScan(
      String id, String facilityID) async {
    // user_facility_id = storage.getItem('user_facility_id');
    // print(user_facility_id);
    print("--------------user_facility_id----------------------");
    print(
        baseurl.baseurl + "admin/facility/" + facilityID + "/barcode-scan/$id");
    final response = await http.get(Uri.parse(baseurl.baseurl +
        "admin/facility/" +
        facilityID +
        "/barcode-scan/$id"));

    if (response.statusCode == 200) {
      var parsedJson = json.decode(response.body);
      var success = QrcodeScan.fromJson(parsedJson);
      qrcodeScanMemberList = List<QrcodeScan>();
      print(response.statusCode.toString());
      print(response.statusCode.toString());
      print(response.body.toString());
      qrcodeScanMemberList.add(QrcodeScan(data: success.data));
      print(qrcodeScanMemberList[0].data.licence);

      _showNotificationWithoutSound('Access Granted Successfully');
      _showNotificationWithoutSound(
          "thanks for watching video. The access has been granted");
      Navigator.pushAndRemoveUntil<dynamic>(
        context,
        MaterialPageRoute<dynamic>(
          builder: (BuildContext context) => ScanQRCodeScreen(),
        ),
        (route) => false, //if you want to disable back feature set to false
      );

      loadProgress(false);

      return QrcodeScan.fromJson(jsonDecode(response.body));
    } else {
      print(response.statusCode.toString());
      print(response.statusCode.toString());
      // _showDialog("something went wrong ", true);
      print("asdlkjaskljaskdjaklsjdasd");
      var parsedJson = json.decode(response.body);
      var error = QrcodeScan.fromJson(parsedJson);

      _showDialog(error.message.toString(), true);
      // Navigator.push(
      //   context,
      //   MaterialPageRoute(
      //       builder: (context) => AceessDenied(
      //             message: qrScanAceessDeniedList[0].data.toString(),
      //           )),
      // );
      loadProgress(false);
      // Navigator.pushNamed(context, '/AceessDenied',);
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load QrCodeWidget');
    }
  }
}

/**/
