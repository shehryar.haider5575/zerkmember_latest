import 'dart:convert';
import 'dart:io';

import 'package:Zerk/model/model_LoginInvaildUserdetails.dart';
import 'package:Zerk/model/model_SpecifiedFacilityDetail.dart';
import 'package:Zerk/model/model_userDetails.dart';
import 'package:Zerk/utils/baseurl.dart';
import 'package:Zerk/utils/maproute.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:localstorage/localstorage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

class ProfileViewDetails extends StatefulWidget {
  ProfileViewDetails({
    Key key,
    @required this.facilityId,
    this.bussinessName,
    this.address,
    this.firstName,
    this.lastName,
  }) : super(key: key);

  String facilityId, bussinessName, address, firstName, lastName = '';

  @override
  _ProfileViewDetailsState createState() => _ProfileViewDetailsState();
}

class _ProfileViewDetailsState extends State<ProfileViewDetails> {
  Future<UserDetails> _futureUserDetailsData;
  Baseurl baseUrl = Baseurl();
  List<UserDetailsData> userDetailsData;
  List<LoginInvaildUserdetails> loginInvaildUserdetails;

  Future<SpecifiedFacilityDetail> futureSpecifiedFacilityDetail;
  List<SpecifiedFacilityDetail> specifiedFacilityDetailList;
  final LocalStorage storage = new LocalStorage('token');
  String token;
  getStringValuesSF(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String stringValue = prefs.getString(key);
    print("ProfileViewDetails: $stringValue");
    return stringValue;
  }

  @override
  void initState() {
    super.initState();
    futureSpecifiedFacilityDetail =
        fetchspecifiedFacilityDetail(widget.facilityId);
  }

  _launchEmail(String email, String subject) {
    final Uri _emailLaunchUri = Uri(
        scheme: 'mailto',
        path: email,
        queryParameters: {'subject': '$subject'});
    launch(_emailLaunchUri.toString());
  }

  _launchCaller(String phone) async {
    String url = "tel:${phone.toString()}";
    if (await canLaunch(
      url,
    )) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url, forceWebView: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/images/sign_up_bg.png'),
              fit: BoxFit.cover,
            ),
          ),
          child: Stack(
            children: [
// Title  Profile Management
              Positioned(
                top: Alignment.topCenter.x,
                child: Container(
                  // color: Colors.orange,
                  height: MediaQuery.of(context).size.height * .22,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width / 6,
                        // color: Colors.red,
                        child: InkWell(
                          onTap: () {
                            Navigator.pop(context, '/BottomNavBar');
                            // Navigator.pop(
                            //     context,
                            //     MaterialPageRoute(
                            //         fullscreenDialog: true,
                            //         builder: (context) => BottomNavBar()),
                            //   );
                          },
                          child: SizedBox(
                            child: Icon(
                              Icons.arrow_back,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width / 1.5,

                        //   color: Colors.black,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text(
                              "Details",
                              style: TextStyle(
                                fontSize: 22.0,
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),

// Form Card Widget Profile Management
              Positioned(
                top: 110,
                // top: Alignment.center.x,
                child: Container(
                  //  alignment: Alignment.center,
                  width: MediaQuery.of(context).size.width,
                  // color: Colors.amber,
                  height: MediaQuery.of(context).size.height * .8,
                  child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20),
                      ),
                      elevation: 30,
                      shadowColor: Colors.grey,
                      child: FutureBuilder<SpecifiedFacilityDetail>(
                        future: futureSpecifiedFacilityDetail,
                        builder: (context, snapshot) {
                          if (snapshot.hasData) {
                            return Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  height:
                                      MediaQuery.of(context).size.height / 4.6,
                                  width: MediaQuery.of(context).size.width,
                                  color: Colors.yellow[100],
                                  //child: Text("ASD"),
                                  child: Container(
                                    height: 20.0,
                                    width: MediaQuery.of(context).size.width,
                                    child: FadeInImage.assetNetwork(
                                      fit: BoxFit.cover,
                                      placeholder:
                                          'assets/images/place_holder.png',
                                      image: snapshot.data.data.avatar == null
                                          ? 'https://leadagency.club/wp-content/uploads/2021/09/placeholder-660.png'
                                          : snapshot.data.imageUrl +
                                              snapshot.data.data.avatar
                                                  .toString(),
                                    ),
                                  ),
                                ),
                                Container(
                                  height:
                                      MediaQuery.of(context).size.height / 1.9,
                                  width: MediaQuery.of(context).size.width,
                                  // color: Colors.grey,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 20.0, top: 0.0),
                                        child: Container(
                                          child: Text(
                                            widget.bussinessName,
                                            style: TextStyle(
                                              fontSize: 20.0,
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 20.0, top: 8.0),
                                        child: Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              .5,
                                          child: Text(
                                            widget.address,
                                            // snapshot.data.data.address
                                            //     .toString(),
                                            //    snapshot.data.address.toString(),
                                            // '168 West Magnolia St.\nOuter Nunavut, NU \nX0A3Y7',
                                            style: TextStyle(
                                              letterSpacing: 1.0,
                                              fontSize: 16.0,
                                              color: Colors.black,
                                              fontWeight: FontWeight.normal,
                                            ),
                                          ),
                                        ),
                                      ),
                                      Divider(),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 0.0, top: 5.0),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceAround,
                                          children: [
                                            Container(
                                              child: InkWell(
                                                child: CircleAvatar(
                                                    backgroundColor:
                                                        Colors.white,
                                                    child: Icon(
                                                      Icons.call,
                                                      color: Colors.orange,
                                                    )),
                                                onTap: () {
                                                  _launchCaller(snapshot
                                                      .data.data.phone
                                                      .toString());
                                                },
                                              ),
                                            ),
                                            Container(
                                              child: InkWell(
                                                child: CircleAvatar(
                                                    backgroundColor:
                                                        Colors.white,
                                                    child: Icon(
                                                      Icons.directions,
                                                      color: Colors.orange,
                                                    )),
                                                onTap: () {
                                                  MapUtils.openMap(
                                                      snapshot
                                                          .data.data.addressLat,
                                                      snapshot.data.data
                                                          .addressLong);
                                                  // snapshot.data.data.addressLat.toString() + snapshot.data.data.addressLong.toString();
                                                },
                                              ),
                                            ),
                                            Container(
                                              child: InkWell(
                                                child: CircleAvatar(
                                                    backgroundColor:
                                                        Colors.white,
                                                    child: Icon(
                                                      Icons.web,
                                                      color: Colors.orange,
                                                    )),
                                                onTap: () {
                                                  _launchURL(snapshot
                                                      .data.data.website
                                                      .toString());
                                                },
                                              ),
                                            ),
                                            Container(
                                              child: InkWell(
                                                child: CircleAvatar(
                                                    backgroundColor:
                                                        Colors.white,
                                                    child: Icon(
                                                      Icons.email,
                                                      color: Colors.orange,
                                                    )),
                                                onTap: () {
                                                  _launchEmail(
                                                      snapshot.data.data.email
                                                          .toString(),
                                                      'subject');
                                                },
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Divider(),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 20.0, right: 20.0),
                                        child: Container(
                                          child: TextField(
                                            enableInteractiveSelection: false,
                                            enabled: false,
                                            decoration: InputDecoration(
                                                // hintText: snapshot
                                                //     .data.data.createdAt
                                                //     .toString(),
                                                hintText: snapshot
                                                        .data.data.firstName +
                                                    snapshot.data.data.lastName
                                                        .toString(),
                                                prefixIcon: SizedBox(
                                                  height: 30,
                                                  width: 30,
                                                  child: Image.asset(
                                                    'assets/images/dob_icon.png',
                                                    color: Colors.orange,
                                                  ),
                                                ),
                                                hintStyle: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 12.0)),
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 20.0, right: 20.0),
                                        child: Container(
                                          child: TextField(
                                            enableInteractiveSelection: false,
                                            enabled: false,
                                            decoration: InputDecoration(
                                                // hintText: snapshot
                                                //     .data.data.createdAt
                                                //     .toString(),
                                                hintText: snapshot
                                                    .data.data.createdAt
                                                    .toString(),
                                                prefixIcon: SizedBox(
                                                  height: 30,
                                                  width: 30,
                                                  child: Image.asset(
                                                    'assets/images/dob_icon.png',
                                                    color: Colors.orange,
                                                  ),
                                                ),
                                                hintStyle: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 12.0)),
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 20.0, right: 20.0),
                                        child: Container(
                                          child: TextField(
                                            enableInteractiveSelection: false,
                                            enabled: false,
                                            decoration: InputDecoration(
                                                hintText: snapshot
                                                        .data.data.services
                                                        .toString()
                                                        .isEmpty
                                                    ? "services is not provided"
                                                    : snapshot.data.data.website
                                                        .toString(),
                                                // hintText:
                                                //     "168 West Magnolia St. Outer Nunavut, NU X0A 3Y7",
                                                prefixIcon: SizedBox(
                                                  height: 30,
                                                  width: 30,
                                                  child: Image.asset(
                                                    'assets/images/last_name.png',
                                                    color: Colors.orange,
                                                  ),
                                                ),
                                                hintStyle: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 12.0)),
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 20.0, right: 20.0),
                                        child: Container(
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Container(
                                                // color: Colors.red,
                                                height: MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    .05,
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    .3,
                                                child: TextFormField(
                                                  enableInteractiveSelection:
                                                      false,
                                                  enabled: false,
                                                  decoration: InputDecoration(
                                                    hintText: "OpenAt  " +
                                                        snapshot
                                                            .data.data.openAt
                                                            .toString(),
                                                    hintStyle: TextStyle(
                                                        color: Colors.black,
                                                        fontSize: 13.0),
                                                  ),
                                                ),
                                              ),
                                              Padding(
                                                padding: EdgeInsets.only(
                                                    left: 10.0,
                                                    right: 10.0,
                                                    top: 20.0),
                                              ),
                                              Container(
                                                // color: Colors.white,
                                                height: MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    .05,
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    .3,
                                                //color: Colors.white
                                                child: TextFormField(
                                                  enableInteractiveSelection:
                                                      false,
                                                  enabled: false,
                                                  decoration: InputDecoration(
                                                      hintText: "CloseAt  " +
                                                          snapshot
                                                              .data.data.closeAt
                                                              .toString(),
                                                      hintStyle: TextStyle(
                                                          color: Colors.black,
                                                          fontSize: 12.0)),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                // Container(
                                //   height:
                                //       MediaQuery.of(context).size.height / 1.75,
                                //   width: MediaQuery.of(context).size.width,
                                //   // color: Colors.red,
                                //   child: Column(
                                //     children: [
                                //       Column(
                                //         crossAxisAlignment:
                                //             CrossAxisAlignment.center,
                                //         mainAxisAlignment:
                                //             MainAxisAlignment.start,
                                //         children: [
                                //           Row(
                                //             crossAxisAlignment:
                                //                 CrossAxisAlignment.center,
                                //             mainAxisAlignment:
                                //                 MainAxisAlignment.center,
                                //             children: [
                                //               Container(
                                //                 // color: Colors.red,
                                //                 height: MediaQuery.of(context)
                                //                         .size
                                //                         .height *
                                //                     .05,
                                //                 width: MediaQuery.of(context)
                                //                         .size
                                //                         .width *
                                //                     .4,
                                //                 child: TextFormField(
                                //                   enableInteractiveSelection:
                                //                       false,
                                //                   enabled: false,
                                //                   decoration: InputDecoration(
                                //                     // hintText: widget.first_name,
                                //                     // snapshot
                                //                     //     .data.data.firstName
                                //                     //     .toString(),
                                //                     hintText: snapshot
                                //                         .data.data.firstName
                                //                         .toString(),
                                //                     // hintText: 'John',
                                //                     prefixIcon: SizedBox(
                                //                       height: 30,
                                //                       width: 30,
                                //                       child: Image.asset(
                                //                         'assets/images/first_name.png',
                                //                         color: Colors.orange,
                                //                       ),
                                //                     ),
                                //                     hintStyle: TextStyle(
                                //                         color: Colors.black,
                                //                         fontSize: 13.0),
                                //                   ),
                                //                 ),
                                //               ),
                                //               Padding(
                                //                 padding: EdgeInsets.only(
                                //                     left: 10.0,
                                //                     right: 10.0,
                                //                     top: 20.0),
                                //               ),
                                //               Container(
                                //                 // color: Colors.white,
                                //                 height: MediaQuery.of(context)
                                //                         .size
                                //                         .height *
                                //                     .05,
                                //                 width: MediaQuery.of(context)
                                //                         .size
                                //                         .width *
                                //                     .4,
                                //                 //color: Colors.white
                                //                 child: TextFormField(
                                //                   enableInteractiveSelection:
                                //                       false,
                                //                   enabled: false,
                                //                   decoration: InputDecoration(
                                //                       hintText: snapshot
                                //                           .data.data.lastName
                                //                           .toString(),
                                //                       //  snapshot
                                //                       //     .data.data.lastName
                                //                       //     .toString(),
                                //                       // 'Smith',
                                //                       prefixIcon: SizedBox(
                                //                         height: 30,
                                //                         width: 30,
                                //                         child: Image.asset(
                                //                           'assets/images/last_name.png',
                                //                           color: Colors.orange,
                                //                         ),
                                //                       ),
                                //                       hintStyle: TextStyle(
                                //                           color: Colors.black,
                                //                           fontSize: 12.0)),
                                //                 ),
                                //               ),
                                //             ],
                                //           ),
                                //         ],
                                //       ),
                                //       Padding(
                                //         padding: EdgeInsets.only(
                                //             left: 20.0, right: 25.0),
                                //         child: Column(
                                //           children: [
                                //             Padding(
                                //               padding:
                                //                   EdgeInsets.only(top: 5.0),
                                //             ),
                                //             TextField(
                                //               enableInteractiveSelection: false,
                                //               enabled: false,
                                //               decoration: InputDecoration(
                                //                   // hintText: snapshot
                                //                   //     .data.data.createdAt
                                //                   //     .toString(),
                                //                   hintText: snapshot
                                //                       .data.data.createdAt
                                //                       .toString(),
                                //                   prefixIcon: SizedBox(
                                //                     height: 30,
                                //                     width: 30,
                                //                     child: Image.asset(
                                //                       'assets/images/dob_icon.png',
                                //                       color: Colors.orange,
                                //                     ),
                                //                   ),
                                //                   hintStyle: TextStyle(
                                //                       color: Colors.black,
                                //                       fontSize: 12.0)),
                                //             ),
                                //             Padding(
                                //               padding:
                                //                   EdgeInsets.only(top: 5.0),
                                //             ),
                                //             TextField(
                                //               enableInteractiveSelection: false,
                                //               enabled: false,
                                //               decoration: InputDecoration(
                                //                   // hintText: snapshot
                                //                   //     .data.data.email
                                //                   //     .toString(),
                                //                   hintText: snapshot
                                //                       .data.data.email
                                //                       .toString(),
                                //                   prefixIcon: SizedBox(
                                //                     height: 30,
                                //                     width: 30,
                                //                     child: Image.asset(
                                //                       'assets/images/last_name.png',
                                //                       color: Colors.orange,
                                //                     ),
                                //                   ),
                                //                   hintStyle: TextStyle(
                                //                       color: Colors.black,
                                //                       fontSize: 12.0)),
                                //             ),
                                //             Padding(
                                //               padding:
                                //                   EdgeInsets.only(top: 5.0),
                                //             ),
                                //             InkWell(
                                //               splashColor: Colors.orange[700],
                                //               highlightColor:
                                //                   Colors.orange[700],
                                //               child: TextField(
                                //                 enableInteractiveSelection:
                                //                     false,
                                //                 enabled: false,
                                //                 decoration: InputDecoration(
                                //                   hintText: snapshot
                                //                           .data.data.website
                                //                           .toString()
                                //                           .isEmpty
                                //                       ? "website link is not available"
                                //                       : snapshot
                                //                           .data.data.website
                                //                           .toString(),
                                //                   prefixIcon: SizedBox(
                                //                     height: 30,
                                //                     width: 30,
                                //                     child: Image.asset(
                                //                       'assets/images/password_icon.png',
                                //                       color: Colors.orange,
                                //                     ),
                                //                   ),
                                //                   hintStyle: TextStyle(
                                //                       fontSize: 12.0,
                                //                       decoration:
                                //                           TextDecoration.none,
                                //                       color: Colors.orange,
                                //                       fontWeight:
                                //                           FontWeight.normal),
                                //                 ),
                                //               ),
                                //               // Text(
                                //               //   "https://zerk.com.au/panel/public/terms-and-condition",
                                //               //   style: TextStyle(
                                //               //       fontSize: 15.0,
                                //               //       decoration:
                                //               //           TextDecoration.none,
                                //               //       color: Colors.orange,
                                //               //       fontWeight:
                                //               //           FontWeight.normal),
                                //               // ),
                                //               onTap: () {
                                //                 _launchURL(snapshot
                                //                     .data.data.website
                                //                     .toString());
                                //               },
                                //             ),
                                //             Padding(
                                //               padding:
                                //                   EdgeInsets.only(top: 5.0),
                                //             ),
                                //             TextField(
                                //               enableInteractiveSelection: false,
                                //               enabled: false,
                                //               decoration: InputDecoration(
                                //                   hintText: snapshot
                                //                       .data.data.phone
                                //                       .toString(),
                                //                   // hintText:
                                //                   //     "168 West Magnolia St. Outer Nunavut, NU X0A 3Y7",
                                //                   prefixIcon: SizedBox(
                                //                     height: 30,
                                //                     width: 30,
                                //                     child: Image.asset(
                                //                       'assets/images/last_name.png',
                                //                       color: Colors.orange,
                                //                     ),
                                //                   ),
                                //                   hintStyle: TextStyle(
                                //                       color: Colors.black,
                                //                       fontSize: 12.0)),
                                //             ),
                                //             TextField(
                                //               enableInteractiveSelection: false,
                                //               enabled: false,
                                //               decoration: InputDecoration(
                                //                   hintText: snapshot
                                //                           .data.data.services
                                //                           .toString()
                                //                           .isEmpty
                                //                       ? "services is not provided"
                                //                       : snapshot
                                //                           .data.data.website
                                //                           .toString(),
                                //                   // hintText:
                                //                   //     "168 West Magnolia St. Outer Nunavut, NU X0A 3Y7",
                                //                   prefixIcon: SizedBox(
                                //                     height: 30,
                                //                     width: 30,
                                //                     child: Image.asset(
                                //                       'assets/images/last_name.png',
                                //                       color: Colors.orange,
                                //                     ),
                                //                   ),
                                //                   hintStyle: TextStyle(
                                //                       color: Colors.black,
                                //                       fontSize: 12.0)),
                                //             ),
                                //             Padding(
                                //               padding:
                                //                   EdgeInsets.only(top: 3.0),
                                //             ),
                                //             Text(
                                //               'Operating hours'.toUpperCase(),
                                //               style: TextStyle(
                                //                 fontWeight: FontWeight.normal,
                                //                 color: Colors.black,
                                //               ),
                                //             ),
                                //             //-------OPERTING HOURS--------
                                //             Row(
                                //               crossAxisAlignment:
                                //                   CrossAxisAlignment.center,
                                //               mainAxisAlignment:
                                //                   MainAxisAlignment.center,
                                //               children: [
                                //                 Container(
                                //                   // color: Colors.red,
                                //                   height: MediaQuery.of(context)
                                //                           .size
                                //                           .height *
                                //                       .05,
                                //                   width: MediaQuery.of(context)
                                //                           .size
                                //                           .width *
                                //                       .3,
                                //                   child: TextFormField(
                                //                     enableInteractiveSelection:
                                //                         false,
                                //                     enabled: false,
                                //                     decoration: InputDecoration(
                                //                       hintText: "OpenAt  " +
                                //                           snapshot
                                //                               .data.data.openAt
                                //                               .toString(),
                                //                       hintStyle: TextStyle(
                                //                           color: Colors.black,
                                //                           fontSize: 13.0),
                                //                     ),
                                //                   ),
                                //                 ),
                                //                 Padding(
                                //                   padding: EdgeInsets.only(
                                //                       left: 10.0,
                                //                       right: 10.0,
                                //                       top: 20.0),
                                //                 ),
                                //                 Container(
                                //                   // color: Colors.white,
                                //                   height: MediaQuery.of(context)
                                //                           .size
                                //                           .height *
                                //                       .05,
                                //                   width: MediaQuery.of(context)
                                //                           .size
                                //                           .width *
                                //                       .3,
                                //                   //color: Colors.white
                                //                   child: TextFormField(
                                //                     enableInteractiveSelection:
                                //                         false,
                                //                     enabled: false,
                                //                     decoration: InputDecoration(
                                //                         hintText: "CloseAt  " +
                                //                             snapshot.data.data
                                //                                 .closeAt
                                //                                 .toString(),
                                //                         hintStyle: TextStyle(
                                //                             color: Colors.black,
                                //                             fontSize: 12.0)),
                                //                   ),
                                //                 ),
                                //               ],
                                //             ),
                                //           ],
                                //         ),
                                //       ),
                                //     ],
                                //   ),
                                // ),
                              ],
                            );
                          } else if (snapshot.hasError) {
                            print(snapshot.error);
                            return Text("sssssss${snapshot.error}");
                          }

                          // By default, show a loading spinner.
                          return Center(
                            child: showProgressLoader(context),
                          );
                        },
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget showProgressLoader(BuildContext context) {
    if (Platform.isAndroid) {
      return CircularProgressIndicator(
        color: Colors.orange,
      );
    }
    if (Platform.isIOS) {
      return CupertinoActivityIndicator();
    }
  }

  //-------------UserDetailsDataAPI--------------------------------
  Future<UserDetails> userDetailsDataApi(String token) async {
    final response = await http.get(
      Uri.parse(baseUrl.baseurl + "user/details"),
      headers: <String, String>{
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + token,
      },
    );

    if (response.statusCode == 200) {
      print("----------------bbbbbbbbbbbbbbbbbbbbb---------");
      print(json.decode(response.body));

      return UserDetails.fromJson(json.decode(response.body));
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load data');
    }
  }

  //-------------UserDetailsDataAPI--------------------------------
  Future<SpecifiedFacilityDetail> fetchspecifiedFacilityDetail(
      String id) async {
    final response = await http.get(
      Uri.parse(baseUrl.baseurl + 'admin/facility/detail/$id'),
      headers: <String, String>{
        'Accept': 'application/json'
        // 'Authorization': 'Bearer ' + token,
      },
    );

    if (response.statusCode == 200) {
      print("----------------fetchspecifiedFacilityDetail---------");
      print(json.decode(response.body.toString()));
      var parsedJson = json.decode(response.body);
      var success = SpecifiedFacilityDetail.fromJson(parsedJson);

      specifiedFacilityDetailList = List<SpecifiedFacilityDetail>();
      specifiedFacilityDetailList
          .add(SpecifiedFacilityDetail(data: success.data));
      print(specifiedFacilityDetailList[0].data.bussinessName);
      print("----------------vvvvvvvvvvvvvvv---------");
      return SpecifiedFacilityDetail.fromJson(json.decode(response.body));
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load fetchspecifiedFacilityDetail');
    }
  }
}
