import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:Zerk/model/PushNotificationModel.dart';
import 'package:Zerk/model/model_NotificationVideoUrl.dart';
import 'package:Zerk/model/model_userDetails.dart';
import 'package:Zerk/screens/profile_widget.dart';
import 'package:Zerk/utils/baseurl.dart';
import 'package:Zerk/utils/video_items.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:http/http.dart' as http;
import 'package:localstorage/localstorage.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:video_player/video_player.dart';

class QRCodeForIOS extends StatefulWidget {
  @override
  _QRCodeForIOSState createState() => _QRCodeForIOSState();
}

class _QRCodeForIOSState extends State<QRCodeForIOS> {
  final LocalStorage storage = LocalStorage('token');
  String userId;
  String token;
  String qrCodeId;
  Uint8List bytes = Uint8List(0);
  GlobalKey globalKey = new GlobalKey();
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
  String _dataString = "Hello from this QR";
  TextEditingController _inputController;
  TextEditingController _outputController;
  // String userid, token;
  Baseurl baseurl = Baseurl();
  // final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  // final LocalStorage storage = new LocalStorage('token');
  bool visible = false;
  bool facilityAccessAd;
  String videoUrl, facilityID;
  final FirebaseMessaging firebaseMessaging = FirebaseMessaging.instance;
  // FirebaseMessaging messaging;

  dynamic _notificationInfo;

  _handleNotification(Map<dynamic, dynamic> message) async {
    print("3434eqef");

    var data = message['data'] ?? message;
    String expectedAttribute = data['expectedAttribute'];
    // print(data);
    // print(expectedAttribute);

    // String dataMap =
    //     json.decode(message['notification']['body']['message']);
    print("onMessage--------------1111");
    print("onMessage----: $message");
    // print("onMessage----: $dataMap");
    print('---------------');
    print(message['notification']['body']);
    print('---------------');

    var parsedJson = json.decode(message['notification']['body']);
    print(parsedJson['message'].toString());
    print('mmmmmmmmm');
    var success = NotificationVideoUrl.fromJson(parsedJson);
    print(success.message);
    // _showDialog(success.message.toString(), false);
    print(success.videoUrl);
    facilityID = success.facility.toString();
    videoUrl = success.videoUrl;
    print(videoUrl);

    // showOverlayNote(message);
    // showOverlayNotification(massage);

    if (videoUrl != null) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => VideoItems(
            userid: userId,
            facilityID: facilityID,
            videoUrl: videoUrl,
          ),
        ),
      );
      // loadProgress(true);
      // setState(() {
      //   // storage.setItem("facilityAccessAd", true);
      // });
    } else {
      // if(success.message.isNotEmpty){
      //
      // }
      // _showDialog("videoUrlIsInValid", false);
      // widget.facilityAccessAd == false
      //     ? loadProgress(false)
      //     :
      // loadProgress(false);
    }
    if (success.message == 'Access Granted Successfully') {
      _showNotificationWithoutSound('Access Granted Successfully');
      // _showNotificationWithoutSound("The access has been granted");
    }

    /// [...]
  }

  final Map<String, Item> _items = <String, Item>{};

  Item _itemForMessage(Map<String, dynamic> message) {
    String notification = '';
    notification = message['notification']['body'];
    print(notification);
    final dynamic data = message['data'] ?? message;
    print(data);
    final String itemId = data['id'];
    print(itemId);
    final Item item = _items.putIfAbsent(
        itemId, () => Item(itemId: itemId, data: notification))
      ..status = data['status'];
    return item;
  }

  void _navigateToItemDetail(Map<String, dynamic> message) {
    final Item item = _itemForMessage(message);
    // Clear away dialogs
    Navigator.popUntil(context, (Route<dynamic> route) => route is PageRoute);
    if (!item.route.isCurrent) {
      Navigator.push(context, item.route);
    }
  }

  void _showDialog(String contentstring, bool value) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        if (value == false) {
          print(value);
          Future.delayed(Duration(seconds: 1), () {
            Navigator.of(context).pop(true);
          });
        } else {}
        return AlertDialog(
          content: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Text(
                  contentstring,
                  textAlign: TextAlign.left,
                  maxLines: 4,
                  style: TextStyle(
                      color: Colors.orange[700], fontWeight: FontWeight.bold),
                ),
              )
            ],
          ),
        );
      },
    );
  }

  Future _showNotificationWithoutSound(String string) async {
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        'your channel id', 'your channel name', 'your channel description',
        playSound: false, importance: Importance.max, priority: Priority.high);
    var iOSPlatformChannelSpecifics =
        new IOSNotificationDetails(presentSound: false);
    var platformChannelSpecifics = new NotificationDetails(
        android: androidPlatformChannelSpecifics,
        iOS: iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
      0,
      'ZERK',
      '$string',
      platformChannelSpecifics,
      payload: '$string',
    );
  }

  Future onSelectNotification(String payload) async {
    showDialog(
      context: context,
      builder: (_) {
        return new AlertDialog(
          title: Text("ZERK"),
          content: Text("The access Successfully"),
        );
      },
    );
  }

  Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) {
    if (message.containsKey('data')) {
      // Handle data message
      final dynamic data = message['data'];
      if (Platform.isIOS) {
        message = _handleNotification(message);
      }
      print(data.toString());
    }

    if (message.containsKey('notification')) {
      // Handle notification message
      final dynamic notification = message['notification'];
      print(notification.toString());
    }
//     _firebaseMessaging.configure(
//       onMessage: (Map<String, dynamic> message) async {
//         print("onMessage: $message");
//         print("--------------------notification--------------------");
//         String string = message['notification']['body'];
// //        String newbody = message['notification']['body']['title'];
// //        print(newbody);
//         _showDialog('onMessage' + string.toString(), true);
// //          _showItemDialog(message);
//       },
//       onLaunch: (Map<String, dynamic> message) async {
//         print("onLaunch: $message");
//         String string = message['notification']['body'];
//         _showDialog('onLaunch $string', true);
//         _navigateToItemDetail(message);
// //          myBackgroundMessageHandler(message);
//       },
//       onResume: (Map<String, dynamic> message) async {
//         print("onResume: $message");
//         String string = message['notification']['body'];
//         _showDialog('onResume' + string.toString(), true);
// //          _navigateToItemDetail(message);
// //          myBackgroundMessageHandler(message);
//       },
//     );
//     _firebaseMessaging
//         .requestNotificationPermissions(const IosNotificationSettings(
//       sound: true,
//       badge: true,
//       alert: true,
//       provisional: false,
//     ));
//     _firebaseMessaging.onIosSettingsRegistered
//         .listen((IosNotificationSettings settings) {
//       print("Settings registered: $settings");
//     });

    // _firebaseMessaging.getToken().then((String token) {
    //   assert(token != null);
    //   setState(() {});
    // });

    // Or do other work.
  }

  Future<void> _firebaseMessagingBackgroundHandler(
      RemoteMessage message) async {
    print("Handling a background message: ${message.messageId}");
  }

  void registerNotification() async {
    FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

    NotificationSettings settings = await firebaseMessaging.requestPermission(
      alert: true,
      badge: true,
      provisional: false,
      sound: true,
    );
    print('register Notification');

    if (settings.authorizationStatus == AuthorizationStatus.authorized) {
      print('User granted permission');

      FirebaseMessaging.onMessage.listen((RemoteMessage message) {
        print('-------2322424-');
        print(
            'Message title: ${message.notification?.title}, body: ${message.notification?.body}, data: ${message.data}');
        print('-------2322424-');

        // // Parse the message received
        // PushNotification notification = PushNotification(
        //   title: message.notification?.title,
        //   body: message.notification?.body,
        //   dataTitle: message.data['title'],
        //   dataBody: message.data['body'],
        // );
        //
        // setState(() {
        //   _notificationInfo = 'asdfadfafd';
        //   // _totalNotifications++;
        // });

        if (_notificationInfo != null) {
          // For displaying the notification as an overlay
          // showSimpleNotification(
          //   Text(_notificationInfo.title),
          //   leading: NotificationBadge(totalNotifications: _totalNotifications),
          //   subtitle: Text(_notificationInfo.body),
          //   background: Colors.cyan.shade700,
          //   duration: Duration(seconds: 2),
          // );
          _showDialog(message.notification.body, true);
        }
      });
    } else {
      _showDialog("Please give required permissions", true);
      print('User declined or has not accepted permission');
    }
  }

  checkForInitialMessage() async {
    print('adfasdfasdsdfdasfasdfasdf');

    // await Firebase.initializeApp();
    RemoteMessage initialMessage =
        await FirebaseMessaging.instance.getInitialMessage();

    if (initialMessage != null) {
      PushNotification notification = PushNotification(
          // title: initialMessage.notification?.title,
          // body: initialMessage.notification?.body,
          // dataTitle: initialMessage.data['title'],
          // dataBody: initialMessage.data['body'],
          );
      setState(() {
        _notificationInfo = initialMessage.notification.body;
        // _totalNotifications++;
      });
    }
  }

  @override
  initState() {
    super.initState();
    // loadProgress(false);
    // this._inputController = new TextEditingController();
    // this._outputController = new TextEditingController();
    userId = storage.getItem('userid');
    token = storage.getItem('token');
    qrCodeId = userId;

    // widget.facilityAccessAd = null;f
    // facilityAccessAd = storage.getItem("facilityAccessAd");
    print("--------------asdfasdfasdfasfsfasfdf:" +
        storage.getItem('userid') +
        userId.toString());
    // _generateBarCode(userid);
    userDetailsDataApi(token);
    print(".--------------" + facilityAccessAd.toString());
    print(".--------------" + facilityAccessAd.toString());
    // firebaseMessaging = FirebaseMessaging.instance;
    firebaseMessaging.getToken().then((value) {
      print(value);
    });
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      print("message received");
      print('*******');
      // PushNotification notification = PushNotification(
      //   title: message.notification?.title,
      //   body: message.notification?.body,
      //   dataTitle: message.data['title'],
      //   dataBody: message.data['body'],
      // );

      // print(notification.body);
      // print(notification.title);
      print('*******');
      var paredJson = jsonDecode(message.notification.body);
      var success = NotificationVideoUrl.fromJson(paredJson);
      // _showDialog('Thank you for becoming a member', false);
      print(paredJson);
      facilityID = success.facility.toString();
      print('**********');
      print(facilityID);
      print('***********');
      videoUrl = success.videoUrl.toString();
      print('*********');
      print('vedio url' + videoUrl);
      if (videoUrl == "" || videoUrl == 'null') {
        print('if ' + videoUrl);
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => QRCodeForIOS()));
      } else if (videoUrl != 'null') {
        print('else ' + videoUrl);
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => VideoItems(
              userid: userId,
              facilityID: facilityID,
              videoUrl: videoUrl,
            ),
          ),
        );
      }
      if (success.message == 'Access Granted Successfully') {
        _showNotificationWithoutSound('Access Granted Successfully');
        // _showNotificationWithoutSound("The access has been granted");
      }

      // grantedprint('????????????');
      // if (Platform.isIOS) {
      //   message = _handleNotification(message);
      // }
      // _showDialog(string.toString(), true);
//          _showItemDialog(message);
//       },
      // onLaunch: (Map<String, dynamic> message) async {
      //   print("onLaunch: $message");
      //   if (Platform.isIOS) {
      //     message = _handleNotification(message);
      //   }
      //   String string = message['notification']['body'];
      //   _showDialog(string.toString(), true);
      //   // _navigateToItemDetail(message);
      //   myBackgroundMessageHandler(message);
      // },
      // onResume: (Map<String, dynamic> message) async {
      //   print("onResume: $message");
      //   if (Platform.isIOS) {
      //     message = _handleNotification(message);
      //   }
      //   String string = message['notification']['body'];
      //   _showDialog(string.toString(), true);
      //   myBackgroundMessageHandler(message);
      // },
      // );
      //   print(message.notification.body);
    });

//     // _firebaseMessaging.configure(
//     //   onMessage: (Map<String, dynamic> message) async {
//     //     // String dataMap =
//     //     //     json.decode(message['notification']['body']['message']);
//     //     print("onMessage--------------1111");
//     //     print("onMessage----: $message");
//     //     // print("onMessage----: $dataMap");
//     //     print('---------------');
//     //     print(message['notification']['body']);
//     //     print('---------------');
//     //
//         var parsedJson = json.decode(message['notification']['body']);
//     //     print(parsedJson['message'].toString());
//     //     print('mmmmmmmmm');
//         var success = NotificationVideoUrl.fromJson(parsedJson);
//     //     print(success.message);
//     //     // _showDialog(success.message.toString(), false);
//     //     print(success.videoUrl);
//     //     facilityID = success.facility.toString();
//     //     videoUrl = success.videoUrl;
//         print(videoUrl);
//
//         if (videoUrl != null) {
//           Navigator.push(
//             context,
//             MaterialPageRoute(
//               builder: (context) => VideoItems(
//                 userid: userId,
//                 facilityID: facilityID,
//                 videoUrl: videoUrl,
//               ),
//             ),
//           );
//           // loadProgress(true);
//           // setState(() {
//           //   // storage.setItem("facilityAccessAd", true);
//           // });
//         } else {
//           // if(success.message.isNotEmpty){
//           //
//           // }
//           // _showDialog("videoUrlIsInValid", false);
//           // widget.facilityAccessAd == false
//           //     ? loadProgress(false)
//           //     :
//           // loadProgress(false);
//         }
//         if (success.message == 'Access Granted Successfully') {
//           _showNotificationWithoutSound('Access Granted Successfully');
//           _showNotificationWithoutSound("The access has been granted");
//         }
//
//         print('????????????');
//         if (Platform.isIOS) {
//           message = _handleNotification(message);
//         }
//         // _showDialog(string.toString(), true);
// //          _showItemDialog(message);
//       },
//       onLaunch: (Map<String, dynamic> message) async {
//         print("onLaunch: $message");
//         if (Platform.isIOS) {
//           message = _handleNotification(message);
//         }
//         String string = message['notification']['body'];
//         _showDialog(string.toString(), true);
//         // _navigateToItemDetail(message);
//         myBackgroundMessageHandler(message);
//       },
//       onResume: (Map<String, dynamic> message) async {
//         print("onResume: $message");
//         if (Platform.isIOS) {
//           message = _handleNotification(message);
//         }
//         String string = message['notification']['body'];
//         _showDialog(string.toString(), true);
//         myBackgroundMessageHandler(message);
//       },
//     );

    var initializationSettingsAndroid =
        new AndroidInitializationSettings('zerk');
    var initializationSettingsIOS = new IOSInitializationSettings();
    var initializationSettings = new InitializationSettings(
        android: initializationSettingsAndroid, iOS: initializationSettingsIOS);
    flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);
  }

  Future<NotificationVideoUrl> notificationVideoUrl(
      Map<String, dynamic> message) async {
    final response = await http.get(Uri.parse(message.toString()));
    print(response.statusCode.toString());
    print(response.body.toString());
    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      print(response.statusCode.toString());
      print(response.body.toString());
      var parsedJson = json.decode(message.toString());
      var success = NotificationVideoUrl.fromJson(parsedJson);
      print(success.videoUrl.toString());
      // storage.setItem('totalAvails', success.videoUrl.toString());
      return NotificationVideoUrl.fromJson(
          json.decode(message['notification']['body']));
      // return NotificationVideoUrl.fromJson(jsonDecode(response.body));
    } else {
      print(response.statusCode.toString());
      print(response.body.toString());
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load Notification Video Url');
    }
  }

  Future<UserDetails> userDetailsDataApi(String token) async {
    final response = await http.get(
      Uri.parse(baseurl.baseurl + "user/details"),
      headers: <String, String>{
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + token,
      },
    );

    if (response.statusCode == 200) {
      print("----------------userDetailsDataApi---------");
      print(json.decode(response.body));
      var parsedJson = json.decode(response.body);
      var success = UserDetails.fromJson(parsedJson);
      storage.setItem('totalAvails', success.data.totalAvails.toString());
      print('******************');
      print('THIS IS TOTAL AVAILS ${storage.getItem('totalAvails')}');
      print('******************');
      return UserDetails.fromJson(json.decode(response.body));
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load data');
    }
  }

  _contentWidget() {
    final bodyHeight = MediaQuery.of(context).size.height -
        MediaQuery.of(context).viewInsets.bottom;
    return Container(
      color: const Color(0xFFFFFFFF),
      child: Column(
        children: <Widget>[
          Expanded(
            child: Center(
              child: RepaintBoundary(
                key: globalKey,
                child: QrImage(
                  data: _dataString,
                  size: 0.5 * bodyHeight,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget showQR(BuildContext context) {
    if (Platform.isAndroid) {
      return _qrCodeWidgetAndroid(this.bytes, context);
    }
    if (Platform.isIOS) {
      return _qrCodeWidgetIos(this.bytes, context);
    }
  }

  Widget _qrCodeWidgetAndroid(Uint8List bytes, BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(5),
      child: Center(
        // elevation: 6,
        child: Column(
          children: <Widget>[
            Padding(
              padding:
                  EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 190,
                    child: bytes.isEmpty
                        ? Center(
                            child: Text('Empty code ... ',
                                style: TextStyle(color: Colors.black38)),
                          )
                        : Image.memory(bytes),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _qrCodeWidgetIos(Uint8List bytes, BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(5),
      child: Center(
        // elevation: 6,
        child: Column(
          children: <Widget>[
            Padding(
              padding:
                  EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: MediaQuery.of(context).size.height / 3.5,
                    child: QrImage(
                      data: bytes.toString(),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  loadProgress(bool value) {
    if (visible == value) {
      setState(() {
        visible = value;
      });
    } else {
      setState(() {
        visible = value;
      });
    }
  }

  @override
  void dispose() {
    super.dispose();
    VideoPlayerController.network(videoUrl).dispose();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/images/login_bg.png'),
              fit: BoxFit.cover,
              alignment: Alignment.topLeft),
        ),
        child: Stack(
          children: [
            //VideoPlayerWidget

            // widget.facilityAccessAd == null

// Settings TitleL
            Positioned(
              top: Alignment.topCenter.x,
              child: Container(
                // color: Colors.black,
                height: MediaQuery.of(context).size.height * .22,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width / 6,
                      // color: Colors.red,
                      child: InkWell(
                        onTap: () {
                          // loadProgress(true);
                          Navigator.popAndPushNamed(context, '/BottomNavBar');
                          // Navigator.pop(
                          //     context,
                          //     MaterialPageRoute(
                          //         fullscreenDialog: true,
                          //         builder: (context) => BottomNavBar()),
                          //   );
                        },
                        child: SizedBox(
                          child: Icon(
                            Icons.arrow_back,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width / 1.5,
                      //   color: Colors.black,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            "Scan QR Code ",
                            // "Scan QR Code ${widget.facilityAccessAd.toString()}",
                            style: TextStyle(
                              fontSize: 22.0,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),

            // icons
            Positioned(
              // top: Alignment.bottomRight.x,
              right: Alignment.centerRight.x,
              left: Alignment.centerLeft.x,
              bottom: MediaQuery.of(context).size.height -
                  MediaQuery.of(context).size.height * .25,
              // bottom: Alignment.centerLeft.x,
              child: Container(
                // color: Colors.red,
                height: MediaQuery.of(context).size.height * .10,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    // Container(
                    //   // width: MediaQuery.of(context).size.width / 1.5,
                    //   // color: Colors.black,
                    //   child: Row(
                    //     crossAxisAlignment: CrossAxisAlignment.center,
                    //     mainAxisAlignment: MainAxisAlignment.center,
                    //     children: [
                    //       Padding(
                    //         padding: EdgeInsets.only(left: 10.0),
                    //         child: SizedBox(
                    //           child: Icon(
                    //             Icons.hourglass_full,
                    //             color: Colors.white,
                    //           ),
                    //         ),
                    //       ),
                    //     ],
                    //   ),
                    // ),
                    Container(
                      // width: MediaQuery.of(context).size.width / 6,
                      // color: Colors.brown,
                      child: InkWell(
                        onTap: () {
                          // loadProgress(true);
                          // Navigator.popAndPushNamed(context, '/BottomNavBar');
                          // Navigator.pop(
                          //     context,
                          //     MaterialPageRoute(
                          //         fullscreenDialog: true,
                          //         builder: (context) => BottomNavBar()),
                          //   );
                        },
                        child: SizedBox(
                          width: MediaQuery.of(context).size.width / 6,
                          height: MediaQuery.of(context).size.height / 11,
                          child: Container(
                            // color: Colors.black,
                            // child: Image.asset('assets/images/wine-bottle.png',color: Colors.orange[500],),
                            child: CircleAvatar(
                                backgroundColor: Colors.white,
                                child: Padding(
                                  padding: EdgeInsets.all(10.0),
                                  child: Image.asset(
                                    'assets/images/water-bottle.png',
                                    color: Colors.orange[500],
                                  ),
                                )),
                          ),
                        ),
                        // SizedBox(
                        //   child: Icon(
                        //     Icons.arrow_back,
                        //     color: Colors.white,
                        //   ),
                        // ),
                      ),
                    ),
                    Container(
                      // width: MediaQuery.of(context).size.width / 6,
                      // color: Colors.brown,
                      child: InkWell(
                        onTap: () {
                          // loadProgress(true);
                          // Navigator.popAndPushNamed(context, '/BottomNavBar');
                          // Navigator.pop(
                          //     context,
                          //     MaterialPageRoute(
                          //         fullscreenDialog: true,
                          //         builder: (context) => BottomNavBar()),
                          //   );
                        },
                        child: SizedBox(
                          width: MediaQuery.of(context).size.width / 6,
                          height: MediaQuery.of(context).size.height / 11,
                          child: Container(
                            // color: Colors.black,
                            // child: Image.asset('assets/images/wine-bottle.png',color: Colors.orange[500],),
                            child: CircleAvatar(
                                backgroundColor: Colors.white,
                                child: Padding(
                                  padding: EdgeInsets.all(10.0),
                                  child: Image.asset(
                                    'assets/images/headphone.png',
                                    color: Colors.orange[500],
                                  ),
                                )),
                          ),
                        ),
                        // SizedBox(
                        //   child: Icon(
                        //     Icons.arrow_back,
                        //     color: Colors.white,
                        //   ),
                        // ),
                      ),
                    ),
                    Container(
                      // width: MediaQuery.of(context).size.width / 6,
                      // color: Colors.brown,
                      child: InkWell(
                        onTap: () {
                          // loadProgress(true);
                          // Navigator.popAndPushNamed(context, '/BottomNavBar');
                          // Navigator.pop(
                          //     context,
                          //     MaterialPageRoute(
                          //         fullscreenDialog: true,
                          //         builder: (context) => BottomNavBar()),
                          //   );
                        },
                        child: SizedBox(
                          width: MediaQuery.of(context).size.width / 6,
                          height: MediaQuery.of(context).size.height / 11,
                          child: Container(
                            // color: Colors.black,
                            // child: Image.asset('assets/images/wine-bottle.png',color: Colors.orange[500],),
                            child: CircleAvatar(
                                backgroundColor: Colors.white,
                                child: Padding(
                                  padding: EdgeInsets.all(10.0),
                                  child: Image.asset(
                                    'assets/images/towel.png',
                                    color: Colors.orange[500],
                                  ),
                                )),
                          ),
                        ),
                        // SizedBox(
                        //   child: Icon(
                        //     Icons.arrow_back,
                        //     color: Colors.white,
                        //   ),
                        // ),
                      ),
                    ),
                  ],
                ),
              ),
            ),

            // Settings Card Widget
            Positioned(
              //   top: 110,
              left: Alignment.center.y,
              right: Alignment.center.y,
              top: Alignment.center.y,
              bottom: Alignment.center.y,
              // top: Alignment.center.x,
              child: Padding(
                padding: const EdgeInsets.only(top: 60.0),
                child: Align(
                  alignment: Alignment.center,
                  child: SingleChildScrollView(
                    child: Container(
                      margin: const EdgeInsets.only(top: 30.0),
                      //  alignment: Alignment.center,
                      width: MediaQuery.of(context).size.width,

                      //    color: Colors.amber,
                      // height: MediaQuery.of(context).size.height * 80,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20.0),
                        child: SingleChildScrollView(
                          child: Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20),
                            ),
                            // elevation: 5,
                            shadowColor: Colors.grey,
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 30.0),
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  SizedBox(
                                    height: 20,
                                  ),
                                  SizedBox(
                                    width:
                                        MediaQuery.of(context).size.width / 6,
                                    height:
                                        MediaQuery.of(context).size.height / 11,
                                    child: Container(
                                      // color: Colors.black,
                                      child: Image.asset(
                                          'assets/images/qrcodecamera_icon.png'),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  Container(
                                    child: Text(
                                      "Please scan QRCode\nwith Required device",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontSize: 18.0,
                                        color: Colors.grey,
                                        fontWeight: FontWeight.normal,
                                      ),
                                    ),
                                  ),
                                  // _contentWidget(),
                                  // _qrCodeWidgetIos(this.bytes, context),
                                  QrImage(
                                    data: qrCodeId,
                                    size:
                                        MediaQuery.of(context).size.height / 4,
                                    backgroundColor: Colors.white,
                                  ),

                                  // Align(
                                  //   alignment: Alignment.center,
                                  //   child: Container(
                                  //     child: Row(
                                  //       mainAxisAlignment:
                                  //           MainAxisAlignment.center,
                                  //       crossAxisAlignment:
                                  //           CrossAxisAlignment.end,
                                  //       children: [
                                  //         SizedBox(
                                  //           // width: 220.0,
                                  //           // height: 60.0,
                                  //           height: MediaQuery.of(context)
                                  //                   .size
                                  //                   .width /
                                  //               8,
                                  //           width: MediaQuery.of(context)
                                  //                   .size
                                  //                   .width /
                                  //               2,
                                  //
                                  //           child: RaisedButton(
                                  //             shape: RoundedRectangleBorder(
                                  //                 borderRadius:
                                  //                     BorderRadius.circular(
                                  //                         42.0),
                                  //                 side: BorderSide(
                                  //                     color: Colors.white)),
                                  //             onPressed: () {
                                  //               // Navigator.pushNamed(
                                  //               //     context, '/VideoItems');
                                  //               Navigator.push(
                                  //                 context,
                                  //                 MaterialPageRoute(
                                  //                   builder: (context) =>
                                  //                       VideoItems(
                                  //                     userid: userid,
                                  //                     facilityID: facilityID,
                                  //                     videoUrl: videoUrl,
                                  //                   ),
                                  //                 ),
                                  //               );
                                  //               // loadProgress(false);
                                  //             },
                                  //             color: Colors.white,
                                  //             textColor: Colors.orange[300],
                                  //             child: Text("SCAN",
                                  //                 style: TextStyle(
                                  //                   fontSize: 20.0,
                                  //                   letterSpacing: 1,
                                  //                   fontWeight: FontWeight.bold,
                                  //                 )),
                                  //           ),
                                  //         ),
                                  //       ],
                                  //     ),
                                  //   ),
                                  // ),
                                  SizedBox(
                                    height: 40,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
            // storage.getItem("facilityAccessAd") == true
            //  ?
            Visibility(
              visible: visible,
              child: Positioned(
                child: VideoItems(
                  videoPlayerController: VideoPlayerController.network(
                    // videoUrl,
                    // 'https://www.radiantmediaplayer.com/media/big-buck-bunny-360p.mp4',
                    'https://www.youtube.com/watch?v=9xwazD5SyVg&ab_channel=applekids',
                    // 'https://www.youtube.com/watch?v=EwTZ2xpQwpA&ab_channel=TayZonday',
                    // 'https://zerk.com.au//panel//public//uploads//1dcCLu2E1YJdJvBHKRXY2OMvBtwKOm3thVAEIxNx.mp4',
                    // 'https://assets.mixkit.co/videos/preview/mixkit-a-girl-blowing-a-bubble-gum-at-an-amusement-park-1226-large.mp4',
                  ),
                  looping: false,
                  autoplay: true,
                  // facilityID: facilityID,
                  // userid: userid,
                  facilityID: '6',
                  userid: '1',
                  // facilityAccessAd: facilityAccessAd,
                ),
                // ListView(
                //   children: [
                //
                //   ],
                // ),
              ),
            ),

            // : Container(
            //     // child: Text("asdsadasdasd"),
            //     ),
          ],
        ),
      ),
    );
  }
}
