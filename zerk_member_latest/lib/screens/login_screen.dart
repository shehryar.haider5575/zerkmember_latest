import 'dart:convert';

import 'package:Zerk/model/PushNotificationModel.dart';
import 'package:Zerk/model/model_LoginInvaildUser.dart';
import 'package:Zerk/model/model_LoginInvaildUserdetails.dart';
import 'package:Zerk/model/model_userDetails.dart';
import 'package:Zerk/model/model_userLogin.dart';
import 'package:Zerk/utils/baseurl.dart';
import 'package:Zerk/utils/checkbox_group.dart';
import 'package:fcm_config/fcm_config.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:localstorage/localstorage.dart';
import 'package:shared_preferences/shared_preferences.dart';
// import 'package:zerk_app/model/PushNotificationModel.dart';
// import 'package:zerk_app/model/model_LoginInvaildUser.dart';
// import 'package:zerk_app/model/model_LoginInvaildUserdetails.dart';
// import 'package:zerk_app/model/model_userDetails.dart';
// import 'package:zerk_app/model/model_userLogin.dart';
// import 'package:zerk_app/utils/baseurl.dart';
// import 'package:zerk_app/utils/checkbox_group.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();
  Future<UserLogin> _futureUserLogin;

  String responseUserLoginResult = "";
  //LOGIN
  // final FirebaseMessaging firebaseMessaging = FirebaseMessaging.instance;

  List<UserLoginData> tokenList;
  List<LoginInvaildUser> registerUserError;
  //Details
  List<UserDetails> userDetailsData;
  List<LoginInvaildUserdetails> loginInvaildUserdetails;
  final FirebaseMessaging firebaseMessaging = FirebaseMessaging.instance;
  bool _validate = false;
  String username, password;
  bool visible = false;
  GlobalKey<FormState> _key = new GlobalKey();
  Baseurl baseurl = Baseurl();
  final LocalStorage storage = new LocalStorage('token');
  // final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  String deviceToken = "Waiting for token...";

  setStringValuesSF(String key, String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(key, value);
    prefs.commit();
    print("getStringValuesSF: $value");
    //  prefs.setString('stringValue', "abc");
  }

  void _showErrorDialog(String error, String string) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            "$error",
            textAlign: TextAlign.center,
          ),
          content: Text(
            "$string",
            textAlign: TextAlign.center,
          ),
        );
      },
    );

    loadProgress(false);
    // nameController.clear();
    // emailController.clear();
    // contactNumberController.clear();
    // passwordController.clear();
    // confirmpasswordController.clear();
  }

  void _showDialog(String contentstring) {
    // flutter defined function
    showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          elevation: 5,

          // title: Text(
          //   "$titlestring",
          //   // "Sign Up Successfully",
          //   textAlign: TextAlign.center,
          //    style: TextStyle(
          //         color: Colors.orange[700],
          //       ),
          // ),
          content: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              // Padding(
              //   padding: const EdgeInsets.only(right: 3),
              //   child: CircleAvatar(
              //     radius: 15,
              //     backgroundColor: Colors.orange[700],
              //     child: SizedBox(
              //       child: Icon(
              //         Icons.login_rounded,
              //         //  size: 25,
              //         color: Colors.white,
              //         // color: Colors.orange[700],
              //       ),
              //     ),
              //   ),
              // ),

              Text(
                "$contentstring",
                textAlign: TextAlign.justify,
                // maxLines: 2,
                style: TextStyle(
                    color: Colors.orange[700], fontWeight: FontWeight.bold),

                // "Sign Up Successfully",
              ),
            ],
          ),
        );
      },
    );

    loadProgress(false);
//  getStringValuesSF("token");
    // nameController.clear();
    // emailController.clear();
    // contactNumberController.clear();
    // passwordController.clear();
    // confirmpasswordController.clear();
  }

  loadProgress(bool value) {
    if (visible == value) {
      setState(() {
        visible = value;
      });
    } else {
      setState(() {
        visible = value;
      });
    }

//                 Visibility(
//  maintainSize: true,
//               maintainAnimation: true,
//               maintainState: true,
//               visible: visible,

//               child: CircularProgressIndicator()
//               ),
  }

  @override
  void initState() {
    super.initState();
    print('[INFO] THIS IS LOGIN INIT');
    firebaseMessaging.getToken().then((token) => {
          if (token != null)
            {
              setState(() {
                deviceToken = token;
              })
            }
        });
    print('this is on message function');
    FirebaseMessaging.onMessage.listen((RemoteMessage remoteMessage) {
      PushNotification notification = PushNotification(
        title: remoteMessage.notification?.title,
        body: remoteMessage.notification?.body,
        dataTitle: remoteMessage.data['title'],
        dataBody: remoteMessage.data['body'],
      );
    });

    print('this is on app opened request');
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage remoteMessage) {
      PushNotification notification = PushNotification(
        title: remoteMessage.notification?.title,
        body: remoteMessage.notification?.body,
        dataTitle: remoteMessage.data['title'],
        dataBody: remoteMessage.data['body'],
      );
    });

    // firebaseMessaging.getToken().then((token) {
    //   print(token);
    //   if (token != null) {
    //     setState(() {
    //       deviceToken = token;
    //     });
    //   }
    // });
    // // print('------32452345------' + deviceToken);
    // FirebaseMessaging.onMessage.listen((RemoteMessage message) {
    //   print("message recieved");
    //   print('*******');
    //   PushNotification notification = PushNotification(
    //     title: message.notification?.title,
    //     body: message.notification?.body,
    //     dataTitle: message.data['title'],
    //     dataBody: message.data['body'],
    //   );}

    // print(notification.body);
    // print(notification.title);
    // print('*******');
    // var paredJson = jsonDecode(message.notification.body);
    // var success = NotificationVideoUrl.fromJson(paredJson);
    // // _showDialog('Thank you for becoming a member', false);
    // print(paredJson);
    // facilityID = success.facility.toString();
    // print('**********');
    // print(facilityID);
    // print('***********');
    // videoUrl = success.videoUrl;
    // print('*********');
    // print('vedio url' + videoUrl);
    // if (videoUrl != null) {
    //   Navigator.push(
    //     context,
    //     MaterialPageRoute(
    //       builder: (context) => VideoItems(
    //         userid: userId,
    //         facilityID: facilityID,
    //         videoUrl: videoUrl,
    //       ),
    //     ),
    //   );
    // } else {}
    // if (success.message == 'Access Granted Successfully') {
    //   _showNotificationWithoutSound('Access Granted Successfully');
    //   // _showNotificationWithoutSound("The access has been granted");
    // }

    // grantedprint('????????????');
    // if (Platform.isIOS) {
    //   message = _handleNotification(message);
    // }
    // _showDialog(string.toString(), true);
//          _showItemDialog(message);
//       },
    // onLaunch: (Map<String, dynamic> message) async {
    //   print("onLaunch: $message");
    //   if (Platform.isIOS) {
    //     message = _handleNotification(message);
    //   }
    //   String string = message['notification']['body'];
    //   _showDialog(string.toString(), true);
    //   // _navigateToItemDetail(message);
    //   myBackgroundMessageHandler(message);
    // },
    // onResume: (Map<String, dynamic> message) async {
    //   print("onResume: $message");
    //   if (Platform.isIOS) {
    //     message = _handleNotification(message);
    //   }
    //   String string = message['notification']['body'];
    //   _showDialog(string.toString(), true);
    //   myBackgroundMessageHandler(message);
    // },
    // );
    //   print(message.notification.body);
    // });

//     _firebaseMessaging.configure(
//       onMessage: (Map<String, dynamic> message) async {
//         print("onMessage: $message");
// //        _showItemDialog(message);
//       },
//       onLaunch: (Map<String, dynamic> message) async {
//         print("onLaunch: $message");
// //        _navigateToItemDetail(message);
//       },
//       onResume: (Map<String, dynamic> message) async {
//         print("onResume: $message");
// //        _navigateToItemDetail(message);
//       },
//     );
//     _firebaseMessaging.requestNotificationPermissions(
//         const IosNotificationSettings(
//             sound: true, badge: true, alert: true, provisional: false));
//     _firebaseMessaging.onIosSettingsRegistered
//         .listen((IosNotificationSettings settings) {
//       print("Settings registered: $settings");
//     });
//     _firebaseMessaging.getToken().then((String token) {
//       assert(token != null);
//       setState(() {
//         deviceToken = token;
//       });
//       print(deviceToken);
//     });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: WillPopScope(child: getBody(), onWillPop: onWillPop),
    );
  }

  getBody() {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        image: DecorationImage(
            image: AssetImage('assets/images/login_bg.png'),
            fit: BoxFit.cover,
            alignment: Alignment.topLeft
            //fit: BoxFit.contain
            //   alignment: Alignment.center
            ),
      ),
      child: SingleChildScrollView(
        child: Form(
          key: _key,
          autovalidate: _validate,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              //   Spacer(
              //     flex: 20,
              //   ),
              SizedBox(
                height: 50,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 25.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: 60,
                          width: 60,
                          child: Image.asset('assets/images/sign_up_icon.png'),
                        ),
                      ],
                    ),
                  ),
                  Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 35, left: 20, right: 0, bottom: 0),
                        child: Text(
                          "LOGIN",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 42.0,
                              fontWeight: FontWeight.bold,
                              fontFamily: "Poppins-Bold",
                              letterSpacing: 4),
                        ),
                      ),
                    ],
                  ),
                ],
              ),

              Padding(
                padding: EdgeInsets.only(left: 25.0, right: 25.0),
                child: Column(
                  children: [
                    TextFormField(
                      controller: usernameController,
                      validator: validateEmail,
                      keyboardType: TextInputType.text,
                      autocorrect: true,
                      onSaved: (String val) {
                        username = val;
                      },
                      style: TextStyle(color: Colors.white),
                      decoration: InputDecoration(
                          hintText: "Enter Email",
                          contentPadding: const EdgeInsets.all(10.0),

                          // prefixIcon: SizedBox(
                          //   height: 30,
                          //   width: 30,
                          //   child: Image.asset('assets/images/dob_icon.png'),
                          // ),
                          hintStyle:
                              TextStyle(color: Colors.white, fontSize: 15.0)),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 8.0),
                    ),
                    TextFormField(
                      controller: passwordController,
                      // validator: validatePassword,
                      cursorColor: Colors.white,
                      keyboardType: TextInputType.visiblePassword,
                      autocorrect: true,
                      obscureText: true,
                      onSaved: (String val) {
                        password = val;
                      },
                      style: TextStyle(color: Colors.white),
                      decoration: InputDecoration(
                          hintText: "Password",
                          contentPadding: const EdgeInsets.all(10.0),
                          // prefixIcon: SizedBox(
                          //   height: 30,
                          //   width: 30,
                          //   child: Image.asset('assets/images/password_icon.png'),
                          // ),
                          hintStyle:
                              TextStyle(color: Colors.white, fontSize: 15.0)),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: [
                        InkWell(
                          splashColor: Colors.orange[700],
                          highlightColor: Colors.orange[700],
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Text(
                              "Forgot Password?",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: "Poppins-Bold",
                                  letterSpacing: 2),
                            ),
                          ),
                          onTap: () {
                            Navigator.pushNamed(
                                context, '/ForgotPasswordScreen');
                          },
                        ),
                      ],
                    ),
                  ],
                ),
              ),

              Center(
                child: Visibility(
                    //  maintainSize: true,
                    //               maintainAnimation: true,
                    //               maintainState: true,
                    visible: visible,
                    child: CircularProgressIndicator(
                      color: Colors.white,
                    )),
              ),

              // Spacer(
              //   flex: 1,
              // ),
              SizedBox(
                height: 40,
              ),
              //SIGN UP BUTTON
              Padding(
                padding: const EdgeInsets.only(left: 30, top: 30),
                child: Container(
                  //   color: Colors.black,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      SizedBox(
                        width: 220.0,
                        height: 60.0,
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(42.0),
                              side: BorderSide(color: Colors.white)),
                          onPressed: () {
                            //     if (passwordController.text !=
                            //     confirmpasswordController.text) {
                            //   print(passwordController.text +
                            //       " NotMatch " +
                            //       confirmpasswordController.text);
                            // } else {
                            //   print(passwordController.text +
                            //       "Match" +
                            //       confirmpasswordController.text);
                            //   //   _sendToServer();
                            // }
                            _sendToServer();
                          },
                          color: Colors.white,
                          textColor: Colors.orange,
                          child: Text("LOGIN".toUpperCase(),
                              style: TextStyle(
                                fontSize: 25.0,
                                letterSpacing: 2,
                                fontWeight: FontWeight.bold,
                              )),
                        ),
                      ),
                    ],
                  ),
                ),
              ),

              Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: Container(
                  // color: Colors.red,
                  //   width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height / 3.5,

                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            SizedBox(
                              height: 50,
                            ),
                            Container(
                              // alignment: Alignment.bottomLeft,
                              width: MediaQuery.of(context).size.width,
                              height: MediaQuery.of(context).size.height / 9,
                              // color: Colors.pink,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  CheckboxGroup(
                                      activeColor: Colors.orange,
                                      checkColor: Colors.white,
                                      labelStyle:
                                          TextStyle(color: Colors.black54),
                                      labels: <String>[
                                        "Keep Me Logged In",
                                      ],
                                      onChange: (bool isChecked, String label,
                                          int index) {
                                        print(
                                            "isChecked: $isChecked   label: $label  index: $index");
                                      },
                                      onSelected: (List<String> checked) {
                                        //   _showDialog();
                                        print("checked: ${checked.toString()}");
                                      }),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 25, top: 0, bottom: 10),
                              child: Container(
                                //       width: MediaQuery.of(context).size.width,
                                // height: MediaQuery.of(context).size.height*.5,
                                //   color: Colors.grey,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    Text(
                                      "Don't Have An Account? ",
                                      style: TextStyle(
                                        color: Colors.black54,
                                        fontFamily: "Poppins-Medium",
                                        fontSize: 14,
                                        fontWeight: FontWeight.normal,
                                      ),
                                    ),
                                    SizedBox(
                                      width: 24,
                                    ),
                                    InkWell(
                                      onTap: () {
                                        Navigator.popAndPushNamed(
                                            context, '/SignUpScreen');
                                        //   Navigator.push(
                                        //     context,
                                        //     MaterialPageRoute(
                                        //         fullscreenDialog: true,
                                        //         builder: (context) =>
                                        //             SignUpScreen()),
                                        //   );
                                      },
                                      child: Container(
                                        decoration: new BoxDecoration(
                                          color: Colors.white,
                                          boxShadow: [
                                            BoxShadow(
                                                color: Colors.black12,
                                                offset: Offset(0, 0),
                                                blurRadius: 10,
                                                spreadRadius: 3)
                                          ],
                                          borderRadius:
                                              BorderRadius.circular(40),
                                        ),
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              15, 4, 15, 4),
                                          child: Center(
                                            child: Text(
                                                " Sign Up".toUpperCase(),
                                                style: TextStyle(
                                                    color: Colors.orange,
                                                    fontSize: 18,
                                                    fontWeight: FontWeight.bold,
                                                    fontFamily:
                                                        "Poppins-Bold")),
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<bool> onWillPop() {
    return SystemNavigator.pop();
  }

  //------userLoginApi------------------------------------
  Future<UserLogin> userLoginApi(
    String email,
    String password,
    String deviceToken,
  ) async {
    print('this is login function');
    final http.Response response = await http.post(
      Uri.parse(baseurl.baseurl + "user/login"),
      headers: <String, String>{
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<dynamic, dynamic>{
        'email': email,
        'password': password,
        'device_token': deviceToken,
      }),
    );
    print('this is email $email');
    print('this is password $password');
    print('this is token $deviceToken');

    print('this is login response ${response.body}');
    if (response.statusCode == 200) {
      print("-------------------------");
      print(json.decode(response.body));

      var parsedJson = json.decode(response.body);
      var success = UserLogin.fromJson(parsedJson);

      tokenList = List<UserLoginData>();
      tokenList.add(UserLoginData(token: success.data.token));
      storage.setItem("password", passwordController.text.toString());
      setStringValuesSF("token", tokenList[0].token.toString());
      storage.setItem("token", tokenList[0].token.toString());
      //   print("================HIT_userDetailsDataApi==================");

      print("/////================tokenList==================");
      print(storage.getItem('token'));
      //    storage.setItem("token", tokenList[0].token.toString());
      //   print(tokenList[0].token.toString());

      //   userDetailsDataApi(tokenList[0].token.toString());
      _showDialog('Login Successfully');
      userDetailsDataApi(storage.getItem('token'));
      Navigator.popAndPushNamed(context, '/BottomNavBar');
      // Navigator.pushNamed(context, MaterialPageRoute(builder: (context) =>BottomNavBar() ));
      // print(storage.getItem("token"));

      return UserLogin.fromJson(json.decode(response.body));
    } else {
      registerUserError = List<LoginInvaildUser>();
      var parsedJson = json.decode(response.body);
      var error = LoginInvaildUser.fromJson(parsedJson);

      registerUserError
          .add(LoginInvaildUser(data: error.data, message: error.message));

      String message = json.decode(response.body)['message'] ?? '';
      String data = json.decode(response.body)['data'] ?? '';

      _showErrorDialog(message, data);
      print(registerUserError[0].data.toString());
// //   print(registerUserError[0].token.toString());
//      _showErrorDialog(registerUserError[0].message.toString(),
//          registerUserError[0].data.toString());
      //    _showErrorDialog("The email has already been taken.");
      //   print(response.body.toString());
      throw Exception('Failed to Register the User');
    }
  }

  //-------------UserDetailsDataAPI--------------------------------
  Future<UserDetailsData> userDetailsDataApi(String token) async {
    final response = await http.get(
      Uri.parse(baseurl.baseurl + "user/details"),
      headers: <String, String>{
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + token,
      },
    );

    if (response.statusCode == 200) {
      print(json.decode(response.body));
      userDetailsData = List<UserDetails>();

      // setStringValuesSF("UserDetailsData", response.body.toString());

      storage.setItem("UserDetailsData", storage.getItem('token'));

      var parsedJson = json.decode(response.body);
      var success = UserDetails.fromJson(parsedJson);

      userDetailsData = List<UserDetails>();
      userDetailsData.add(UserDetails(data: success.data));
      print('||||||||' + userDetailsData[0].data.id.toString());
      print('||||||||' + userDetailsData[0].data.firstName.toString());
      print('||||||||' + userDetailsData[0].data.lastName.toString());
      print('||||||||' + userDetailsData[0].data.email.toString());
      print('||||||||' + userDetailsData[0].data.avatar.toString());

      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      print('**************************');
      print('**************************');
      print('**************************');
      print('**************************');
      print(userDetailsData[0].data.id.toString());
      print('**************************');
      print('**************************');
      sharedPreferences.setString('id', userDetailsData[0].data.id.toString());
      print(sharedPreferences.getString('id'));

      print('||||||||' + success.data.id.toString());
      print('||||||||' + success.data.firstName.toString());
      print('||||||||' + success.data.lastName.toString());
      print('||||||||' + success.data.email.toString());
      print('||||||||' + success.data.avatar.toString());
      print('vvvvvvvvvv' + success.data.membershipId.toString());
      print('vvvvvvvvvv' + success.data.membershipId.toString());
      print('vvvvvvvvvv' + success.data.membershipId.toString());

      print('THIS IS LOCAL STORAGE DATA **********');

      storage.setItem('userid', success.data.id.toString());
      storage.setItem('stripeCustomerId', success.data.stripeCustomerId);
      storage.setItem('firstName', success.data.firstName.toString());
      storage.setItem('lastName', success.data.lastName.toString());
      storage.setItem('email', success.data.email.toString());
      storage.setItem('avatar', success.data.avatar.toString());
      storage.setItem('userMembershipId', success.data.membershipId.toString());

      print("asdsadjadasdkasdj");
      print(success.data.totalAvails.toString());
      if (success.data.totalAvails != null) {
        print("isNOTequaltoNULL");
        storage.setItem('totalAvails', success.data.totalAvails.toString());
      } else {
        storage.setItem('totalAvails', "0");
        print("isEqualtoNULL");
      }

      setStringValuesSF('userid', success.data.id.toString());
      setStringValuesSF('firstName', success.data.firstName.toString());
      setStringValuesSF('lastName', success.data.lastName.toString());
      setStringValuesSF('email', success.data.avatar.toString());
      setStringValuesSF('avatar', success.data.avatar.toString());

      print(storage.getItem("avatar").toString() +
          storage.getItem("email").toString());
      print("-------///////////////////////////////////////////-------ID:" +
          userDetailsData[0].data.id.toString() +
          success.data.id.toString());

      return UserDetailsData.fromJson(json.decode(response.body));
    } else {
      print(response.statusCode.toString());
      print(response.body.toString());
      String message = json.decode(response.body)['message'] ?? '';
      String data = json.decode(response.body)['data'] ?? '';
      _showDialog(message);
      if (message == 'The access token is invalid.') {
        Navigator.popAndPushNamed(context, '/LoginScreen');
      } else {
        Navigator.popAndPushNamed(context, '/BottomNavBar');
      }

      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load album');
    }
  }

  _sendToServer() {
    print('this is send to server method');
    if (_key.currentState.validate()) {
      // No any error in validation
      _key.currentState.save();
      userLoginApi(
          usernameController.text, passwordController.text, deviceToken);
      loadProgress(true);
      print("Name $username");
      print("Password $password");
      print("LoginPush Messaging token: " + deviceToken);
      // firebaseMessaging.getToken().then((token) {
      //   print('32452345' + token);
      //   deviceToken = token;
      // });
      print('------32452345------' + deviceToken);
    } else {
      // validation error
      setState(() {
        _validate = true;
      });
    }
  }

  //validatePassword
  String validatePassword(String value) {
    Pattern pattern =
        r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
    RegExp regex = new RegExp(pattern);
    print(value);
    if (value.isEmpty) {
      return 'Please enter password';
    } else {
      if (!regex.hasMatch(value))
        return 'Enter valid password';
      else
        return null;
    }
  }

//validateEmail
  String validateEmail(String value) {
    String pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = new RegExp(pattern);
    if (value.length == 0) {
      return "Email is Required";
    } else if (!regExp.hasMatch(value)) {
      return "Invalid Email";
    } else {
      return null;
    }
  }
}
