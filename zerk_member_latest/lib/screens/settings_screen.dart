import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:Zerk/model/model_CancelTransactions.dart';
import 'package:Zerk/model/model_CreateCustomersCard.dart';
import 'package:Zerk/model/model_InvaildCreateCardToken.dart';
import 'package:Zerk/model/model_InvaildTransactionStore.dart';
import 'package:Zerk/model/model_InvaildUserProfileUpdated.dart';
import 'package:Zerk/model/model_InvalidCreateCustomersCard.dart';
import 'package:Zerk/model/model_TransactionStore.dart';
import 'package:Zerk/model/model_createCardTokenModel.dart';
import 'package:Zerk/model/model_stripeAPIChargeModel.dart';
import 'package:Zerk/model/model_userDetails.dart';
import 'package:Zerk/model/model_userProfileUpdated.dart';
import 'package:Zerk/utils/baseurl.dart';
// import 'package:Zerk/services/payment-service.dart';

import 'package:Zerk/utils/my_strings.dart';
import 'package:Zerk/utils/payment_card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';
import 'package:http/http.dart' as http;
import 'package:localstorage/localstorage.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

import 'bottom_menu.dart';

class Settings extends StatefulWidget {
//  Settings({Key key}) : super(key: key);

  Settings({
    Key key,
    @required this.userId,
    this.membershipId,
    this.amount,
    this.isVisible = true,
    this.membershipName,
  }) : super(key: key);

  String userId = "";
  String membershipId = "";
  double amount;
  final bool isVisible;
  String membershipName = "";

  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
  final ScrollController _scrollController = ScrollController();
  final cardnumberController = TextEditingController();
  final expirydateController = TextEditingController();
  var _paymentCard = PaymentCard();
  var _formKey = new GlobalKey<FormState>();
  var _autoValidate = false;
  var _scaffoldKey = new GlobalKey<ScaffoldState>();
  static String secret =
      'sk_test_51HbQCpJrvceP60phuzzvHaxtXprQv8DRJpvS97ie0Qx7CYhySy2rvdMYy8bTFlnLlH6bA7PfBGwrq1g71jbsSPbu00PNg2wv0Z';
  List<CreateCardTokenModel> stripeAPIChargeList;
  bool circularProgressIndicatorVisible = false;

  Baseurl baseurl = Baseurl();
  final cvcController = TextEditingController();
  final LocalStorage storage = new LocalStorage('token');

  // final ScrollController _scrollController = ScrollController();
  static const int sortName = 0;
  static const int sortStatus = 1;
  bool isAscending = true;
  int sortType = sortName;
  Future<CancelTransactions> futureCancelTransactions;
  String userid = "", membershipid = "", status = "";
  String amount,
      token,
      sourceCardId,
      stripeCustomerID,
      cancelSubscription,
      member;
  int stripeAmount, mystripeAmount;
  int zero = 0000;
  List<InvaildTransactionStore> invaildTransactionStoreList;

  List<UserDetails> userDetailsData;

  String stripeNewCustomerId;
  final GlobalKey expansionTileKey = GlobalKey();

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url, forceWebView: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  loadProgress(bool value) {
    if (circularProgressIndicatorVisible == value) {
      setState(() {
        circularProgressIndicatorVisible = value;
      });
    } else {
      setState(() {
        circularProgressIndicatorVisible = value;
      });
    }
  }

  void _scrollToSelectedContent({GlobalKey expansionTileKey}) {
    final keyContext = expansionTileKey.currentContext;
    if (keyContext != null) {
      Future.delayed(Duration(milliseconds: 200)).then((value) {
        Scrollable.ensureVisible(keyContext,
            duration: Duration(milliseconds: 200));
      });
    }
  }

  void _showDialog(String contentstring, bool value) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        if (value == false) {
          print(value);
          Future.delayed(Duration(seconds: 2), () {
            Navigator.of(context).pop(true);
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (context) => BottomNavBar()),
                ModalRoute.withName("/BottomNavBar"));
          });
        } else {}

//            Center()

//        Future.delayed(Duration(seconds: 1), () {
//                Navigator.of(context).pop(true);
//              });
        // return object of type Dialog
        return AlertDialog(
          elevation: 5,
          content: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Text(
                  "$contentstring",
                  textAlign: TextAlign.left,
                  maxLines: 4,
                  style: TextStyle(
                      color: Colors.orange[700], fontWeight: FontWeight.bold),
                ),
              )
            ],
          ),
        );
      },
    );
  }

  onItemPress(BuildContext context, int index) async {
    switch (index) {
      case 0:
        payViaNewCard(context);
        break;
      // case 1:
      //   Navigator.pushNamed(context, '/existing-cards');
      //   break;
    }
  }

  payViaNewCard(BuildContext context) async {
    ProgressDialog dialog = new ProgressDialog(context);
    dialog.style(message: 'Please wait...');
    await dialog.show();
    // var response = await StripeService.payWithNewCard(
    //     amount: storage.getItem("amount"), currency: 'USD');
    await dialog.hide();
    print("--------------------body------------------------------");
    // print(response.paymentMethodId);
    // print(response.paymentIntentId);
    print(storage.getItem("userid").toString());
    print(storage.getItem("membershipid").toString());
    print(storage.getItem("amount").toString());
    // print(userid);
    // print(membershipid);
    // print(amount);
    print(status);
  }

  //Login User Details

  Future<UserDetailsData> fetchCustomerStripeId() async {
    print("---------------------NEW CUSTOMER ID_card_id---------------------");
    String userID = storage.getItem('userid').toString();
    print(
        "asdjhflasdjhfklasdjfhaslk dfas dkfakjhdf alfadsjhf lkasdf jla dfasdf$userID");
    final response = await http.get(
      // baseurl.baseurl + "user/member-details/4",
      Uri.parse(
          "https://zerk.com.au/panel/public/api/user/member-details/$userID"),
      headers: <String, String>{
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );

    if (response.statusCode == 200) {
      // if (response != null) {
      print("234218347ehfwefhapoifhapf");

      print(json.decode(response.body));
      userDetailsData = List<UserDetails>();
      var parsedJson = json.decode(response.body);
      var success = UserDetails.fromJson(parsedJson);
      print(
          "---------------------NEW CUSTOMER ID_card_id---------------------");
      print(success.data.stripeCustomerId);
      stripeNewCustomerId = success.data.stripeCustomerId;
      // setStringValuesSF("UserDetailsData", response.body.toString());
    }
  }

  //End Login Detai Method
  @override
  void initState() {
    super.initState();
    fetchCustomerStripeId();
    // StripeService.init();
    print("source_card_id---------------------");
    sourceCardId = storage.getItem('source_card_id').toString();
    print(sourceCardId);
    if (storage.getItem("amount") != null) {
      amount = storage.getItem("amount").toString();

      // stripeAmount = widget.amount.toInt();
      // if (widget.amount == null) {
      //   stripeAmount = 0;
      // } else {
      //   stripeAmount = widget.amount.toInt();
      // }

      if (widget.amount != null) {
        stripeAmount = widget.amount.toInt();
      } else {
        if (widget.amount != null) {
          stripeAmount = widget.amount.toInt();
        }
      }

      // stripeAmount = widget.amount.toString();

      // mystripeAmount = stripeAmount + zero.toInt();
      // int myamount = widget.amount.truncate();
      print("if" + amount.toString());

      print("mmmmmmmmmmmm" + stripeAmount.toString());
      // print("nnnnnnnnnnnn" + mystripeAmount.toString());
      // print("nnnnnnnnnnnn" + zero.toString());
    } else {
      // amount = '00000';
      print("else" + amount.toString());
    }

    token = storage.getItem('token');

    if (storage.getItem("stripeCustomerID") != null) {
      stripeCustomerID = storage.getItem("stripeCustomerID");
    } else {
      stripeCustomerID = '';
    }

    // member = storage.getItem('member');
    checkMemberSubscription();
    // setState(() {
    //   member = storage.getItem('member');
    //   cancelSubscription = storage.getItem('cancelSubscription');
    // });

    // print('member' + member);
    // print('cancelSubscription' + cancelSubscription);

    _paymentCard.type = CardType.Others;
    cardnumberController.addListener(_getCardTypeFrmNumber);

    var initializationSettingsAndroid =
        new AndroidInitializationSettings('zerk.png');
    var initializationSettingsIOS = new IOSInitializationSettings();
    var initializationSettings = new InitializationSettings(
        android: initializationSettingsAndroid, iOS: initializationSettingsIOS);
    flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);

    userid = storage.getItem("userid").toString();

    // futureCancelTransactions = fetchCancelTransactionsLogs(userid);
  }

  checkMemberSubscription() {
    setState(() {
      member = storage.getItem('member');
      cancelSubscription = storage.getItem('cancelSubscription');
    });
  }

  Future onSelectNotification(String payload) async {
    showDialog(
      context: context,
      builder: (_) {
        return new AlertDialog(
          title: Text("ZERK"),
          content: Text("$payload"),
        );
      },
    );
  }

  Future _showNotificationWithoutSound(String string) async {
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        'your channel id', 'your channel name', 'your channel description',
        playSound: false, importance: Importance.max, priority: Priority.high);
    var iOSPlatformChannelSpecifics =
        new IOSNotificationDetails(presentSound: false);
    var platformChannelSpecifics = new NotificationDetails(
        android: androidPlatformChannelSpecifics,
        iOS: iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
      0,
      'ZERK',
      '$string',
      platformChannelSpecifics,
      payload: '$string',
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        // child: SingleChildScrollView(
        //
        // ),
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/images/sign_up_bg.png'),
              fit: BoxFit.cover,
            ),
          ),
          child: Container(
            child: Stack(
              fit: StackFit.expand,
              children: [
// Settings Title
                Positioned(
                  top: Alignment.topCenter.x,
                  child: Container(
                    // color: Colors.orange,
                    height: MediaQuery.of(context).size.height * .22,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width / 6,
                          // color: Colors.red,
                          child: InkWell(
                            onTap: () {
                              Navigator.pop(context, '/BottomNavBar');
                              // Navigator.pop(
                              //     context,
                              //     MaterialPageRoute(
                              //         fullscreenDialog: true,
                              //         builder: (context) => BottomNavBar()),
                              //   );
                            },
                            child: SizedBox(
                              child: Icon(
                                Icons.arrow_back,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width / 1.5,
                          //   color: Colors.black,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                "Settings",
                                style: TextStyle(
                                  fontSize: 22.0,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),

// Settings Card Widget
                Positioned(
                  // top: 110,
                  top: MediaQuery.of(context).size.height * 0.15,
                  // top: Alignment.center.x,
                  // child: SingleChildScrollView(
                  //
                  // ),
                  child: Container(
                    //  alignment: Alignment.center,
                    width: MediaQuery.of(context).size.width,
                    // color: Colors.amber,
                    // height: MediaQuery.of(context).size.height * 12,
                    height: MediaQuery.of(context).size.height * 0.7,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10.0),
                      // child: SingleChildScrollView(
                      //
                      // ),
                      child: Card(
                        color: Colors.white,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20),
                        ),
                        // elevation: 5,
                        // shadowColor: Colors.grey,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 30.0),
                          // child: Expanded(
                          child: Container(
                            // height: 200,
                            child: ListView(
                              scrollDirection: Axis.vertical,
                              shrinkWrap: true,
                              children: <Widget>[
                                // ListTile(title: Text('Item 1')),
                                // ListTile(title: Text('Item 2')),
                                // ListTile(title: Text('Item 3')),
                                // ListTile(title: Text('Item 1')),
                                // ListTile(title: Text('Item 2')),
                                // ListTile(title: Text('Item 3')),
                                // ListTile(title: Text('Item 1')),
                                // ListTile(title: Text('Item 2')),
                                // ListTile(title: Text('Item 3')),
                                // ListTile(title: Text('Item 3')),
                                // ListTile(title: Text('Item 1')),
                                // ListTile(title: Text('Item 2')),
                                // ListTile(title: Text('Item 3')),
                                // ListTile(title: Text('Item 1')),
                                // ListTile(title: Text('Item 2')),
                                // ListTile(title: Text('Item 3')),
                                // ListTile(title: Text('Item 1')),
                                // ListTile(title: Text('Item 2')),
                                // ListTile(title: Text('Item 3')),
                                // ListTile(title: Text('Item 1')),
                                // ListTile(title: Text('Item 2')),
                                // ListTile(title: Text('Item 3')),
                                // ListTile(title: Text('Item 1')),
                                // ListTile(title: Text('Item 2')),
                                // ListTile(title: Text('Item 3')),
                                // ListTile(title: Text('Item 1')),
                                // ListTile(title: Text('Item 2')),
                                // ListTile(title: Text('Itejkkjhjkm 3')),
                                SizedBox(height: 20.0),
                                Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 15, vertical: 15),
                                  child: InkWell(
                                    onTap: () {
                                      _launchURL(
                                          "https://zerk.com.au/panel/public/terms-and-condition");
                                    },
                                    child: Text(
                                      "Terms and conditions",
                                      style: TextStyle(
                                          fontSize: 18.0,
                                          fontWeight: FontWeight.normal),
                                    ),
                                  ),
                                ),
                                // Divider(),
                                // ExpansionTile(
                                //   title: InkWell(
                                //     splashColor: Colors.orange[700],
                                //     highlightColor: Colors.orange[700],
                                //     child: Text(
                                //       "Terms and conditions",
                                //       style: TextStyle(
                                //           fontSize: 18.0,
                                //           fontWeight: FontWeight.normal),
                                //     ),
                                //     onTap: () {
                                //       _launchURL(
                                //           "https://zerk.com.au/panel/public/terms-and-condition");
                                //     },
                                //   ),
                                // ),
                                SizedBox(height: 10.0),
                                // Padding(
                                //   padding: EdgeInsets.only(left: 20.0),
                                //   child:
                                //
                                // ),
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 15, vertical: 10),
                                  child: InkWell(
                                    onTap: () {
                                      _launchURL(
                                          "https://zerk.com.au/panel/public/privacy-policy");
                                    },
                                    child: Text(
                                      "Privacy policy",
                                      style: TextStyle(
                                          fontSize: 18.0,
                                          fontWeight: FontWeight.normal),
                                    ),
                                  ),
                                ),

                                SizedBox(height: 5.0),
                                ExpansionTile(
                                  key: expansionTileKey,
                                  onExpansionChanged: (value) {
                                    if (value) {
                                      _scrollToSelectedContent(
                                          expansionTileKey: expansionTileKey);
                                    }
                                  },
                                  initiallyExpanded: true,
                                  title: Text(
                                    "Credit Card",
                                    style: TextStyle(
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.normal),
                                  ),
                                  children: <Widget>[
                                    Container(
                                      margin: const EdgeInsets.only(
                                          top: 10.0, bottom: 10.0),
                                      child: Form(
                                        key: _formKey,
                                        autovalidate: _autoValidate,
                                        child: Column(
                                          children: [
                                            Row(
                                              children: [
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          left: 10.0),
                                                  child: Text('Card Number'),
                                                ),
                                              ],
                                            ),
                                            Container(
                                              margin: const EdgeInsets.only(
                                                  top: 20.0, bottom: 20.0),
                                              child: TextFormField(
                                                keyboardType:
                                                    TextInputType.number,
                                                maxLength: 16,
                                                // inputFormatters: [
                                                //   WhitelistingTextInputFormatter
                                                //       .digitsOnly,
                                                //   new LengthLimitingTextInputFormatter(
                                                //       16),
                                                //   new CardNumberInputFormatter()
                                                // ],
                                                controller:
                                                    cardnumberController,
                                                decoration: new InputDecoration(
                                                  border:
                                                      const UnderlineInputBorder(),
//                                                  filled: true,
//                                                  icon: CardUtils.getCardIcon(
//                                                      _paymentCard.type),
                                                  hintText:
                                                      'What number is written on card?',
                                                  labelText: 'Number',
                                                ),
                                                onSaved: (String value) {
                                                  print('onSaved = $value');
                                                  print(
                                                      'Num controller has = ${cardnumberController.text}');
                                                  _paymentCard.number =
                                                      CardUtils
                                                          .getCleanedNumber(
                                                              value);
                                                },
                                                validator:
                                                    CardUtils.validateCardNum,
                                              ),
                                            ),
                                            Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Text(
                                                  'Expiry Date',
                                                  style: TextStyle(
                                                    fontSize: 13.0,
                                                    fontWeight:
                                                        FontWeight.normal,
                                                  ),
                                                ),
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          right: 20.0),
                                                  child: Text(
                                                    'Verification Value',
                                                    style: TextStyle(
                                                      fontSize: 13.0,
                                                      fontWeight:
                                                          FontWeight.normal,
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Container(
                                                  margin: const EdgeInsets.only(
                                                      top: 20.0, bottom: 20.0),
                                                  //   color: Colors.red,
                                                  height: MediaQuery.of(context)
                                                          .size
                                                          .height *
                                                      .07,
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      .3,
                                                  child: TextFormField(
                                                    controller:
                                                        expirydateController,
                                                    maxLength: 4,
//                                                    inputFormatters: [
//                                                      WhitelistingTextInputFormatter
//                                                          .digitsOnly,
//                                                      new LengthLimitingTextInputFormatter(
//                                                          4),
//                                                      new CardMonthInputFormatter()
//                                                    ],
                                                    decoration:
                                                        new InputDecoration(
//                                                     labelText: "MM/YY",
//                                                 hintText: 'MM/YY',
//                                                 labelText: 'Expiry Date',
                                                      fillColor: Colors.white,
                                                      hintText: 'MM/YY',
//                                                     border:
//                                                         new OutlineInputBorder(
//                                                       borderRadius:
//                                                           new BorderRadius
//                                                               .circular(10.0),
//                                                       borderSide:
//                                                           new BorderSide(),
//                                                     ),
                                                      //fillColor: Colors.green
                                                    ),
                                                    // validator: CardUtils
                                                    //     .validateDate,
                                                    keyboardType:
                                                        TextInputType.number,
                                                    validator: (val) {
                                                      if (val.length == 0) {
                                                        return "Expiry month is invalid";
                                                      } else {
                                                        return null;
                                                      }
                                                    },
//                                                   onSaved: (value) {
//                                                     List<int> expiryDate = CardUtils.getExpiryDate(value);
//                                                     _paymentCard.month = expiryDate[0];
//                                                     _paymentCard.year = expiryDate[1];
//                                                   },

                                                    style: new TextStyle(
                                                      fontFamily: "Poppins",
                                                    ),
                                                  ),

//                                                  child:   TextFormField(
//                                                    inputFormatters: [
//                                                      WhitelistingTextInputFormatter.digitsOnly,
//                                                      new LengthLimitingTextInputFormatter(4),
//                                                      new CardMonthInputFormatter()
//                                                    ],
//                                                    decoration: new InputDecoration(
//                                                      border: const UnderlineInputBorder(),
////                                                      filled: true,
//

////                                                      labelText: 'Expiry Date',
//                                                    ),
//                                                    validator: CardUtils.validateDate,
//                                                    keyboardType: TextInputType.number,
//                                                    onSaved: (value) {
//                                                      List<int> expiryDate = CardUtils.getExpiryDate(value);
//                                                      _paymentCard.month = expiryDate[0];
//                                                      _paymentCard.year = expiryDate[1];
//                                                    },
//                                                  ),
                                                ),
                                                Container(
                                                  margin: const EdgeInsets.only(
                                                      top: 20.0, bottom: 20.0),

                                                  // color: Colors.white,
                                                  height: MediaQuery.of(context)
                                                          .size
                                                          .height *
                                                      .07,
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      .3,

                                                  child: TextFormField(
                                                    onChanged: (value) {
                                                      detectDelayText();
                                                    },
                                                    controller: cvcController,
                                                    maxLength: 3,
//                                                    inputFormatters: [
//                                                      WhitelistingTextInputFormatter
//                                                          .digitsOnly,
//                                                      new LengthLimitingTextInputFormatter(
//                                                          4),
//                                                      new CardMonthInputFormatter()
//                                                    ],
                                                    decoration:
                                                        new InputDecoration(
//                                                     labelText: "MM/YY",
//                                                 hintText: 'MM/YY',
//                                                 labelText: 'Expiry Date',
                                                      fillColor: Colors.white,
                                                      hintText: 'CVV',
//                                                     border:
//                                                         new OutlineInputBorder(
//                                                       borderRadius:
//                                                           new BorderRadius
//                                                               .circular(10.0),
//                                                       borderSide:
//                                                           new BorderSide(),
//                                                     ),
                                                      //fillColor: Colors.green
                                                    ),
//                                                 validator: CardUtils.validateDate,
                                                    keyboardType:
                                                        TextInputType.number,
                                                    validator: (val) {
                                                      if (val.length == 0) {
                                                        return "CVC is invalid";
                                                      } else {
                                                        return null;
                                                      }
                                                    },
//                                                   onSaved: (value) {
//                                                     List<int> expiryDate = CardUtils.getExpiryDate(value);
//                                                     _paymentCard.month = expiryDate[0];
//                                                     _paymentCard.year = expiryDate[1];
//                                                   },

                                                    style: new TextStyle(
                                                      fontFamily: "Poppins",
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.end,
                                              children: [
                                                _getPayButton(
                                                    member, cancelSubscription),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 10.0),
                                ExpansionTile(
                                  title: Text(
                                    "Subscriptions",
                                    style: TextStyle(
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.normal),
                                  ),
                                  children: <Widget>[
                                    Container(
                                      margin: const EdgeInsets.only(
                                          left: 0.0, top: 0.0, bottom: 0.0),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.all(3.0),
                                            child: Text(
                                              "The recommended payment\ngateway for this platform is Stripe",
                                              style: TextStyle(
                                                  fontSize: 15.0,
                                                  decoration:
                                                      TextDecoration.none,
                                                  color: Colors.grey),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.all(3.0),
                                            child: GestureDetector(
                                                child:
                                                    // child: WebView(
                                                    //   initialUrl:
                                                    //       'https://stripe.com/au',
                                                    //   javascriptMode: JavascriptMode
                                                    //       .unrestricted,
                                                    // ),
                                                    Text(
                                                        "https://stripe.com/au",
                                                        style: TextStyle(
                                                            fontSize: 13.0,
                                                            decoration:
                                                                TextDecoration
                                                                    .none,
                                                            color:
                                                                Colors.orange)),
                                                onTap: () {
                                                  _launchURL(
                                                      "https://stripe.com/au");
                                                  // do what you need to do when "Click here" gets clicked
                                                }),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                top: 10.0),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.all(3.0),
                                            child: Text("Refer to STRIPE fees",
                                                style: TextStyle(
                                                    fontSize: 15.0,
                                                    decoration:
                                                        TextDecoration.none,
                                                    color: Colors.grey)),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.all(3.0),
                                            child: GestureDetector(
                                                child: Text(
                                                  "https://stripe.com/au/pricing",
                                                  style: TextStyle(
                                                      fontSize: 13.0,
                                                      decoration:
                                                          TextDecoration.none,
                                                      color: Colors.orange),
                                                ),
                                                onTap: () {
                                                  _launchURL(
                                                      "https://stripe.com/au/pricing");
                                                  // do what you need to do when "Click here" gets clicked
                                                }),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                top: 10.0),
                                          ),
                                          // ******************* Past Subscriptions *************
                                          // Row(
                                          //   mainAxisAlignment:
                                          //       MainAxisAlignment.spaceEvenly,
                                          //   crossAxisAlignment:
                                          //       CrossAxisAlignment.center,
                                          //   children: [
                                          //     AutoSizeText(
                                          //         "Past\nSubscriptions",
                                          //         style: TextStyle(
                                          //             fontSize: 15.0,
                                          //             decoration:
                                          //                 TextDecoration.none,
                                          //             color: Colors.grey)),
                                          //     Icon(
                                          //       Icons.arrow_forward_ios,
                                          //       color: Colors.orange,
                                          //       size: 24.0,
                                          //     ),
                                          //     Container(
                                          //       // width: 100,
                                          //       width: MediaQuery.of(context)
                                          //               .size
                                          //               .width *
                                          //           .3,
                                          //       // color: Colors.black,
                                          //       child: Column(
                                          //         crossAxisAlignment:
                                          //             CrossAxisAlignment.end,
                                          //         children: [
                                          //           Card(
                                          //             shape:
                                          //                 RoundedRectangleBorder(
                                          //               borderRadius:
                                          //                   BorderRadius
                                          //                       .circular(70),
                                          //             ),
                                          //             shadowColor:
                                          //                 Colors.grey[800],
                                          //             elevation: 20,
                                          //             // child: SizedBox(
                                          //             //   height: 30,
                                          //             //   width: 100,
                                          //
                                          //             child: ConstrainedBox(
                                          //               constraints:
                                          //                   BoxConstraints(
                                          //                 minWidth: 80.0,
                                          //                 maxWidth: 120.0,
                                          //                 minHeight: 30.0,
                                          //                 maxHeight: 30.0,
                                          //               ),
                                          //               child: RaisedButton(
                                          //                 shape: RoundedRectangleBorder(
                                          //                     borderRadius:
                                          //                         BorderRadius
                                          //                             .circular(
                                          //                                 22.0),
                                          //                     side: BorderSide(
                                          //                         color: Colors
                                          //                             .white)),
                                          //                 onPressed: () {
                                          //                   //   Navigator.push(
                                          //                   //     context,
                                          //                   //     MaterialPageRoute(
                                          //                   //         fullscreenDialog: true,
                                          //                   //         builder: (context) => BottomNavBar()),
                                          //                   //   );
                                          //                 },
                                          //                 color: Colors.white,
                                          //                 textColor:
                                          //                     Colors.orange,
                                          //                 child: Text("Cancel",
                                          //                     style: TextStyle(
                                          //                       fontSize: 16.0,
                                          //                       fontWeight:
                                          //                           FontWeight
                                          //                               .bold,
                                          //                     )),
                                          //               ),
                                          //             ),
                                          //             // ),
                                          //           ),
                                          //         ],
                                          //       ),
                                          //     ),
                                          //   ],
                                          // ),
                                          // ************************* Past Subscriptions **********
                                        ],
                                      ),
                                    ),
                                    // ExpansionTile(
                                    //   title: Text(
                                    //     'Sub title',
                                    //   ),
                                    //   children: <Widget>[
                                    //     ListTile(
                                    //       title: Text('data'),
                                    //     )
                                    //   ],
                                    // ),
                                    // ListTile(
                                    //   title: Text(
                                    //     'data'
                                    //   ),
                                    // ),
                                  ],
                                ),
                                SizedBox(height: 10.0),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                // ),

                Positioned(
                  top: Alignment.center.y,
                  bottom: Alignment.center.x,
                  left: Alignment.center.x,
                  right: Alignment.center.y,
                  child: Visibility(
                      visible: circularProgressIndicatorVisible,
                      child: Center(
                        child: CircularProgressIndicator(),
                      )),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _getBodyWidget() {
    return Positioned(
        // color: Colors.orange,
        height: MediaQuery.of(context).size.height / 2,
        // height: MediaQuery.of(context).size.height,
        child: FutureBuilder<CancelTransactions>(
          future: futureCancelTransactions,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data.data.isEmpty) {
                return Text(
                  'No Cancellations',
                );
              }
              return HorizontalDataTable(
                leftHandSideColumnWidth: 0,
                // rightHandSideColumnWidth: 1100,
                rightHandSideColumnWidth: 900,
                isFixedHeader: true,
                headerWidgets: _getTitleWidget(),
                leftSideItemBuilder: generateFirstColumnRow,
                rightSideItemBuilder: _generateRightHandSideColumnRow,
                // itemCount: user.userInfo.length,
                itemCount: snapshot.data.data.length,
                rowSeparatorWidget: const Divider(
                  color: Colors.black54,
                  height: 1.0,
                  thickness: 0.0,
                ),
                // leftHandSideColBackgroundColor: Color(0xFF2A2A2A),
                // rightHandSideColBackgroundColor: Color(0xFFFFFFFF),
              );
            } else if (snapshot.hasError) {
              return Text("${snapshot.error}" + "ccccc");
            }

            // By default, show a loading spinner.
            return Center(child: showProgressLoader(context));
          },
        ));
  }

  // ignore: missing_return
  Widget generateFirstColumnRow(BuildContext context, int index) {
    // return Container(
    //   child: Text(snapshot.data.data[index].membership.name),
    //   width: 100,
    //   height: 52,
    //   padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
    //   alignment: Alignment.centerLeft,
    // );
  }
  Widget _generateRightHandSideColumnRow(BuildContext context, int index) {
    return FutureBuilder<CancelTransactions>(
      future: futureCancelTransactions,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          if (snapshot.data.data.isEmpty) {
            return Text(
              'No Cancellations',
            );
          }
          // String remainingAvails = snapshot.data.data[index].remainingAvails;
          storage.setItem("remainingAvailsKey",
              snapshot.data.data[index].remainingAvails.toString());

          return Row(
            children: <Widget>[
              Container(
                // color: Colors.red,
                child: Text(
                  snapshot.data.data[index].membership.name == null
                      ? ""
                      : snapshot.data.data[index].membership.name,
                  textAlign: TextAlign.left,
                  style: TextStyle(),
                ),
                // width: 150,
                // height: 52,
                width: MediaQuery.of(context).size.width * 0.35,

                padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                alignment: Alignment.center,
              ),
              Container(
                // color: Colors.pink,
                child: Text(snapshot.data.data[index].amount.toString() == null
                    ? ''
                    : snapshot.data.data[index].amount.toString()),
                // width: 80,
                // height: 52,
                width: MediaQuery.of(context).size.width * 0.35,
                padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                alignment: Alignment.center,
              ),
              Container(
                // color: Colors.amber,

                child: Text(snapshot.data.data[index].date == null
                    ? ''
                    : snapshot.data.data[index].date),
                // width: 150,
                // height: 52,
                width: MediaQuery.of(context).size.width * 0.35,
                padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                alignment: Alignment.center,
              ),
              Container(
                // color: Colors.blue,
                child: Text(
                    snapshot.data.data[index].cancelAt.toString() == null
                        ? ''
                        : snapshot.data.data[index].cancelAt.toString()),
                // width: 80,
                // height: 52,
                width: MediaQuery.of(context).size.width * 0.35,
                padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                alignment: Alignment.center,
              ),
              Container(
                // color: Colors.blueGrey,
                child: Text(snapshot.data.data[index].expiredAt == null
                    ? ''
                    : snapshot.data.data[index].expiredAt.toString()),
                // width: 100,
                // height: 52,
                width: MediaQuery.of(context).size.width * 0.35,
                padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                alignment: Alignment.center,
              ),
              Container(
                // color: Colors.blueGrey,
                child:
                    // Text("expiredAt"),
                    Text(snapshot.data.data[index].expiredAt == null
                        ? ''
                        : snapshot.data.data[index].expiredAt),
                // width: 100,
                // height: 52,
                width: MediaQuery.of(context).size.width * 0.35,
                padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                alignment: Alignment.center,
              ),
            ],
          );
        } else if (snapshot.hasError) {
          return Text("${snapshot.error}" + "ppppp");
        }

        // By default, show a loading spinner.
        return Center(
            child: CircularProgressIndicator(
          color: Colors.orange[800],
        ));
      },
    );
  }

  List<Widget> _getTitleWidget() {
    return [
      FlatButton(
        color: Colors.red,
        padding: EdgeInsets.all(0),
        child: _getTitleItemWidget(
            'Name' + (sortType == sortName ? (isAscending ? '↓' : '↑') : ''),
            50),
        onPressed: () {
          sortType = sortName;
          isAscending = !isAscending;
          // user.sortName(isAscending);
          setState(() {});
        },
      ),
      // FlatButton(
      //   padding: EdgeInsets.all(0),
      //   child: _getTitleItemWidget(
      //       'Credits' +
      //           (sortType == sortStatus ? (isAscending ? '↓' : '↑') : ''),
      //       100),
      //   onPressed: () {
      //     sortType = sortStatus;
      //     isAscending = !isAscending;
      //     user.sortStatus(isAscending);
      //     setState(() {});
      //   },
      // ),

      // _getTitleItemWidget('        Name ', 200),
      // _getTitleItemWidget('Credits ', 80),
      // _getTitleItemWidget('Remaining Avails ', 150),
      // _getTitleItemWidget('Amount', 100),
      // _getTitleItemWidget('Date', 100),
      // _getTitleItemWidget('Status', 100),

      _getTitleItemWidget('Name ', MediaQuery.of(context).size.width * 0.35),
      _getTitleItemWidget(
        'Amount ',
        MediaQuery.of(context).size.width * 0.35,
      ),
      _getTitleItemWidget(
        'Transaction Date',
        MediaQuery.of(context).size.width * 0.35,
      ),
      _getTitleItemWidget(
        'CancelAt',
        MediaQuery.of(context).size.width * 0.35,
      ),
      _getTitleItemWidget(
        'ExpiredAt',
        MediaQuery.of(context).size.width * 0.35,
      ),
      // _getTitleItemWidget(
      //   'ExpiredAt',
      //   MediaQuery.of(context).size.width * 0.35,
      // ),
      // _getTitleItemWidget(
      //   'Status',
      //   MediaQuery.of(context).size.width * 0.35,
      // ),
    ];
  }

  Widget _getTitleItemWidget(String label, double width) {
    return Container(
      child: Text(label, style: TextStyle(fontWeight: FontWeight.bold)),
      width: width,
      height: 50,
      padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
      alignment: Alignment.center,
    );
  }

  setStringValuesSF(String key, String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(key, value);
    print("getStringValuesSF: $value");
    //  prefs.setString('stringValue', "abc");
  }

  Widget showProgressLoader(BuildContext context) {
    if (Platform.isAndroid) {
      return CircularProgressIndicator();
    }
    if (Platform.isIOS) {
      return CupertinoActivityIndicator();
    }
  }

  void _validateInputs() {
    final FormState form = _formKey.currentState;
    if (!form.validate()) {
      setState(() {
        _autoValidate = true; // Start validating on every change.
      });
//      _showInSnackBar('Please fix the errors in red before submitting.');
    } else {
      form.save();

      if (member == '1' && cancelSubscription == '0') {
        print("updatestripecard---------------------");
        UpdatecreateCardToken(
            secret,
            cardnumberController.text,
            expirydateController.text.toString()[0] +
                expirydateController.text.toString()[1],
            expirydateController.text.toString()[2] +
                expirydateController.text.toString()[3],
            cvcController.text);
        CreateCustomersCard(customer: stripeNewCustomerId, id: token);
        print('**********************************************');
        print('***** THIS IS INSIDE UPDATE CARD TOKEN *******');
        print(member);
        print(cancelSubscription);
        print(sourceCardId);
        // CreateCustomersCard()
      } else {
        createCardToken(
            secret,
            cardnumberController.text,
            expirydateController.text.toString()[0] +
                expirydateController.text.toString()[1],
            expirydateController.text.toString()[2] +
                expirydateController.text.toString()[3],
            cvcController.text);
      }

      print('----save--');
      print(cardnumberController.text);
      print('exp_month');
      print(expirydateController.text.toString()[0] +
          expirydateController.text.toString()[1]);
      print('exp_year');
      print(expirydateController.text.toString()[2] +
          expirydateController.text.toString()[3]);
      print('cvc');
      print(cvcController.text);

      // createCardToken(
      //     secret,
      //     cardnumberController.text,
      //     expirydateController.text.toString()[0] +
      //         expirydateController.text.toString()[1],
      //     expirydateController.text.toString()[2] +
      //         expirydateController.text.toString()[3],
      //     cvcController.text);
      // Encrypt and send send payment details to payment gateway
//      _showInSnackBar('Payment card is valid');
    }
  }

  // String validateDate(String value) {
  //   if (value.isEmpty) {
  //     return Strings.fieldReq;
  //   }
  //
  //   int year;
  //   int month;
  //   // The value contains a forward slash if the month and year has been
  //   // entered.
  //   if (value.contains(new RegExp(r'(\/)'))) {
  //     var split = value.split(new RegExp(r'(\/)'));
  //     // The value before the slash is the month while the value to right of
  //     // it is the year.
  //     month = int.parse(split[0]);
  //     year = int.parse(split[1]);
  //   }
  //   else {
  //     // Only the month was entered
  //     month = int.parse(value.substring(0, (value.length)));
  //     year = -1; // Lets use an invalid year intentionally
  //   }
  //
  //   if ((month < 1) || (month > 12)) {
  //     // A valid month is between 1 (January) and 12 (December)
  //     return 'Expiry month is invalid';
  //   }
  //
  //   var fourDigitsYear = convertYearTo4Digits(year);
  //   if ((fourDigitsYear < 1) || (fourDigitsYear > 2099)) {
  //     // We are assuming a valid should be between 1 and 2099.
  //     // Note that, it's valid doesn't mean that it has not expired.
  //     return 'Expiry year is invalid';
  //   }
  //
  //   if (!hasDateExpired(month, year)) {
  //     return "Card has expired";
  //   }
  //   return null;
  // }

  Widget _getPayButton(String member, String cancelSubscription) {
    if (Platform.isIOS) {
      if (member == '1' && cancelSubscription == '0') {
        return CupertinoButton(
          onPressed: _validateInputs,
          color: CupertinoColors.activeBlue,
          child: Text(
            Strings.update,
            style: const TextStyle(fontSize: 17.0),
          ),
        );
      } else {
        return Visibility(
          visible: widget.isVisible,
          child: CupertinoButton(
            onPressed: _validateInputs,
            color: CupertinoColors.activeBlue,
            child: Text(
              Strings.pay,
              style: const TextStyle(fontSize: 17.0),
            ),
          ),
        );
      }
    } else {
      if (member == '1' && cancelSubscription == '0') {
        return RaisedButton(
          onPressed: _validateInputs,
          color: Colors.orange[500],
          splashColor: Colors.deepPurple,
          shape: RoundedRectangleBorder(
            borderRadius: const BorderRadius.all(const Radius.circular(100.0)),
          ),
          padding: const EdgeInsets.symmetric(vertical: 15.0, horizontal: 40.0),
          textColor: Colors.white,
          child: new Text(
            Strings.update.toUpperCase(),
            style: const TextStyle(fontSize: 17.0),
          ),
        );
      } else {
        return Visibility(
          visible: widget.isVisible,
          child: RaisedButton(
            onPressed: _validateInputs,
            color: Colors.orange[500],
            splashColor: Colors.deepPurple,
            shape: RoundedRectangleBorder(
              borderRadius:
                  const BorderRadius.all(const Radius.circular(100.0)),
            ),
            padding:
                const EdgeInsets.symmetric(vertical: 15.0, horizontal: 40.0),
            textColor: Colors.white,
            child: new Text(
              Strings.pay.toUpperCase(),
              style: const TextStyle(fontSize: 17.0),
            ),
          ),
        );
      }
    }
  }

  void _showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      duration: new Duration(seconds: 3),
    ));
  }

  void _getCardTypeFrmNumber() {
    String input = CardUtils.getCleanedNumber(cardnumberController.text);
    CardType cardType = CardUtils.getCardTypeFrmNumber(input);
    setState(() {
      this._paymentCard.type = cardType;
    });
  }

  //-----------------------------------------------------
  // Future<CancelTransactions> fetchCancelTransactionsLogs(String id) async {
  //   print("------------------------fetchCancelTransactionsLogs");
  //   print(id);
  //   final response = await http.get(
  //     baseurl.baseurl + "transaction/cancel/$id",
  //     headers: <String, String>{
  //       'Accept': 'application/json',
  //       'Content-Type': 'application/json; charset=UTF-8',
  //     },
  //   );
  //
  //   if (response.statusCode == 200) {
  //     print(response.body.toString());
  //     return CancelTransactions.fromJson(json.decode(response.body));
  //   } else {
  //     throw Exception('Failed to TransactionHistory Logs');
  //   }
  // }

  //---------------CreateCardTokenModel-----------------------------------------------
  Future<CreateCardTokenModel> createCardToken(String token, String number,
      String exp_month, String exp_year, String cvc) async {
    Map<String, dynamic> body = {
      'card[number]': number,
      'card[exp_month]': exp_month,
      'card[exp_year]': exp_year,
      'card[cvc]': cvc
    };
    final http.Response response = await http.post(
      Uri.parse('https://api.stripe.com/v1/tokens'),
      headers: <String, String>{
//       'Content-Type': 'application/json',
        'Authorization':
            'Bearer sk_test_51HbQCpJrvceP60phuzzvHaxtXprQv8DRJpvS97ie0Qx7CYhySy2rvdMYy8bTFlnLlH6bA7PfBGwrq1g71jbsSPbu00PNg2wv0Z',
        'Content-Type': 'application/x-www-form-urlencoded',
      },

      body: body,
      encoding: Encoding.getByName("utf-8"),
//      jsonEncode(<String, String>{
//        'card[number]': number,
//        'card[exp_month]': exp_month,
//        'card[exp_year]': exp_year,
//        '': cvc,
//      }),
    );

    if (response.statusCode == 200) {
//      print(body.toString());
      print('------createCardToken-----------');
      print(response.statusCode.toString());
      print(response.body.toString());
      var parsedJson = json.decode(response.body);
      var success = CreateCardTokenModel.fromJson(parsedJson);
      storage.setItem("tok_id", success.id.toString()).toString();
      print(success.id.toString());
      loadProgress(true);

      String stripeTokenID = storage.getItem('tok_id');
      print(stripeTokenID);
      // String stripeCustomerId = storage.getItem('stripeCustomerId');
      // storage.setItem("stripeCustomerID", "cus_JWaXd05Dzunhjt");
      String stripeCustomerID = storage.getItem("stripeCustomerId");
      print('THIS IS STRIPE CUSTOMER ID ************************');
      print('*****************************');
      print(stripeCustomerID);
      print('------HitCreateCustomersCard-----------');
      createCustomersCard(stripeTokenID, stripeCustomerID);
//       stripeAPICharge(
//           // stripeAmount.toString(),
//           stripeAmount.toString() + "00",
// //        storage.getItem("amount").toString(),
//           'USD',
//           widget.membershipName,
//           storage.getItem("tok_id").toString());

      return CreateCardTokenModel.fromJson(jsonDecode(response.body));
    } else {
      print(response.statusCode.toString());
      print(response.body.toString());
      var parsedJson = json.decode(response.body);
      var success = InvaildCreateCardTokenModel.fromJson(parsedJson);
      _showDialog(success.error.message, true);
      loadProgress(true);
      throw Exception('Failed to create CardToken .');
    }
  }

  //---------------CreateCardTokenModel-----------------------------------------------
  Future<CreateCardTokenModel> UpdatecreateCardToken(String token,
      String number, String exp_month, String exp_year, String cvc) async {
    Map<String, dynamic> body = {
      'card[number]': number,
      'card[exp_month]': exp_month,
      'card[exp_year]': exp_year,
      'card[cvc]': cvc
    };
    final http.Response response = await http.post(
      Uri.parse('https://api.stripe.com/v1/tokens'),
      headers: <String, String>{
//       'Content-Type': 'application/json',
        'Authorization':
            'Bearer sk_test_51HbQCpJrvceP60phuzzvHaxtXprQv8DRJpvS97ie0Qx7CYhySy2rvdMYy8bTFlnLlH6bA7PfBGwrq1g71jbsSPbu00PNg2wv0Z',
        'Content-Type': 'application/x-www-form-urlencoded',
      },

      body: body,
      encoding: Encoding.getByName("utf-8"),
//      jsonEncode(<String, String>{
//        'card[number]': number,
//        'card[exp_month]': exp_month,
//        'card[exp_year]': exp_year,
//        '': cvc,
//      }),
    );

    if (response.statusCode == 200) {
//      print(body.toString());
      print('------createCardToken-----------');
      print(response.statusCode.toString());
      print(response.body.toString());
      var parsedJson = json.decode(response.body);
      var success = CreateCardTokenModel.fromJson(parsedJson);
      storage.setItem("tok_id", success.id.toString()).toString();
      print(success.id.toString());
      loadProgress(true);

      String stripeTokenID = storage.getItem('tok_id');
      print(stripeTokenID);
      // storage.setItem("stripeCustomerID", "cus_JWaXd05Dzunhjt");
      String stripeCustomerID = stripeNewCustomerId;
      print(stripeNewCustomerId);
      print('------HitCreateCustomersCard-----------');

      // if (member == '1' && cancelSubscription == '0') {
      //   print("updatestripecard---------------------");
      //   createCustomersCard(stripeTokenID, stripeCustomerID);
      //   print(member);
      //   print(cancelSubscription);
      // } else {
      //
      // }

      updateCreateCustomersCard(stripeTokenID, stripeCustomerID);
//       stripeAPICharge(
//           // stripeAmount.toString(),
//           stripeAmount.toString() + "00",
// //        storage.getItem("amount").toString(),
//           'USD',
//           widget.membershipName,
//           storage.getItem("tok_id").toString());

      return CreateCardTokenModel.fromJson(jsonDecode(response.body));
    } else {
      print(response.statusCode.toString());
      print(response.body.toString());
      var parsedJson = json.decode(response.body);
      var success = InvaildCreateCardTokenModel.fromJson(parsedJson);
      _showDialog(success.error.message, true);
      loadProgress(true);
      throw Exception('Failed to create CardToken .');
    }
  }

  //----------------------CreateCustomersCard-----------------------------------------

  Future<CreateCustomersCard> createCustomersCard(
      String source, String customerID) async {
    print('THIS IS SOURCE ***************');
    print('THIS IS SOURCE ***************');
    print('this is source: $source');

    Map<String, dynamic> body = {
      'source': source,
    };
    final http.Response response = await http.post(
      Uri.parse('https://api.stripe.com/v1/customers/$customerID/sources'),
      headers: <String, String>{
        'Authorization':
            'Bearer sk_test_51HbQCpJrvceP60phuzzvHaxtXprQv8DRJpvS97ie0Qx7CYhySy2rvdMYy8bTFlnLlH6bA7PfBGwrq1g71jbsSPbu00PNg2wv0Z',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: body,
    );

    if (response.statusCode == 200) {
      print('------CreateCustomersCard-----------');
      print(response.statusCode.toString());
      // print(response.body.toString());
      print('this is response ${response.body}');
      var parsedJson = json.decode(response.body);
      var success = CreateCustomersCard.fromJson(parsedJson);
      storage
          .setItem("CreateCustomersCardId", success.id.toString())
          .toString();
      print(success.id.toString());
      loadProgress(true);

      String createCustomersCardId =
          storage.getItem("CreateCustomersCardId").toString();

      stripeAPICharge(stripeAmount.toString() + "00", 'USD',
          widget.membershipName, createCustomersCardId, stripeCustomerID);
      return CreateCustomersCard.fromJson(jsonDecode(response.body));
    } else {
      print(response.statusCode.toString());
      print(response.body.toString());
      var parsedJson = json.decode(response.body);
      var success = InvalidCreateCustomersCard.fromJson(parsedJson);
      _showDialog(success.error.message, true);
      loadProgress(false);
      throw Exception('Failed to create CardToken .');
    }
  }

  Future<CreateCustomersCard> updateCreateCustomersCard(
      String source, String customerID) async {
    Map<String, dynamic> body = {
      'source': source,
    };
    final http.Response response = await http.post(
      Uri.parse('https://api.stripe.com/v1/customers/$customerID/sources'),
      headers: <String, String>{
//       'Content-Type': 'application/json',
        'Authorization':
            'Bearer sk_test_51HbQCpJrvceP60phuzzvHaxtXprQv8DRJpvS97ie0Qx7CYhySy2rvdMYy8bTFlnLlH6bA7PfBGwrq1g71jbsSPbu00PNg2wv0Z',
        'Content-Type': 'application/x-www-form-urlencoded',
      },

      body: body,
      encoding: Encoding.getByName("utf-8"),
//      jsonEncode(<String, String>{
//        'card[number]': number,
//        'card[exp_month]': exp_month,
//        'card[exp_year]': exp_year,
//        '': cvc,
//      }),
    );

    if (response.statusCode == 200) {
//      print(body.toString());
      print('------CreateCustomersCard-----------');
      print(response.statusCode.toString());
      print(response.body.toString());
      var parsedJson = json.decode(response.body);
      var success = CreateCustomersCard.fromJson(parsedJson);
      storage
          .setItem("CreateCustomersCardId", success.id.toString())
          .toString();
      print(success.id.toString());
      loadProgress(true);

      String selectedDate = storage.getItem("selectedDate");
      String firstName = storage.getItem("firstnameController").toString();
      String lastName = storage.getItem("lastnameController").toString();
      String name = firstName.toString() + "" + lastName.toString();
      String phone = storage.getItem("cellPhoneController").toString();
      String email = storage.getItem("emailController").toString();
      String userId = storage.getItem('userid');
      String password = storage.getItem("password");
      String stripeCustomerID = storage.getItem("stripeCustomerID");
      String createCustomersCardId =
          storage.getItem("CreateCustomersCardId").toString();
      userProfileUpdated(
        firstName,
        lastName,
        email,
        selectedDate,
        password,
        userId,
        stripeCustomerID,
        createCustomersCardId,
      );
//       stripeAPICharge(
//         // stripeAmount.toString(),
//           stripeAmount.toString() + "00",
// //        storage.getItem("amount").toString(),
//           'USD',
//           widget.membershipName,
//           // storage.getItem("tok_id").toString()
//           createCustomersCardId,
//           stripeCustomerID);

      // dialog.style(message: 'Transaction successful');
      // await dialog.hide();

////      stripeAPIChargeList = List<CreateCardTokenModel>();
//   if(success.id.toString() !=null){
//     print('success' +success.id.toString());
//
//   }
//   else{
//     print('failed' +success.id.toString());
//   }
      _showDialog("Update Card Successful", false);
      return CreateCustomersCard.fromJson(jsonDecode(response.body));
    } else {
      print(response.statusCode.toString());
      print(response.body.toString());
      var parsedJson = json.decode(response.body);
      var success = InvalidCreateCustomersCard.fromJson(parsedJson);
      _showDialog(success.error.message, true);
      loadProgress(false);
      throw Exception('Failed to create CardToken .');
    }
  }

  //---------------StripeAPIChargeModel-----------------------------------------------

  Future<StripeAPIChargeModel> stripeAPICharge(String amount, String currency,
      String description, String source, String stripeCustomerID) async {
    Map<String, dynamic> body = {
      'amount': amount,
      'currency': currency,
      'description': description,
      'source': source,
      'customer': stripeCustomerID,
    };

    print('**** DATA TEST ***');
    print('amount: $amount');
    print('currency: $currency');
    print('description: $description');
    print('source: $source');
    print('stripeCustomerId: $stripeCustomerID');
    print('****** Ends ******');
    // print('amount: $amount');
    final http.Response response = await http.post(
      Uri.parse('https://api.stripe.com/v1/charges'),
      headers: <String, String>{
        'Authorization':
            'Bearer sk_test_51HbQCpJrvceP60phuzzvHaxtXprQv8DRJpvS97ie0Qx7CYhySy2rvdMYy8bTFlnLlH6bA7PfBGwrq1g71jbsSPbu00PNg2wv0Z',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: body,
      encoding: Encoding.getByName("utf-8"),
    );

    if (response.statusCode == 200) {
      print(body.toString());
      print('------StripeAPIChargeModel-----------');
      print(response.statusCode.toString());
      print('this is create card response');
      print(response.body.toString());
      var parsedJson = json.decode(response.body);
      var success = StripeAPIChargeModel.fromJson(parsedJson);
      storage.setItem("ch_id", success.id.toString());
      print(storage.getItem("tok_id").toString());
      print(storage.getItem("ch_id").toString());
      print(widget.userId.toString() + "" + widget.membershipId.toString());
      print(cardnumberController.text
          .replaceAll(new RegExp(r"\s+"), "")
          .toString());
      print(cardnumberController.text
          .replaceAll(new RegExp(r"\s+\b|\b\s"), "")
          .toString());
      print(success.status.toString());
      print("===========storeTransactionFromSetting==============");
      storeTransaction(
        storage.getItem("tok_id"),
        storage.getItem("ch_id"),
        widget.userId,
        widget.membershipId,
        stripeAmount.toString(),
        success.status.toString(),
        cardnumberController.text.replaceAll(new RegExp(r"\s+"), "").toString(),
        cvcController.text.toString(),
        expirydateController.text.toString(),
      );

      return StripeAPIChargeModel.fromJson(jsonDecode(response.body));
    } else {
      print('----Failed--stripeAPICharge-----------');
      print(body.toString());
      print(response.statusCode.toString());
      print(response.body.toString());
      var parsedJson = json.decode(response.body);
      var success = InvaildCreateCardTokenModel.fromJson(parsedJson);
      _showDialog(success.error.message, true);
      throw Exception('Failed to stripeAPICharge .');
    }
  }

//----------------------storeTransaction-------------------------------
  Future<TransactionStore> storeTransaction(
      String pm_id,
      String pr_id,
      String customer_id,
      String membership_id,
      String amount,
      String status,
      String card,
      String cvc,
      String expiryDate) async {
    final http.Response response = await http.post(
      Uri.parse(baseurl.baseurl + "transaction/store"),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'pm_id': pm_id,
        'pr_id': pr_id,
        'customer_id': customer_id,
        'membership_id': membership_id,
        'amount': amount,
        'status': status,
        'card': card,
        'cvv': cvc,
        'card_expiry_date': expiryDate,
      }),
    );

    if (response.statusCode == 200) {
      var parsedJson = json.decode(response.body);
      var success = TransactionStore.fromJson(parsedJson);
//      ProgressDialog dialog = new ProgressDialog(context);
      print(
          '---------------------storeTransaction-------------------------------');
      print(response.statusCode.toString());
      print(response.body.toString());
      if (token != null) {
        userDetailsDataApi(token);
      } else {
        print('userDetailsDataApiiiiiiiiiiiiiiii');
      }
//      await dialog.hide();
//      _showDialog(success.message.toString(), true);
      loadProgress(false);

      _showNotificationWithoutSound("Transaction Status Successful");

      String selectedDate = storage.getItem("selectedDate");
      String firstName = storage.getItem("firstnameController").toString();
      String lastName = storage.getItem("lastnameController").toString();
      String name = firstName.toString() + "" + lastName.toString();
      String phone = storage.getItem("cellPhoneController").toString();
      String email = storage.getItem("emailController").toString();
      String userId = storage.getItem('userid');
      String password = storage.getItem("password");
      String stripeCustomerID = storage.getItem("stripeCustomerID");
      String createCustomersCardId =
          storage.getItem("CreateCustomersCardId").toString();
      userProfileUpdated(
        firstName,
        lastName,
        email,
        selectedDate,
        password,
        userId,
        stripeCustomerID,
        createCustomersCardId,
      );

      _showDialog('Transaction Store Successful', false);
      // Navigator.pushAndRemoveUntil(
      //     context,
      //     MaterialPageRoute(builder: (context) => BottomNavBar()),
      //     ModalRoute.withName("/BottomNavBar"));
      return TransactionStore.fromJson(jsonDecode(response.body));
    } else {
      print(
          '-------------------Failed--storeTransaction-------------------------------');
      print(response.statusCode.toString());
      print(response.body.toString());
      invaildTransactionStoreList = List<InvaildTransactionStore>();
      var parsedJson = json.decode(response.body);
      var success = InvaildTransactionStore.fromJson(parsedJson);
      _showNotificationWithoutSound("Transaction status Failed");
      _showDialog('Transaction status Failed', false);
      throw Exception('Failed to Store Transaction');
    }
  }

  //------UserProfileUpdatedData------------------------------------
  Future<UserProfileUpdatedData> userProfileUpdated(
    String first_name,
    String last_name,
    String email,
    String dob,
    String password,
    // String token,
    String id,
    String stripeCustomerId,
    String sourceCardId,
  ) async {
    final http.Response response = await http.post(
      Uri.parse(baseurl.baseurl + "user/update/" + id),
      headers: <String, String>{
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=UTF-8',
        // 'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'first_name': first_name,
        'last_name': last_name,
        'dob': dob,
        'email': email,
        'password': password,
        'stripe_customer_id': stripeCustomerId,
        'source_card_id': sourceCardId
        // 'password_confirmation': password_confirmation,
      }),
    );

    if (response.statusCode == 200) {
      print(json.decode(response.statusCode.toString()));
      print("------------UserProfileUpdatedData-------------");
      print(json.decode(response.body));
      var parsedJson = json.decode(response.body);
      var success = UserProfileUpdatedData.fromJson(parsedJson);
      loadProgress(false);
      return UserProfileUpdatedData.fromJson(json.decode(response.body));
    } else {
      print(baseurl.baseurl + "user/update/" + id);
      print(json.decode(response.statusCode.toString()));
      print(json.decode(response.body));
      var parsedJson = json.decode(response.body.toString());
      var success = InvaildUserProfileUpdated.fromJson(parsedJson);
      _showDialog(success.data.email.toString(), false);

      throw Exception('Failed to Register the Users');
    }
  }

  //-------------UserDetailsDataAPI--------------------------------
  Future<UserDetailsData> userDetailsDataApi(String token) async {
    final response = await http.get(
      Uri.parse(baseurl.baseurl + "user/details"),
      headers: <String, String>{
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + token,
      },
    );

    if (response.statusCode == 200) {
      print(json.decode(response.body));
      userDetailsData = List<UserDetails>();
      storage.setItem("UserDetailsData", storage.getItem('token'));
      var parsedJson = json.decode(response.body);
      var success = UserDetails.fromJson(parsedJson);
      userDetailsData = List<UserDetails>();
      userDetailsData.add(UserDetails(data: success.data));
      print('||||||||' + userDetailsData[0].data.id.toString());
      print('||||||||' + userDetailsData[0].data.firstName.toString());
      print('||||||||' + userDetailsData[0].data.lastName.toString());
      print('||||||||' + userDetailsData[0].data.email.toString());
      print('||||||||' + userDetailsData[0].data.avatar.toString());

      print('||||||||' + success.data.id.toString());
      print('||||||||' + success.data.firstName.toString());
      print('||||||||' + success.data.lastName.toString());
      print('||||||||' + success.data.email.toString());
      print('||||||||' + success.data.avatar.toString());

      storage.setItem('userid', success.data.id.toString());
      storage.setItem('firstName', success.data.firstName.toString());
      storage.setItem('lastName', success.data.lastName.toString());
      storage.setItem('email', success.data.email.toString());
      storage.setItem('avatar', success.data.avatar.toString());
      storage.setItem('member', success.data.member.toString());
      storage.setItem(
          'cancelSubscription', success.data.cancelSubscription.toString());
      storage.setItem('source_card_id', success.data.sourceCardId.toString());

      // storage.setItem('totalAvails', success.data.totalAvails.toString());
      if (success.data.totalAvails != null) {
        print("isNOTequaltoNULL");
        storage.setItem('totalAvails', success.data.totalAvails.toString());
      } else {
        storage.setItem('totalAvails', "0");
        print("isEqualtoNULL");
      }
      setStringValuesSF('userid', success.data.id.toString());
      setStringValuesSF('firstName', success.data.firstName.toString());
      setStringValuesSF('lastName', success.data.lastName.toString());
      setStringValuesSF('email', success.data.avatar.toString());
      setStringValuesSF('avatar', success.data.avatar.toString());

      print(storage.getItem("avatar").toString() +
          storage.getItem("email").toString());

      return UserDetailsData.fromJson(json.decode(response.body));
    } else if (response.statusCode == 401) {
      String messageData = json.decode(response.body)['message'];
      print('$messageData');
      Navigator.popAndPushNamed(context, '/BottomNavBar');
    } else {
      print(response.statusCode.toString());
      print(response.body.toString());
      String messageData = json.decode(response.body)['message'] ?? '';
      String data = json.decode(response.body)['data'] ?? '';
      if (messageData == 'The access token is invalid.') {
        Navigator.popAndPushNamed(context, '/LoginScreen');
      } else {
        Navigator.popAndPushNamed(context, '/BottomNavBar');
      }
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load User Details Data Api');
    }
  }

  detectDelayText() {
    Future.delayed(Duration(seconds: 2), () {
      SystemChannels.textInput.invokeMethod('TextInput.hide');
    });
  }
}
