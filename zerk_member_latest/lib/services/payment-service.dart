// import 'dart:convert';
// import 'package:flutter/services.dart';
// import 'package:http/http.dart' as http;
// import 'package:stripe_payment/stripe_payment.dart';
//
// class StripeTransactionResponse {
//   String message;
//   bool success;
//   String paymentMethodId;
//   String paymentIntentId;
//
//   StripeTransactionResponse(
//       {this.message, this.success, this.paymentMethodId, this.paymentIntentId});
//   //  StripeTransactionResponse({this.paymentMethodId, this.paymentIntentId});
// }
//
// class StripeService {
//   static String apiBase = 'https://api.stripe.com/v1';
//   static String paymentApiUrl = '${StripeService.apiBase}/payment_intents';
//   static String secret =
//       'sk_test_51HbQCpJrvceP60phuzzvHaxtXprQv8DRJpvS97ie0Qx7CYhySy2rvdMYy8bTFlnLlH6bA7PfBGwrq1g71jbsSPbu00PNg2wv0Z';
//   static Map<String, String> headers = {
//     'Authorization': 'Bearer ${StripeService.secret}',
//     'Content-Type': 'application/x-www-form-urlencoded'
//   };
//   static init() {
//     StripePayment.setOptions(StripeOptions(
//         publishableKey:
//             "pk_test_51HbQCpJrvceP60phepxElGZr36dkHZD8ZjhTPB3ffTEWdQW5W3KxAwaIImBMqb3u86mVeKPYmVhply3H5jVJXmJ100uAu1Bvwy",
//         merchantId: "Test",
//         androidPayMode: 'test'));
//   }
//
//   static Future<StripeTransactionResponse> payViaExistingCard(
//       {String amount, String currency, CreditCard card}) async {
//     try {
//       var paymentMethod = await StripePayment.createPaymentMethod(
//           PaymentMethodRequest(card: card));
//
//       var paymentIntent =
//           await StripeService.createPaymentIntent(amount, currency);
//       var response = await StripePayment.confirmPaymentIntent(PaymentIntent(
//           clientSecret: paymentIntent['client_secret'],
//           paymentMethodId: paymentMethod.id));
//
//       if (response.status == 'succeeded') {
//
//         return new StripeTransactionResponse(
//             message: 'Transaction successful', success: true);
//       } else {
//         return new StripeTransactionResponse(
//             message: 'Transaction failed', success: false);
//       }
//     } on PlatformException catch (err) {
//       return StripeService.getPlatformExceptionErrorResult(err);
//     } catch (err) {
//       return new StripeTransactionResponse(
//           message: 'Transaction failed: ${err.toString()}', success: false);
//     }
//   }
//
//   static Future<StripeTransactionResponse> payWithNewCard(
//       {String amount, String currency}) async {
//     try {
//       var paymentMethod = await StripePayment.paymentRequestWithCardForm(
//           CardFormPaymentRequest());
//       var paymentIntent =  await StripeService.createPaymentIntent(amount, currency);
//
//       //  print(paymentIntent[0].);
//       var response = await StripePayment.confirmPaymentIntent(PaymentIntent(
//           clientSecret: paymentIntent['client_secret'],
//           paymentMethodId: paymentMethod.id));
//
//       if (response.status == 'succeeded') {
//         print('-------------------------------------');
// //        print(response.toJson());
// //        print(paymentIntent);
//         print(paymentMethod.card.number);
//         print(paymentMethod.card.expMonth);
//         print(paymentMethod.card.expYear);
//         print(paymentMethod.card.cvc);
//         print(paymentMethod.card.name);
//
//
//         return new StripeTransactionResponse(
//           message: 'Transaction successful',
//           success: true,
//           paymentMethodId: paymentMethod.id.toString(),
//           paymentIntentId: response.paymentIntentId.toString(),
//         );
//       } else {
//         return new StripeTransactionResponse(
//             message: 'Transaction failed', success: false);
//       }
//     } on PlatformException catch (err) {
//       return StripeService.getPlatformExceptionErrorResult(err);
//     } catch (err) {
//       return new StripeTransactionResponse(
//           message: 'Transaction failed: ${err.toString()}', success: false);
//     }
//   }
//
//   static getPlatformExceptionErrorResult(err) {
//     String message = 'Something went wrong';
//     if (err.code == 'cancelled') {
//       message = 'Transaction cancelled';
//     }
//
//     return new StripeTransactionResponse(message: message, success: false);
//   }
//
//   static Future<Map<String, dynamic>> createPaymentIntent(
//       String amount, String currency) async {
//     try {
//       Map<String, dynamic> body = {
//         'amount': amount,
//         'currency': currency,
//         'payment_method_types[]': 'card'
//       };
//       var response = await http.post(StripeService.paymentApiUrl,
//           body: body, headers: StripeService.headers);
//       print('createPaymentIntent===================');
//       print(paymentApiUrl.toString());
//       print(json.decode(response.body));
// //      print(body['card'].toString());
//       return jsonDecode(response.body);
//     } catch (err) {
//       print('err charging user: ${err.toString()}');
//     }
//     return null;
//   }
// }
